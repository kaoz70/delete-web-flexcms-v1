/*global search, modules, Fx, Request, Element, $, system, window, document, clearTimeout, setTimeout, swfobject, console */

// Avoid `console` errors in browsers that lack a console.
(function () {
    "use strict";
    var method,
        noop = function () {},
        methods = [
            "assert", "clear", "count", "debug", "dir", "dirxml", "error",
            "exception", "group", "groupCollapsed", "groupEnd", "info", "log",
            "markTimeline", "profile", "profileEnd", "table", "time", "timeEnd",
            "timeStamp", "trace", "warn"
        ],
        length = methods.length,
        console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());

var catalogFilters = {

    pathName: "",
    cleanPathName: "",
    filterArr: [],

    init: function () {
        "use strict";
        catalogFilters.pathName = document.location.pathname;
        $("#module_filtros input").click(catalogFilters.clickFilterHandler);
    },

    clickFilterHandler: function () {

        "use strict";

        var targetContainer = $(".main_content");

        targetContainer.addClass("loading");
        targetContainer.fadeOut(function () {
            $.post({
                url: system.base_url + "search/productFilters",
                context: targetContainer,
                data: $("#module_filtros").serialize()
            })
                .done(function (data) {
                    targetContainer.removeClass("loading");
                    targetContainer.html(data);
                    targetContainer.fadeIn();
                })
                .fail(function () {
                    targetContainer.removeClass("loading");
                    targetContainer.fadeIn();
                });
        });

    }

};

function initProductAccordionMenu(id) {
    "use strict";

    $(".category > li > a").click(function (e) {
        e.preventDefault();
    });

    $(id).accordion({
        container: false,
        head: "a",
        initShow: ".active"
    });

}

function validHandler(e) {
    "use strict";

    var $form = $(e.currentTarget);

    //Force check and see if its a "valid" event, because this was being executed on "submit" also (2 requests)
    if (e.type === "valid") {

        if ($form.hasClass("form_contacto")) {

            var modal = $("<div>Enviando, por favor espere...</div>")
                .attr("data-reveal", "")
                .addClass("small reveal-modal").appendTo("body")
                .foundation("reveal", "open");

            $.ajax({
                type: "POST",
                url: $form.attr("action"),
                data: $form.serialize(),
                dataType: "json",
                success: function (data) {
                    if (data.sent === true) {
                        modal
                            .empty()
                            .html($(data.message))
                            .append("<a class='close-reveal-modal'>&#215;</a>");

                    } else {

                        modal
                            .empty()
                            .html($(data.message))
                            .append("<a class='close-reveal-modal'>&#215;</a>");

                    }
                }
            });

        } else {
            $form.submit();
        }

    }

}

function resetHandler(e) {
    "use strict";
    $(e.currentTarget).find("div.error").removeClass("error");
}

$(document).ready(function () {
    "use strict";

    var scrollEl;

    catalogFilters.init();

    if ( $.isFunction($.fn.foundation) ) {
        $(document).foundation();
    }

    $(".mapa_ubicaciones li").click(function () {

        var id = $(this).attr("data-id"),
            slidesParent = $(this)
                .parent()
                .parent()
                .find(".mapa_slides");

        slidesParent
            .children()
            .fadeOut();

        slidesParent
            .find("[data-id='" + id + "']")
            .fadeIn();

        $(this)
            .parent()
            .find("li")
            .removeClass("selected");

        $(this).addClass("selected");

    });

    if ( $.isFunction($.fn.magnificPopup) ) {
        $(".thumbs.imagenes").magnificPopup({
            delegate: "a",
            type: "image",
            preload: [3, 3],
            // Delay in milliseconds before popup is removed
            removalDelay: 300,
            // Class that is added to popup wrapper and background
            // make it unique to apply your CSS animations just to this exact popup
            mainClass: "mfp-fade",
            zoom: {
                enabled: true, // By default it's false, so don't forget to enable it

                duration: 300, // duration of the effect, in milliseconds
                easing: "ease-in-out", // CSS transition easing function

                // The "opener" function should return the element from which popup will be zoomed in
                // and to which popup will be scaled down
                // By defailt it looks for an image tag:
                opener: function (openerElement) {
                    // openerElement is the element on which popup was initialized, in this case its <a> tag
                    // you don't need to add "opener" option if this code matches your needs, it's defailt one.
                    return openerElement.is("img") ? openerElement : openerElement.find("img");
                }
            },
            gallery: {
                enabled: true
            }
        });
    }

    if ( $.isFunction($.fn.bxSlider) ) {
        $(".videos ul.thumbs").bxSlider({
            minSlides: 3,
            maxSlides: 4,
            slideWidth: 120,
            slideMargin: 10
        });
    }

    $("[data-abide]")
        .on("submit", function (e) {
            if ($(e.currentTarget).hasClass("form_contacto")) {
                e.preventDefault();
            }
        })
        .on("valid", validHandler)
        .on("reset", resetHandler);

    /**
     * Load pop-up pages with ajax
     */
    $(document.body).on("click", ".popup a, a.popup", function (e) {

        e.preventDefault();

        var modal;

        //Create the modal window
        modal = $("<div>Cargando, por favor espere...</div>")
            .attr("data-reveal", "")
            .addClass("small reveal-modal").appendTo("body")
            .foundation("reveal", "open");

        $.post($(this).attr("href"))
            .done(function (data) {

                var form,
                    html = $(data);

                //Append the request HTML to the modal window
                modal
                    .empty()
                    .html($("<div>").html(html))
                    .append("<a class='close-reveal-modal'>&#215;</a>");

                //$(document).foundation("reveal", "close");

                //Initialize the form validation if there is a form element
                form = modal.find("form[data-abide]");
                if (form) {

                    $(document).foundation("abide", "events");

                    form
                        .on("submit", function (ev) {
                            ev.preventDefault();
                        })
                        .on("valid", validHandler)
                        .on("reset", resetHandler);
                }

            });
    });

    //Show the popup banner if it wasn't seen and if it exists
    if (system.hasOwnProperty("popup_banner") && !$.cookie("popup_banner_" + system.popup_banner.id)) {

        //Foundation Modal
        if(typeof($.fn.foundation) != 'undefined'){
            //Create the modal window
            var popup = $(system.popup_banner.html)
                .addClass("small reveal-modal")
                .appendTo("body")
                .foundation("reveal", "open");
            popup.append("<a class='close-reveal-modal'>&#215;</a>");
        }

        //Bootstrap Modal
        else {
            $("#popup").modal('show');
        }


        //Store the cookie so that we don't show the popup again
        $.cookie("popup_banner_" + system.popup_banner.id, true);
    }

});
