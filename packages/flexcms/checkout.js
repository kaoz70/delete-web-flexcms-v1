/**
 * Created by Miguel on 23-Mar-17.
 */

(function() {
    'use strict';

// Declare app level module which depends on views, and components
angular
    .module('app', ['ngSanitize'])
    .controller('checkout', function ($scope, Request, $timeout) {

        var cartUrl = system.base_url + system.lang + '/' + system.pag_pedidos;

        $scope.order_number = null;
        $scope.formClass = '';
        $scope.loading = false;
        $scope.shipping = null;
        $scope.loadingMessage = 'Enviando...';
        $scope.contact_fields = [];
        $scope.shipping_fields = [];
        $scope.billing_fields = [];
        $scope.shippingOptions = [];
        $scope.sameBillingInfo = true;
        $scope.saveShippingFields = false;
        $scope.paymentType = null;
        $scope.paymentTranslated = null;
        $scope.email = {
            userFieldRelContent: '',
            userFieldRequired: true

        };
        $scope.bank = {
            required: false,
            document: ''
        };
        $scope.prices = {
            subtotal: '',
            shipping: '',
            tax: '',
            total: ''
        };

        $scope.activeStep = 1;

        $scope.sameBillingInfoCheck = function () {
            $scope.sameBillingInfo = !$scope.sameBillingInfo;
        };
        
        $scope.nextStep = function ($event, index, fields) {

            var error = false;

            $event.preventDefault();

            function validateField(field) {
                if(field.userFieldRequired && (field.userFieldRelContent === '' || field.userFieldRelContent === undefined)) {
                    field.error = true;
                    field.error_message = 'Este campo es requerido';
                    error = true;
                }
            }

            angular.forEach(fields, function (field) {
                validateField(field);
            });

            validateField($scope.email);

            if(error) {
                return;
            }

            $scope.activeStep = index + 1;
        };

        $scope.editStep = function (index) {
            $scope.activeStep = index;
        };

        $scope.stepClass = function (index) {

            var classString = 'disabled';

            if($scope.activeStep > index) {
                classString = 'done';
            }
            if($scope.activeStep === index) {
                classString = 'active';
            }

            return classString;
        };

        // Scroll to active step
        $scope.$watch('activeStep', function() {
            // Wait until the height changes to get the new scroll position
            $timeout(function () {
                //jQuery dependent animation
                $("body").animate({scrollTop: $('#step' + $scope.activeStep).position().top}, "slow");
            }, 100)
        });

        $scope.requiredValidation = function (field) {
            return !(field.error && !field.userFieldRelContent);
        };

        var getFieldData = function () {

            $scope.formClass = 'loading';
            $scope.loading = true;

            var fields = $scope.contact_fields.concat($scope.shipping_fields);
            fields = fields.concat($scope.billing_fields);
            var inputs = {};

            angular.forEach(fields, function (item) {
                var key = item.userFieldOrderCol ? item.userFieldOrderCol : 'field_' + item.userFieldId;
                inputs[key] = item.userFieldRelContent;
            });

            return {
                user_fields: inputs,
                sameBillingInfo: $scope.sameBillingInfo,
                saveShippingFields: $scope.saveShippingFields,
                payment_method: $scope.paymentTranslated,
                payment_document_id: $scope.bank.document,
                email: $scope.email.userFieldRelContent,
                paymentType: $scope.paymentType
            };

        };

        $scope.checkoutSubmit = function () {

            Request.post(getFieldData(), cartUrl + '/send').then(function (request) {
                console.log(request);
                if(request.data.order_saved) {
                    $scope.loadingMessage = 'Orden enviada con &eacute;xito';
                    window.location.replace(cartUrl + '/checkout_complete/' + request.data.order_number);
                } else {
                    $scope.loadingMessage = 'Hubo un problema al enviar la orden';
                }

            });

        };

        $scope.paymentSubmit = function (ev, form) {
            ev.preventDefault();

            $('#checkout-modal').modal('show');

            var form = $(ev.target).closest('form'),
            fieldData = getFieldData();

            //Do not change the order status yet
            fieldData.ord_status = 1;

            //Save the order
            Request.post(getFieldData(), cartUrl + '/send').then(function (request) {
                if(request.data.order_saved) {
                    $scope.loadingMessage = 'Orden guardada con &eacute;xito, enviando a pago';
                    form.submit();
                    $('#checkout-modal').modal('hide');
                }
            });

        };

        $scope.updateShipping = function () {
            Request.get(system.base_url + 'ajax/cart/updateShipping/' + $scope.shipping).then(function (request) {
                $scope.prices = request.data.prices;
            });
        }

    })

    .service('Request', function($http){

        /**
         * Send a POST request to the server
         *
         * @param data
         * @param url
         */
        this.post = function (data, url) {
            return $http({
                method: 'POST',
                url: url,
                data: data,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            });
        };
        
        this.get = function (url) {
            return $http({
                method: 'GET',
                url: url
            });
        };

    })

}());