/*global $, document /

/**
 * Created by Miguel on 26/11/2014.
 */

var flexicartText = {
    adding: "A&ntilde;adiendo...",
    deleting: "Eliminando..",
    success: "Ir al carrito",
    error: "Error!"
}

/**
 * Submit the cart form if a select option or input element is changed.
 */
function onChangeListener() {

    // Loop through shipping select and input fields creating object of their names and values that will then be submitted via 'post'
    var data = {},
        pedido = $(".pedido");

    if($(this).attr("type") === "radio") {
        data[$(this).attr('name')] = $(this).val();
    } else {
        pedido.find('select, input').each(function() {
            data[$(this).attr('name')] = $(this).val();
        });
    }

    // Set 'update' so controller knows to run update method.
    data['update'] = true;

    // !IMPORTANT NOTE: As of CI 2.0, if csrf (cross-site request forgery) protection is enabled via CI's config, this must be included to submit the token.
    data['csrf_test_name'] = $('input[name="csrf_test_name"]').val();

    $.post(system.base_url + '/ajax/cart/detail/' +  system.lang, data, function(response) {
        $('#cart').html($(response.detail).contents().unwrap());
    });

}

/**
 * Remove the delected item form the cart
 * @param e
 */
function onDeleteListener(e) {

    var el = $(this),
        currentText = $(this).text();

    e.preventDefault();

    $(this)
        .addClass("loading")
        .text(flexicartText.deleting);

    $.ajax({
        type: "POST",
        dataType: "json",
        url: $(this).attr("href")
    })
        .done(function (data) {

            $("#cart").html($(data.detail).contents().unwrap());
            $("#cart_count").text(data.count);

            //Foundation tooltips don't hide if we remove the item
            $(".tooltip").hide();

        })
        .fail(function () {

            el.text(flexicartText.error)
                .removeClass("loading");

            window.setTimeout(function () {
                el.text(currentText);
            }, 2000);

        });

}

/**
 * Add the current product to the cart
 * @param ev
 */
function onAddListener(ev) {

    ev.preventDefault();

    var form = $(this).closest("form"),
        el = $(this),
        currentText = $(this).html();

    $(this)
        .addClass("loading")
        .html(flexicartText.adding);

    $.ajax({
        type: "POST",
        dataType: "json",
        url: form.attr("action"),
        data: form.serialize()
    })
        .done(function (data) {

            $("#message").html(data.message);
            $("#mini_cart").html(data.mini_cart);
            $("#cart_count")
                .addClass("updated")
                .html(data.count);

            el.html(flexicartText.success)
                .addClass("success")
                .removeClass("loading")
                .off() //Remove all events to this button
                .on("click", function () {

                    //Prevent the form from submitting again
                    form.on("submit", function (e) {
                        e.preventDefault();
                    });

                    //Go to the cart url
                    window.location = system.base_url + system.lang + '/' + system.pag_pedidos;

                });

            window.setTimeout(function () {
                el.removeClass("success");
                $("#cart_count").removeClass("updated");
            }, 2000);

        })
        .fail(function () {

            el.text(flexicartText.error)
                .addClass("error")
                .removeClass("loading");

            window.setTimeout(function () {
                el.removeClass("error").text(currentText);
            }, 2000);

        });

}

$(document).ready(function () {
    "use strict";

    var pedido = $(".pedido");

    pedido.find(".add").on("click", onAddListener);
    pedido.find(".delete").on("click", onDeleteListener);
    $("#cart").on("change", "select, input", onChangeListener);

});
