<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="call-to-action text-center">
				<? foreach ($noticias as $noticia): ?>
				<div class="row">
					<div class="col-sm-8">
						<h1 class="title"><?=$noticia->publicacionNombre?></h1>
						<p><?=character_limiter(strip_tags($noticia->publicacionTexto),220)?></p>
					</div>
					<div class="col-sm-4">
						<br>
						<p><a href="<?=base_url()?><?=$diminutivo?>/<?=$paginaNoticiaUrl?>/<?=$noticia->publicacionUrl?>" class="btn btn-lg btn-default btn-animated">Leer m&aacute;s<i class="fa fa-arrow-right pl-20"></i></a></p>
					</div>
				</div>
				<? endforeach ?>
			</div>
		</div>
	</div>
</div>
