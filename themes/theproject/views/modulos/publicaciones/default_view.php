<div class="block clearfix">

	<? foreach ($noticias as $noticia): ?>

		<div class="media margin-clear">
			<div class="media-left">
				<div class="overlay-container">
					<img src="<?=base_url()?>assets/public/images/noticias/noticia_<?=$noticia->publicacionId?><?=$imageSize?>.<?=$noticia->publicacionImagen?>" alt="<?=$noticia->publicacionNombre?>" />
					<a href="<?=base_url()?><?=$diminutivo?>/<?=$paginaNoticiaUrl?>/<?=$noticia->publicacionUrl?>" class="overlay-link small"><i class="fa fa-link"></i></a>
				</div>
			</div>
			<div class="media-body">
				<h6 class="media-heading"><a href="<?=base_url()?><?=$diminutivo?>/<?=$paginaNoticiaUrl?>/<?=$noticia->publicacionUrl?>"><?=character_limiter(strip_tags($noticia->publicacionTexto),20)?></a></h6>
				<p class="small margin-clear"><i class="fa fa-calendar pr-10"></i><?=strftime("%d de %B de %Y", strtotime($html->publicacionFecha))?></p>

                <? foreach ($noticia->images as $image): ?>
                    <img src="<?= base_url("assets/public/images/noticias/noticia_{$noticia->publicacionId}_{$image->publicacionImagenId}.{$image->publicacionImagenExtension}") ?>" alt="<?= $image->publicacionImagenNombre ?>">
                <? endforeach; ?>

			</div>
			<hr>
		</div>

	<? endforeach ?>
	<?=$pagination?>

</div>