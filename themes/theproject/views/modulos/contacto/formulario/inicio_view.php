<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="call-to-action text-center">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <h2 class="title">Escr&iacute;benos</h2>
                        <div class="separator"></div>
                        <?= form_open('submit/contacto/' . $diminutivo, array('class' => 'form-inline margin-clear form_contacto')); ?>

                            <?php foreach($campos as $row):?>

                                <div class="col-md-6">
                                    <div class="form-group has-feedback mb-10">

                                        <?php switch($row->inputTipoId): case 1: case 13: ?>
                                            <input <?=$row->contactoCampoRequerido ? 'required' : ''?>

                                                placeholder="<?=$row->contactoCampoPlaceholder?>"
                                                class="form-control form-control-lg <?=$row->contactoCampoClase?>"
                                                name="campos[<?=$row->contactoCampoId?>]"
                                                id="campos_<?=$row->contactoCampoId?>"
                                                type="text"

                                                <? if($row->contactoCampoRequerido): ?>
                                                    data-fv-notempty="true"
                                                    data-fv-notempty-message="<?=lang('required')?>"
                                                <? endif ?>

                                                <? if($row->contactoCampoValidacion === 'email'): ?>
                                                    data-fv-emailaddress="true"
                                                    data-fv-emailaddress-message="<?=lang($row->contactoCampoValidacion)?>"
                                                <? endif ?>

                                                />
                                            <i class="fa form-control-feedback"></i>
                                            <?php break; ?>
                                        <?php case 3:?>
                                            <textarea <?=$row->contactoCampoRequerido ? 'required' : ''?>

                                                pattern="<?=$row->contactoCampoValidacion?>"
                                                placeholder="<?=$row->contactoCampoPlaceholder?>"
                                                rows="5"
                                                cols="5"
                                                class="form-control form-control-lg <?=$row->contactoCampoClase?>"
                                                name="campos[<?=$row->contactoCampoId?>]"
                                                id="campos_<?=$row->contactoCampoId?>"

                                                <? if($row->contactoCampoRequerido): ?>
                                                    data-fv-notempty="true"
                                                    data-fv-notempty-message="<?=lang('required')?>"
                                                <? endif ?>

                                                ></textarea>
                                            <?php break;?>
                                        <?php case 8:?>
                                            <select class="contacto_mapa <?=$row->contactoCampoClase?>" name="campos[<?=$row->contactoCampoId?>]" id="campos_<?=$row->contactoCampoId?>">
                                                <? foreach ($ubicaciones as $key => $ubicacion): ?>
                                                    <option value="<?=$ubicacion->mapaUbicacionId?>"><?=$ubicacion->mapaUbicacionNombre?></option>
                                                <? endforeach ?>
                                            </select>
                                            <?php break;?>
                                        <?php endswitch;?>

                                    </div>
                                </div>


                            <?php endforeach;?>

                            <input type="hidden" value="<?=$diminutivo?>" name="idioma" />
                            <button type="submit" class="btn btn-lg btn-gray-transparent btn-animated margin-clear"><?=lang('ui_button_send')?> <i class="fa fa-send"></i></button>
                        <?= form_close() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>