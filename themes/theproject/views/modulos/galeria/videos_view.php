<div class="content video" data-youtube-carrousel >
    <div class="flex-video">
        <div data-youtube-player>Cargando video...</div>
    </div>
    <? if(count($videos) > 1): ?>
    <ul class="videos">
        <?php foreach($videos as $key => $value): ?>
            <? if($value->descargaArchivo != ''): ?>
                <li>
                    <a href="http://www.youtube.com/v/<?=$value->descargaArchivo?>?version=3&amp;hl=es_ES&amp;rel=0">
                        <img width="120" height="90" data-youtube-item="<?=$value->descargaArchivo?>" src="http://img.youtube.com/vi/<?=$value->descargaArchivo?>/1.jpg">
                    </a>
                </li>
            <? endif?>
        <?php endforeach;?>
    </ul>
    <? endif ?>
</div>
