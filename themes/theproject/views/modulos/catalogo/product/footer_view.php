<? foreach ($productos as $producto): ?>
	<div class="col-xs-6">
		<div class="overlay-container mb-10">
			<img src="<?=base_url()?>assets/public/images/catalog/prod_<?=$producto->productoId?><?=$imageSize?>.<?=$producto->productoImagenExtension?>" alt="<?=$producto->productoNombre?>" />
			<a href="<?=base_url()?><?=$diminutivo?>/<?=$paginaCatalogoUrl?>/<?=$producto->categoria->id?>/<?=$producto->categoria->productoCategoriaUrl?>/<?=$producto->productoUrl?>" class="overlay-link small">
				<i class="fa fa-link"></i>
			</a>
		</div>
	</div>
<? endforeach ?>
