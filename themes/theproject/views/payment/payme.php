<form action="<?=$form_url?>" method="post" name="frmVPOS2">
    <input type="hidden" name="acquirerId" value="<?php echo $config[$config['environment']]['vpos2_idEntCommerce']; ?>"/>
    <input type="hidden" name="idCommerce" value="<?php echo $config[$config['environment']]['commerce_id']; ?>"/>
    <input type="hidden" name="purchaseOperationNumber" value="<?php echo $purchaseOperationNumber; ?>"/>
    <input type="hidden" name="purchaseAmount" value="<?php echo $price; ?>"/>
    <input type="hidden" name="purchaseCurrencyCode" value="<?= $config['currency_code'] ?>"/>
    <input type="hidden" name="language" value="ES"/>
    <input type="hidden" name="shippingFirstName" value="<?= $user->first_name ?>"/>
    <input type="hidden" name="shippingLastName" value="<?= $user->last_name ?>"/>
    <input type="hidden" name="shippingEmail" value="<?= $user->email ?>"/>
    <input type="hidden" name="shippingAddress" value="Quito"/>
    <input type="hidden" name="shippingZIP" value="00000"/>
    <input type="hidden" name="shippingCity" value="Quito"/>
    <input type="hidden" name="shippingState" value="Pichincha"/>
    <input type="hidden" name="shippingCountry" value="EC"/>

    <? //Parametro para la Integracion con Pay-me. Contiene el valor del parametro codCardHolderCommerce.?>
    <input type="hidden" name="userCommerce" value="<?= $user->id ?>"/>

    <? //Parametro para la Integracion con Pay-me. Contiene el valor del parametro codAsoCardHolderWallet. ?>
    <input type="hidden" name="userCodePayme" value="<?= $user->payme_wallet_code ?>"/>

    <input type="hidden" name="descriptionProducts" value="<?=$product?>"/>
    <input type="hidden" name="programmingLanguage" value="PHP"/>

    <? //Ejemplo envío campos reservados en parametro reserved1. ?>
    <input type="hidden" name="reserved1" value="{{order_number}}"/>
    <input type="hidden" name="purchaseVerification" value="<?php echo $purchaseVerification; ?>"/>

    <input ng-click="paymentSubmit($event, frmVPOS2)" type="submit" name="submit" value="Pagar con PayMe" >

</form>

