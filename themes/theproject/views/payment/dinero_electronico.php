
<h1>Pago con su celular (Dinero Electr&oacute;nico)</h1>
<div class="content">

	<? if($error): ?>
		<?= $error ?>
	<? else: ?>

	<form id="dineroElectronico" action="<?=$form_url?>" method="post">
		<p>Ingrese los datos con los que se registro cuando marco al <strong>*153#</strong> para poder usar el Dinero Electronico</p>
		<p>
			<label>Cedula</label>
			<input type="text" value="<?=$user->fac_ruc?>" name="document">
		</p>
		<p>
			<label>Celular</label>
			<input type="text" value="<?= preg_match('/0\d{9}/', $user->fac_telefono) ? $user->fac_telefono : ''?>" name="phone">
		</p>
		<p class="detail">Ud va a realizar una compra de</p>
		<p class="price"><strong>$<?=$price?> USD</strong></p>
		<p>
			<button class="button boton">Pagar</button>
		</p>

		<input name="proveedor" value="DineroElectronico" type="hidden">

	</form>

	<script type="text/javascript">

		window.addEvent('domready', function () {

			"use strict";
			var form =  document.id("dineroElectronico"),
				modal = new Element('div', {
					'class' : 'modal'
				}),
				mbody = new Element('div', {
					'class' : 'mbody'
				}),
				cerrar = new Element('a', {
					'class' : 'close',
					'text' : 'X',
					'href' : 'javascript:void(0);',
					events : {
						click : function () {
							modal.fade(0);
						}
					}
				});

			modal.inject(document.body);
			mbody.inject(modal);
			cerrar.inject(modal);

			form.addEvents({
				'submit' : function (e) {
					e.preventDefault();

					this.set('send', {
						onSuccess : function (results) {

							var json = JSON.decode(results);
							mbody.set('html', json.message);

							console.log(json);

							if(json.success) {
								window.location.replace(json.redirect);
							}

						},
						onFailure : function () {
							mbody.set('html', "Hubo un error conectando con el servidor");
							modal.fade(1);
						}
					});

					modal.setStyle('opacity', 0);
					modal.setStyle('display', 'block');
					mbody.set('text', 'Esperando validacion, revise su celular');
					modal.fade(1);
					this.send();

				}
			});




		});



		/*$(function() {
			"use strict";

			$("#dineroElectronico").click(function(e) {

				e.preventDefault();

				var name = $("#name").val();
				var email = $("#email").val();
				var phone = $("#phone").val();
				var message = $("#message").val();
				$("#returnmessage").empty(); // To empty previous error/success message.
				// Checking for blank fields.
				if (name === "" || email === "" || phone === "") {
					alert("Por favor llene los campos requeridos");
				} else {
					// Returns successful data submission message when the entered information is stored in database.
					$.post($("form").attr("action"), {
						name: name,
						email: email,
						message: message,
						phone: phone
					}, function(data) {
						$("#returnmessage").append(data.message);
						console.log(data.success);
						if(data.success) {
							$("form")[0].reset();
						}
					});
				}
			});

		});*/
	</script>

<? endif ?>


</div>
