<div class="content">

    <form action="<?=$env_config['url']?>"
          name="twoCheckoutGatewayButton"
          method="post">

        <input type="hidden" name="sid" value="<?=$config['sellerId']?>" />
        <input type="hidden" name="mode" value="2CO" />
        <input type="hidden" name="class" value="TwoCheckoutGateway">
        <input type="hidden" name="x_receipt_link_url" value="<?= base_url($diminutivo . '/' . $pagPedidos->paginaNombreURL . '/payment-processed') ?>">
        <input type="hidden" name="lang" value="sp">

        <input type="hidden" name="li_0_type" value="product" />
        <input type="hidden" name="li_0_name" value="{{order_number}}" />

        <? if($tangible): ?>
            <?
            $total = $this->flexi_cart->total(TRUE, FALSE, TRUE) - $this->flexi_cart->shipping_total(TRUE, FALSE, TRUE);
            ?>
            <input type="hidden" name="li_0_price" value="<?=$total?>" />
            <input type="hidden" name="li_0_tangible" value="Y" />
        <? else: ?>
            <input type="hidden" name="li_0_price" value="<?=$this->flexi_cart->total(TRUE, FALSE, TRUE)?>" />
            <input type="hidden" name="li_0_tangible" value="N" />
        <? endif; ?>

        <input type="hidden" name="card_holder_name" value="<?= $user->first_name ?> <?= $user->last_name ?>" />
        <input type="hidden" name="ship_name" value="<?= $user->first_name ?> <?= $user->last_name ?>" />
        <input type="hidden" name="email" value="<?=$user->email?>" />

        <input type="hidden" name="ship_country" value="Ecuador" />
        <input type="hidden" name="country" value="Ecuador" />

        <? if($tangible): ?>
            <input type="hidden" name="li_1_type" value="shipping" />
            <input type="hidden" name="li_1_name" value="Env&iacute;o" />
            <input type="hidden" name="li_1_price" value="<?= $this->flexi_cart->shipping_total(TRUE, FALSE, TRUE) ?>" />
        <? endif; ?>

        <div ng-repeat="field in contact_fields">
            <input type="hidden" name="{{field.twoCheckoutName}}" ng-value="field.userFieldRelContent" />
        </div>

        <div ng-repeat="field in shipping_fields">
            <input type="hidden" name="{{field.twoCheckoutName}}" ng-value="field.userFieldRelContent" />
        </div>

        <input ng-click="paymentSubmit($event, twoCheckoutGatewayButton)" name="submit" type="submit" value="Pagar con 2checkout" />
    </form>

</div>

<script src="https://www.2checkout.com/static/checkout/javascript/direct.min.js"></script>
