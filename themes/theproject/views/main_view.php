<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="<?=$diminutivo?>"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="<?=$diminutivo?>"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="<?=$diminutivo?>"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="<?=$diminutivo?>"> <!--<![endif]-->

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title><?=$titulo?></title>
	<meta name="description" content="<?=$meta_description?>">
	<meta name="keywords" content="<?=$meta_keywords?>">
	<meta name="generator" content="FlexCMS">

	<? //Facebook OpenGraph meta tags ?>
	<meta property="og:title" content="<?=$og_title?>" />
	<meta property="og:site_name" content="<?=$nombre_sitio?>" />
	<meta property="og:url" content="<?=current_url()?>" />
	<meta property="og:image" content="<?=$og_image?>" />
	<meta property="og:description" content="<?=$og_description?>"/>

	<!-- Mobile Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- Web Fonts -->
	<link href='http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,500italic,700,700italic' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Raleway:700,400,300' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=PT+Serif' rel='stylesheet' type='text/css'>

	<link type="text/plain" rel="author" href="<?=base_url()?>humans.txt">

	<?php Assets::css_group('system', array_merge($assets_banner_css, array(
		'packages/bootstrap/dist/css/bootstrap.min.css',
		'packages/formvalidation/dist/css/formValidation.min.css',
		'themes/' . $theme . '/fonts/font-awesome/css/font-awesome.css',
		'themes/' . $theme . '/fonts/fontello/css/fontello.css',
		'themes/' . $theme . '/plugins/magnific-popup/magnific-popup.css',
		'themes/' . $theme . '/css/animations.css',
		'themes/' . $theme . '/css/facebook_feed.scss',
		'themes/' . $theme . '/plugins/owl-carousel/owl.carousel.css',
		'themes/' . $theme . '/plugins/owl-carousel/owl.transitions.css',
		'themes/' . $theme . '/plugins/hover/hover-min.css',
		'themes/' . $theme . '/css/style.css',
		'themes/' . $theme . '/css/skins/promocionales.css',
		'themes/' . $theme . '/css/custom.css',
	)), $theme); ?>

	<script>
		var system = {
			base_url: '<?=base_url()?>',
			<?= !empty($pagPedidos) ? "pag_pedidos: '" . $pagPedidos->paginaNombreURL . "'," . PHP_EOL : ""?>
			<?= !empty($pagAutenticacion) ? "pag_autenticacion: '" . $pagAutenticacion->paginaNombreURL . "'," . PHP_EOL : ""?>
			<?= $popup_banner ? "popup_banner: { html:'" . $popup_banner->html . "',id: " . $popup_banner->id . "}," . PHP_EOL : ""?>
			lang: '<?=$diminutivo?>',
			fb_login: false
		}
	</script>

</head>

<? $this->benchmark->mark('body_html_start');?>
<body class="no-trans transparent-header">

<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>

<div class="page-wrapper">

	<!-- page wrapper start -->
	<!-- ================ -->
	<div class="page-wrapper">

		<!-- header-container start -->
		<div class="header-container">

			<!-- header start -->
			<!-- classes:  -->
			<!-- "fixed": enables fixed navigation mode (sticky menu) e.g. class="header fixed clearfix" -->
			<!-- "dark": dark version of header e.g. class="header dark clearfix" -->
			<!-- "full-width": mandatory class for the full-width menu layout -->
			<!-- "centered": mandatory class for the centered logo layout -->
			<!-- ================ -->
			<header class="header  fixed full-width  clearfix">

				<!-- header-right start -->
				<!-- ================ -->
				<div class="header-right clearfix">

					<!-- main-navigation start -->
					<!-- classes: -->
					<!-- "onclick": Makes the dropdowns open on click, this the default bootstrap behavior e.g. class="main-navigation onclick" -->
					<!-- "animated": Enables animations on dropdowns opening e.g. class="main-navigation animated" -->
					<!-- "with-dropdown-buttons": Mandatory class that adds extra space, to the main navigation, for the search and cart dropdowns -->
					<!-- ================ -->
					<div class="main-navigation  animated with-dropdown-buttons">

						<!-- navbar start -->
						<!-- ================ -->
						<nav class="navbar navbar-default" role="navigation">
							<div class="container-fluid">

								<!-- Toggle get grouped for better mobile display -->
								<div class="navbar-header">
									<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
										<span class="sr-only">Toggle navigation</span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
									</button>

									<!-- header-left start -->
									<!-- ================ -->
									<div class="header-left clearfix">

										<!-- logo -->
										<div id="logo" class="logo">
											<a href="<?=base_url()?>"><img id="logo_img" src="<?= base_url('themes/' . $theme . '/images/logo.png') ?>" alt="<?=$nombre_sitio?>"></a>
										</div>

									</div>
									<!-- header-left end -->

								</div>

								<!-- Collect the nav links, forms, and other content for toggling -->
								<div class="collapse navbar-collapse" id="navbar-collapse-1">

									<!-- main-menu -->
									<? render_menu('main-menu', $menu['tree'], $menu, array('class' => 'nav navbar-nav navbar-right')) ?>
									<!-- main-menu end -->

									<!-- header dropdown buttons -->
									<div class="header-dropdown-buttons hidden-xs hidden-sm">
										<div class="btn-group dropdown">
											<button type="button" class="btn dropdown-toggle" data-toggle="dropdown"><i class="icon-search"></i></button>
											<ul class="dropdown-menu dropdown-menu-right dropdown-animation">
												<li>
													<form id="search" role="search" class="search-box margin-clear">
														<div class="form-group has-feedback">
															<input name="searchString" placeholder="<?=$this->lang->line('ui_search')?>..." type="text" class="form-control" placeholder="Search">
															<i class="icon-search form-control-feedback"></i>
														</div>
														<div class="searchResult" style="display: none"></div>
													</form>
												</li>
											</ul>
										</div>
										<div class="btn-group dropdown">
											<button type="button" class="btn dropdown-toggle" data-toggle="dropdown"><i class="icon-basket-1"></i><span id="cart_count" class="cart-count default-bg"><?= count($mini_cart_items) ?></span></button>
											<ul class="dropdown-menu dropdown-menu-right dropdown-animation cart">
												<li>
													<?= $this->load->view('paginas/cart/mini_cart_view', array('mini_cart_items' => $mini_cart_items), TRUE) ?>
													<div class="panel-body text-right">
														<a href="<?= base_url($diminutivo . '/' . $pagPedidos->paginaNombreURL )?>" class="btn btn-group btn-gray btn-sm">Ver carrito</a>
														<a href="<?= base_url($diminutivo . '/' . $pagPedidos->paginaNombreURL . '/checkout' )?>" class="btn btn-group btn-gray btn-sm">Checkout</a>
													</div>
												</li>
											</ul>
										</div>
									</div>
									<!-- header dropdown buttons end-->

								</div>

							</div>
						</nav>
						<!-- navbar end -->

					</div>
					<!-- main-navigation end -->
				</div>
				<!-- header-right end -->

				<div id="message"></div>

			</header>

			<!-- header end -->
		</div>
		<!-- header-container end -->

		<div id="page-start"></div>

		<? if($error_message): ?>
			<?= $error_message ?>
		<? endif ?>

		<?= render_content() ?>

		<!-- footer top start -->
		<!-- ================ -->
		<div class="dark-bg  default-hovered footer-top animated-text">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="call-to-action text-center">
							<div class="row">
								<div class="col-sm-8">
									<h2>Haga su pedido con nosotros</h2>
									<h2>No gaste m&aacute;s tiempo</h2>
								</div>
								<div class="col-sm-4">
									<p class="mt-10"><a href="<?= base_url('es/pedidos')?>" class="btn btn-animated btn-lg btn-gray-transparent ">M&aacute;s informaci&oacute;n<i class="fa fa-cart-arrow-down pl-20"></i></a></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- footer top end -->

		<!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
		<!-- ================ -->
		<footer id="footer" class="clearfix ">

			<!-- .footer start -->
			<!-- ================ -->
			<div class="footer">
				<div class="container">
					<div class="footer-inner">
						<div class="row">
							<div class="col-md-3">
								<div class="footer-content">
									<div class="logo-footer"><img id="logo-footer" src="<?= base_url('themes/' . $theme . '/images/logo.png') ?>" alt=""></div>
									<?= article(1, 'footer_view') ?>
								</div>
							</div>
							<div class="col-md-3">
								<div class="footer-content">
									<h2 class="title">Sitio Web</h2>
									<div class="separator-2"></div>
									<nav class="mb-20">
										<? render_menu('footer-menu', $menu['tree'], $menu, array('class' => 'nav nav-pills nav-stacked list-style-icons'), 'footer_view') ?>
									</nav>
								</div>
							</div>
							<div class="col-md-3">
								<div class="footer-content">
									<h2 class="title">Productos</h2>
									<div class="separator-2"></div>
									<div class="row grid-space-10">
										<?= important_products(4, 'footer_view') ?>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="footer-content">
									<h2 class="title">Cont&aacute;ctanos</h2>
									<div class="separator-2"></div>
									<?= article(2, 'footer_view') ?>
									<ul class="social-links circle animated-effect-1">
										<li class="facebook"><a target="_blank" href="https://www.facebook.com/byppromocionales"><i class="fa fa-facebook"></i></a></li>
										<li class="twitter"><a target="_blank" href="http://www.twitter.com"><i class="fa fa-twitter"></i></a></li>
										<li class="googleplus"><a target="_blank" href="http://plus.google.com"><i class="fa fa-google-plus"></i></a></li>
									</ul>
									<div class="separator-2"></div>
									<?= address([5], 'footer_view') ?>

                                    <h2 class="title">Suscribirse a bolet&iacute;n</h2>
                                    <form action="<?=base_url('submit/boletin/024a8be22b')?>" method="post">
                                        <input type="text" name="email">
                                        <input type="submit" value="enviar">
                                    </form>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- .footer end -->

			<!-- .subfooter start -->
			<!-- ================ -->
			<div class="subfooter">
				<div class="container">
					<div class="subfooter-inner">
						<div class="row">
							<div class="col-md-12">
								<p class="text-center">Copyright © 2015 <a target="_blank" href="http://dejabu.ec">Dejabu</a>. Todos los derechos reservados</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- .subfooter end -->

		</footer>
		<!-- footer end -->

	</div>
	<!-- page-wrapper end -->

</div>

<? $this->benchmark->mark('body_html_end'); ?>

<?php Assets::js_group('system', array_merge(array(
	'packages/foundation/js/vendor/jquery.js',
	'packages/jquery.easing/js/jquery.easing.min.js',
), $assets_banner_js), $theme); ?>

<script src="<?= base_url('assets/slideshow_js') ?>"></script>

<?php Assets::js_group('footer', array(
	'packages/bootstrap/dist/js/bootstrap.min.js',
	'packages/formvalidation/dist/js/formValidation.min.js',
	'packages/formvalidation/dist/js/language/' . ($diminutivo === 'es' ? 'es_ES' : 'en_US') . '.js',
	'packages/flexcms/search.js',
	'packages/flexcms/cart.js',
	'packages/flexcms/system.js',
	'packages/flexcms/youtube.js',
	'themes/' . $theme . '/plugins/modernizr.js',
	'themes/' . $theme . '/plugins/magnific-popup/jquery.magnific-popup.min.js',
	'themes/' . $theme . '/plugins/waypoints/jquery.waypoints.min.js',
	'themes/' . $theme . '/plugins/jquery.countTo.js',
	'themes/' . $theme . '/plugins/jquery.parallax-1.1.3.js',
	'themes/' . $theme . '/plugins/owl-carousel/owl.carousel.js',
	'themes/' . $theme . '/plugins/jquery.browser.js',
	'themes/' . $theme . '/plugins/isotope/isotope.pkgd.min.js',
	'themes/' . $theme . '/plugins/SmoothScroll.js',
	'themes/' . $theme . '/plugins/jquery.validate.js',
	'themes/' . $theme . '/js/facebook_feed.js',
	'themes/' . $theme . '/js/template.js',
	'themes/' . $theme . '/js/custom.js',
), $theme); ?>

<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-55ce1ee324baa05f" async="async"></script>

<!-- Page rendered in {elapsed_time} seconds -->
</body>
</html>
