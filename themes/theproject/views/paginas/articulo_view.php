<div class="container">
    <div class="row">

        <!-- main start -->
        <!-- ================ -->
        <div class="main col-md-12">

            <!-- page-title start -->
            <!-- ================ -->
            <h1 class="page-title"><?=$articuloTitulo?></h1>
            <div class="separator-2 mb-20"></div>
            <!-- page-title end -->

            <?=$articuloContenido?>

        </div>
        <!-- main end -->

    </div>
</div>