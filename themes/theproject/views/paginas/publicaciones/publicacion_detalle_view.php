<h1 class="page-title"><?=$html->publicacionNombre?></h1>



<article class="blogpost full">
	<header>
		<div class="post-info">
			<span class="post-date">
				<i class="icon-calendar"></i>
				<?=strftime("%d de %B de %Y", strtotime($html->publicacionFecha))?>
			</span>
		</div>
	</header>
	<div class="blogpost-content">
		<div id="carousel-blog-post" class="carousel slide mb-20" data-ride="carousel">
			<!-- Indicators -->
			<?

			$count = 0;
			if($html->publicacionImagen != '') $count++;
			$count += count($imagenes);

			if($count > 1):

			?>
			<ol class="carousel-indicators">
				<? for($i = 0; $i < $count; $i++): ?>
					<li data-target="#carousel-blog-post" data-slide-to="<?=$i?>" class="<?= !$i ? 'active' : ''?>"></li>
				<? endfor ?>
			</ol>

			<? endif ?>

			<!-- Wrapper for slides -->
			<div class="carousel-inner" role="listbox">

				<? if($html->publicacionImagen != ''): ?>
					<div class="item active">
						<div class="overlay-container">
							<img src="<?=base_url()?>assets/public/images/noticias/noticia_<?=$html->publicacionId;?>.<?=$html->publicacionImagen;?>" alt="<?=$html->publicacionNombre;?>" />
							<a class="overlay-link popup-img" href="<?=base_url()?>assets/public/images/noticias/noticia_<?=$html->publicacionId;?>_popup.<?=$html->publicacionImagen;?>"><i class="fa fa-search-plus"></i></a>
						</div>
					</div>
				<? endif ?>

				<? foreach($imagenes as $imagen):?>
					<div class="item">
						<div class="overlay-container">
							<img src="<?=base_url()?>assets/public/images/noticias/noticia_<?=$imagen->publicacionId?>_<?=$imagen->publicacionImagenId?>_big.<?=$imagen->publicacionImagenExtension?>" />
							<a class="overlay-link popup-img" href="<?=base_url()?>assets/public/images/noticias/noticia_<?=$imagen->publicacionId?>_<?=$imagen->publicacionImagenId?>_popup.<?=$imagen->publicacionImagenExtension?>"><i class="fa fa-search-plus"></i></a>
						</div>
					</div>
				<? endforeach; ?>

			</div>
		</div>

		<?=$html->publicacionTexto?>

	</div>
	<footer class="clearfix">
		<div class="addthis_sharing_toolbox"></div>
	</footer>
</article>

