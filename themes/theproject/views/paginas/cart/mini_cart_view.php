<div id="mini_cart">

    <table class="table table-hover">
        <thead>
            <tr>
                <th class="quantity">CANT</th>
                <th class="product">Producto</th>
                <th class="amount">Subtotal</th>
            </tr>
        </thead>
        <tbody>
            <? if(! empty($mini_cart_items)): ?>

                <? foreach($mini_cart_items as $row): ?>
                    <tr>
                        <td class="quantity"><?= $row['quantity'] ?> x</td>
                        <td class="product"><a href="<?= $row['url'] ?>"><?= $row['name'] ?></a></td>
                        <td class="amount">
                            <? // If an item discount exists, strike out the standard item total and display the discounted item total.
                            if ($row['discount_quantity'] > 0): ?>
                                <span class="strike"><?= $row['price_total'] ?></span><br/>
                                <?= $row['discount_price_total'] ?>
                            <? // Else, display item total as normal.
                            else: ?>
                                <?= $row['price_total'] ?>
                            <? endif ?>
                        </td>
                    </tr>
                <? endforeach ?>

                <tr>
                    <td class="total-quantity" colspan="2"><?= $this->flexi_cart->total_items() ?> Items en Total</td>
                    <td class="total-amount"><?= $this->flexi_cart->total() ?></td>
                </tr>

            <? else: ?>
                <tr>
                    <td colspan="3" class="empty">El carrito est&aacute; vac&iacute;o!</td>
                </tr>
            <? endif ?>
        </tbody>
    </table>

</div>
