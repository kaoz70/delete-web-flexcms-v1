<?= $message ?>

<!-- page-title start -->
<!-- ================ -->
<h1 class="page-title text-center">Gracias <i class="fa fa-smile-o pl-10"></i></h1>
<div class="separator"></div>
<!-- page-title end -->
<p class="lead text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis a deleniti sequi necessitatibus, rem asperiores, magnam labore ullam sint placeat excepturi. Vero corrupti consequuntur id eum esse, rerum, iure neque.</p>

<table class="table">
    <tbody>
    <tr>
        <th>N&uacute;mero de orden:</th>
        <td><?=$summary_data['ord_order_number']?></td>
    </tr>
    <tr>
        <th>M&eacute;todo de pago:</th>
        <td><?=$summary_data['ord_payment_method']?></td>
    </tr>
    <tr>
        <th>Documento de transaccion:</th>
        <td><?=$summary_data['ord_payment_id']?></td>
    </tr>
    <tr>
        <th>Email a donde se enviaron los datos:</th>
        <td><?= $_SESSION['user_fields']['ord_email'] ?></td>
    </tr>
    </tbody>
</table>

<p class="text-center">
    <a href="<?= base_url($diminutivo . '/productos') ?>" class="btn btn-default btn-lg">Seguir Comprando!</a>
</p>