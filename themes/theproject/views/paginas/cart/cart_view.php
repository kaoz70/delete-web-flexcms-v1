<div id="cart" class="row">

    <!-- main start -->
    <!-- ================ -->
    <div class="main col-md-12">

        <!-- page-title start -->
        <!-- ================ -->
        <h1 class="page-title">Carrito de Compras</h1>
        <div class="separator-2"></div>
        <!-- page-title end -->

        <?= $message ?>

        <?= form_open($diminutivo . '/' . $pagPedidos->paginaNombreURL) ?>

        <table class="table cart table-hover table-colored">
            <thead>
            <tr>
                <th>Producto </th>
                <th>Precio </th>
                <th>Cantidad</th>
                <th>Eliminar </th>
                <th class="amount">Total </th>
            </tr>
            </thead>
            <tbody>

            <?php
            if (! empty($cart_items)) :
                $i = 0;
                foreach($cart_items as $row) : $i++;

                    $stock = check_stock($row['id'], $row['quantity']);

                ?>

                    <tr class="pedido">
                        <td class="product">
                            <div class="row">
                                <div class="col-xs-4 mt-20">
                                    <?= $stock->icon ?>
                                    <a class="" href="<?= $row['url'] ?>"><?= $row['name'] ?></a>
                                    <input type="hidden" name="items[<?= $i ?>][row_id]" value="<?= $row['row_id'] ?>"/>
                                </div>
                                <div class="col-xs-8">
                                    <div class="form-group">
                                        <? if ($this->flexi_cart->item_option_status($row['row_id']) && isset($row['option_data'])):
                                            foreach($row['option_data'] as $option_column => $option_data): ?>

                                                <?/**
                                                Displaying items options if they exist, as an editable field,
                                                this example uses a custom field ('option_data') containing an array of option data.
                                                 */?>

                                                <div class="row">
                                                    <div class="col-xs-4">
                                                        <label class="mt-10"><?= $option_data->productoCampoValor ?></label>
                                                    </div>
                                                    <div class="col-xs-8">
                                                        <select class="form-control" name="items[<?= $i ?>][options][<?= $option_column ?>]">
                                                            <? foreach($option_data->options as $data): ?>
                                                                <option
                                                                    value="<?= $data ?>"
                                                                    <?= set_select('items['.$i.'][options]['.$option_column.']', $data, ($data == $row['options'][$option_column])) ?>>
                                                                    <?= $data ?>
                                                                </option>
                                                            <? endforeach ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            <? endforeach ?>
                                        <? elseif($this->flexi_cart->item_option_status($row['row_id'])): ?>
                                            <? //Example of displaying an items options if they exist, but as text, rather than an editable field ?>
                                            <?= $this->flexi_cart->item_options($row['row_id'], TRUE).'<br/>';?>
                                        <? endif ?>
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td class="price"><?= $row['price'] ?></td>
                        <td class="quantity">
                            <div class="form-group">
                                <input type="text" name="items[<?=$i?>][quantity]" value="<?= $row['quantity'] ?>" maxlength="3" class="form-control"/>
                            </div>
                        </td>
                        <td class="remove">
                            <a
                                class="btn btn-remove btn-sm btn-default delete"
                                href="<?= base_url() ?>ajax/cart/delete/<?=$diminutivo?>/<?php echo $row['row_id'];?>"
                                title="Click para eliminar el item del carrito">Eliminar</a>
                        <td class="amount"><?= $row['price_total'] ?></td>
                    </tr>

                <? endforeach ?>
            <? endif ?>

            <? if ($this->flexi_cart->item_summary_savings_total(FALSE) > 0) : ?>
            <tr class="discount">
                <th colspan="4">Item Summary Discount Savings Total</th>
                <td><?php echo $this->flexi_cart->item_summary_savings_total();?></td>
            </tr>
            <?php endif; ?>

            <tr>
                <td colspan="5">
                    <div class="form-group">
                        <label>Tipo de pedido</label>
                        <select id="shipping_options" class="form-control" name="shipping[db_option]" style="display: inline-block; width: 227px;">
                            <option data-value="0" value="0"> - Seleccione uno - </option>
                            <? if (! empty($shipping_options)): ?>

                                <? foreach($shipping_options as $shipping): ?>
                                    <option data-value="<?= $shipping['value'] ?>" value="<?= $shipping['id'] ?>" <?= ($shipping['id'] == $this->flexi_cart->shipping_id()) ? 'selected="selected"' : NULL; ?>>
                                        <?= $shipping['name']." : ".$shipping['description'] ?>
                                    </option>
                                <? endforeach ?>

                            <? else: ?>
                                <option value="0">Se le enviar&aacute; una cotizaci&oacute;n antes de despachar el pedido.</option>
                            <? endif ?>
                        </select>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="total-quantity" colspan="4">Subtotal</td>
                <td class="total-amount"><?= $this->flexi_cart->item_summary_total() ?></td>
            </tr>

            <?php if ($this->flexi_cart->summary_discount_status()) { ?>
                <tr class="discount">
                    <th>Resumen de descuentos</th>
                    <td>&nbsp;</td>
                </tr>

                <?php if ($this->flexi_cart->item_summary_discount_status()) { ?>
                    <!--
                        Rather than repeating the descriptions of each item discount listed via the cart,
                        this example summarises the discount totals of all items.
                    -->
                    <tr class="discount">
                        <th>
                        <span class="pad_l_20">
                            &raquo; Item discount discount savings
                        </span>
                        </th>
                        <td>
                            <?php echo $this->flexi_cart->item_summary_savings_total();?>
                        </td>
                    </tr>
                <?php } ?>

                <!--
                    This example uses the 'summary_discount_data()' function to return an array of summary discount values and descriptions.
                    An alternative to using a custom loop to return this discount array, is to call the 'summary_discount_description()' function,
                    which will return a formatted string of all summary discounts.
                -->
                <?php foreach($discounts as $discount) { ?>
                    <tr class="discount">
                        <th>
                        <span class="pad_l_20">
                            &raquo; <?php echo $discount['description'];?>
                            <?php if (! empty($discount['id'])) { ?>
                                : <a href="<?php echo $base_url; ?>standard_library/unset_discount/<?php echo $discount['id']; ?>">Remove</a>
                            <?php } ?>
                        </span>
                        </th>
                        <td><?php echo $discount['value'];?></td>
                    </tr>
                <?php } ?>
                <tr class="discount">
                    <th>Discount Savings Total</th>
                    <td><?php echo $this->flexi_cart->cart_savings_total();?></td>
                </tr>
            <?php } ?>

            <tr>
                <td class="total-quantity" colspan="4">Cargos por envio</td>
                <td class="total-amount"><?php echo $this->flexi_cart->shipping_total();?></td>
            </tr>

            <tr>
                <td class="total-quantity" colspan="4"><?php echo $this->flexi_cart->tax_name()." al ".$this->flexi_cart->tax_rate(); ?></td>
                <td class="total-amount"><?php echo $this->flexi_cart->tax_total();?></td>
            </tr>

            <tr>
                <td class="total-quantity" colspan="4">Total <?= $this->flexi_cart->total_items() ?> Items</td>
                <td class="total-amount"><?= $this->flexi_cart->total() ?></td>
            </tr>

            </tbody>
        </table>

        <? if($this->flexi_cart->minimum_order(FALSE, TRUE)): ?>
            <div data-alert class="alert-box warning">
                <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> NOTA: COSTO M&Iacute;NIMO DE COMPRA <?=$this->flexi_cart->minimum_order()?>
            </div>
        <? endif; ?>

        <div class="text-right">
            <input type="submit" name="update" value="Actualizar Carrito" class="btn btn-group btn-default"/>
            <input type="submit" name="checkout" value="Checkout" class="btn btn-group btn-default"/>
        </div>

        <?= form_close() ?>

    </div>
    <!-- main end -->

</div>
