<?

$contact_fields = [];
$billing_fields = [];
$shipping_fields = [];

foreach ($user_fields as $field) {
    switch ($field->userFieldType) {
        case 'contact':
            $contact_fields[] = $field;
            break;
        case 'billing':
            $billing_fields[] = $field;
            break;
        case 'shipping':
            $shipping_fields[] = $field;
            break;
    }
}

?>

<div class="row">

    <!-- main start -->
    <!-- ================ -->
    <div class="main col-md-12">

        <!-- page-title start -->
        <!-- ================ -->
        <h1 class="page-title">Checkout Review</h1>
        <div class="separator-2"></div>
        <!-- page-title end -->

        <?= $message ?>

        <table class="table cart">
            <thead>
            <tr>
                <th>Producto</th>
                <th>Precio</th>
                <th>Cantidad</th>
                <th class="amount">Total</th>
            </tr>
            </thead>
            <tbody>

            <?php
            if (! empty($cart_items)) :
            $i = 0;
            foreach($cart_items as $row) : $i++;
            ?>

                <tr>
                    <td class="product">
                        <a href="<?= $row['url'] ?>"><?= $row['name'] ?></a>
                    </td>
                    <td class="price"><?= $row['price'] ?></td>
                    <td class="quantity">
                        <div class="form-group">
                            <input type="text" class="form-control" value="<?= $row['quantity'] ?>" disabled>
                        </div>
                    </td>
                    <td class="amount"><?= $row['price_total'] ?></td>
                </tr>

            <? endforeach ?>
            <? endif ?>

            <tr>
                <td class="total-quantity" colspan="3">Subtotal</td>
                <td class="amount"><?= $this->flexi_cart->sub_total() ?></td>
            </tr>

            <tr>
                <td class="total-quantity" colspan="3">Cargos por envio</td>
                <td class="amount"><?php echo $this->flexi_cart->shipping_total();?></td>
            </tr>

            <tr>
                <td class="total-quantity" colspan="3"><?= $this->flexi_cart->tax_name() ?> (<?= $this->flexi_cart->tax_rate() ?>)</td>
                <td class="amount"><?= $this->flexi_cart->tax_total() ?></td>
            </tr>

            <tr>
                <td class="total-quantity" colspan="3">Total <?= $this->flexi_cart->total_items() ?> Items</td>
                <td class="total-amount"><?= $this->flexi_cart->total() ?></td>
            </tr>

            </tbody>
        </table>

        <div class="space-bottom"></div>
        <table class="table">
            <thead>
            <tr>
                <th colspan="2">Informaci&oacute;n para la Factura</th>
            </tr>
            </thead>
            <tbody>
            <? foreach ($billing_fields as $field): ?>

                <tr>
                    <td><?=$field->userFieldLabel?></td>
                    <td class="information"><?= $_SESSION['user_fields'][$field->userFieldOrderCol] ?></td>
                </tr>

            <? endforeach; ?>
            <tr>
                <td>Informaci&oacute;n Adicional</td>
                <td><?= $_SESSION['user_fields']['ord_ship_comments'] ?></td>
            </tr>
            </tbody>
        </table>
        <div class="space-bottom"></div>
        <table class="table">
            <thead>
            <tr>
                <th colspan="2">Informaci&oacute;n para la entrega</th>
            </tr>
            </thead>
            <tbody>
            <? foreach ($shipping_fields as $field): ?>

                <tr>
                    <td><?=$field->userFieldLabel?></td>
                    <td class="information"><?= $_SESSION['user_fields'][$field->userFieldOrderCol] ?></td>
                </tr>

            <? endforeach; ?>
            </tbody>
        </table>
        <div class="space-bottom"></div>

        <div class="text-right">
            <a href="<?= base_url($diminutivo . '/' . $pagPedidos->paginaNombreURL . '/checkout') ?>" class="btn btn-group btn-default"><i class="icon-left-open-big"></i> Regresar</a>

            <? if($usePaymentGateway): ?>
                <a href="<?= base_url($diminutivo . '/' . $pagPedidos->paginaNombreURL . '/payment-methods') ?>" class="btn btn-group btn-default"><i class="fa fa-cart-arrow-down pl-20"></i> Pagar Pedido</a>
            <? else: ?>
                <a href="<?= base_url($diminutivo . '/' . $pagPedidos->paginaNombreURL . '/send') ?>" class="btn btn-group btn-default"><i class="icon-check"></i> Enviar Pedido</a>
            <? endif; ?>
        </div>

    </div>
    <!-- main end -->

</div>