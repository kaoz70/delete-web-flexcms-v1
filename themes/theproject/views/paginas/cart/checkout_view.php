<script src="<?= base_url('packages/angular/angular.min.js') ?>"></script>
<script src="<?= base_url('packages/angular-sanitize/angular-sanitize.min.js') ?>"></script>
<script src="<?= base_url('packages/flexcms/checkout.js') ?>"></script>

<div class="container" ng-app="app" ng-controller="checkout" ng-cloak>

    <div class="row" ng-class="formClass">

        <!-- main start -->
        <!-- ================ -->
        <div class="main col-md-12">

            <!-- page-title start -->
            <!-- ================ -->
            <h1 class="page-title">Checkout</h1>
            <div class="separator-2"></div>
            <!-- page-title end -->

            <?= $message; ?>

            <!-- Angular init some PHP vars -->
            <div ng-init="contact_fields = <?= htmlspecialchars(json_encode($contact_fields)); ?>"></div>
            <div ng-init="shipping_fields = <?= htmlspecialchars(json_encode($shipping_fields)); ?>"></div>
            <div ng-init="billing_fields = <?= htmlspecialchars(json_encode($billing_fields)); ?>"></div>
            <div ng-init="shippingOptions = <?= htmlspecialchars(json_encode($shipping_options)); ?>"></div>
            <div ng-init="email.userFieldRelContent = '<?= $user->email ?>'"></div>
            <div ng-init="shipping = '<?= $this->flexi_cart->shipping_id() ?>'"></div>
            <div ng-init="prices.subtotal = '<?= $this->flexi_cart->sub_total() ?>'"></div>
            <div ng-init="prices.shipping = '<?= $this->flexi_cart->shipping_total();?>'"></div>
            <div ng-init="prices.tax = '<?= $this->flexi_cart->tax_total() ?>'"></div>
            <div ng-init="prices.total = '<?= $this->flexi_cart->total() ?>'"></div>

            <div class="row">
                <div class="col-sm-6">

                    <fieldset ng-class="stepClass(1)">
                        <legend>1 Informacion del cliente</legend>

                        <div ng-show="activeStep == 1">

                            <div class="row">
                                <div class="form-group">
                                    <label for="checkout_email" class="col-md-2 control-label">
                                        Email <small class="text-default">*</small>
                                    </label>
                                    <div class="col-md-10" ng-class="requiredValidation(email) ? 'valid' : 'error'">
                                        <input name="checkout[email]"
                                               ng-required="email.userFieldRequired"
                                               type="text"
                                               ng-model="email.userFieldRelContent"
                                               id="checkout_email"
                                        />
                                        <span class="error" ng-hide="requiredValidation(email)">{{email.error_message}}</span>
                                    </div>
                                </div>
                            </div>

                            <div class="row" ng-repeat="field in contact_fields">

                                <div class="form-group">
                                    <label for="checkout_{{field.userFieldId}}" class="col-md-2 control-label">
                                        {{field.userFieldLabel}} <small ng-show="field.userFieldRequired == 1" class="text-default">*</small>
                                    </label>
                                    <div class="col-md-10" ng-class="requiredValidation(field) ? 'valid' : 'error'">
                                        <input name="checkout[{{field.userFieldId}}]"
                                               ng-required="field.userFieldRequired"
                                               type="text"
                                               ng-model="field.userFieldRelContent"
                                               id="checkout_{{field.userFieldId}}"
                                        />
                                        <span class="error" ng-hide="requiredValidation(field)">{{field.error_message}}</span>
                                    </div>
                                </div>

                            </div>

                            <a href="#" class="btn btn-primary" ng-click="nextStep($event, 1, contact_fields)">Continuar</a>

                        </div>

                        <div ng-show="activeStep > 1">
                            <button ng-click="editStep(1)">Editar</button>
                            <div>{{email.userFieldRelContent}}</div>
                            <div ng-repeat="field in contact_fields">{{field.userFieldRelContent}}</div>
                        </div>

                    </fieldset>

                    <fieldset ng-class="stepClass(2)">
                        <legend>2 Datos de Envio</legend>

                        <div ng-show="activeStep == 2">
                            <div class="row" ng-repeat="field in shipping_fields">

                                <div class="form-group">
                                    <label for="checkout_{{field.userFieldId}}" class="col-md-2 control-label">
                                        {{field.userFieldLabel}} <small ng-show="field.userFieldRequired == 1" class="text-default">*</small>
                                    </label>
                                    <div class="col-md-10" ng-class="requiredValidation(field) ? 'valid' : 'error'">
                                        <input name="checkout[{{field.userFieldOrderCol}}]"
                                               ng-required="field.userFieldRequired"
                                               type="text"
                                               ng-model="field.userFieldRelContent"
                                               id="checkout_{{field.userFieldId}}"
                                        />
                                        <span class="error" ng-hide="requiredValidation(field)">{{field.error_message}}</span>
                                    </div>
                                </div>

                            </div>

                            <label>
                                <input ng-model="saveShippingFields"
                                       value="1"
                                       type="checkbox"
                                       ng-click="!saveShippingFields"
                                       id="save_to_profile" > Recordar &eacute;sta informaci&oacute;n para la pr&oacute;xima ocasi&oacute;n.
                            </label>

                            <div class="form-group">
                                <label>Tipo de pedido</label>
                                <select id="shipping_options"
                                        ng-change="updateShipping()"
                                        class="form-control"
                                        ng-model="shipping">
                                    <option value="0"> - Seleccione uno - </option>
                                    <option ng-repeat="option in shippingOptions" ng-value="option.id">
                                        {{option.name}} : {{option.description}}
                                    </option>
                                    <option ng-hide="shippingOptions.length" value="0">Se le enviar&aacute; una cotizaci&oacute;n antes de despachar el pedido.</option>
                                </select>
                            </div>

                            <a href="#" class="btn btn-primary" ng-click="nextStep($event, 2, shipping_fields)">Continuar</a>

                        </div>

                        <div ng-show="activeStep > 2">
                            <button ng-click="editStep(2)">Editar</button>
                            <div ng-repeat="field in shipping_fields">{{field.userFieldRelContent}}</div>
                        </div>

                    </fieldset>

                    <fieldset ng-class="stepClass(3)">
                        <legend>3 Datos de Facturacion</legend>

                        <div ng-show="activeStep == 3">
                            <div class="checkbox padding-top-clear">
                                <label>
                                    <input name="same_info"
                                           value="1"
                                           type="checkbox"
                                           ng-click="sameBillingInfoCheck()"
                                           id="billing-info-check" <?= isset($_SESSION['user_fields']['same_info']) && $_SESSION['user_fields']['same_info'] ? 'checked' : '' ?>> Mi informaci&oacute;n de <strong>Facturacion</strong> es la misma que la de <strong>Envio</strong>.
                                </label>
                            </div>

                            <div ng-hide="sameBillingInfo">
                                <div class="row" ng-repeat="field in billing_fields">

                                    <div class="form-group">
                                        <label for="checkout_{{field.userFieldId}}" class="col-md-2 control-label">
                                            {{field.userFieldLabel}} <small ng-show="field.userFieldRequired == 1" class="text-default">*</small>
                                        </label>
                                        <div class="col-md-10" ng-class="requiredValidation(field) ? 'valid' : 'error'">
                                            <input name="checkout[{{field.userFieldOrderCol}}]"
                                                   ng-required="field.userFieldRequired"
                                                   type="text"
                                                   ng-model="field.userFieldRelContent"
                                                   id="checkout_{{field.userFieldId}}"
                                            />
                                            <span class="error" ng-hide="requiredValidation(field)">{{field.error_message}}</span>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <a href="#" class="btn btn-primary" ng-click="nextStep($event, 3, billing_fields)">Continuar</a>

                        </div>

                        <div ng-show="activeStep > 3">
                            <button ng-click="editStep(3)">Editar</button>
                            <div ng-repeat="field in billing_fields">{{field.userFieldRelContent}}</div>
                        </div>

                    </fieldset>

                    <fieldset ng-class="stepClass(4)">
                        <legend>4 Compra</legend>

                        <div ng-show="activeStep == 4">

                            <ul>
                                <li><label><input type="radio" name="payment[]" ng-click="paymentType = 'bank'; paymentTranslated = 'Banco'">Dep&oacute;sito o transferencia</label></li>
                                <li><label><input type="radio" name="payment[]" ng-click="paymentType = 'card'; paymentTranslated = 'Tarjeta'">Tarjetas de credito</label></li>
                            </ul>

                            <div ng-show="paymentType === 'bank'">

                                <div class="row">
                                    <div class="col-sm-6">
                                        Banco
                                    </div>
                                    <div class="col-sm-6">
                                        Nombre
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        Cuenta
                                    </div>
                                    <div class="col-sm-6">
                                        Tipo
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        Cedula
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="checkout_bank_document" class="col-md-2 control-label">
                                        Documento de dep&oacute;sito o transferencia
                                    </label>
                                    <div class="col-md-10">
                                        <input name="checkout[ord_payment_id]"
                                               ng-required="bank.required"
                                               type="text"
                                               ng-model="bank.document"
                                               id="checkout_bank_document"
                                        />
                                        <span class="error" ng-hide="bank.document">Debe ingresar el documento de dep&oacute;sito o transferencia</span>
                                    </div>
                                </div>

                                <input type="hidden" name="checkout[ord_payment_method]" ng-value="paymentTranslated">

                                <button ng-click="checkoutSubmit()" class="btn btn-ptimary">Enviar</button>

                            </div>

                            <div ng-show="paymentType === 'card'">

                                <? foreach ($paymentMethods as $key => $class): ?>
                                    <?= $class::button("Orden: " . $orderNumber, $orderNumber, $diminutivo) ?>
                                <? endforeach; ?>

                            </div>

                        </div>

                    </fieldset>

                </div>
                <div class="col-sm-6">

                    <table class="table cart">
                        <thead>
                        <tr>
                            <th>Producto</th>
                            <th>Precio</th>
                            <th>Cantidad</th>
                            <th class="amount">Total</th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php

                        if (! empty($cart_items)) :

                            $i = 0;
                            $subtotal = 0;

                            foreach($cart_items as $row) : $i++;
                                $stock = check_stock($row['id'], $row['quantity']);

                                //Si no tiene stock se lo elimina del carrito, pero se lo muestra debajo para informar
                                if($stock->code === 0) {
                                    $this->flexi_cart->update_cart([
                                        [
                                            'row_id' => $row['row_id'],
                                            'quantity' => 0,
                                        ]
                                    ], FALSE, FALSE, TRUE);
                                }


                                ?>

                                <tr class="<?= $stock->code === 0 ? 'no-stock' : '' ?>">
                                    <td class="product">
                                        <?= $stock->icon ?>
                                        <a href="<?= $row['url'] ?>"><?= $row['name'] ?></a>
                                        <small><?= $stock->message ?></small>
                                    </td>
                                    <td class="price"><?= $row['price'] ?></td>
                                    <td class="quantity">
                                        <div class="form-group">
                                            <input type="text" class="form-control" value="<?= $row['quantity'] ?>" disabled>
                                        </div>
                                    </td>
                                    <td class="amount"><?= $row['price_total'] ?></td>
                                </tr>

                            <? endforeach ?>
                        <? endif ?>

                        <tr>
                            <td class="total-quantity" colspan="3">Cargos por envio</td>
                            <td class="amount">{{prices.shipping}}</td>
                        </tr>

                        <tr>
                            <td class="total-quantity" colspan="3">Subtotal</td>
                            <td class="amount">{{prices.subtotal}}</td>
                        </tr>

                        <tr>
                            <td class="total-quantity" colspan="3"><?= $this->flexi_cart->tax_name() ?> (<?= $this->flexi_cart->tax_rate() ?>)</td>
                            <td class="amount">{{prices.tax}}</td>
                        </tr>

                        <tr>
                            <td class="total-quantity" colspan="3">Total <?= $this->flexi_cart->total_items() ?> Items</td>
                            <td class="total-amount">{{prices.total}}</td>
                        </tr>

                        </tbody>
                    </table>

                </div>
            </div>



        </div>
        <!-- main end -->

    </div>

    <div id="checkout-modal" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Checkout</h4>
                </div>
                <div class="modal-body">
                    <p>{{loadingMessage}}</p>
                </div>
            </div>
        </div>
    </div>

</div>
