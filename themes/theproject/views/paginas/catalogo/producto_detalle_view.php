<style>
    .row.pos_2 .columns.pos_2,
    h1.titulo + .separator-2,
    h1.titulo {
        display: none;
    }
    .row.pos_2 .columns.pos_1 {
        width: 100%;
    }

</style>

<!-- page-title start -->
<!-- ================ -->
<h1 class="page-title"><?=$producto->productoNombre?></h1>
<div class="separator-2"></div>
<!-- page-title end -->

<div class="row">
	<div class="col-md-4">
		<!-- pills start -->
		<!-- ================ -->
		<!-- Nav tabs -->
		<ul class="nav nav-pills" role="tablist">
			<li class="active"><a href="#pill-1" role="tab" data-toggle="tab" title="images"><i class="fa fa-camera pr-5"></i> Foto</a></li>
			<? if($producto->videos && isset($producto->videos[0]) && $producto->videos[0]->contenido): ?>
			<li><a href="#pill-2" role="tab" data-toggle="tab" title="video"><i class="fa fa-video-camera pr-5"></i> Video</a></li>
			<? endif ?>
		</ul>
		<!-- Tab panes -->
		<div class="tab-content clear-style">
			<div class="tab-pane active" id="pill-1">
				<div class="owl-carousel content-slider-with-large-controls">

					<div class="overlay-container overlay-visible">
						<img src="<?=base_url()?>assets/public/images/catalog/prod_<?=$producto->productoId;?>.<?=$producto->productoImagenExtension;?>" alt="<?=$producto->productoNombre;?>" />
						<a href="<?=base_url()?>assets/public/images/catalog/prod_<?=$producto->productoId;?>_popup.<?=$producto->productoImagenExtension;?>" alt="<?=$producto->productoNombre;?>" class="popup-img overlay-link" title="image title"><i class="icon-plus-1"></i></a>
					</div>

					<? foreach($producto->imagenes as $row):?>
						<? if(count($row->contenido) != 0): ?>

							<? foreach($row->contenido as $imagen):?>
								<div class="overlay-container overlay-visible">
									<img src="<?= base_url() ?>assets/public/images/catalog/gal_<?=$producto->productoId;?>_<?=$imagen->productoImagenId?>_big.<?=$imagen->productoImagen?>" alt="<?=$imagen->productoImagenNombre?>" />
									<a href="<?= base_url() ?>assets/public/images/catalog/gal_<?=$producto->productoId;?>_<?=$imagen->productoImagenId?>_big.<?=$imagen->productoImagen?>" class="popup-img overlay-link" title="image title"><i class="icon-plus-1"></i></a>
								</div>
							<? endforeach; ?>

						<? endif ?>
					<? endforeach; ?>

				</div>
			</div>

			<div class="tab-pane" id="pill-2">
				<div class="embed-responsive embed-responsive-16by9">
					<? if($producto->videos): ?>
						<? foreach ($producto->videos as $key => $row): ?>
							<? foreach($row->contenido as $video): ?>
								<iframe class="embed-responsive-item" width="560" height="315" src="https://www.youtube.com/embed/<?=$video->productoVideo?>" frameborder="0" allowfullscreen></iframe>
							<? endforeach ?>
						<? endforeach; ?>
					<? endif ?>
				</div>
			</div>
		</div>
		<!-- pills end -->
	</div>
	<div class="col-md-8 pv-30">

		<h2>Descripci&oacute;n</h2>

		<? foreach($producto->campos as $row):?>
			<? if($row->contenido != ''): ?>
				<div class="campo <?= $row->clase?>">
					<? if($row->contenido != ''): ?>
						<? if($row->labelHabilitado): ?>
							<h4><?=$row->label?></h4>
						<? endif;?>
						<div class="info texto"><?=$row->contenido->productoCampoRelContenido?></div>
						<div class="separator-2"></div>
					<? endif ?>
				</div>
			<? endif ?>
		<? endforeach; ?>

		<? foreach ($producto->listado_predefinido as $listado) : ?>
			<? if($listado->mostrar_nombre): ?>
				<h4><?=$listado->nombre?></h4>
			<? endif;?>
			<p>
				<? foreach($listado->contenido as $key => $list): ?>
					<?= $key ? ' / ' : '' ?> <?= $list->productoCamposListadoPredefinidoTexto ?>
				<? endforeach ?>
			</p>
			<div class="separator-2"></div>
		<? endforeach ?>

		<h4>Stock</h4>
		<p><?= $producto->stock_quantity ?: 0 ?></p>
		<div class="separator-2"></div>

		<div style="overflow: hidden;">
			<div class="pull-right addthis_sharing_toolbox"></div>
		</div>

		<hr class="mb-20">

		<div class="light-gray-bg p-20 bordered clearfix">

			<?= form_open('ajax/cart/add', array('class' => 'pedido clearfix', 'id' => 'pedido', 'role' => 'form')); ?>
				<div class="col-md-3">
					<? foreach($producto->precios as $row):?>
						<? if($row->contenido != ''): ?>
							<span class="product price"><i class="icon-tag pr-10"></i>$<?=number_format($row->contenido, 2, '.', '')?></span>
						<? endif ?>
					<? endforeach; ?>
				</div>
				<div class="col-md-3">
					<div class="form-group row no-gutter">
						<div class="col-xs-6">
							<label class="mt-10">Cantidad</label>
						</div>
						<div class="col-xs-6">
							<input type="text" class="form-control" value="1">
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group row no-gutter">
						<div class="col-xs-4">
							<label class="mt-10">Color</label>
						</div>
						<div class="col-xs-8">
							<select class="form-control">
								<? foreach ($producto->listado_predefinido as $listado) : ?>
									<? if($listado->mostrar_en_pedido): ?>
										<? foreach($listado->contenido as $list): ?>
											<option value="<?= $list->productoCamposListadoPredefinidoTexto ?>"><?= $list->productoCamposListadoPredefinidoTexto ?></option>
										<? endforeach ?>
									<? endif ?>
								<? endforeach ?>
							</select>
						</div>

					</div>
				</div>
				<div class="col-md-3">
					<div class="product elements-list pull-right clearfix">
						<input type="submit" value="A&ntilde;adir al Carrito" class="add margin-clear btn btn-default">
					</div>
				</div>

				<? foreach ($producto->listado_predefinido as $listado) : ?>
					<? if($listado->mostrar_en_pedido): ?>
						<input type="hidden" name="campo[<?=$listado->campo_id?>]" value="<?= array_key_exists(0, $listado->contenido) ? $listado->contenido[0]->productoCamposListadoPredefinidoTexto : ''?>">
					<? endif ?>
				<? endforeach ?>
				<input type="hidden" name="cantidad" value="1">
				<input type="hidden" name="productoId" value="<?=$producto->productoId?>">
				<input type="hidden" name="idioma" value="<?=$diminutivo?>">

			<?= form_close() ?>

		</div>
	</div>
</div>

<h3>Productos Relacionados</h3>
<div class="separator-2"></div>

<div class="row masonry-grid-fitrows grid-space-20">

	<? foreach($producto->productosRelacionados as $rel): ?>

		<div class="col-md-2 col-sm-4 masonry-grid-item mt-10">
			<div class="listing-item white-bg bordered mb-20">
				<div class="overlay-container">
					<img src="<?=base_url()?>assets/public/images/catalog/prod_<?=$rel->productoId?>_popup.<?=$rel->productoImagenExtension?>" alt="<?=$rel->productoNombre?>" />
					<a class="overlay-link popup-img-single" href="<?=base_url()?>assets/public/images/catalog/prod_<?=$rel->productoId?>_popup.<?=$rel->productoImagenExtension?>"><i class="fa fa-search-plus"></i></a>
					<div class="overlay-to-top links">
                        <span class="small">
                            <a href="<?=base_url()?><?=$diminutivo?>/<?=$paginaCatalogoUrl?>/<?=$rel->categoria->id?>/<?=$rel->categoria->productoCategoriaUrl?>/<?=$rel->productoUrl?>" class="btn-sm-link"><i class="icon-link pr-5"></i>Ver Detalles</a>
                        </span>
					</div>
				</div>
				<div class="body">
					<h3><a href="<?=base_url()?><?=$diminutivo?>/<?=$paginaCatalogoUrl?>/<?=$rel->categoria->id?>/<?=$rel->categoria->productoCategoriaUrl?>/<?=$rel->productoUrl?>"><?=$rel->productoNombre?></a></h3>
					<p class="small">
						<? foreach($rel->campos as $campo): ?>
							<?= character_limiter(strip_tags($campo->contenido->productoCampoRelContenido),1000); ?>
						<? endforeach; ?>
					</p>
					<div class="elements-list clearfix">

						<? foreach($rel->precios as $precio):?>
							<? if($precio->contenido): ?>
								<span class="price">$<?=number_format((double)$precio->contenido, 2, '.', '')?></span>
							<? endif ?>
						<? endforeach; ?>

						<div class="pedido">
							<?= form_open('ajax/cart/add', array('class' => 'pedido', 'id' => 'pedido', 'data-abide' => '')); ?>
							<? foreach ($rel->listado as $listado) : ?>
								<? if($listado->mostrar_en_pedido): ?>
									<input type="hidden" name="campo[<?=$listado->campo_id?>]" value="<?= array_key_exists(0, $listado->contenido) ? $listado->contenido[0]->productoCamposListadoPredefinidoTexto : ''?>">
								<? endif ?>
							<? endforeach ?>
							<input type="hidden" name="cantidad" value="1">
							<input type="hidden" name="productoId" value="<?=$rel->productoId?>">
							<input type="hidden" name="idioma" value="<?=$diminutivo?>">
							<a href="#" class="add pull-right margin-clear btn btn-sm btn-default-transparent btn-animated">A&ntilde;adir Al Carrito<i class="fa fa-shopping-cart"></i></a>
							<?= form_close() ?>
						</div>

					</div>
				</div>
			</div>
		</div>

	<? endforeach ?>

</div>