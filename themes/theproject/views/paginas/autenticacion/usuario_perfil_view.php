<div class="main_content">

    <div class="container">
        <div class="col-md-8">
            <h2><?=$this->lang->line('ui_auth_header_profile')?></h2>

            <?=$message?>
            <div>
                <strong>Nombre:</strong>
                <?= $user->first_name?>
            </div>

            <div>
                <strong>Apellido:</strong>
                <?= $user->last_name?>
            </div>
            <div>
                <strong>Email:</strong>
                <?= $user->email?>
            </div>

            <? foreach($userCampos as $campo): ?>
                <div>
                    <strong><?=$campo->userFieldLabel?></strong>
                    <?=$campo->userFieldRelContent?>
                </div>
            <? endforeach ?>
        </div>
        <div class="col-md-4">
            <h3>Controles</h3>
            <ul>
                <li><a href="<?= base_url($diminutivo . '/' . $pagina_url . '/edit') ?>">Editar perfil</a></li>
                <li><a href="<?= base_url($diminutivo . '/' . $pagina_url . '/password/edit') ?>">Cambiar contrase&ntilde;a</a></li>
                <? if(isset($pagPedidos)): ?>
                    <li><a href="<?= base_url($diminutivo . '/' . $pagPedidos->paginaNombreURL . '/orders') ?>">Sus pedidos</a></li>
                <? endif; ?>
                <li><a href="<?= base_url($diminutivo . '/' . $pagina_url . '/logout') ?>">Cerrar Sessi&oacute;n</a></li>
            </ul>
        </div>
    </div>

</div>