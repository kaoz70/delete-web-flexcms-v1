<? if(isset($message)) : ?>
    <div class="success callout" data-closable>
        <?= $message ?>
        <button class="close-button" aria-label="Dismiss alert" type="button" data-close>
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
<? endif; ?>

<? if(isset($error)) : ?>
    <div class="alert callout" data-closable>
        <?= $error ?>
        <button class="close-button" aria-label="Dismiss alert" type="button" data-close>
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
<? endif; ?>

<div class="text-right">
    <a class="button" href="<?= base_url("$diminutivo/$pagina_url/closed") ?>">Ver tickets cerrados</a>
    <a class="button" href="<?= base_url("$diminutivo/$pagina_url/new") ?>"><i class="fa fa-plus" aria-hidden="true"></i> Crear ticket de soporte</a>
</div>

<h3>Tickets abiertos <small><?= count($tickets)?></small></h3>

<div class="row">
    <div class="columns small-12 medium-12 large-7">
        <ul class="tickets">
            <? foreach ($tickets as $ticket): ?>
                <li>

                    <div class="row">
                        <div class="columns small-7 medium-5">
                            <a href="<?= base_url("$diminutivo/$pagina_url/ticket/{$ticket->uuid}") ?>">
                                <i class="fa fa-file-o" aria-hidden="true"></i>
                                Ticket #<span class="ticket_number"><?= $ticket->uuid ?></span>
                            </a>
                        </div>

                        <div class="columns small-4 hide-for-small-only">
                            <span class="date"><?= $ticket->created_at ?></span>
                        </div>

                        <div class="columns small-5 medium-3 text-right">

                            <span data-tooltip aria-haspopup="true"
                                  title="Prioridad: <?= lang('ticket_priority_' . $ticket->priority) ?>"
                                  class="<?= ticket_class($ticket->priority) ?> badge"><?= lang('ticket_priority_' . $ticket->priority)[0] ?></span>

                            <span data-tooltip aria-haspopup="true"
                                  title="Nivel: <?= lang('ticket_level_' . $ticket->level) ?>"
                                  class="<?= ticket_class($ticket->level) ?> badge"><?= lang('ticket_level_' . $ticket->level)[0] ?></span>

                            <span data-tooltip aria-haspopup="true"
                                  title="Nivel: <?= lang('ticket_type_' . $ticket->type) ?>"
                                  class="<?= ticket_class($ticket->type) ?> badge"><?= lang('ticket_type_' . $ticket->type)[0] ?></span>

                        </div>
                    </div>



                </li>
            <? endforeach; ?>
        </ul>
    </div>
</div>

