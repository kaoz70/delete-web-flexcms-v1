/**
 * Created by miguel on 7/20/15.
 */

var galleryOptions = {
        enabled: true, // set to true to enable gallery

        preload: [0, 2], // read about this option in next Lazy-loading section

        navigateByImgClick: true,

        arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>', // markup of an arrow button

        tPrev: 'Previous (Left arrow key)', // title for left button
        tNext: 'Next (Right arrow key)', // title for right button
        tCounter: '<span class="mfp-counter">%curr% of %total%</span>' // markup of counter
    },
    loadingPortfolio = false;

/**
 * Create the popups
 * @param gallery_options
 */
function portfolioGallery(gallery_options){

    $('.images').each(function() { // the containers for all your galleries
        /*$(this).magnificPopup({
         delegate: 'a', // child items selector, by clicking on it popup will open
         gallery: galleryOptions,
         type: 'image'
         });*/

        var images = [],
            small_images_src = $(this).find('.small a'),
            index = 0;

        //Large image src
        images.push({
            src: $(this).find('.large').attr('href')
        });

        //Small images src
        small_images_src.each(function () {
            images.push({
                src: $(this).attr('href')
            });
        });

        //Hidden images src
        $(this).find('.hidden').each(function () {
            images.push({
                src: $(this).attr('data-src')
            });
        });

        //Get the current image index so that the popup will show the correct image
        small_images_src.click(function () {
            index = small_images_src.index($(this)) + 1; // +1 because of the bigger image
        });

        $(this).magnificPopup({
            //delegate: 'a', // the selector for gallery item
            type: 'image',
            items: images,
            gallery: gallery_options,

            callbacks: {
                open: function() {
                    // Will fire when this exact popup is opened
                    // this - is Magnific Popup object
                    this.goTo(index);
                },
                close: function() {
                    // Will fire when popup is closed
                    index = 0;
                }
            }

        });

    });

}

/**
 * Checks if an element is visible in the browsers viewport
 * @param elem
 * @returns {boolean}
 */
function isElementInViewport(elem) {
    var $elem = $(elem);

    // Get the scroll position of the page.
    var scrollElem = ((navigator.userAgent.toLowerCase().indexOf('webkit') != -1) ? 'body' : 'html');
    var viewportTop = $(scrollElem).scrollTop();
    var viewportBottom = viewportTop + $(window).height();

    // Get the position of the element on the page.
    var elemTop = Math.round( $elem.offset().top );
    var elemBottom = elemTop + $elem.height();

    return ((elemTop < viewportBottom) && (elemBottom > viewportTop));
}

/**
 * Check if it's time to start the animation.
 */
function checkAnimation() {
    var $elem = $('.mod_breadcrumbs > div');

    // If the animation has already been started
    if ($elem.hasClass('start') || $elem.length === 0) return;

    if (isElementInViewport($elem)) {
        // Start the animation
        $elem.addClass('start');

        $elem.animate({
            scrollLeft: $elem.find('ol').width()
        }, {
            easing: 'easeOutCubic',
            duration: 2000
        });
    }
}

/**
 * Handles the window's resize event
 */
function resizeHandler() {

    $(".vertical .large img").each(function(){
        resizeThumbs($(this));
    });

}

/**
 * Resizes the thumbs so that the 3 of them add up to the same height of the big image
 * @param e
 */
function resizeThumbs(e) {
    var targetHeight = e.height() - 10;
    e.closest('.large').next().find('a').height(targetHeight / 3);
}

/**
 * Loads the feed from Blueink's facebook page
 */
function facebook_feed(popupOptions){

    $(window).scroll(function(){
        checkAnimation();

        /*
         Are we in portafolio page?
         Have scrolled down all the way?
         Are we already loading more data?
         */
        if($('.module.mod_facebook').hasClass('facebook_feed') && $(window).scrollTop() + $(window).height() > $(document).height() - 700 && !loadingPortfolio) {

            loadingPortfolio = true;

            var parent = $('[data-feed-id]');

            parent.append($('<div class="trabajo loading"><div class="loader"></div>Cargando m&aacute;s trabajos...</div>'));

            var d = new Date();

            $.get(system.base_url + 'ajax/module/facebook_feed/' + system.lang + '/' + $('[data-feed-id]').attr('data-feed-id') + '?' + d.getTime())
                .done(function (data) {
                    $('.trabajo.loading').remove();

                    //Add only the children since we already have the parent in the dom
                    parent.append($(data).children());

                    //Wait for the images to load so that we can get their height and be able to resize the thumbs
                    $(".vertical .large img").on("load",function(){
                        resizeThumbs($(this));
                    });

                    portfolioGallery(galleryOptions);

                    loadingPortfolio = false;
                })
                .fail(function () {
                    $('.trabajo.loading').remove();
                    loadingPortfolio = false;
                });

        }

    });

    $(window).resize(resizeHandler);
    resizeHandler();
    portfolioGallery($('.facebook.list'), popupOptions);

}