<div class="content">

    <? foreach ($servicios as $servicio): ?>
        <div class="bottom">
            <h4><?=$servicio->servicioTitulo?></h4>
            <div class="texto">
                <? $arr = explode('<hr />', $servicio->servicioTexto) ?>
                <?= $arr[0] ?>
            </div>
            <div class="servicios-index">
                <? foreach($servicio->imagenes as $img): ?>
                    <a href="<?= base_url('assets/public/images/servicios/gal_' . $servicio->servicioId . '_' . $img->id . '.' . $img->extension) ?>">
                        <img src="<?= base_url('assets/public/images/servicios/gal_' . $servicio->servicioId . '_' . $img->id . '_index.' . $img->extension) ?>">
                    </a>
                <? endforeach ?>
            </div>

            <a class="mas" href="<?=base_url($diminutivo.'/'.$servicio->pagina->paginaNombreURL.'/'.$servicio->servicioUrl)?>">Leer m&aacute;s</a>

        </div>
    <?php endforeach;?>

</div>


