<div class="content">

    <div class="swiper swiper-container">

        <div class="swiper-wrapper">
            <? foreach ($servicios as $servicio): ?>
                <div class="top swiper-slide">
                    <? if ($servicio->servicioImagen != ''): ?>
                        <img src="<?=base_url('assets/public/images/servicios/servicio_'.$servicio->servicioId.$imageSize.'.'.$servicio->servicioImagen)?>" />
                    <? endif ?>
                    <h4><?=$servicio->servicioTitulo?></h4>
                </div>
            <?php endforeach;?>
        </div>

    </div>

    <div id="servicios-prev" class="swiper-button-prev swiper-button-white"></div>
    <div id="servicios-next" class="swiper-button-next swiper-button-white"></div>

</div>


