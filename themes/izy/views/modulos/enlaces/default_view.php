<div class="content">
    <div class="enlaces-swiper swiper-container">

        <div class="swiper-wrapper">
            <?php foreach($enlaces as $key => $value): ?>
                <div class="swiper-slide">
                    <? if($value->enlaceImagen != ''): ?>
                        <img src="<?=base_url()?>assets/public/images/enlaces/enlace_<?=$value->enlaceId?><?=$imageSize?>.<?=$value->enlaceImagen?>">
                    <? endif?>
                </div>
            <?php endforeach;?>
        </div>

    </div>

    <div id="enlaces-prev" class="swiper-button-prev swiper-button-white"></div>
    <div id="enlaces-next" class="swiper-button-next swiper-button-white"></div>

</div>
