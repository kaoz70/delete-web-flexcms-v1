<?

if(!isset($next_url)) {
	$fb_url = "https://graph.facebook.com/v2.2/{$url}/feed?limit=3&fields=attachments,message,type&access_token={$appId}";
} else {
	$fb_url = $next_url;
}

if($data = file_get_contents($fb_url)):

	$feed = json_decode($data);
	//Store the url in session because it has sensible data (app_id, app_secret)
	$this->session->set_userdata([
		'next_url' => $feed->paging->next
	]);

	?>

	<div data-feed-id="177" class="facebook list">

		<? foreach ($feed->data as $key => $data): ?>
			<? if ($data->type === 'photo' && isset($data->message)): ?>
				<article class="trabajo">

					<span class="post-date">
						<i class="icon-calendar"></i>
						<?=strftime("%d de %B de %Y", strtotime($data->created_time))?>
					</span>

					<p><?=auto_link($data->message)?></p>
					<div class="separator-2"></div>

					<?

					//post has more images
					if(isset($data->attachments->data[0]->subattachments)){
						$first_image = $data->attachments->data[0]->subattachments->data[0];
						$single = FALSE;
					}
					//post has only 1 image
					else {
						$first_image = $data->attachments->data[0];
						$single = TRUE;
					}

					$vertical_height = $first_image->media->image->height;

					?>

					<div class="images <?= $single ? 'single' : '' ?> <?= $first_image->media->image->width > $first_image->media->image->height ? 'horizontal' : 'vertical' ?>">

						<a class="large <?= $key || isset($next_url) ? 'wow fadeInUp' : ''?>" data-wow-delay="300ms" href="<?=$first_image->media->image->src?>">
							<div class="image">
								<img src="<?=$first_image->media->image->src?>" width="<?=$first_image->media->image->width?>" height="<?=$first_image->media->image->height?>" >
							</div>
						</a>

						<? if(isset($data->attachments->data[0]->subattachments)):?>
							<div class="small">
								<? foreach($data->attachments->data[0]->subattachments->data as $key => $data):?>
									<? if($key > 0 && $key < 4): //limit to 4 small images ?>
										<a href="<?=$data->media->image->src?>" class="wow fadeInUp" data-wow-delay="<?= ($key * 200) ?>ms">
											<div class="image">
												<span style="background-image: url(<?=$data->media->image->src?>)"></span>
											</div>
										</a>
									<? else: ?>
										<span class="hidden" data-src="<?=$data->media->image->src?>"></span>
									<? endif ?>
								<? endforeach ?>
							</div>
						<? endif ?>

					</div>

				</article>
				<div class="separator" style="margin-top: 50px; margin-bottom: 50px;"></div>
			<? endif ?>
		<? endforeach ?>
	</div>

<? else: ?>
	<p>Hubo un problema al intentar mostrar el portafolio</p>
<? endif ?>
