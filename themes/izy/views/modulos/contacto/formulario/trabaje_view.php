<? //INFO: check http://formvalidation.io/ for this new validator docs ?>

<div class="contact-form">

    <?= form_open_multipart('submit/vacantes/' . $diminutivo, array(
        'id' => 'cv-form',
        'class' => 'form_contacto margin-clear',
        'role' => 'form',
        'data-fv-framework' => 'foundation',
        'data-fv-icon-valid' => 'glyphicon glyphicon-ok',
        'data-fv-icon-invalid' => 'glyphicon glyphicon-remove',
        'data-fv-icon-validating' => 'glyphicon glyphicon-refresh',
    )); ?>

    <h3>Formulario</h3>
    <p>Ayúdanos llenando el siguiente formulario y carga tu hoja de vida</p>

    <div class="row">
        <div class="column medium-6 small-12">

            <div class="row">
                <div class="column small-12">
                    <input required
                           placeholder="*Nombres"
                           name="first_name"
                           type="text"
                           data-fv-row=".small-12"
                           data-fv-notempty="true"
                           data-fv-notempty-message="<?=lang('required')?>"
                    />
                </div>
            </div>
            <div class="row">
                <div class="column small-12">
                    <input required
                           placeholder="*Apellidos"
                           name="last_name"
                           type="text"
                           data-fv-row=".small-12"
                           data-fv-notempty="true"
                           data-fv-notempty-message="<?=lang('required')?>"
                    />
                </div>
            </div>
            <div class="row">
                <div class="column small-12">
                    <input required
                           placeholder="*C&eacute;dula"
                           name="id"
                           type="number"
                           data-fv-row=".small-12"
                           data-fv-notempty="true"
                           data-fv-notempty-message="<?=lang('required')?>"
                    />
                </div>
            </div>
            <div class="row">
                <div class="column small-12">
                    <input required
                           placeholder="*Tel&eacute;fono"
                           name="phone"
                           type="number"
                           data-fv-row=".small-12"
                           data-fv-notempty="true"
                           data-fv-notempty-message="<?=lang('required')?>"
                    />
                </div>
            </div>
            <div class="row">
                <div class="column small-12">
                    <input required
                           placeholder="*Email"
                           name="email"
                           type="email"
                           data-fv-row=".small-12"
                           data-fv-notempty="true"
                           data-fv-notempty-message="<?=lang('required')?>"
                    />
                </div>
            </div>
            <div class="row">
                <div class="column small-12">
                    <input required
                           placeholder="*Cuidad"
                           name="city"
                           type="text"
                           data-fv-row=".small-12"
                           data-fv-notempty="true"
                           data-fv-notempty-message="<?=lang('required')?>"
                    />
                </div>
            </div>

            <div class="row">
                <div class="column small-12">
                    <select name="vacantes"
                            data-fv-row=".small-12"
                            data-fv-notempty="true"
                            data-fv-notempty-message="<?=lang('required')?>"
                    >
                        <option value="">*Vacante</option>
                        <?= page_articles(207, 'vacantes_view') ?>
                    </select>
                </div>
            </div>

            <div class="text-right">
                <input type="reset" value="<?=lang('ui_button_reset')?>" class="button">
                <input type="submit" value="<?=lang('ui_button_send')?>" class="alert button">
            </div>

        </div>
        <div class="column medium-6 small-12 fondo">

            <label class="upload-cv">
                <div class="alert button">Cargar hoja de vida</div>
                <input type="file"
                       name="userfile"
                       data-fv-row=".small-12"
                       data-fv-notempty="true"
                       data-fv-notempty-message="<?=lang('required')?>"
                >
            </label>

            <ul>
                <li>Tama&ntilde;o m&aacute;ximo para subir es de 9 MB.</li>
                <li>Solamente documentos (DOC, DOCX, PDF) est&aacute;n permitidos.</li>
            </ul>
        </div>
    </div>

    <input type="hidden" value="<?=$diminutivo?>" name="idioma" />

    <div class="reveal modal"
         data-reveal
         data-close-on-click="true"
         data-animation-in="slide-in-down"
         data-animation-out="slide-out-up" >

        <div class="ajax-response">
            <div class="message"><?= lang('ui_sending') ?></div>
            <div class="error no"></div>
        </div>

        <button class="close-button" data-close aria-label="Close reveal" type="button">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    
    <?= form_close() ?>

</div>
