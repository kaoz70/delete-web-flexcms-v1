<? //INFO: check http://formvalidation.io/ for this new validator docs ?>

<div class="contact-form">

    <?= form_open('submit/contacto/' . $diminutivo, [
        'id' => 'contact-form',
        'class' => 'form_contacto margin-clear',
        'role' => 'form',
        'data-fv-framework' => 'foundation',
        'data-fv-icon-valid' => 'glyphicon glyphicon-ok',
        'data-fv-icon-invalid' => 'glyphicon glyphicon-remove',
        'data-fv-icon-validating' => 'glyphicon glyphicon-refresh',
    ]); ?>

    <?php if(count($contactos) > 1) :?>
        <select name="contacto" class="select">
            <?php foreach($contactos as $row):?>
                <option id="contacto_<?=$row->contactoId?>" value="<?=$row->contactoId?>"><?=$row->contactoNombre?></option>
            <?php endforeach;?>
        </select>
    <?php endif;?>

    <div class="row">

        <?php foreach($campos as $key => $row):?>

            <div class="columns small-12 medium-6">

                <div class="row">
                    <div class="columns small-12">
                        <?php switch($row->inputTipoId): case 1: case 13: ?>
                            <input <?=$row->contactoCampoRequerido ? 'required' : ''?>

                                placeholder="<?=$row->contactoCampoPlaceholder?>"
                                data-fv-row=".small-12"
                                name="campos[<?=$row->contactoCampoId?>]"
                                id="campos_<?=$row->contactoCampoId?>"
                                type="<?= $row->contactoCampoValidacion ?: 'text' ?>"

                                <? if($row->contactoCampoRequerido): ?>
                                    data-fv-notempty="true"
                                    data-fv-notempty-message="<?=lang('required')?>"
                                <? endif ?>

                                <? if($row->contactoCampoValidacion === 'email'): ?>
                                    data-fv-emailaddress="true"
                                    data-fv-emailaddress-message="<?=lang($row->contactoCampoValidacion)?>"
                                <? endif ?>

                            />
                            <?php break; ?>
                        <?php case 3:?>
                            <textarea <?=$row->contactoCampoRequerido ? 'required' : ''?>

                                pattern="<?=$row->contactoCampoValidacion?>"
                                placeholder="<?=$row->contactoCampoPlaceholder?>"
                                rows="5"
                                cols="75"
                                data-fv-row=".small-12"
                                name="campos[<?=$row->contactoCampoId?>]"
                                id="campos_<?=$row->contactoCampoId?>"

                                <? if($row->contactoCampoRequerido): ?>
                                    data-fv-notempty="true"
                                    data-fv-notempty-message="<?=lang('required')?>"
                                <? endif ?>

                            ></textarea>
                            <?php break;?>
                        <?php case 8:?>
                            <select class="contacto_mapa form-control" name="campos[<?=$row->contactoCampoId?>]" id="campos_<?=$row->contactoCampoId?>">
                                <? foreach ($ubicaciones as $key => $ubicacion): ?>
                                    <option value="<?=$ubicacion->mapaUbicacionId?>"><?=$ubicacion->mapaUbicacionNombre?></option>
                                <? endforeach ?>
                            </select>
                            <?php break;?>
                        <?php endswitch;?>

                        <? if($key === count($campos) - 1): ?>
                            
                            <div class="row" style="margin-top: 33px;">
                                <div class="column">
                                    <input type="reset" value="<?=lang('ui_button_reset')?>" class="button full-width">
                                </div>
                                <div class="column">
                                    <input type="submit" name="submitButton" value="<?=lang('ui_button_send')?>" class="alert button full-width">
                                </div>
                            </div>

                        <? endif; ?>

                    </div>
                </div>

            </div>
        <?php endforeach;?>

    </div>

    <input type="hidden" value="<?=$diminutivo?>" name="idioma" />

    <div class="reveal modal"
         data-reveal
         data-close-on-click="true"
         data-animation-in="slide-in-down"
         data-animation-out="slide-out-up" >

        <div class="ajax-response">
            <div class="message"><?= lang('ui_sending') ?></div>
            <div class="error no"></div>
        </div>

        <button class="close-button" data-close aria-label="Close reveal" type="button">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>

    <?= form_close() ?>

</div>
