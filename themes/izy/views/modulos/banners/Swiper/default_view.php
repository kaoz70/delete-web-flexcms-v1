<div class="swiper-container banner_<?=$banner['bannerId']?> <?=$banner_class?>">
    <div class="swiper-wrapper">
        <? foreach ($images as $image): ?>
        <div class="swiper-slide">
            <img src="<?=base_url()?>assets/public/images/banners/banner_<?=$image['bannerId']?>_<?=$image['bannerImagesId']?>.<?=$image['bannerImageExtension']?>" alt="">
            <? if($image['bannerImageLink'] != ''): ?>
            <a href="<?=$image['bannerImageLink']?>">Click</a>
            <? endif ?>
            <div class="reflection"></div>
        </div>
        <? endforeach ?>
    </div>
</div>