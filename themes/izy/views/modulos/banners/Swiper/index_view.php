<div class="swiper-container banner_<?=$banner['bannerId']?> <?=$banner_class?>">
    <div class="swiper-wrapper">
        <? foreach ($images as $image): ?>

        <?php

        $hasText = false;

        foreach ($image['labels'] as $label) {
            if($label->bannerCamposTexto) {
                $hasText = true;
                break;
            }
        }

        ?>

        <div class="swiper-slide">

            <img src="<?=base_url()?>assets/public/images/banners/banner_<?=$image['bannerId']?>_<?=$image['bannerImagesId']?>.<?=$image['bannerImageExtension']?>" alt="">

            <? if($image['bannerImageLink'] != ''): ?>
                <a href="<?=$image['bannerImageLink']?>">
            <? endif ?>

                <? if($hasText): ?>
                    <div class="texto">
                        <? foreach ($image['labels'] as $label): ?>
                            <<?=$label->bannerCampoClase?>><?=$label->bannerCamposTexto?></<?=$label->bannerCampoClase?>>
                        <? endforeach ?>
                    </div>
                <? endif; ?>

            <? if($image['bannerImageLink'] != ''): ?>
                </a>
            <? endif ?>

        </div>
        <? endforeach ?>
    </div>

    <div class="swiper-button-prev swiper-button-red"></div>
    <div class="swiper-button-next swiper-button-red"></div>

</div>