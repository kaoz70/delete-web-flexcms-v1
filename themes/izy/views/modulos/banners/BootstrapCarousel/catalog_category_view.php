<header id="banner_<?=$banner['bannerId']?>" class="carousel slide zoom carousel-fade">

    <div class="carousel-inner" role="listbox">

        <? foreach ($images as $key => $image): ?>

            <div class="item<?= !$key ? ' active' : ''?>">

                <img
                    src="<?=base_url()?>assets/public/images/banners/banner_<?= $image['bannerId'] ?>_<?= $image['bannerImagesId'] ?>.<?= $image['bannerImageExtension'] ?>"
                    alt="banner-img">

                <div class="carousel-caption overlay">
                    <div class="content">
                        <div class="text wow bounceIn" data-wow-delay="0.2s">
                            <h1><?= $image['bannerImageName'] ?></h1>
                            <hr>
                        </div>
                    </div>
                </div>

            </div>

        <? endforeach ?>

    </div>

    <a class="left carousel-control" href="#banner_<?=$banner['bannerId']?>" role="button" data-slide="prev">
        <img src="themes/photography/images/06_left.png" alt="prev-img">
        <span class="sr-only">Anterior</span>
    </a>
    <a class="right carousel-control" href="#banner_<?=$banner['bannerId']?>" role="button" data-slide="next">
        <img src="themes/photography/images/05_right.png" alt="next-img">
        <span class="sr-only">Siguiente</span>
    </a>

</header>