<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="row masonry-grid-fitrows grid-space-10">

				<? foreach ($productos as $producto): ?>

					<div class="col-md-3 col-sm-6 masonry-grid-item">
						<div class="listing-item white-bg bordered mb-20">
							<div class="overlay-container">
								<img src="<?=base_url()?>assets/public/images/catalog/prod_<?=$producto->productoId?><?=$imageSize?>.<?=$producto->productoImagenExtension?>" alt="<?=$producto->productoNombre?>" />
								<a class="overlay-link popup-img-single" href="<?=base_url()?>assets/public/images/catalog/prod_<?=$producto->productoId?><?=$imageSize?>.<?=$producto->productoImagenExtension?>"><i class="fa fa-search-plus"></i></a>
								<div class="overlay-to-top links">
									<span class="small">
										<a href="<?=base_url()?><?=$diminutivo?>/<?=$paginaCatalogoUrl?>/<?=$producto->categoria->id?>/<?=$producto->categoria->productoCategoriaUrl?>/<?=$producto->productoUrl?>" class="btn-sm-link"><i class="icon-link pr-5"></i>Ver Detalles</a>
									</span>
								</div>
							</div>
							<div class="body">
								<h3><a href="<?=base_url()?><?=$diminutivo?>/<?=$paginaCatalogoUrl?>/<?=$producto->categoria->id?>/<?=$producto->categoria->productoCategoriaUrl?>/<?=$producto->productoUrl?>"><?=$producto->productoNombre?></a></h3>
								<p class="small">
									<? foreach($producto->campos as $campo): ?>
										<?= character_limiter(strip_tags($campo->contenido->productoCampoRelContenido),1000); ?>
									<? endforeach; ?>
								</p>
								<div class="elements-list clearfix">

									<? $precio_existe = FALSE; ?>

									<? foreach($producto->precios as $precio):?>
										<? if($precio->contenido): ?>
											<span class="price">$<?=number_format((double)$precio->contenido, 2, '.', '')?></span>
											<? $precio_existe = TRUE; ?>
										<? endif ?>
									<? endforeach; ?>

									<? if($producto->stock_quantity && $precio_existe): ?>
										<div class="pedido">
											<?= form_open('ajax/cart/add', array('class' => 'pedido')); ?>
												<? foreach ($producto->listado as $listado) : ?>
													<? if($listado->mostrar_en_pedido): ?>
														<input type="hidden" name="campo[<?=$listado->campo_id?>]" value="<?= array_key_exists(0, $listado->contenido) ? $listado->contenido[0]->productoCamposListadoPredefinidoTexto : ''?>">
													<? endif ?>
												<? endforeach ?>
												<input type="hidden" name="cantidad" value="1">
												<input type="hidden" name="productoId" value="<?=$producto->productoId?>">
												<input type="hidden" name="idioma" value="<?=$diminutivo?>">
												<a href="#" class="add pull-right margin-clear btn btn-sm btn-default-transparent btn-animated">A&ntilde;adir Al Carrito<i class="fa fa-shopping-cart"></i></a>
											<?= form_close() ?>
										</div>
									<? else: ?>
										<small>Fuera de Stock</small>
									<? endif ?>

								</div>
							</div>
						</div>
					</div>

				<? endforeach ?>

			</div>

		</div>
	</div>
</div>
