<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="<?=$diminutivo?>"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="<?=$diminutivo?>"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="<?=$diminutivo?>"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="<?=$diminutivo?>"> <!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title><?=$titulo?></title>
    <meta name="description" content="<?=$meta_description?>">
    <meta name="keywords" content="<?=$meta_keywords?>">
    <meta name="generator" content="FlexCMS">

    <? //Facebook OpenGraph meta tags ?>
    <meta property="og:title" content="<?=$og_title?>" />
    <meta property="og:site_name" content="<?=$nombre_sitio?>" />
    <meta property="og:url" content="<?=current_url()?>" />
    <meta property="og:image" content="<?=$og_image?>" />
    <meta property="og:description" content="<?=$og_description?>"/>

    <link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon.png?v=M4o0maam7j">
    <link rel="icon" type="image/png" href="/favicon-32x32.png?v=M4o0maam7j" sizes="32x32">
    <link rel="icon" type="image/png" href="/favicon-16x16.png?v=M4o0maam7j" sizes="16x16">
    <link rel="manifest" href="/manifest.json?v=M4o0maam7j">
    <link rel="mask-icon" href="/safari-pinned-tab.svg?v=M4o0maam7j" color="#5bbad5">
    <link rel="shortcut icon" href="/favicon.ico?v=M4o0maam7j">
    <meta name="theme-color" content="#ffffff">

    <!-- Mobile Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Web Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Lato:400,300,900,700&subset=latin-ext,latin' rel='stylesheet' type='text/css'>

    <link type="text/plain" rel="author" href="<?=base_url()?>humans.txt">

    <?php Assets::css_group('system', array_merge($assets_banner_css, array(
        'packages/formvalidation/dist/css/formValidation.min.css',
        'themes/' . $theme . '/packages/jQuery.TosRUs/dist/css/jquery.tosrus.all.css',
        'themes/' . $theme . '/packages/font-awesome/css/font-awesome.min.css',
        'themes/' . $theme . '/packages/foundation-sites/dist/foundation-flex.min.css',
        'themes/' . $theme . '/packages/motion-ui/dist/motion-ui.css',
        'themes/' . $theme . '/css/general.scss',
        'themes/' . $theme . '/css/pages.scss',
    )), $theme); ?>

    <script>
        var system = {
            base_url: '<?=base_url()?>',
            <?= !empty($pagPedidos) ? "pag_pedidos: '" . $pagPedidos->paginaNombreURL . "'," . PHP_EOL : ""?>
            <?= !empty($pagAutenticacion) ? "pag_autenticacion: '" . $pagAutenticacion->paginaNombreURL . "'," . PHP_EOL : ""?>
            <?= $popup_banner ? "popup_banner: { html:'" . $popup_banner->html . "',id: " . $popup_banner->id . "}," . PHP_EOL : ""?>
            lang: '<?=$diminutivo?>',
            fb_login: false
        }
    </script>

    <? if($this->uri->segment(2) === 'contacto'): ?>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDgKBf7uFgE0MXgeUQVu2Ipgy6JEmlg3yg"></script>
    <? endif; ?>

    <?php Assets::js_group('system', array_merge(array(
        'packages/foundation/js/vendor/jquery.js',
        'packages/jquery.easing/js/jquery.easing.min.js',
    ), $assets_banner_js), $theme); ?>

    <script src="<?= base_url('assets/slideshow_js') ?>"></script>
    <script src="<?= base_url('themes/' . $theme . '/js/servicios.js?' . time()) ?>"></script>

</head>

<? $this->benchmark->mark('body_html_start');?>
<body <?= $clase ?>>

<div class="off-canvas-wrapper">
    <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
        <div class="off-canvas position-left" id="offCanvas" data-off-canvas>

            <!-- Close button -->
            <button class="close-button" aria-label="Close menu" type="button" data-close>
                <span aria-hidden="true">&times;</span>
            </button>

            <!-- Menu -->
            <? render_menu('main-menu', $menu['tree'], $menu, [
                'id' => 'mobile-menu',
                'class' => 'vertical menu',
            ]) ?>

        </div>

        <div class="off-canvas-content" data-off-canvas-content>
            <header class="show-for-large">

                <div class="row">
                    <div class="column medium-2">
                        <a href="<?=base_url()?>">
                            <img id="logo" src="<?= base_url('themes/' . $theme . '/images/logo.png') ?>" alt="<?=$nombre_sitio?>">
                        </a>
                    </div>
                    <div class="column medium-10">

                        <!-- main-menu -->
                        <? render_menu('main-menu', $menu['tree'], $menu, [
                            'id' => 'main-menu',
                            'class' => 'dropdown menu',
                            'data-dropdown-menu' => '',
                        ]) ?>
                        <!-- main-menu end -->

                    </div>
                </div>

            </header>

            <div class="title-bar hide-for-large">
                <div class="title-bar-left">
                    <button class="menu-icon" type="button" data-open="offCanvas"></button>
                    <span class="title-bar-title">MENU</span>
                </div>
            </div>

            <div class="show-for-small hide-for-large text-center">
                <a href="<?=base_url()?>">
                    <img style="margin: 20px" src="<?= base_url('themes/' . $theme . '/images/logo.png') ?>" alt="<?=$nombre_sitio?>">
                </a>
            </div>

            <section id="main_content">
                <?= render_content() ?>
            </section>

            <footer>
                <div class="row">
                    <div class="column">
                        <?= article(24, 'footer_view') ?>
                    </div>
                </div>
            </footer>
        </div>
    </div>
</div>

<? $this->benchmark->mark('body_html_end'); ?>

<?php Assets::js_group('footer', array(
    'themes/' . $theme . '/packages/foundation-sites/dist/foundation.min.js',
    'themes/' . $theme . '/packages/hammerjs/hammer.min.js',
    'themes/' . $theme . '/packages/jQuery.TosRUs/dist/js/jquery.tosrus.all.min.js',
    'packages/formvalidation/dist/js/formValidation.min.js',
    'packages/formvalidation/dist/js/framework/foundation.min.js',
    'packages/formvalidation/dist/js/language/' . ($diminutivo === 'es' ? 'es_ES' : 'en_US') . '.js',
    'packages/flexcms/search.js',
    'packages/flexcms/cart.js',
    'packages/flexcms/system.js',
    'packages/flexcms/youtube.js',
    'themes/' . $theme . '/js/custom.js',
), $theme); ?>

<!-- Page rendered in {elapsed_time} seconds -->
</body>
</html>
