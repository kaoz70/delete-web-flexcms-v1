<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<h2 class="text-center">Nuestras <strong>Categor&iacute;as</strong></h2>
			<div class="separator"></div>
		</div>
	</div>
</div>

<div class="pl-10 pr-10">

	<div class="row">

		<div class="col-sm-offset-2">
			<? foreach ($children as $childNode): ?>

				<? if($childNode->temporal !== 1) : ?>

					<div class="col-sm-3">

						<div class="overlay-container bordered overlay-visible">
							<img src="<?= base_url('assets/public/images/catalog/cat_' . $childNode->id . '.' . $childNode->categoriaImagen) ?>" alt="">
							<a class="overlay-link" href="<?= base_url($lang . '/' . $page . '/' . $childNode->id . '/' . $childNode->productoCategoriaUrl) ?>"><i class="fa fa-plus"></i></a>
							<div class="overlay-bottom">
								<div class="text">
									<h3 class="title"><?= $childNode->productoCategoriaNombre ?></h3>
									<div class="separator light"></div>
									<p class="small margin-clear"><em><?= $childNode->productoCategoriaTexto ?></em></p>
								</div>
							</div>
						</div>

					</div>

				<? endif ?>

			<? endforeach ?>
		</div>
	</div>

</div>