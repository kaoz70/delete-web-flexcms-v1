<ul<?=$attrs?>>
<? foreach ($servicios as $servicio) : ?>

	<li class="<?= (in_array($servicio->servicioId, $path) ? 'active' : '') ?>" data-id="<?= $servicio->servicioId ?>">
		<a href="<?= base_url($diminutivo . '/' . $page . '/' . $servicio->servicioUrl) ?>"><?= $servicio->servicioTitulo ?></a>
	</li>

<? endforeach ?>
</ul>