<ul<?=$attrs?>>

    <? foreach ($children as $childNode): ?>

        <? if($childNode->temporal !== 1 AND $childNode->paginaEnabled) : ?>

            <li class="<?= (in_array($childNode->id, $menu['path']) ? 'active' : '')?>" data-id="<?= $childNode->id ?>">

                <a href="<?= base_url($lang . '/' . $childNode->paginaNombreURL) ?>"><span><?= $childNode->paginaNombreMenu ?></span></a>

                <? //Show the catalog menu
                if (array_key_exists('page', $menu['catalog']) && $menu['catalog']['page']->paginaNombreURL === $childNode->paginaNombreURL) {
                    render_generic_menu($menu['catalog']['tree'], $menu['catalog']['path'], $menu['catalog']['page']->paginaNombreURL, FALSE, 'catalog_view', ['class' => 'category menu']);
                } ?>

                <? //Show the gallery menu
                if (array_key_exists('page', $menu['gallery']) && $menu['gallery']['page']->paginaNombreURL === $childNode->paginaNombreURL) {
                    render_generic_menu($menu['gallery']['tree'], $menu['gallery']['path'], $menu['gallery']['page']->paginaNombreURL, FALSE, 'gallery_view',['class' => 'category menu']);
                } ?>

                <? //Show the services menu
                foreach ($menu['servicios'] as $servicio) {
                    if ($servicio->paginaNombreURL === $childNode->paginaNombreURL && (bool)$servicio->moduloParam2) {
                        echo services_menu($servicio->id, $servicio->paginaNombreURL);
                    }
                }
                ?>

                <? //If the page has children call this function again
                if ($childNode->getChildrenCount()) {
                    $attrs_array['class'] = array_key_exists('class', $attrs_array) ? $attrs_array['class'] : 'menu';
                    render_menu_uncached($childNode, $menu, ['class' => 'menu'], $view);
                } ?>

            </li>

        <? endif ?>

    <? endforeach ?>

</ul>