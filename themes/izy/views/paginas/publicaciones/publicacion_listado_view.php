<div class="main_content <?=$moduleClass?>">
    <div>
        <div class="noticias">
            <?php foreach($html as $noticia): ?>
            <article class="list <?=$noticia->publicacionClase?>">

                <?
                $tituloArr = explode('|', $noticia->publicacionNombre);
                ?>

                <h2><a href="<?= base_url() . $diminutivo . '/' . $pagina_url . '/' . $noticia->publicacionUrl ?>"><?=$tituloArr[0]?></a></h2>
                <? if(count($tituloArr) > 1): ?>
                    <h3><?=$tituloArr[1]?></h3>
                <? endif; ?>

                <? if($noticia->publicacionImagen): ?>
                    <img src="<?=base_url()?>assets/public/images/noticias/noticia_<?=$noticia->publicacionId?>.<?=$noticia->publicacionImagen?>"
                         alt="<?=$noticia->publicacionNombre?>" />
                <? endif; ?>

                <div class="texto"><?=character_limiter(strip_tags($noticia->publicacionTexto),350)?></div>
                <div class="text-right">
                    <a class="mas" href="<?= base_url() . $diminutivo . '/' . $pagina_url . '/' . $noticia->publicacionUrl ?>"><span>Leer m&aacute;s</span></a>
                </div>

            </article>
            <?php endforeach;?>
            <?=$pagination?>
        </div>
    </div>
</div>