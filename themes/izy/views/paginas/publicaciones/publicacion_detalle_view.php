<?
$tituloArr = explode('|', $html->publicacionNombre);
?>

<article class="content">

    <h2><?=$tituloArr[0]?></h2>
    <? if(count($tituloArr) > 1): ?>
        <h3><?=$tituloArr[1]?></h3>
    <? endif; ?>

    <? if($html->publicacionImagen != ''): ?>
        <img src="<?=base_url()?>assets/public/images/noticias/noticia_<?=$html->publicacionId;?>.<?=$html->publicacionImagen;?>"
             alt="<?=$html->publicacionNombre;?>" />
    <? endif ?>

    <?=$html->publicacionTexto?>

</article>

<a class="button" href="<?= base_url("{$diminutivo}/{$pagina_url}") ?>">Regresar</a>