<div class="main_content container">
	<h2>Cambiar contrase&ntilde;a</h2>

    <?= form_open($update_link, array('data-abide' => '', 'class' => 'custom')); ?>
        <?=$message?>

        <div class="input <?= form_error('password') == '' ? '' : 'error' ?>">
            <label for="old_password">Ingrese su contrase&ntilde;a actual</label>
            <input id="old_password" type="password" name="old_password" value="" autocomplete="off" />
            <? if(form_error('old_password')): ?>
                <?= form_error('old_password'); ?>
            <? else: ?>
                <small class="error"><?=$this->lang->line('required')?></small>
                <small class="error"><?=$this->lang->line('password')?></small>
            <? endif ?>
        </div>

    <hr>

        <div class="input <?= form_error('password') == '' ? '' : 'error' ?>">
            <label for="password">Nueva <?=$this->lang->line('ui_auth_password')?></label>
            <input id="password" type="password" name="password" value="" autocomplete="off" />
            <? if(form_error('password')): ?>
                <?= form_error('password'); ?>
            <? else: ?>
                <small class="error"><?=$this->lang->line('required')?></small>
                <small class="error"><?=$this->lang->line('password')?></small>
            <? endif ?>
        </div>

        <div class="input <?= form_error('password_confirm') == '' ? '' : 'error' ?>">
            <label for="password_confirm"><?=$this->lang->line('ui_auth_password_again')?> nueva</label>
            <input id="password_confirm" type="password" name="password_confirm" value="" autocomplete="off" />
            <? if(form_error('password_confirm')): ?>
                <?= form_error('password_confirm'); ?>
            <? else: ?>
                <small class="error"><?=$this->lang->line('required')?></small>
                <small class="error"><?=$this->lang->line('password')?></small>
            <? endif ?>
        </div>

		<input type="hidden" name="redirect" value="<?=current_url()?>" />
		<input class="button small" type="submit" name="submit" value="<?=$this->lang->line('ui_button_modify')?>" />
		
	<?= form_close() ?>

</div>