<div class="main_content">
    <p>Error: <?= $error ?></p>
    <p>C&oacute;digo: <?= $error_code ?></p>
    <? if (isset($error_description)): ?>
        <p>Descripci&oacute;n: <?= $error_description ?></p>
    <? endif; ?>
    <? if (isset($error_reason)): ?>
        <p>Motivo: <?= $error_reason ?></p>
    <? endif; ?>
    <a href="<?= $returnUrl ?>" class="button">Regresar</a>
</div>