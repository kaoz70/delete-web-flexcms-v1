<style type="text/css">
    h1 {
        display: none;
    }
</style>

<article data-bannerIndex="<?= $servicio->servicioPosicion - 1 ?>" class="main_content <?=$servicio->servicioClase?>">
    <h3><?=$servicio->servicioTitulo?></h3>
    <div class="texto"><?=$servicio->servicioTexto?></div>

    <div class="servicios-swiper swiper-container">
        <div class="swiper-wrapper">
            <? foreach($servicio->imagenes as $img): ?>
                <div class="swiper-slide">
                    <a href="<?= base_url('assets/public/images/servicios/gal_' . $servicio->servicioId . '_' . $img->id . '.' . $img->extension) ?>">
                        <img src="<?= base_url('assets/public/images/servicios/gal_' . $servicio->servicioId . '_' . $img->id . '_thumb.' . $img->extension) ?>">
                    </a>
                </div>
            <? endforeach ?>
        </div>

        <div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div>

    </div>

</article>