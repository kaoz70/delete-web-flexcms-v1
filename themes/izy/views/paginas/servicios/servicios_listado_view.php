<div class="main_content <?=$moduleClass?>">
    <? foreach ($servicios as $servicio): ?>
        <div class="list <?=$servicio->servicioClase?>">
            <h2><?=$servicio->servicioTitulo?></h2>
            <div class="texto">
                <? $arr = explode('<hr />', $servicio->servicioTexto) ?>
                <?= $arr[0] ?>
            </div>

            <div class="text-right">
                <a class="mas" href="<?=base_url($diminutivo.'/'.$paginaServiciosUrl.'/'.$servicio->servicioUrl)?>"><span>Leer m&aacute;s</span></a>
            </div>

        </div>
    <? endforeach ?>
</div>