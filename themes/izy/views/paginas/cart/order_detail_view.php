<div>
    <strong>N&uacute;mero de orden: </strong>
    <?= $summary_data[$this->flexi_cart->db_column('order_summary', 'order_number')]; ?>
</div>
<div>
    <strong>Fecha: </strong>
    <?= date(
        'd \d\e M Y',
        strtotime($summary_data[$this->flexi_cart->db_column('order_summary', 'date')])
    ); ?>
</div>

<? if (!empty($summary_data['ord_ship_comments'])): ?>
    <div>
        <strong>Comentarios: </strong>
        <?php echo $summary_data['ord_ship_comments']; ?>
    </div>
<? endif; ?>

<hr>

<table>
    <thead>
    <tr>
        <th>Nombre del producto</th>
        <th>Personalizado</th>
        <th>Color</th>
        <th>Talla</th>
        <th>Precio</th>
        <th>Cantidad pedida</th>
        <th>Total</th>
    </tr>
    </thead>
    <tbody>
    <?php
    if (!empty($item_data)):
        foreach ($item_data as $row): ?>

            <?
            $options = json_decode($row[$this->flexi_cart->db_column('order_details', 'item_options')]);
            $order_detail_id = $row[$this->flexi_cart->db_column('order_details', 'id')];
            ?>

            <tr>
                <td class="text-left">

                    <? //Item Name ?>
                    <?= $row[$this->flexi_cart->db_column('order_details', 'item_name')]; ?>

                    <? //Display an item status message if it exists ?>
                    <? if(!empty($row[$this->flexi_cart->db_column('order_details', 'item_status_message')])): ?>
                        <?= $row[$this->flexi_cart->db_column('order_details','item_status_message')] ?>
                    <? endif; ?>

                </td>

                <td class="text-left">
                    <? if($options->text): ?>
                        <div>Texto: <?= $options->text ?></div>
                        <div>Posicion: <?= $options->position ?></div>
                    <? endif; ?>
                </td>


                <td>
                    <?= $options->Colores ?>
                </td>

                <td>
                    <?= $options->Tallas ?>
                </td>

                <td>
                    <?php
                    // If an item discount exists.
                    if ($row[$this->flexi_cart->db_column('order_details', 'item_discount_quantity')] > 0) {
                        // If the quantity of non discounted items is zero, strike out the standard price.
                        if ($row[$this->flexi_cart->db_column(
                                'order_details',
                                'item_non_discount_quantity'
                            )] == 0
                        ) {
                            echo '<span class="strike">'.$this->flexi_cart->format_currency(
                                    $row[$this->flexi_cart->db_column('order_details', 'item_price')],
                                    true,
                                    2,
                                    true
                                ).'</span><br/>';
                        } // Else, display the quantity of items that are at the standard price.
                        else {
                            echo number_format(
                                    $row[$this->flexi_cart->db_column(
                                        'order_details',
                                        'item_non_discount_quantity'
                                    )]
                                ).' @ '.
                                $this->flexi_cart->format_currency(
                                    $row[$this->flexi_cart->db_column('order_details', 'item_price')],
                                    true,
                                    2,
                                    true
                                ).'<br/>';
                        }

                        // If there are discounted items, display the quantity of items that are at the discount price.
                        if ($row[$this->flexi_cart->db_column('order_details', 'item_discount_quantity')] > 0) {
                            echo number_format(
                                    $row[$this->flexi_cart->db_column('order_details', 'item_discount_quantity')]
                                ).' @ '.
                                $this->flexi_cart->format_currency(
                                    $row[$this->flexi_cart->db_column('order_details', 'item_discount_price')],
                                    true,
                                    2,
                                    true
                                );
                        }
                    } // Else, display price as normal.
                    else {
                        echo $this->flexi_cart->format_currency(
                            $row[$this->flexi_cart->db_column('order_details', 'item_price')],
                            true,
                            2,
                            true
                        );
                    }
                    ?>
                </td>
                <td>
                    <?php echo round($row[$this->flexi_cart->db_column('order_details', 'item_quantity')], 2); ?>
                </td>

                <td class="text-right">
                    <?php
                    // If an item discount exists, strike out the standard item total and display the discounted item total.
                    if ($row[$this->flexi_cart->db_column('order_details', 'item_discount_quantity')] > 0) {
                        echo '<span class="strike">'.$this->flexi_cart->format_currency(
                                $row[$this->flexi_cart->db_column('order_details', 'item_price_total')],
                                true,
                                2,
                                true
                            ).'</span><br/>';
                        echo $this->flexi_cart->format_currency(
                            $row[$this->flexi_cart->db_column('order_details', 'item_discount_price_total')],
                            true,
                            2,
                            true
                        );
                    } // Else, display item total as normal.
                    else {
                        echo $this->flexi_cart->format_currency(
                            $row[$this->flexi_cart->db_column('order_details', 'item_price_total')],
                            true,
                            2,
                            true
                        );
                    }
                    ?>
                </td>
            </tr>

            <? // If an item discount exists.
            if (!empty($row[$this->flexi_cart->db_column('order_details', 'item_discount_description')])): ?>
                <tr class="discount">
                    <td colspan="6">
                        Discount: <?php echo $row[$this->flexi_cart->db_column(
                            'order_details',
                            'item_discount_description'
                        )]; ?>
                    </td>
                </tr>
            <? endif ?>

        <? endforeach ?>
    <? else: ?>
        <tr>
            <td colspan="7" class="empty">
                <h4>No hay items asociados a esta orden!</h4>
            </td>
        </tr>
    <? endif; ?>
    </tbody>
    <tfoot>
    <?php if ($summary_data[$this->flexi_cart->db_column('order_summary', 'item_summary_savings_total')] > 0) { ?>
        <tr class="discount">
            <th colspan="6">Item Summary Discount Total</th>
            <td class="align_ctr">
                <?php echo $this->flexi_cart->format_currency(
                    $summary_data[$this->flexi_cart->db_column('order_summary', 'item_summary_savings_total')],
                    true,
                    2,
                    true
                ); ?></td>
        </tr>
    <?php } ?>
    <tr>
        <th colspan="6">Subtotal</th>
        <td class="text-right"><?= $this->flexi_cart->format_currency(
                $summary_data[$this->flexi_cart->db_column('order_summary', 'item_summary_total')],
                true,
                2,
                true
            ); ?></td>
    </tr>
    <tr>
        <td colspan="6">Envio: <?php echo $summary_data[$this->flexi_cart->db_column(
                'order_summary',
                'shipping_name'
            )]; ?></td>
        <td class="text-right"><?php echo $this->flexi_cart->format_currency(
                $summary_data[$this->flexi_cart->db_column('order_summary', 'shipping_total')],
                true,
                2,
                true
            ); ?></td>
    </tr>

    <!-- Display discounts -->
    <?php if ($summary_data[$this->flexi_cart->db_column('order_summary', 'savings_total')] > 0) { ?>
        <tr class="discount">
            <th>Discount Summary</th>
            <td>&nbsp;</td>
        </tr>

        <!-- Item discounts -->
        <?php if ($summary_data[$this->flexi_cart->db_column(
                'order_summary',
                'item_summary_savings_total'
            )] > 0
        ) { ?>
            <tr class="discount">
                <td>
                                    <span class="pad_l_20">
                                        Item discount savings : <?php echo $this->flexi_cart->format_currency(
                                            $summary_data[$this->flexi_cart->db_column(
                                                'order_summary',
                                                'item_summary_savings_total'
                                            )],
                                            true,
                                            2,
                                            true
                                        ); ?>
                                    </span>
                </td>
                <td>&nbsp;</td>
            </tr>
        <?php } ?>

        <!-- Summary discounts -->
        <?php if ($summary_data[$this->flexi_cart->db_column('order_summary', 'summary_savings_total')] > 0) { ?>
            <tr class="discount">
                <td class="pad_l_20">
                    <?php echo $summary_data[$this->flexi_cart->db_column(
                        'order_summary',
                        'summary_discount_description'
                    )]; ?>
                </td>
                <td>&nbsp;</td>
            </tr>
        <?php } ?>

        <!-- Total of all discounts -->
        <tr class="discount">
            <td>Discount Savings Total</td>
            <td><?php echo $this->flexi_cart->format_currency(
                    $summary_data[$this->flexi_cart->db_column('order_summary', 'savings_total')],
                    true,
                    2,
                    true
                ); ?></td>
        </tr>
    <?php } ?>

    <!-- Display summary of all surcharges -->
    <?php if ($summary_data[$this->flexi_cart->db_column('order_summary', 'surcharge_total')] > 0): ?>
        <tr class="surcharge">
            <th>Surcharge Summary</th>
            <td>&nbsp;</td>
        </tr>
        <tr class="surcharge">
            <td class="pad_l_20">
                <?php echo $summary_data[$this->flexi_cart->db_column(
                    'order_summary',
                    'surcharge_description'
                )]; ?>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr class="surcharge">
            <td>Surcharge Total</td>
            <td><?php echo $this->flexi_cart->format_currency(
                    $summary_data[$this->flexi_cart->db_column('order_summary', 'surcharge_total')],
                    true,
                    2,
                    true
                ); ?></td>
        </tr>
    <? endif; ?>

    <!-- Display summary of all reward vouchers -->
    <? if($summary_data[$this->flexi_cart->db_column('order_summary', 'reward_voucher_total')] > 0): ?>
        <tr class="voucher">
            <th>Reward Voucher Summary</th>
            <td>&nbsp;</td>
        </tr>
        <tr class="voucher">
            <td class="pad_l_20">
                <?php echo $summary_data[$this->flexi_cart->db_column(
                    'order_summary',
                    'reward_voucher_description'
                )]; ?>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr class="voucher">
            <td>Reward Voucher Total</td>
            <td><?php echo $this->flexi_cart->format_currency(
                    $summary_data[$this->flexi_cart->db_column('order_summary', 'reward_voucher_total')],
                    true,
                    2,
                    true
                ); ?></td>
        </tr>
    <? endif ?>

    <!-- Display refund summary -->
    <?php if ($refund_data[$this->flexi_cart->db_column('order_details', 'item_price')] > 0) { ?>
        <tr class="refund">
            <td>
                Refund Cancelled Items
                <small>
                    This value is an <em class="uline">estimate</em> of the orders total refund value,
                    however, it does not include any percentage based surcharges or discounts that may have
                    been applied to the orders summary values. The grand total below does not include this
                    refund.
                </small>
            </td>
            <td>
                <?php
                if ($refund_data[$this->flexi_cart->db_column('order_details', 'item_discount_price')] > 0) {
                    echo $this->flexi_cart->format_currency(
                        $refund_data[$this->flexi_cart->db_column('order_details', 'item_discount_price')],
                        true,
                        2,
                        true
                    );
                } else {
                    echo $this->flexi_cart->format_currency(
                        $refund_data[$this->flexi_cart->db_column('order_details', 'item_price')],
                        true,
                        2,
                        true
                    );
                }
                ?>
            </td>
        </tr>
    <?php } ?>
    <tr>
        <th colspan="6">
            IVA al <?= $summary_data[$this->flexi_cart->db_column('order_summary', 'tax_rate')]; ?>%
        </th>
        <td class="text-right"><?php echo $this->flexi_cart->format_currency(
                $summary_data[$this->flexi_cart->db_column('order_summary', 'tax_total')],
                true,
                2,
                true
            ); ?></td>
    </tr>
    <tr class="grand_total">
        <th colspan="6">Total</th>
        <td class="text-right"><?php echo $this->flexi_cart->format_currency(
                $summary_data[$this->flexi_cart->db_column('order_summary', 'total')],
                true,
                2,
                true
            ); ?></td>
    </tr>
    </tfoot>
</table>

<div class="row">
    <div class="column medium-6 small-12">
        <h2>Detalles para la Facturaci&oacute;n</h2>
        <div>
            <strong>Nombre: </strong>
            <?php echo $summary_data['ord_bill_first_name']; ?>
        </div>
        <div>
            <strong>Apellido: </strong>
            <?php echo $summary_data['ord_bill_last_name']; ?>
        </div>
        <div>
            <strong>Direcci&oacute;n 01: </strong>
            <?php echo $summary_data['ord_bill_address_01']; ?>
        </div>
        <div>
            <strong>Direcci&oacute;n 02: </strong>
            <?php echo $summary_data['ord_bill_address_02']; ?>
        </div>
        <div>
            <strong>Cuidad / Pueblo: </strong>
            <?php echo $summary_data['ord_bill_city']; ?>
        </div>
    </div>
    <div class="column medium-6 small-12">
        <h2>Detalles para el Envio</h2>
        <div>
            <strong>Direcci&oacute;n 01: </strong>
            <?php echo $summary_data['ord_ship_address_01']; ?>
        </div>
        <div><strong>Direcci&oacute;n 02: </strong>
            <?php echo $summary_data['ord_ship_address_02']; ?>
        </div>
        <div><strong>Cuidad / Pueblo: </strong>
            <?php echo $summary_data['ord_ship_city']; ?>
        </div>
        <div><strong>C&oacute;digo postal: </strong>
            <?php echo $summary_data['ord_ship_post_code']; ?>
        </div>
    </div>
</div>

<hr>

<h2>Detalles de contacto</h2>
<div>
    <strong>Email: </strong>
    <?php echo $summary_data['ord_email']; ?>
</div>
<div>
    <strong>Tel&eacute;fono: </strong>
    <?php echo $summary_data['ord_bill_phone']; ?>
</div>

<hr>

<h2>Detalle del pago</h2>
<div>
    <strong>Moneda: </strong>
    <?php echo $summary_data[$this->flexi_cart->db_column(
        'order_summary',
        'currency_name'
    )]; ?>
</div>
<div>
    <strong>M&eacute;todo de pago: </strong>
    <?= urldecode($summary_data['ord_payment_method']); ?>
</div>