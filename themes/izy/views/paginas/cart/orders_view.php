<table>

    <thead>
    <tr>
        <th>Pedido</th>
        <th>Fecha</th>
        <th>Total</th>
        <td></td>
    </tr>

    </thead>
    <tbody>
    <? foreach ($orders as $order): ?>
        <tr>
            <td><a href="<?= $base_url ?>/orders/<?= $order['ord_order_number'] ?>"><?= $order['ord_order_number'] ?></a></td>
            <td><?= $order['ord_date'] ?></td>
            <td>$<?= $order['ord_total'] ?></td>
            <td><a class="button" href="<?= $base_url ?>/orders/<?= $order['ord_order_number'] ?>">Ver</a></td>
        </tr>
    <? endforeach; ?>
    </tbody>

</table>

<div class="text-center">
    <a href="<?= base_url($diminutivo . '/' . $pagAutenticacion->paginaNombreURL) ?>" class="button"/>Volver</a>
</div>