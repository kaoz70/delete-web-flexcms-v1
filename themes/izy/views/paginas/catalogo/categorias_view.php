<?php foreach($categorias as $row):?>

	<div class="listing-item mb-20">
		<div class="row grid-space-0">
			<div class="col-sm-6 col-md-4 col-lg-3">
				<div class="overlay-container">
					<img src="<?=base_url()?>assets/public/images/catalog/cat_<?=$row['id']?>.<?=$row['categoriaImagen']?>" alt="<?=$row['productoCategoriaNombre'];?>" />
					<a class="overlay-link" href="<?=base_url() . $diminutivo . '/' . $pagina_url . '/' . $row['id'] . '/' . $row['productoCategoriaUrl'] ?>"><i class="fa fa-plus"></i></a>
				</div>
			</div>
			<div class="col-sm-6 col-md-8 col-lg-9">
				<div class="body">
					<h3 class="margin-clear"><a href="<?=base_url() . $diminutivo . '/' . $pagina_url . '/' . $row['id'] . '/' . $row['productoCategoriaUrl'] ?>"><?=$row['productoCategoriaNombre']?></a></h3>
					<a class="ver-productos mb-20" href="<?=base_url() . $diminutivo . '/' . $pagina_url . '/' . $row['id'] . '/' . $row['productoCategoriaUrl'] ?>"><i class="fa fa-link"></i> Ver Productos</a>
					<?=$row['productoCategoriaDescripcion']?>
				</div>
			</div>
		</div>
	</div>

<?php endforeach; ?>

<?= $pagination ?>
