<? if(count($products) > 0): ?>
    <div class="row">

        <? foreach ($products as $producto): ?>

            <div class="column medium-<?= $this->input->get('span') ?: 6 ?> small-12 product">

                <div class="row">
                    <div class="column">
                        <a href="<?=base_url()?><?=$diminutivo?>/<?=$paginaCatalogoUrl?>/<?=$producto->categoria->id?>/<?=$producto->categoria->productoCategoriaUrl?>/<?=$producto->productoUrl?>">
                            <img src="<?=base_url()?>assets/public/images/catalog/prod_<?=$producto->productoId?>_list.<?=$producto->productoImagenExtension?>" alt="<?=$producto->productoNombre?>" />
                        </a>
                        <h2><a href="<?=base_url()?><?=$diminutivo?>/<?=$paginaCatalogoUrl?>/<?=$producto->categoria->id?>/<?=$producto->categoria->productoCategoriaUrl?>/<?=$producto->productoUrl?>"><?=$producto->productoNombre?></a></h2>
                        <? foreach($producto->precios as $precio):?>
                            <? if($precio->contenido): ?>
                                <span class="price">$<?=number_format((double)$precio->contenido, 2, '.', '')?></span>
                            <? endif ?>
                        <? endforeach; ?>
                    </div>

                </div>

                <div class="row tools">
                    <div class="column medium-7 small-12">
                        <? if(isset($producto->precios[0]) && $producto->precios[0]->contenido): ?>
                            <div class="pedido">
                                <?= form_open('ajax/cart/add', array('class' => 'pedido', 'id' => 'pedido', 'data-abide' => '')); ?>

                                <?
                                $listado = $producto->listado_predefinido[0];
                                $colorSelected = isset($listado->contenido[0]) ? $listado->contenido[0]->productoCamposListadoPredefinidoTexto : '';
                                ?>

                                <input required type="hidden"
                                       name="campo[<?=$listado->campo_id?>]"
                                       id="campo_<?=$listado->campo_id?>"
                                       value="<?=$colorSelected?>">

                                <?
                                $listado = $producto->listado_predefinido[1];
                                $sizeSelected = isset($listado->contenido[1]) ? $listado->contenido[0]->productoCamposListadoPredefinidoTexto : '';
                                ?>

                                <input required type="hidden"
                                       name="campo[<?=$listado->campo_id?>]"
                                       id="campo_<?=$listado->campo_id?>"
                                       value="<?=$sizeSelected?>">

                                <input type="hidden"
                                       name="campo[text]"
                                       value="">

                                <input type="hidden" name="cantidad" value="1">
                                <input type="hidden" name="productoId" value="<?=$producto->productoId?>">
                                <input type="hidden" name="idioma" value="<?=$diminutivo?>">
                                <a href="#" class="add"><span>&#43;</span> A&ntilde;adir Al Carrito</a>
                                <?= form_close() ?>
                            </div>
                        <? endif; ?>
                    </div>
                    <div class="column medium-5 small-12">
                        <div class="share text-right">

                            Compartir <i class="fa fa-share-alt" aria-hidden="true"></i>

                            <div class="sharing-box">
                                <!-- Go to www.addthis.com/dashboard to customize your tools -->
                                <div class="addthis_sharing_toolbox right"
                                     data-url="<?=base_url()?><?=$diminutivo?>/<?=$paginaCatalogoUrl?>/<?=$producto->categoria->id?>/<?=$producto->categoria->productoCategoriaUrl?>/<?=$producto->productoUrl?>"
                                     data-title="<?=$producto->productoNombre?>"></div>
                            </div>

                        </div>

                    </div>
                </div>

            </div>

        <? endforeach ?>

    </div>

    <?=$pagination?>
<? else: ?>
    <div class="error">No existen resultados para esta búsqueda, inténte con otros términos.</div>
<? endif ?>