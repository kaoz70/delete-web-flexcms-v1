<div class="row list">

    <? foreach ($products as $producto): ?>

        <div class="column small-12 medium-6 large-4">

            <a class="proyecto"
               href="<?=base_url()?><?=$diminutivo?>/<?=$paginaCatalogoUrl?>/<?=$producto->categoria->id?>/<?=$producto->categoria->productoCategoriaUrl?>/<?=$producto->productoUrl?>">

                <img src="<?=base_url()?>assets/public/images/catalog/prod_<?=$producto->productoId?>_thumb.<?=$producto->productoImagenExtension?>" alt="<?=$producto->productoNombre?>" />

                <div class="detail">
                    <h2><?=$producto->productoNombre?></h2>

                    <div class="text">
                        <? foreach($producto->campos as $campo): ?>
                            <?= $campo->contenido->productoCampoRelContenido; ?>
                        <? endforeach; ?>
                    </div>
                </div>

            </a>

        </div>

    <? endforeach ?>

</div>

<?=$pagination?>