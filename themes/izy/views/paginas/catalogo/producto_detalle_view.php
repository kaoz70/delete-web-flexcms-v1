<style>

    h1:not(.heading) {
        display: none;
    }

</style>

<h1 class="heading"><?=$producto->productoNombre?></h1>

<article class="detail">

    <? foreach($producto->campos as $key => $row):?>
        <? if($row->contenido != ''): ?>
            <? if($row->contenido != ''): ?>
                <? if($row->labelHabilitado): ?>
                    <h2><?=$row->label?></h2>
                <? endif;?>
                <?=$row->contenido->productoCampoRelContenido?>
            <? endif ?>
            <? if(!$key): ?>
                <hr />
            <? endif ?>
        <? endif ?>
    <? endforeach; ?>

    <div class="servicios-swiper swiper-container">
        <div class="swiper-wrapper">

            <div class="swiper-slide">
                <a href="<?=base_url()?>assets/public/images/catalog/prod_<?=$producto->productoId;?>.<?=$producto->productoImagenExtension;?>"
                   title="<?=$producto->productoNombre;?>">
                    <img src="<?=base_url()?>assets/public/images/catalog/prod_<?=$producto->productoId;?>.<?=$producto->productoImagenExtension;?>" alt="<?=$producto->productoNombre;?>" />
                </a>
            </div>

            <? foreach($producto->imagenes as $row):?>
                <? if(count($row->contenido) != 0): ?>

                    <? foreach($row->contenido as $imagen):?>
                        <div class="swiper-slide">
                            <a href="<?= base_url() ?>assets/public/images/catalog/gal_<?=$producto->productoId;?>_<?=$imagen->productoImagenId?>_big.<?=$imagen->productoImagen?>"
                               title="<?=$imagen->productoImagenNombre?>">
                                <img src="<?= base_url() ?>assets/public/images/catalog/gal_<?=$producto->productoId;?>_<?=$imagen->productoImagenId?>_big.<?=$imagen->productoImagen?>" alt="<?=$imagen->productoImagenNombre?>" />
                            </a>
                        </div>
                    <? endforeach; ?>

                <? endif ?>
            <? endforeach; ?>

        </div>

        <div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div>

    </div>

</article>
