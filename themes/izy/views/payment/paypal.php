<form method="post"
      name="paypalGatewayButton"
      action="<?= base_url($diminutivo . '/' . $pagPedidos->paginaNombreURL . '/payment-send') ?>">
    <input type=hidden name=class value=PayPalGateway>
    <button ng-click="paymentSubmit($event, paypalGatewayButton)">
        <img src="https://www.paypal.com/es_XC/i/logo/PayPal_mark_37x23.gif" align="left" style="margin-right:7px;">
        <span style="font-size:11px; font-family: Arial, Verdana;">Un modo más seguro y sencillo de pagar.</span>
    </button>
</form>