<? if($productos): ?>
<div class="resultSet catalog clearfix">
<h4>Productos</h4>
<div class="separator-2"></div>
<? endif ?>
<? foreach($productos as $row): ?>
    <div class="col-xs-6">
		<div class="overlay-container mb-10">
			<img src="<?=base_url()?>assets/public/images/catalog/prod_<?=$row->id?>.<?=$row->extension?>" alt="<?=$row->nombre?>" />
			<a href="<?= base_url($lang . '/' . $row->pagina . '/' . $row->categoriaId . '/' . $row->categoriaUrl . '/' . $row->productoUrl) ?>" class="overlay-link small">
				<i class="fa fa-link"></i>
			</a>

			<div class="listing-item white-bg bordered">
	            <span class="small">
	                <a
	                href="<?= base_url($lang . '/' . $row->pagina . '/' . $row->categoriaId . '/' . $row->categoriaUrl . '/' . $row->productoUrl) ?>"
	                class="btn-sm-link"><?=$row->nombre?></a>
	            </span>
	        </div>

		</div>

	</div>
<? endforeach ?>
<? if($productos): ?></div><? endif ?>

<? foreach($publicaciones as $pagina): ?>
    <div class="page clearfix">
        <h4><?= $pagina->paginaNombre ?></h4>
        <div class="separator-2"></div>
        <? foreach($pagina->publicaciones as $row): ?>
        <div>
            <a href="<?= base_url($lang . '/' . $pagina->paginaNombreURL . '/' . $row->publicacionUrl) ?>">
                <? if($row->publicacionImagen): ?>
                    <img style="display: inline" src="<?= base_url('assets/public/images/noticias/noticia_' . $row->id . '_search.' . $row->publicacionImagen) ?>">
                <? endif ?>
                <?=$row->publicacionNombre?>
            </a>
        </div>
        <? endforeach ?>
    </div>
<? endforeach ?>
<? if($publicaciones): ?></div><? endif ?>

<? if($articulos): ?>
<div class="resultSet articulos clearfix">
<h4>Articulos</h4>
<div class="separator-2"></div>
<? endif ?>
<? foreach($articulos as $row): ?>
    <div>
        <a href="<?= base_url($lang . '/' . $row->paginaNombreURL) ?>"><?=$row->articuloTitulo?></a>
    </div>
<? endforeach ?>
<? if($articulos): ?></div><? endif ?>

<? if($faqs): ?>
<div class="resultSet faq">
<h4>Preguntas Frecuentes</h4>
<div class="separator-2"></div>
<? endif ?>
<? foreach($faqs as $row): ?>
    <div>
        <a href="<?= base_url($lang . '/' . $row->pagina . '#respuesta_' . $row->id) ?>"><?=$row->pregunta?></a>
    </div>
<? endforeach ?>
<? if($faqs): ?></div><? endif ?>

<? if($descargas): ?>
<div class="resultSet gallery">
<h4>Galeria</h4>
<div class="separator-2"></div>
<? endif ?>
<? foreach($descargas as $row): ?>
    <div>
        <a href="<?= base_url($lang . '/' . $row->descargaUrl) ?>">
            <? if($row->descargaArchivo): ?>
                <img src="<?= base_url('assets/public/images/downloads/img_' . $row->id . '_search.' . $row->descargaArchivo) ?>">
            <? endif ?>
            <?=$row->descargaNombre?>
        </a>
    </div>
<? endforeach ?>
<? if($descargas): ?></div><? endif ?>