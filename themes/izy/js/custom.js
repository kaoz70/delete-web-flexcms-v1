/* Theme Name: The Project - Responsive Website Template
 * Author:HtmlCoder
 * Author URI:http://www.htmlcoder.me
 * Author e-mail:htmlcoder.me@gmail.com
 * Version:1.0.0
 * Created:March 2015
 * License URI:http://support.wrapbootstrap.com/
 * File Description: Place here your custom scripts
 */

/*var mySwiper = new Swiper('.mod_banner .swiper', {
    slidesPerView: 1,
    paginationClickable: true,
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    spaceBetween: 0
});*/

var mySwiper = new Swiper('.mod_servicios .swiper', {
    slidesPerView: 4,
    paginationClickable: true,
    nextButton: '#servicios-next',
    prevButton: '#servicios-prev',
    spaceBetween: 0,
    // Responsive breakpoints
    breakpoints: {
        // when window width is <= 320px
        790: {
            slidesPerView: 1
        },
        // when window width is <= 480px
        1000: {
            slidesPerView: 3
        },
        // when window width is <= 640px
        1200: {
            slidesPerView: 4
        }
    }
});

var mySwiper = new Swiper('.servicios-swiper', {
    slidesPerView: 3,
    paginationClickable: true,
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    spaceBetween: 20
});

var mySwiper = new Swiper('.enlaces-swiper', {
    slidesPerView: 5,
    autoplay: 3000,
    paginationClickable: true,
    nextButton: '#enlaces-next',
    prevButton: '#enlaces-prev',
    spaceBetween: 20,
    // Responsive breakpoints
    breakpoints: {
        // when window width is <= 320px
        700: {
            slidesPerView: 1
        },
        // when window width is <= 320px
        790: {
            slidesPerView: 2
        },
        // when window width is <= 480px
        1000: {
            slidesPerView: 3
        },
        // when window width is <= 640px
        1200: {
            slidesPerView: 5
        }
    }
});

// When the window has finished loading create our google map below
if(typeof google !== 'undefined') {
    google.maps.event.addDomListener(window, 'load', init);
}

function init() {
    // Basic options for a simple Google Map
    // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
    var mapOptions = {
        // How zoomed in you want the map to start at (always required)
        zoom: 17,

        scrollwheel: false,
        mapTypeControl: false,

        // The latitude and longitude to center the map (always required)
        center: new google.maps.LatLng(-0.191404, -78.482205),

        // How you would like to style the map.
        // This is where you would paste any style found on Snazzy Maps.
        styles: [
            {
                "stylers": [
                    {
                        "visibility": "on"
                    },
                    {
                        "saturation": -100
                    },
                    {
                        "gamma": 0.54
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "water",
                "stylers": [
                    {
                        "color": "#4d4946"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "labels.text",
                "stylers": [
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#ffffff"
                    }
                ]
            },
            {
                "featureType": "road.local",
                "elementType": "labels.text",
                "stylers": [
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#ffffff"
                    }
                ]
            },
            {
                "featureType": "transit.line",
                "elementType": "geometry",
                "stylers": [
                    {
                        "gamma": 0.48
                    }
                ]
            },
            {
                "featureType": "transit.station",
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "gamma": 7.18
                    }
                ]
            }
        ]
    };

    // Get the HTML DOM element that will contain your map
    // We are using a div with id="map" seen below in the <body>
    var mapElement = document.getElementById('map');

    // Create the Google Map using our element and options defined above
    var map = new google.maps.Map(mapElement, mapOptions);

    // Let's also add a marker while we're at it
    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(-0.192104, -78.482205),
        map: map,
        title: 'IZY'
    });
}

$(document).ready(function() {

    $('form')
        .on("reset", function () {
            $(this).data("formValidation").resetForm();
        })
        .on("success.form.fv", function (e) {

            var reveal = new Foundation.Reveal($(".modal")),
                responseContainer = $(".ajax-response"),
                messageContainer = responseContainer.find('.message'),
                errorContainer = $(".ajax-response").find('.error'),
                form = this;

            reveal.open();
            e.preventDefault();

            var data = new FormData(this);

            errorContainer
                .removeClass('yes')
                .empty();

            $.ajax({
                url: $(this).attr("action"),
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success: function(response){

                    if(!response.sent) {

                        errorContainer
                            .removeClass('no')
                            .addClass('yes')
                            .empty()
                            .html($(response.error));

                        form.data("formValidation").resetForm();

                    } else {
                        form.reset();
                    }

                    messageContainer
                        .empty()
                        .html($(response.message));

                }
            });

        })
        .on("submit", function (e) {
            e.preventDefault();
        })
        .formValidation();

    //Popup gallery
    $(".servicios-swiper a, .servicios-index a").tosrus({
        drag: true,
        keys: {
            prev: true,
            next: true
        },
        wrapper: {
            onClick: 'close'
        },
        pagination : {
            add: true
        }
    });

    $(".top.swiper-slide").on("click", function () {

        var bottomSlides = $('.mod_servicios').find('.bottom'),
            index = $(".top.swiper-slide").index(this);

        bottomSlides.slideUp();

        if($(this).hasClass("active")) {
            $(this).removeClass("active");
        } else {
            $(".top.swiper-slide").removeClass("active");
            $(this).addClass("active");
            $(bottomSlides[index]).slideDown();
        }

    });

    //Disable click on menu items with submenu
    $(".is-dropdown-submenu-parent > a").on("click", function (ev) {
        ev.preventDefault();
    });

});