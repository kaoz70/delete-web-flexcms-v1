/* Theme Name: The Project - Responsive Website Template
 * Author:HtmlCoder
 * Author URI:http://www.htmlcoder.me
 * Author e-mail:htmlcoder.me@gmail.com
 * Version:1.0.0
 * Created:March 2015
 * License URI:http://support.wrapbootstrap.com/
 * File Description: Place here your custom scripts
 */

$(document).ready(function() {

    if($("[data-bannerIndex]").length) {
        var index = $("[data-bannerIndex]").attr("data-bannerIndex"),
            mySwiper = $('.mod_banner .swiper-container')[0].swiper;

        console.log("slide index: " + index);

        $(".banner_103").css("display", "block");
        mySwiper.update();
        mySwiper.slideTo(index, 0);

    }

});