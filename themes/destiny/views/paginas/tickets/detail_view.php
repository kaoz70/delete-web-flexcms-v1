<? if(isset($message)) : ?>
    <div class="success callout" data-closable>
        <?= $message ?>
        <button class="close-button" aria-label="Dismiss alert" type="button" data-close>
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
<? endif; ?>

<? if(isset($error)) : ?>
    <div class="alert callout" data-closable>
        <?= $error ?>
        <button class="close-button" aria-label="Dismiss alert" type="button" data-close>
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
<? endif; ?>

<div class="row">
    <div class="columns">
        <a class="button float-left" href="<?= base_url("$diminutivo/$pagina_url") ?><?= $ticket->closed ? '/closed' : '' ?>"><i class="fa fa-angle-left" aria-hidden="true"></i> Regresar</a>
        <? if( ! $ticket->closed): ?>
        <a class="alert button float-right" data-toggle="closeTicketModal">Cerrar Ticket</a>
        <? endif; ?>
    </div>
</div>

<div id="detail">
    <div class="text-center">
        <h3>Ticket #<?= $ticket->uuid ?></h3>
    </div>

    <div class="row data">
        <div class="columns small-12 medium-6">
            <div>
                <strong>Nombre de la instituci&oacute;n:</strong> <?= $ticket->institution ?>
            </div>
        </div>
        <div class="columns small-12 medium-6">
            <div>
                <strong>&Aacute;rea de trabajo:</strong> <?= $ticket->area ?>
            </div>
        </div>
    </div>

    <div class="row data">
        <div class="columns small-12 medium-6">
            <div>
                <strong>Estado del soporte:</strong> <?= $ticket->status ?>
            </div>
        </div>
        <div class="columns small-12 medium-6">
            <div>
                <strong>N&uacute;mero de contrato relacionado:</strong> <?= $ticket->contract_number ?>
            </div>
        </div>
    </div>

    <div class="row data">
        <div class="columns small-12 medium-6">
            <div>
                <strong>Estado:</strong> <?= $ticket->closed ? 'Cerrado' : 'Abierto' ?>
            </div>
        </div>
        <div class="columns small-12 medium-6">
            <div>
                <strong>Tipo:</strong> <?= lang('ticket_type_' . $ticket->type) ?>
            </div>
        </div>
    </div>

    <div class="row data">
        <div class="columns small-12 medium-6">
            <div>
                <strong>Prioridad:</strong> <?= lang('ticket_priority_' . $ticket->priority) ?>
            </div>
        </div>
        <div class="columns small-12 medium-6">
            <div>
                <strong>Nivel:</strong> <?= lang('ticket_level_' . $ticket->level) ?>
            </div>
        </div>
    </div>

    <div class="row data">
        <? if($ticket->analized_at): ?>
        <div class="columns small-12 medium-6">
            <div>
                <strong>Fecha de an&aacute;lisis:</strong> <?= $ticket->analized_at ?>
            </div>
        </div>
        <? endif; ?>
        <? if($ticket->closed): ?>
        <div class="columns small-12 medium-6">
            <div>
                <strong>Fecha de finalizaci&oacute;n:</strong> <?= $ticket->closed_at ?>
            </div>
        </div>
        <? endif; ?>
        <? if( ! $ticket->closed && $ticket->reopened_at): ?>
        <div class="columns small-12 medium-6">
            <div>
                <strong>Fecha de reapertura:</strong> <?= $ticket->reopened_at ?>
            </div>
        </div>
        <? endif; ?>
    </div>

    <div id="responses">
        <? foreach ($ticket->comments() as $comment): ?>
        <div class="row data response">
            <div class="columns">
                <div class="<?= $comment->is_support ? 'support' : 'user' ?> <?= $comment->status ?>">
                    <? if($comment->status): ?>
                    <span class="badge"><?= lang('ticket_' . $comment->status) ?></span>
                    <? endif; ?>
                    <h5><i class="fa fa-user" aria-hidden="true"></i> <?= $comment->userName() ?></h5>
                    <div class="date"><small><?= $comment->created_at ?></small></div>
                    <div class="detail">
                        <?= $comment->detail ?>
                    </div>
                    <? if($files = json_decode($comment->files)): ?>
                        <div class="files">
                            <div class="row">
                                <? foreach ($files as $file): ?>
                                    <div class="columns small-12 medium-6 large-4">
                                        <a target="_blank" class="file" href="<?= base_url("assets/public/files/tickets/{$ticket->uuid}/{$comment->id}/{$file->file_name}") ?>">
                                            <span><?= $file->file_name ?></span>
                                        </a>
                                    </div>
                                <? endforeach; ?>
                            </div>
                        </div>
                    <? endif; ?>
                </div>
            </div>
        </div>
        <? endforeach; ?>
    </div>

    <hr>

    <div id="comment">

        <fieldset>
            <legend>Nuevo comentario</legend>

            <? if($ticket->closed): ?>
                <p>Este ticket est&aacute; cerrado, al enviar un nuevo comentario se volver&aacute; a abrir</p>
            <? endif; ?>

            <?= form_open("$diminutivo/$pagina_url/comment/{$ticket->uuid}", [
                'method' => 'POST',
                'enctype' => 'multipart/form-data',
                'data-fv-framework' => 'foundation',
            ]) ?>

            <div class="row">
                <div class="columns">

                    <div class="input">
                        <div class="input-group">
                            <span class="input-group-label textarea">Comentario:</span>
                            <textarea cols="20" rows="10"
                                      class="input-group-field"
                                      data-fv-row=".input"
                                      data-fv-notempty="true"
                                      data-fv-notempty-message="<?=lang('required')?>"
                                      name="detail"></textarea>
                        </div>
                    </div>

                </div>
            </div>

            <div class="input-group">
                <span class="input-group-label"><i class="fa fa-paperclip" aria-hidden="true" style="margin-right: 12px"></i>Adjuntar archivos:</span>
                <input class="input-group-field button secondary" style="margin: 0" type="file" name="userfile[]" id="files" multiple>
            </div>

            <ul id="selectedFiles"></ul>

            <div class="text-right">
                <button type="submit" class="button">Enviar</button>
            </div>

            <?= form_close() ?>

        </fieldset>

    </div>

</div>



<div class="reveal" id="closeTicketModal" data-reveal data-close-on-click="true" data-animation-in="slide-in-down" data-animation-out="slide-out-up">
    <h3>Cerrar ticket</h3>
    <p class='lead'>Envie un comentario al cerrar este ticket (opcional)</p>

    <?= form_open("$diminutivo/$pagina_url/close/{$ticket->uuid}", [
        'method' => 'POST',
    ]) ?>

    <textarea cols="20" rows="5"
              name="close_comment"></textarea>

    <div class="text-right">
        <button type="submit" class="button">Cerrar Ticket</button>
    </div>

    <?= form_close() ?>

    <button class="close-button" data-close aria-label="Close reveal" type="button">
        <span aria-hidden="true">&times;</span>
    </button>
</div>