<div class="text-center">
    <h3>Nuevo ticket de soporte</h3>
</div>

<?= form_open("$diminutivo/$pagina_url/insert", [
    'method' => 'POST',
    'enctype' => 'multipart/form-data',
    'data-fv-framework' => 'foundation',
]) ?>

<h4 class="subheader">Datos generales</h4>
<div class="row">
    <div class="columns large-6">
        <div class="input">
            <div class="input-group">
                <span class="input-group-label">Nombre de la instituci&oacute;n:</span>
                <input class="input-group-field"
                       type="text"
                       name="institution"
                       data-fv-row=".input"
                       data-fv-notempty="true"
                       data-fv-notempty-message="<?=lang('required')?>"
                       value="<?= $user->roles()->first()->name ?>">
            </div>
        </div>

        <div class="input">
            <div class="input-group">
                <span class="input-group-label">Nombre de usuario:</span>
                <input class="input-group-field"
                       type="text"
                       name="user_name"
                       data-fv-row=".input"
                       data-fv-notempty="true"
                       data-fv-notempty-message="<?=lang('required')?>"
                       value="<?= $user->first_name ?> <?= $user->last_name ?>">
            </div>
        </div>

    </div>

    <div class="columns large-6">

        <div class="input-group">
            <span class="input-group-label">&Aacute;rea de trabajo:</span>
            <input class="input-group-field" type="text" name="area">
        </div>

    </div>

</div>

<hr>

<h4 class="subheader">Contrato</h4>
<div class="row">
    <div class="columns large-6">
        <div class="input-group">
            <span class="input-group-label">N&uacute;mero de contrato relacionado:</span>
            <input class="input-group-field" type="text" name="contract_number">
        </div>
    </div>
    <div class="columns large-6">
        <div class="input-group">
            <span class="input-group-label">Fecha de vencimiento del contrato:</span>
            <input class="input-group-field" type="date" name="contract_end_date">
        </div>
    </div>
</div>

<hr>

<h4 class="subheader">Prioridad</h4>
<div class="row">
    <div class="columns medium-4">
        <div class="input-group">
            <span class="input-group-label">Tipo:</span>
            <select class="input-group-field" name="type">
                <option value="1"><?= lang('ticket_type_1') ?></option>
            </select>
        </div>
    </div>
    <div class="columns medium-4">
        <div class="input-group">
            <span class="input-group-label">Prioridad:</span>
            <select class="input-group-field" name="priority">
                <option value="1"><?= lang('ticket_priority_1') ?></option>
                <option value="2"><?= lang('ticket_priority_2') ?></option>
                <option value="3"><?= lang('ticket_priority_3') ?></option>
            </select>
        </div>
    </div>
    <div class="columns medium-4">
        <div class="input-group">
            <span class="input-group-label">Nivel:</span>
            <select class="input-group-field" name="level">
                <option value="1"><?= lang('ticket_level_1') ?></option>
                <option value="2"><?= lang('ticket_level_2') ?></option>
                <option value="3"><?= lang('ticket_level_3') ?></option>
            </select>
        </div>
    </div>
</div>

<hr>

<h4 class="subheader">Detalle</h4>
<div class="row">
    <div class="columns">

        <div class="input">
            <div class="input-group">
                <span class="input-group-label">T&iacute;tulo:</span>
                <input class="input-group-field"
                       type="text"
                       data-fv-row=".input"
                       data-fv-notempty="true"
                       data-fv-notempty-message="<?=lang('required')?>"
                       name="title">
            </div>
        </div>

        <div class="input">
            <div class="input-group">
                <span class="input-group-label textarea">Descripci&oacute;n:</span>
                <textarea cols="20" rows="10"
                          class="input-group-field"
                          data-fv-row=".input"
                          data-fv-notempty="true"
                          data-fv-notempty-message="<?=lang('required')?>"
                          name="detail"></textarea>
            </div>
        </div>

    </div>
</div>

<div class="input-group">
    <span class="input-group-label"><i class="fa fa-paperclip" aria-hidden="true" style="margin-right: 12px"></i>Adjuntar archivos:</span>
    <input class="input-group-field button secondary" style="margin: 0" type="file" name="userfile[]" id="files" multiple>
</div>

<ul id="selectedFiles"></ul>

<div class="text-right">
    <a class="alert button" href="<?= base_url("$diminutivo/$pagina_url") ?>">Cancelar</a>
    <button type="submit" class="button">Enviar</button>
</div>

<?= form_close() ?>