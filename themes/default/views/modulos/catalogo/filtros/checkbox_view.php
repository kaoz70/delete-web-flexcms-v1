<div class="content">
    <form id="module_filtros" data-destacados="<?=$productosDestacados?>">
        <h3>Categorias</h3>
        <div id="filtro_categorias">
            <? foreach ($categorias as $key => $categoria): ?>
                <div>
                    <label>
                        <input name="filtro[cat][<?=$categoria['id']?>]"
                               data-id="<?=$categoria['id']?>"
                               type="checkbox"
                               value="<?=$categoria['productoCategoriaUrl']?>">
                        <?=$categoria['productoCategoriaNombre']?>
                    </label>
                </div>
            <? endforeach ?>
        </div>

        <? foreach ($filtros as $key1 => $filtro): ?>
            <h3><?=$filtro -> productoCampoValor ?></h3>
            <div class="campos" data-id="<?=$filtro->productoCampoId?>">
                <? foreach ($filtro->productoFiltros as $key2 => $value): ?>
                    <div>
                        <label>
                            <input name="filtro[<?=$filtro->productoCampoId?>][<?=$key2 ?>]"
                                   type="checkbox"
                                   value="<?=$value->productoCampoRelContenido ?>">
                            <?=$value->productoCampoRelContenido ?>
                        </label>
                    </div>
                <? endforeach ?>
            </div>
        <? endforeach ?>
        <input type="hidden" name="language" value="<?=$diminutivo?>">
    </form>
</div>