<h2><?=$txt_titulo?><a class="cerrar" href="#" >cancelar</a></h2>

<div id="filter-orders" class="filter field">
    <div class="input width_50">
        <label>Agrupar</label>
        <select name="group">
            <option value="status" <?= $select_group == 'status' ? 'selected' : '' ?>>Estado</option>
            <option value="month" <?= $select_group == 'month' ? 'selected' : '' ?>>Mes</option>
            <option value="week" <?= $select_group == 'week' ? 'selected' : '' ?>>Semana</option>
            <option value="day" <?= $select_group == 'day' ? 'selected' : '' ?>>D&iacute;a</option>
        </select>
    </div>
    <div class="input width_50">
        <label>Ordenar</label>
         <select name="order">
            <option value="desc" <?= $select_order == 'desc' ? 'selected' : '' ?>>Desendente</option>
            <option value="asc" <?= $select_order == 'asc' ? 'selected' : '' ?>>Ascendente</option>
        </select>
    </div>
</div>

<? if($search): ?>
    <div class="buscar">
        <input data-page-id="<?=$this->uri->segment(4)?>" type="text" name="searchString" value="Buscar..." />
        <div class="searchButton"></div>
    </div>
    <ul id="<?=$list_id?>" class="contenido_col listado_general searchResults" style="bottom: <?=$bottomMargin?>px; top: 147px;" rel="<?=$url_rel?>">
<? else: ?>
    <ul id="<?=$list_id?>" class="contenido_col listado_general" style="bottom: <?=$bottomMargin?>px" rel="<?=$url_rel?>">
<? endif ?>

    <? foreach($groups as $nombre => $items): ?>
        <li class="pagina field">
            <h3 class="header"><?=$nombre?></h3>
            <ul class="content">
                <? foreach($items as $item): ?>
                    <li class="listado" id="<?=$item[$idx_item_id];?>">
                        <a class="nombre modificar <?=$nivel?>" href="<?=$url_modificar . '/' . $item[$idx_item_id]?>"><span><?=$item[$idx_item_nombre]?></span></a>
                        <a href="<?=$url_eliminar . '/' . $item[$idx_item_id];?>" class="eliminar" >eliminar</a>
                    </li>
                <? endforeach; ?>
            </ul>
        </li>
    <?  endforeach; ?>

</ul>

<?foreach($menu as $item): ?>
    <?=$item?>
<? endforeach ?>

<? if($search): ?>
<script type="text/javascript">
    search.init('<?=$url_search?>', 'es');
    filter.init('filter-orders', '<?=$url_filter?>', 'es');
</script>
<? endif ?>