<!DOCTYPE html>

<head>
    <link href="<?= base_url('packages/foundation/css/normalize.css') ?>" rel="stylesheet">
    <link href="<?= base_url('packages/foundation/css/foundation.css') ?>" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
    <style>

        .th {
            width: 29%;
            position: relative;
            margin: 8px;
        }
        .filename {
            background: rgba(0, 0, 0, 0.58);
            left: 0;
            right: 0;
            bottom: 0;
            position: absolute;
            display: block;
            text-align: center;
            height: 25px;
            vertical-align: bottom;
            color: #fff;
            line-height: 26px;
            font-size: 12px;
        }
    </style>
</head>

<body>

<? if(isset($message) && isset($message['text'])): ?>
<div data-alert class="alert-box <?= $message['class']?>">
    <?= $message['text']?>
    <a href="#" class="close">&times;</a>
</div>
<? endif; ?>

<div class="row">
    <div class="column">
        <h2>Subir archivo</h2>
        <form id="my_form"
              action="<?= base_url('admin/tiny_mce_uploader/upload/english') ?>"
              method="post"
              enctype="multipart/form-data">
            <input name="userfile" type="file" onchange="$('#my_form').submit();this.value='';">
        </form>
    </div>
</div>

<hr>

<div class="row">

    <div class="column">
        <h2>Seleccionar</h2>
        <? foreach ($files as $file): ?>
        <span class="th fichero" data-src="<?= $file['path'] ?>" >
            <img src="<?= $file['path'] ?>" alt=" <?= $file['name'] ?>" />
            <span class="filename"><?= $file['name'] ?></span>
        </span>
        <? endforeach; ?>
    </div>

</div>


<script type="text/javascript" language="javascript">
    $(document).on("click",".fichero",function(){
         item_url = $(this).data("src");
         var args = top.tinymce.activeEditor.windowManager.getParams();
         win = (args.window);
         input = (args.input);
         win.document.getElementById(input).value = item_url;
         top.tinymce.activeEditor.windowManager.close();
     });
</script>

</body>
</html>


