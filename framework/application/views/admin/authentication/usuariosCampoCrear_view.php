<h2><?=$titulo; ?><a class="cerrar" href="#" >cancelar</a></h2>
<div class="contenido_col">

    <?php

    $attributes = array('class' => 'form');
    echo form_open('admin/articulos/' . $link, $attributes);

    ?>

    <div class="field">
        <div class="header">General</div>
        <div class="content">
            <div class="input">

                <fieldset>
                    <legend>Etiqueta</legend>
                    <? foreach ($idiomas as $key => $idioma): ?>
                        <div>
                            <label for="<?=$idioma['idiomaDiminutivo']?>_userFieldLabel"><?=$idioma['idiomaNombre']?></label>
                            <? if(count($traducciones[$idioma['idiomaDiminutivo']]) > 0):?>
                                <input class="required name" name="<?=$idioma['idiomaDiminutivo']?>_userFieldLabel" type="text" value="<?=$traducciones[$idioma['idiomaDiminutivo']]->userFieldLabel?>"/>
                            <? else: ?>
                                <input class="required name" name="<?=$idioma['idiomaDiminutivo']?>_userFieldLabel" type="text" value=""/>
                            <? endif ?>
                        </div>
                    <? endforeach ?>
                </fieldset>

                <fieldset>
                    <legend>Placehoder</legend>
                    <? foreach ($idiomas as $key => $idioma): ?>
                        <div>
                            <label for="<?=$idioma['idiomaDiminutivo']?>_userFieldPlaceholder"><?=$idioma['idiomaNombre']?></label>
                            <? if(count($traducciones[$idioma['idiomaDiminutivo']]) > 0):?>
                                <input class="" name="<?=$idioma['idiomaDiminutivo']?>_userFieldPlaceholder" type="text" value="<?=$traducciones[$idioma['idiomaDiminutivo']]->userFieldPlaceholder?>"/>
                            <? else: ?>
                                <input class="" name="<?=$idioma['idiomaDiminutivo']?>_userFieldPlaceholder" type="text" value=""/>
                            <? endif ?>
                        </div>
                    <? endforeach ?>
                </fieldset>

            </div>

            <div class="input">
                <label for="inputId">Tipo:</label>
                <select id="inputId" name="inputId">
                    <?php foreach ($inputs as $row) : ?>
                        <?php
                        if($inputId == $row->inputId)
                            $selected = "selected";
                        else
                            $selected = '';
                        ?>
                        <option value="<?=$row->inputId;?>" <?=$selected;?> ><?=$row->inputTipoContenido;?></option>
                    <?php endforeach;?>
                </select>
            </div>

            <div class="input check">
                <input type="checkbox" name="userFieldActive" id="userFieldActive" <?= $habilitado; ?> />
                <label for="userFieldActive">Publicado</label>
            </div>

            <div class="input check">
                <input id="userFieldRequired" name="userFieldRequired" type="checkbox" <?=$userFieldRequired?> />
                <label for="userFieldRequired">Obligatorio</label>
            </div>

            <div class="input check">
                <input id="userFieldRequiredAuth" name="userFieldRequiredAuth" type="checkbox" <?=$userFieldRequiredAuth?> />
                <label for="userFieldRequiredAuth">Obligatorio en login/registro</label>
            </div>

            <div class="input">
                <label for="userFieldValidation">Validación:</label>
                <input id="userFieldValidation" name="userFieldValidation" type="text" value="<?=$userFieldValidation?>"/>
            </div>

        </div>
    </div>

    <div class="field">
        <div class="header">Avanzado</div>
        <div class="content">
            <div class="input">
                <label for="userFieldClass">Clase</label>
                <input name="userFieldClass"  id="userFieldClass" type="text" value="<?= $userFieldClass; ?>" />
            </div>
        </div>
    </div>

    <div class="field">
        <div class="header">Shopping Cart</div>
        <div class="content">
            <div class="input">
                <label for="userFieldType">Tipo:</label>
                <select id="userFieldType" name="userFieldType">
                    <option value="profile" <?= $userFieldType == 'profile' ? 'selected' : ''?> >Perfil</option>
                    <option value="billing" <?= $userFieldType == 'billing' ? 'selected' : ''?> >Billing / Facturaci&oacute;n</option>
                    <option value="shipping" <?= $userFieldType == 'shipping' ? 'selected' : ''?> >Shipping / Envio</option>
                </select>
            </div>
            <div class="input">
                <label for="userFieldOrderCol">Empatar columna de BD para este campo:</label>
                <select id="userFieldOrderCol" name="userFieldOrderCol">

                    <option value="" <?= $userFieldOrderCol == '' ? 'selected' : ''?> >Ninguno</option>

                    <optgroup label="Billing / Facturaci&oacute;n">
                        <option value="ord_bill_first_name" <?= $userFieldOrderCol == 'ord_bill_first_name' ? 'selected' : ''?> >ord_bill_first_name</option>
                        <option value="ord_bill_last_name" <?= $userFieldOrderCol == 'ord_bill_last_name' ? 'selected' : ''?> >ord_bill_last_name</option>
                        <option value="ord_bill_company" <?= $userFieldOrderCol == 'ord_bill_company' ? 'selected' : ''?> >ord_bill_company</option>
                        <option value="ord_bill_address_01" <?= $userFieldOrderCol == 'ord_bill_address_01' ? 'selected' : ''?> >ord_bill_address_01</option>
                        <option value="ord_bill_address_02" <?= $userFieldOrderCol == 'ord_bill_address_02' ? 'selected' : ''?> >ord_bill_address_02</option>
                        <option value="ord_bill_city" <?= $userFieldOrderCol == 'ord_bill_city' ? 'selected' : ''?> >ord_bill_city</option>
                        <option value="ord_bill_state" <?= $userFieldOrderCol == 'ord_bill_state' ? 'selected' : ''?> >ord_bill_state</option>
                        <option value="ord_bill_post_code" <?= $userFieldOrderCol == 'ord_bill_post_code' ? 'selected' : ''?> >ord_bill_post_code</option>
                        <option value="ord_bill_country" <?= $userFieldOrderCol == 'ord_bill_country' ? 'selected' : ''?> >ord_bill_country</option>
                        <option value="ord_bill_phone" <?= $userFieldOrderCol == 'ord_bill_phone' ? 'selected' : ''?> >ord_bill_phone</option>
                        <option value="ord_bill_comments" <?= $userFieldOrderCol == 'ord_bill_comments' ? 'selected' : ''?> >ord_bill_comments</option>
                        <option value="ord_bill_id" <?= $userFieldOrderCol == 'ord_bill_id' ? 'selected' : ''?> >ord_bill_id</option>
                    </optgroup>

                    <optgroup label="Shipping / Envio">
                        <option value="ord_ship_first_name" <?= $userFieldOrderCol == 'ord_ship_first_name' ? 'selected' : ''?> >ord_ship_first_name</option>
                        <option value="ord_ship_last_name" <?= $userFieldOrderCol == 'ord_ship_last_name' ? 'selected' : ''?> >ord_ship_last_name</option>
                        <option value="ord_ship_company" <?= $userFieldOrderCol == 'ord_ship_company' ? 'selected' : ''?> >ord_ship_company</option>
                        <option value="ord_ship_address_01" <?= $userFieldOrderCol == 'ord_ship_address_01' ? 'selected' : ''?> >ord_ship_address_01</option>
                        <option value="ord_ship_address_02" <?= $userFieldOrderCol == 'ord_ship_address_02' ? 'selected' : ''?> >ord_ship_address_02</option>
                        <option value="ord_ship_city" <?= $userFieldOrderCol == 'ord_ship_city' ? 'selected' : ''?> >ord_ship_city</option>
                        <option value="ord_ship_state" <?= $userFieldOrderCol == 'ord_ship_state' ? 'selected' : ''?> >ord_ship_state</option>
                        <option value="ord_ship_post_code" <?= $userFieldOrderCol == 'ord_ship_post_code' ? 'selected' : ''?> >ord_ship_post_code</option>
                        <option value="ord_ship_country" <?= $userFieldOrderCol == 'ord_ship_country' ? 'selected' : ''?> >ord_ship_country</option>
                        <option value="ord_ship_phone" <?= $userFieldOrderCol == 'ord_ship_phone' ? 'selected' : ''?> >ord_ship_phone</option>
                        <option value="ord_ship_comments" <?= $userFieldOrderCol == 'ord_ship_comments' ? 'selected' : ''?> >ord_ship_comments</option>
                        <option value="ord_ship_number" <?= $userFieldOrderCol == 'ord_ship_number' ? 'selected' : ''?> >ord_ship_number</option>
                        <option value="ord_ship_id" <?= $userFieldOrderCol == 'ord_ship_id' ? 'selected' : ''?> >ord_ship_id</option>
                    </optgroup>

                </select>
            </div>

            <div class="input">
                <label for="twoCheckoutName">Empatar con campo de 2Checkout:</label>
                <select id="twoCheckoutName" name="twoCheckoutName">
                    <option value="" <?= $twoCheckoutName == '' ? 'selected' : ''?> >Ninguno</option>
                    <option value="street_address" <?= $twoCheckoutName == 'street_address' ? 'selected' : ''?> >street_address</option>
                    <option value="street_address2" <?= $twoCheckoutName == 'street_address2' ? 'selected' : ''?> >street_address2</option>
                    <option value="city" <?= $twoCheckoutName == 'city' ? 'selected' : ''?> >city</option>
                    <option value="state" <?= $twoCheckoutName == 'state' ? 'selected' : ''?> >state</option>
                    <option value="zip" <?= $twoCheckoutName == 'zip' ? 'selected' : ''?> >zip</option>
                    <option value="phone" <?= $twoCheckoutName == 'phone' ? 'selected' : ''?> >phone</option>
                    <option value="ship_name" <?= $twoCheckoutName == 'ship_name' ? 'selected' : ''?> >ship_name</option>
                    <option value="ship_street_address" <?= $twoCheckoutName == 'ship_street_address' ? 'selected' : ''?> >ship_street_address</option>
                    <option value="ship_street_address2" <?= $twoCheckoutName == 'ship_street_address2' ? 'selected' : ''?> >ship_street_address2</option>
                    <option value="ship_city" <?= $twoCheckoutName == 'ship_city' ? 'selected' : ''?> >ship_city</option>
                    <option value="ship_state" <?= $twoCheckoutName == 'ship_state' ? 'selected' : ''?> >ship_state</option>
                    <option value="ship_zip" <?= $twoCheckoutName == 'ship_zip' ? 'selected' : ''?> >ship_zip</option>
                    <option value="ship_country" <?= $twoCheckoutName == 'ship_country' ? 'selected' : ''?> >ship_country</option>
                </select>
            </div>

        </div>
    </div>

    <input id="userFieldId" type="hidden" name="userFieldId" value="<?=$userFieldId;?>" />

    <div class="field">
        <div class="header">Ayuda</div>
        <div class="content">

            <fieldset>
                <legend>Validación</legend>
                <ul>
                    <li>Alfabético: alpha</li>
                    <li>Alfanumérico: alpha_numeric</li>
                    <li>Entero: integer</li>
                    <li>Número: number</li>
                    <li>Contraseña: password</li>
                    <li>Tarjeta: card</li>
                    <li>CCV: cvv</li>
                    <li>Email: email</li>
                    <li>Link: url</li>
                    <li>Dominio: domain</li>
                    <li>Fecha - Hora: datetime</li>
                    <li>Fecha: date</li>
                    <li>Hora: time</li>
                    <li>Mes / Dia / Año: month_day_year</li>
                </ul>
            </fieldset>

            <fieldset>
                <legend>Otro</legend>
                <ul>
                    <li>Campo nombre (Clase): name</li>
                </ul>
            </fieldset>

        </div>
    </div>


    <?= form_close(); ?>

</div>

<a href="<?= $link; ?>" data-level="nivel3" data-edit-url="users/modificarCampo/" data-delete-url="users/eliminarCampo/" class="guardar boton importante n1 <?=$nuevo?>" ><?=$txt_boton;?></a>