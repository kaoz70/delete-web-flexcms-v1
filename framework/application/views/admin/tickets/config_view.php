<h2><?php echo $titulo;?><a class="cerrar" href="#" >cancelar</a></h2>
<div class="contenido_col" rel="configuracion">
    <?= form_open('admin/configuracion/guardar', array('class' => 'form')); ?>

        <div class="field">
            <div class="header">General</div>
            <div class="content">
                <div class="input">
                    <label for="tickets_role">Rol del usuario para soporte</label>
                    <select name="tickets_role">
                        <? foreach ($roles as $role): ?>
                            <option <?= $role->slug === $config->tickets_role ? 'selected' : '' ?> value="<?= $role->slug ?>"><?= $role->name ?></option>
                        <? endforeach; ?>
                    </select>
                </div>
                <div class="input">
                    <label for="tickets_default_email">Email por defecto</label>
                    <input type="text" name="tickets_default_email" value="<?= $config->tickets_default_email ?>">
                </div>
            </div>
        </div>

    </form>

</div>
<a class="guardar boton importante n1" href="<?=base_url();?>admin/config/save"><?=$txt_guardar;?></a>
