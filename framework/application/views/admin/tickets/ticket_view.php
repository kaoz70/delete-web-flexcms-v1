<h2><?=$titulo; ?><a class="cerrar" href="#" data-delete="<?=$removeUrl?>" >cancelar</a></h2>
<div class="contenido_col" style="width: 780px">

    <?= form_open($link, [
        'id' => 'ticket',
        'class' => 'form',
        'method' => 'POST',
        'enctype' => 'multipart/form-data',
    ]); ?>

    <div class="field">
        <div class="header">General</div>
        <div class="content">

            <div class="input small">
                <label>Nombre de la instituci&oacute;n</label>
                <span><?= $ticket->institution ?></span>
            </div>

            <div class="input small">
                <label>&Aacute;rea de trabajo</label>
                <span><?= $ticket->area ?></span>
            </div>

            <div class="input small">
                <label>N&uacute;mero de contrato relacionado</label>
                <span><?= $ticket->contract_number ?></span>
            </div>

            <div class="input small">
                <label>Estado del soporte</label>
                <span><?= $ticket->status ?></span>
            </div>

            <div class="input small">
                <label>Estado</label>
                <? if(!$ticket->closed): ?>
                <select name="closed">
                    <option <?= !$ticket->closed ? 'selected' : '' ?> value="0">Abierto</option>
                    <option <?= $ticket->closed ? 'selected' : '' ?> value="1">Cerrado</option>
                </select>
                <? else: ?>
                <span>Cerrado</span>
                <? endif; ?>
            </div>

            <div class="input small">
                <label>Asignado a</label>
                <? if(!$ticket->closed): ?>
                <select name="asigned_to">
                    <option>-- Sin asignar --</option>
                    <? foreach ($users as $role_user): ?>
                        <? if($role_user->inRole($role)): ?>
                            <option value="<?= $role_user->id ?>"
                                <?= $ticket->asigned_to === $role_user->id ? 'selected' : '' ?>>
                                <?= $role_user->first_name ?> <?= $role_user->last_name ?> (<?= $role_user->email ?>)
                            </option>
                        <? endif; ?>
                    <? endforeach; ?>
                </select>
                <? elseif($support_user = $ticket->supportUser()): ?>
                    <span><?= $support_user->first_name ?> <?= $support_user->last_name ?> (<?= $support_user->email ?>)</span>
                <? else: ?>
                    <span>Sin asignar</span>
                <? endif; ?>
            </div>

            <div class="input small">
                <label>Prioridad</label>
                <span><?= lang('ticket_priority_' . $ticket->priority) ?></span>
            </div>

            <div class="input small">
                <label>Nivel</label>
                <span><?= lang('ticket_level_' . $ticket->level) ?></span>
            </div>

            <div class="input small">
                <label>Tipo</label>
                <span><?= lang('ticket_type_' . $ticket->type) ?></span>
            </div>


        </div>
    </div>

    <div class="field">
        <div class="header">Comentarios</div>
        <div class="content">
            <div id="comments">
                <? foreach ($ticket->comments() as $comment): ?>
                    <div class="row data response">
                        <div class="columns">
                            <div class="<?= ($user->id === $comment->user_id) ? 'support' : 'user' ?> <?= $comment->status ?>">
                                <? if($comment->status): ?>
                                    <span class="badge"><?= lang('ticket_' . $comment->status) ?></span>
                                <? endif; ?>
                                <h5><i class="fa fa-user" aria-hidden="true"></i> <?= $comment->userName() ?></h5>
                                <div class="date"><small><?= $comment->created_at ?></small></div>
                                <div class="detail">
                                    <?= $comment->detail ?>
                                </div>
                                <? if($files = json_decode($comment->files)): ?>
                                    <div class="files">
                                        <div class="row">
                                            <? foreach ($files as $file): ?>
                                                <div class="columns small-12 medium-6 large-4">
                                                    <a class="file external" target="_blank" href="<?= base_url("assets/public/files/tickets/{$ticket->uuid}/{$comment->id}/{$file->file_name}") ?>">
                                                        <i class="fa fa-file" aria-hidden="true"></i> <span><?= $file->file_name ?></span>
                                                    </a>
                                                </div>
                                            <? endforeach; ?>
                                        </div>
                                    </div>
                                <? endif; ?>
                            </div>
                        </div>
                    </div>
                <? endforeach; ?>

                <fieldset>
                    <legend>Nuevo comentario</legend>

                    <? if($ticket->closed): ?>
                        <p>Este ticket est&aacute; cerrado, al enviar un nuevo comentario se volver&aacute; a abrir</p>
                    <? endif; ?>

                    <span class="input-group-label textarea">Comentario:</span>
                    <textarea cols="20" rows="10"
                              class="input-group-field"
                              data-fv-row=".input"
                              data-fv-notempty="true"
                              data-fv-notempty-message="<?=lang('required')?>"
                              name="detail"></textarea>

                    <span class="input-group-label"><i class="fa fa-paperclip" aria-hidden="true" style="margin-right: 12px"></i>Adjuntar archivos:</span>
                    <input class="input-group-field button secondary" style="margin: 0" type="file" name="userfile[]" id="files" multiple>

                    <ul id="selectedFiles"></ul>

                </fieldset>

            </div>
        </div>
    </div>

    <?= form_close(); ?>

</div>

<a href="<?= $link; ?>" data-level="nivel2" data-edit-url="tickets/edit/" data-delete-url="tickets/delete/" class="guardar_ticket boton importante n1 <?=$nuevo?>" ><?=$txt_boton;?></a>

<script type="text/javascript">
    initEditor();
</script>