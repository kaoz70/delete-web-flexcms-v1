<h2><?=$titulo; ?><a class="cerrar" href="#" >cancelar</a></h2>
<div class="contenido_col">

    <?php

    $attributes = array('class' => 'form');
    echo form_open('admin/ubicaciones/' . $link, $attributes);

    ?>

    <div class="field">
        <div class="header">General</div>
        <div class="content">

            <div class="input">
                <label>Nombre</label>
                <input type="text" name="name" class="name required" value="<?=$nombre?>" />
            </div>
            <div class="input">
                <label>Ratio de conversi&oacute;n</label>
                <input type="text" name="exchange_rate" class="required" value="<?=$exchange_rate?>" />
            </div>
            <div class="input">
                <label>S&iacute;mbolo</label>
                <input type="text" name="symbol" class="required" value="<?=$symbol?>" />
            </div>
            <div class="input">
                <label>Separador de Mil</label>
                <input type="text" name="thousand" class="required" value="<?=$thousand?>" />
            </div>
            <div class="input">
                <label>Separador Decimal</label>
                <input type="text" name="decimal" class="required" value="<?=$decimal?>" />
            </div>
            <div class="input check">
                <input type="checkbox" value="1" name="symbol_suffix" id="symbol_suffix" <?=$status?>>
                <label for="symbol_suffix">S&iacute;mbolo es sufijo</label>
            </div>
            <div class="input check">
                <input type="checkbox" value="1" name="status" id="status" <?=$status?>>
                <label for="status">Activo</label>
            </div>

        </div>
    </div>

    <input id="id" type="hidden" name="id" value="<?=$id;?>" />

    <?= form_close(); ?>

</div>

<a href="<?= $link; ?>" data-level="nivel3" data-edit-url="cart/modificar_moneda/" data-delete-url="cart/eliminar_moneda/" class="guardar boton importante n1 no_sort <?=$nuevo?>" ><?=$txt_boton;?></a>