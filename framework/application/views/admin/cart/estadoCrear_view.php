<h2><?= $titulo; ?><a class="cerrar" href="#">cancelar</a></h2>
<div class="contenido_col" style="bottom: 71px;">

    <?php

    $attributes = array('class' => 'form');
    echo form_open('admin/estados/'.$link, $attributes);

    ?>

    <div class="field">
        <div class="header">General</div>
        <div class="content">

            <div class="input">
                <label>Nombre</label>
                <input type="text" name="name" class="name required" value="<?= $nombre ?>"/>

            </div>

            <div class="input check">
                <input id="cancelled" type="checkbox" value="1" name="cancelled" <?= $cancelled ?>>
                <label for="cancelled">Al cancelar</label>
            </div>

            <div class="input check">
                <input id="save_default" type="checkbox" value="1" name="save_default" <?= $save_default ?>>
                <label for="save_default">Al guardar</label>
            </div>

            <div class="input check">
                <input id="resave_default" type="checkbox" value="1" name="resave_default" <?= $resave_default ?>>
                <label for="resave_default">Al guardar nuevamente</label>
            </div>

        </div>
    </div>

    <?= form_close(); ?>

</div>

<a href="<?= $link; ?>" data-level="nivel4"
   data-edit-url="cart/modificar_estado/"
   data-delete-url="cart/eliminar_estado/"
   class="guardar boton importante n1 no_sort <?= $nuevo ?>"><?= $txt_boton; ?></a>