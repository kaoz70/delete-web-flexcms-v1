<?php echo form_open(current_url(), array('class' => 'form')); ?>

<div class="field">
    <div class="header">Inventario</div>
    <div class="content">
        <table class="bordered sortable" id="inventario_table">
            <thead>
            <tr>
                <th>Producto Id</th>
                <th>Codigo</th>
                <th>Nombre</th>
                <th>precio</th>
                <th>Stock</th>
            </tr>
            </thead>
            <tbody>
            <? foreach ($items as $item): ?>
                <tr>
                    <td><?= $item->prodId ?></td>
                    <td><?= $item->codigo ?></td>
                    <td><?= $item->productoNombre ?></td>
                    <td><?= $item->precio ?></td>
                    <td><?= $item->stock_quantity ?></td>
                </tr>
            <? endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<?php echo form_close(); ?>

<a href="<?= base_url('admin/cart/reportDownload/inventario')?>" class="boton importante external">Descargar</a>

<script>
    sorttable.makeSortable($("inventario_table"));
</script>