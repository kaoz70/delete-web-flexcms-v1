<?php echo form_open(current_url(), array('class' => 'form')); ?>

<div class="field">
    <div class="header">Clientes</div>
    <div class="content">
        <table class="bordered sortable" id="clientes_table">
            <thead>
            <tr>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>Email</th>
                <th>Cuidad</th>
                <th>Telefono</th>
                <th>Total</th>
            </tr>
            </thead>

            <tbody>
            <? foreach ($items as $item): ?>
                <tr>
                    <td><?= $item->first_name ?></td>
                    <td><?= $item->last_name ?></td>
                    <td><?= $item->email ?></td>
                    <td><?= $item->ciudad ?></td>
                    <td><?= $item->telef ?></td>
                    <td><?= $item->total ?></td>
                </tr>
            <? endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<?php echo form_close(); ?>

<a href="<?= base_url('admin/cart/reportDownload/clientes')?>" class="boton importante external">Descargar</a>

<script>
    sorttable.makeSortable($("clientes_table"));
</script>