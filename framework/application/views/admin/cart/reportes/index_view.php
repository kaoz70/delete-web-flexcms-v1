<h2>Reportes<a class="cerrar" href="#">cancelar</a></h2>
<div class="contenido_col print" style="width: 850px; bottom: 0">

    <ul class="tabs">
        <li class="active" data-type="ventas">Ventas</li>
        <li data-type="inventario">Inventario</li>
        <li data-type="clientes">Clientes</li>
    </ul>

    <div id="report_content">
        <?
        $data['items'] = $this->Cart->reportVentas($fecha_inicial, $fecha_final)->result();
        $this->load->view('admin/cart/reportes/ventas_view', $data);
        ?>
    </div>

</div>