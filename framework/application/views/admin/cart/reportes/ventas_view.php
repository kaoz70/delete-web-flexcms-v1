<?php echo form_open(current_url(), array('class' => 'form')); ?>

<div class="field">
    <div class="header">Ventas</div>
    <div class="content">

        <div class="w80">
            <div class="input w50 small">
                <label for="fecha_inicial">Fecha Inicial</label>
                <input type="date" id="fecha_inicial" name="fecha_inicial" value="<?= $fecha_inicial ?>">
            </div>
            <div class="input w50 small">
                <label for="fecha_final">Fecha Final</label>
                <input type="date" id="fecha_final" name="fecha_final" value="<?= $fecha_final ?>">
            </div>
        </div>
        <div class="w20">
            <a href="#"
               class="buscar boton importante"
               data-type="ventas"
               style=" width: 100% !important; line-height: 25px; margin: 0;">Buscar</a>
        </div>


        <table class="bordered sortable" id="ventas_table">
            <thead>
            <tr>
                <th>Fecha</th>
                <th>Numero</th>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>Cuidad</th>
                <th>Total</th>
                <th>Descripcion</th>
                <th>Comentarios</th>
            </tr>
            </thead>

            <tbody>
            <? foreach ($items as $item): ?>
                <tr>
                    <td><?= $item->ord_date ?></td>
                    <td><?= $item->ord_order_number ?></td>
                    <td><?= $item->ord_bill_first_name ?></td>
                    <td><?= $item->ord_bill_last_name ?></td>
                    <td><?= $item->ord_bill_city ?></td>
                    <td><?= $item->ord_item_summary_total ?></td>
                    <td><?= $item->ord_status_description ?></td>
                    <td><?= $item->ord_bill_comments ?></td>
                </tr>
            <? endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<?php echo form_close(); ?>

<a href="<?= base_url('admin/cart/reportDownload/ventas/' . $fecha_inicial . '/' . $fecha_final)?>" class="boton importante external">Descargar</a>

<script>
    sorttable.makeSortable($("ventas_table"));
</script>