<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: miguel
 * Date: 6/26/15
 * Time: 3:41 PM
 */

if ( ! function_exists('important_products'))
{
    /**
     * @param $quantity
     * @param string $view
     * @param string $imageSize
     * @param string $categoria_id
     * @param bool $subcategories
     * @return mixed
     */
    function important_products($quantity, $view = 'default_view', $imageSize = '', $categoria_id = 'todas', $subcategories = FALSE)
    {
        $CI =& get_instance();

        $catalogoPagina = $CI->Modulos->getPageByType(4, $CI->m_idioma);
        $data['paginaCatalogoUrl'] = $catalogoPagina->paginaNombreURL;

        //Search for all the important products from a category, including its child categories
        if($categoria_id != 'todas' && $subcategories) {
            $root = CatalogTree::find($categoria_id);
            $root->findChildren(999);

            $ids = flatten_tree($root);
            $ids[] = $categoria_id;

            $productos = [];

           foreach ($ids as $id) {
               $productos = array_merge($productos, $CI->Modulos->getItemsForProductosDestacados($id, $quantity, 0, $CI->m_idioma));
           }

            $data['productos'] = $productos;

        }

        //Search for ALL important products
        else {
            $data['productos'] = $CI->Modulos->getItemsForProductosDestacados($categoria_id, $quantity, 0, $CI->m_idioma);
        }

        $data['diminutivo'] = $CI->m_idioma;
        $data['imageSize'] = $imageSize;

        return $CI->load->view('modulos/catalogo/product/' . $view, $data, TRUE);

    }

    function flatten_tree($node) {

        $ids = [];

        foreach ($node->getChildren() as $cat) {
            $ids[] = $cat->id;

            if(count($cat->getChildren()) > 0) {
                $ids = array_merge($ids, flatten_tree($cat));
            }

        }

        return $ids;

    }

}

if ( ! function_exists('publications'))
{
    /**
     * @param $page_id
     * @param $quantity
     * @param string $view
     * @param string $imageSize
     * @return mixed
     */
    function publications($page_id, $quantity, $view = 'default_view', $imageSize = '')
    {
        $CI =& get_instance();

        $page = $CI->Page->getPage($page_id, $CI->m_idioma);
        $data['diminutivo'] = $CI->m_idioma;
        $data['paginaNoticiaUrl'] = $page->paginaNombreURL;
        $data['imageSize'] = $imageSize;
        $data['noticias'] = $CI->Modulos->getItemsForPublicaciones($page_id, $quantity, 0, $CI->m_idioma);
        $data['pagination'] = '';

        return $CI->load->view('modulos/publicaciones/' . $view, $data, TRUE);

    }
}

if ( ! function_exists('links'))
{
    /**
     * @param $page_id
     * @param $quantity
     * @param string $view
     * @param string $imageSize
     * @return mixed
     */
    function links($page_id, $quantity, $view = 'default_view', $imageSize = '')
    {
        $CI =& get_instance();

        $page = $CI->Page->getPage($page_id, $CI->m_idioma);
        $data['diminutivo'] = $CI->m_idioma;
        $data['paginaEnlacesUrl'] = $page->paginaNombreURL;
        $data['imageSize'] = $imageSize;
        $data['enlaces'] = $CI->Modulos->getItemsForEnlaces($page_id, $quantity, 0, $CI->m_idioma);
        $data['pagination'] = '';

        return $CI->load->view('modulos/enlaces/' . $view, $data, TRUE);

    }
}

if ( ! function_exists('address'))
{
    /**
     * @param string $address
     * @param string $view
     * @param string $imageSize
     * @return mixed
     */
    function address($address = NULL, $view = 'default_view', $imageSize = '')
    {
        $CI =& get_instance();

        //[12, 5, 4] -- Gets individual addresses
        if(is_array($address)) {
            $data['direcciones'] = $CI->Contact->getAddressById($address, $CI->m_idioma);
        }

        //get all
        else {
            $data['direcciones'] = $CI->Contact->getDirecciones($CI->m_idioma);
        }

        $parseData = array(
            'base_url' => base_url(),
            'asset_url' => base_url('themes/' . $CI->m_config->theme) . '/',
        );

        foreach($data['direcciones'] as $dir) {
            $dir->contactoDireccion = $CI->parser->parse_string(auto_link($dir->contactoDireccion, 'email'), $parseData, TRUE);
        }

        $data['diminutivo'] = $CI->m_idioma;
        $data['imageSize'] = $imageSize;
        $data['pagination'] = '';

        return $CI->load->view('modulos/contacto/direcciones/' . $view, $data, TRUE);

    }
}

if ( ! function_exists('article'))
{
    /**
     * @param $article_id
     * @param string $view
     * @return mixed
     */
    function article($article_id, $view = 'default_view')
    {
        $CI =& get_instance();

        $data['articulo'] = $CI->Modulos->articulo($article_id, $CI->m_idioma);
        $data['diminutivo'] = $CI->m_idioma;

        return $CI->load->view('modulos/articulo/' . $view, $data, TRUE);

    }
}

if ( ! function_exists('page_articles'))
{
    /**
     * @param $page_id
     * @param string $view
     * @return mixed
     */
    function page_articles($page_id, $view = 'default_view')
    {
        $CI =& get_instance();

        $data['articulos'] = $CI->Articles->getByPage($page_id, $CI->m_idioma);
        $data['diminutivo'] = $CI->m_idioma;

        return $CI->load->view('modulos/articulo/' . $view, $data, TRUE);

    }
}

if ( ! function_exists('services_menu'))
{
    /**
     * @param $page_id
     * @param string $view
     * @return mixed
     */
    function services_menu($page_id, $page, $view = 'services_view')
    {
        $CI =& get_instance();

        $data['page'] = $page;
        $data['diminutivo'] = $CI->m_idioma;
        $data['servicios'] = $CI->Servicios->getByPage($page_id, $CI->m_idioma);
        $data['path'] = [];

        foreach ($data['servicios'] as $servicio) {
            if($CI->uri->segment(3) == $servicio->servicioUrl) {
                $data['path'][] = $servicio->servicioId;
            }
        }

        if($data['servicios']) {
            return $CI->load->view('menu/' . $view, $data, TRUE);
        } else {
            return NULL;
        }

    }
}

if ( ! function_exists('services'))
{
    /**
     * @param $page_id
     * @param string $view
     * @param int $amount
     * @param string $imageSize
     * @param bool $featured
     * @return null
     */
    function services($page_id, $view = 'default_view', $amount = 9999, $imageSize = '', $featured = FALSE)
    {
        $CI =& get_instance();

        $data['imageSize'] = $imageSize;
        $data['diminutivo'] = $CI->m_idioma;
        $data['servicios'] = $CI->Servicios->getByPage($page_id, $CI->m_idioma, $featured, $amount);

        if($data['servicios']) {
            return $CI->load->view('modulos/servicios/' . $view, $data, TRUE);
        } else {
            return NULL;
        }

    }
}

if ( ! function_exists('form'))
{
    /**
     * @param string $view
     * @return mixed
     */
    function form($view = 'default_view')
    {
        $CI =& get_instance();

        $data['campos'] = $CI->Contact->getContactoInputs($CI->m_idioma);

        return $CI->load->view('modulos/contacto/formulario/' . $view, $data, true);

    }
}

if ( ! function_exists('breadcrumbs'))
{
    /**
     * @param string $view
     * @return mixed
     */
    function breadcrumbs($view = 'breadcrumbs_view')
    {
        $CI =& get_instance();
        $data['lang'] = $CI->m_idioma;
        $data['pagina_url'] = $CI->page->paginaNombreURL;
        $data['breadcrumbs'] = $CI->m_breadcrumbs;
        return $CI->load->view('modulos/' . $view, $data, true);
    }
}

if ( ! function_exists('gallery'))
{

    /**
     * @param $category_id
     * @param int $cantidad
     * @param string $imageSize
     * @param string $view
     * @return string|void
     */
    function gallery($category_id, $view = 'items_view', $cantidad = 99999, $imageSize = '')
    {
        $CI =& get_instance();

        $galeriaPagina = $CI->Modulos->getGaleria($category_id);
        $categoriaGaleria = $CI->Galeria->getCategoria($category_id);

        if($categoriaGaleria && $categoriaGaleria->descargaCategoriaPrivada && !Sentinel::check()) {
            return;
        }

        $pagina = $CI->Modulos->getPageByType(6, $CI->m_idioma);
        $galeriaCol = $CI->Modulos->getItemsForGaleria($category_id, $cantidad, 0, $CI->m_idioma);

        $moduleData['paginaGaleriaUrl'] = '';
        $moduleData['galeria'] = $galeriaCol;

        if(count($galeriaPagina) != 0) {
            if(count($pagina) > 0) {
                $moduleData['paginaGaleriaUrl'] = $pagina->paginaNombreURL;
            } else {
                return '<div data-alert class="alert-box alert">Cree una p&aacute;gina de Galeria para poder ver este m&oacute;dulo</div>';
            }
        }

        $tree = GalleryTree::find($category_id);
        $tree->lang = $CI->m_idioma;
        $tree->findChildren(9999);
        $moduleData['categorias'] = $tree->getChildren();
        $moduleData['category'] = $CI->Descargas->getCategory($category_id, $CI->m_idioma);

        $moduleData['fechas'] = array();
        $fechas = $CI->Descargas->getDates($category_id);

        foreach ($fechas as $key => $value) {
            $fecha = new DateTime(trim($value->descargaFecha));
            $fechaString = $fecha->format('Y');
            array_push($moduleData['fechas'], $fechaString);
        }

        $moduleData['imageSize'] = $imageSize;

        $CI->load->view('modulos/galeria/' . $view, $moduleData);

    }

}

if ( ! function_exists('page'))
{

    function page($id)
    {
        $CI =& get_instance();
        return $CI->Page->getPage((int) $id, $CI->m_idioma);
    }

}


// ------------------------------------------------------------------------
/* End of file module_helper.php */
/* Location: ./system/helpers/module_helper.php */