<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('check_stock'))
{


	/**
	 * @param $product_id
	 * @param $quantity
	 *
	 * @return string
	 */
	function check_stock($product_id, $quantity, $show_if_stock = FALSE)
	{
		$CI =& get_instance();
		$product = $CI->Catalog->getProduct((int)$product_id);
		$out_of_stock_limit = 10;

		//Stock exists
		if($product->stock_quantity && ($product->stock_quantity > $quantity) && $product->stock_quantity > $out_of_stock_limit) {
			if($show_if_stock) {
				$message = 'Este producto se encuentra en stock';
				$icon = '<i class="fa fa-check success" data-toggle="tooltip" data-placement="right" title="' . $message . '"></i>';
			} else {
				$message = '';
				$icon = '';
			}
			$code = 1;
		}

		//Almost no stock
		else if ($product->stock_quantity == $quantity || $product->stock_quantity && ($product->stock_quantity < $out_of_stock_limit)) {
			$message = 'Este producto est&aacute; por quedarse fuera de stock!';
			$icon = '<i class="fa fa-exclamation warning" data-toggle="tooltip" data-placement="right" title="' . $message . '"></i>';
			$code = 2;
		}

		//No stock
		else {
			$message = 'Este producto se qued&oacute; sin stock! Se eliminar&aacute; del carrito';
			$icon = '<i class="fa fa-close error" data-toggle="tooltip" data-placement="right" title="' . $message . '"></i>';
			$code = 0;
		}

		$return = new stdClass();
		$return->code = $code;
		$return->message = $message;
		$return->icon = $icon;

		return $return;

	}

}
