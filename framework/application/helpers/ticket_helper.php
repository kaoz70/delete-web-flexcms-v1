<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Version helpers
 * The function names are pretty self-explanatory
 *
 * @author 		Miguel Suarez <miguelsuarez70@gmail.com>
 * @copyright 	Copyright (c) 2014, Miguel Suarez
 * @version 	1.0.0
 */


if ( ! function_exists('ticket_class')) {
    function ticket_class($index)
    {
        $arr = [
            'alert',
            'warning',
            'success',
        ];
        return $arr[$index - 1];
    }
}

/* End of file versions_helper.php */