<?php

$lang['ticket_status_pending_support'] = 'En espera de respuesta de soporte';
$lang['ticket_status_pending_user'] = 'En espera de respuesta del cliente';
$lang['ticket_status_closed_support'] = 'Cerrado por soporte';
$lang['ticket_status_closed_user'] = 'Cerrado por cliente';
$lang['ticket_open'] = 'abierto aqu&iacute;';
$lang['ticket_close'] = 'cerrado aqu&iacute;';

$lang['ticket_priority_1'] = 'Alta';
$lang['ticket_priority_2'] = 'Media';
$lang['ticket_priority_3'] = 'Baja';

$lang['ticket_level_1'] = 'Afecta a negocio';
$lang['ticket_level_2'] = 'Afecta a negocio2';
$lang['ticket_level_3'] = 'Afecta a negocio3';

$lang['ticket_type_1'] = 'Incidencia';
$lang['ticket_type_2'] = 'Incidencia2';
$lang['ticket_type_3'] = 'Incidencia3';

