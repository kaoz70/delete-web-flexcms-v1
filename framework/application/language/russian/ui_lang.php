<?php

$lang['ui_read_more'] = 'читать больше';
$lang['ui_close'] = 'закрыть';
$lang['ui_view'] = 'views';
$lang['ui_view_all'] = 'views all';
$lang['ui_view_product'] = 'views product';
$lang['ui_button_send'] = 'отправить';
$lang['ui_button_reset'] = 'удалить';
$lang['ui_button_register'] = 'Зарегистрироваться';
$lang['ui_button_modify'] = 'изменить';
$lang['ui_message_no_cart'] = 'Вы можете сделать заказ напрямую со страницы. <a href="%s">Нажмите здесь чтобы перейти к списку продуктов</a>';
$lang['ui_visits'] = 'visits';
$lang['ui_quantity'] = 'количество:';
/*
 * AUTHENTICATION
 */
$lang['ui_auth_header_profile'] = 'Ваш аккаунт';
$lang['ui_auth_header_login'] = 'Логин';
$lang['ui_auth_header_pregister'] = 'Зарегестрироваться ';
$lang['ui_auth_username'] = 'Электронный адрес';
$lang['ui_auth_password'] = 'Пароль';
$lang['ui_auth_email'] = 'Электронный адрес';
$lang['ui_auth_rememeber'] = 'Запомнить мои данные';
$lang['ui_auth_forgot_password_header_text'] = '<h2>Забыли пароль?</h2><p>Пожалуйста, введите свой электронный адрес</p>';
$lang['ui_auth_forgot_password'] = 'Забыли свой пароль?';
$lang['ui_auth_login'] = 'Войти';
$lang['ui_auth_logout'] = 'выйти';
$lang['ui_auth_create_account'] = 'Создать акаунт';
$lang['ui_auth_are_you_new'] = 'Вы новичок на этом сайте?';
$lang['ui_auth_create_account'] = 'Создать акаунт';

$lang['ui_download'] = 'download';
$lang['ui_contact_form_error'] = '<div class="error">При заполнении формуляра была допущена ошибка, пожалуйста, проверьте поля.</div>';
$lang['ui_contact_sent'] = '<div class="success">Ваше сообщение было отправлено!</div>';
$lang['ui_contact_error'] = '<div class="error">При отправке сообщения возникла ошибка, пожалуйста, попробуйте позднее</div>';
$lang['ui_contact_sending'] = 'Ваше сообщение успешно отправлено...';
$lang['ui_contact_form_header'] = 'Обратная связь';
$lang['ui_follow_us'] = 'Подпишитесь на нас:';
$lang['ui_view_cart'] = 'Смотреть мои покупки';
$lang['ui_cart_number'] = 'номер заказа:';
$lang['ui_top'] = 'загрузить';
$lang['ui_back'] = '&lt; вернуться';
$lang['ui_click_to_zoom'] = '+нажмите на изображение чтобы увеличить';

/*
 * SHOPPING CART
 */
$lang['ui_cart_add'] = 'добавить к моим покупкам';
$lang['ui_cart_add_success'] = 'днный продукт был добавлен в вашу тележку';
$lang['ui_cart_conditions_title'] = 'Условия поставки:';
$lang['ui_cart_conditions_text'] = 'Указанная цена относится только к самому продукту. Менеджер по продажам свяжется с вами и сообщит стоимость включая перевозку.';
$lang['ui_cart_units'] = 'Количество стеблей в коробке';
$lang['ui_cart_final_price'] = 'Окончательная цена:';
$lang['ui_cart_update'] = 'Актуализировать изменения';
$lang['ui_cart_delete'] = 'Удалить продукт';
$lang['ui_cart_total'] = 'Всего к оплате:';
$lang['ui_cart_continue'] = '&lt; продолжить закупку';
$lang['ui_cart_checkout'] = 'подтвердить заказ';
$lang['ui_cart_header_client_data'] = 'Информация о клиенте';
$lang['ui_cart_form_footer'] = 'Пожалуйста, заполните форму, чтобы мы смогли связаться с Вами для выполнения Вашего заказа  или перейдите в наш каталог и продолжайте закупку.';
