<?php

$lang['ui_read_more'] = 'read more';
$lang['ui_view'] = 'views';
$lang['ui_view_all'] = 'views all';
$lang['ui_view_product'] = 'views product';
$lang['ui_button_send'] = 'send';
$lang['ui_button_reset'] = 'clear';
$lang['ui_message_no_cart'] = 'Necesita hacer el pedido desde la página del producto que desee. <a href="%s">Haga click aquí para ir a la página de productos</a>';
$lang['ui_visits'] = 'visits';
$lang['ui_auth_username'] = 'Username';
$lang['ui_auth_password'] = 'Password';
$lang['ui_auth_email'] = 'Email';
$lang['ui_auth_rememeber'] = 'Remember me';
$lang['ui_auth_forgot_password_header_text'] = '<h2>Forgot password</h2><p>Please enter you email</p>';
$lang['ui_auth_forgot_password'] = 'Forgot your password?';
$lang['ui_auth_login'] = 'Log in';
$lang['ui_download'] = 'download';
$lang['ui_search'] = 'Rechercher';
$lang['ui_search_no_result'] = "Aucun résultat, essayer avec d'autres termes";
