<?php

$lang['ui_read_more'] = 'leia mais';
$lang['ui_view'] = 'ver';
$lang['ui_view_all'] = 'views all';
$lang['ui_view_product'] = 'ver todos';
$lang['ui_button_send'] = 'enviar';
$lang['ui_button_reset'] = 'limpar';
$lang['ui_message_no_cart'] = 'Precisa fim da página do produto desejado. <a href="%s">Clique aqui para ir para a página do produto</a>';
$lang['ui_visits'] = 'visitas';
$lang['ui_auth_username'] = 'Usuário';
$lang['ui_auth_password'] = 'Senha';
$lang['ui_auth_email'] = 'Email';
$lang['ui_auth_rememeber'] = 'Lembre-se de mim';
$lang['ui_auth_forgot_password_header_text'] = '<h2>Esqueceu sua senha</h2><p>Por favor, indique que você enviar e-mail</p>';
$lang['ui_auth_forgot_password'] = 'Esqueceu sua senha?';
$lang['ui_auth_login'] = 'Entrar';
$lang['ui_download'] = 'baixar';
