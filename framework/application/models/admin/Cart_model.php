<?php

class Cart_model extends CI_Model
{

    ###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###
    // CART ORDERS
    ###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###

    function update_order_details($order_number)
    {
        // Update order status.
        $sql_update = array($this->flexi_cart_admin->db_column('order_summary', 'status') => $this->input->post('update_status'));

        $this->flexi_cart_admin->update_db_order_summary($sql_update, $order_number);

        ### ++++++++++ ###

        // Update shipped and cancelled item quantities.
        foreach($this->input->post('update_details') as $id => $row)
        {
            $sql_update = array();

            // Check that the 'Quantity Shipped' input field was submitted (Incase the field was disabled).
            if (isset($row['quantity_shipped']))
            {
                $sql_update[$this->flexi_cart_admin->db_column('order_details', 'item_quantity_shipped')] = $row['quantity_shipped'];
            }

            // Check that the 'Quantity Cancelled' input field was submitted (Incase the field was disabled).
            if (isset($row['quantity_cancelled']))
            {
                $sql_update[$this->flexi_cart_admin->db_column('order_details', 'item_quantity_cancelled')] = $row['quantity_cancelled'];
            }

            if (! empty($sql_update))
            {
                $this->flexi_cart_admin->update_db_order_details($sql_update, $row['id']);
            }
        }

        echo $this->flexi_cart_admin->get_messages('admin');
    }

    function createUbicacion()
    {

        $sql_insert = array(
            $this->flexi_cart_admin->db_column('location_type', 'name') => '',
            $this->flexi_cart_admin->db_column('location_type', 'parent') => ''
        );

        $this->flexi_cart_admin->insert_db_location_type($sql_insert);

        return $this->db->insert_id();

    }

    function update_location_types($id)
    {

        $sql_update = array(
            $this->flexi_cart_admin->db_column('location_type', 'name') => $this->input->post('name'),
            $this->flexi_cart_admin->db_column('location_type', 'parent') => $this->input->post('parent_location'),
            'loc_type_temporary' => 0
        );

        $this->flexi_cart_admin->update_db_location_type($sql_update, $id);

    }

    function getUbicacion($id)
    {
        $query = $this->db
            ->where('loc_type_id', $id)
            ->get('location_type');
        return $query->row();
    }

    function insert_location($location_type_id)
    {

        $sql_insert = array(
            $this->flexi_cart_admin->db_column('locations', 'type') => $location_type_id,
            $this->flexi_cart_admin->db_column('locations', 'name') => $this->input->post('name'),
            $this->flexi_cart_admin->db_column('locations', 'parent') => $this->input->post('parent_location'),
            $this->flexi_cart_admin->db_column('locations', 'shipping_zone') => $this->input->post('shipping_zone'),
            $this->flexi_cart_admin->db_column('locations', 'tax_zone') => $this->input->post('tax_zone'),
            $this->flexi_cart_admin->db_column('locations', 'status') => $this->input->post('status'),
        );

        return $this->flexi_cart_admin->insert_db_location($sql_insert);

    }

    function getSubUbicacion($id)
    {
        $query = $this->db
            ->where('loc_id', $id)
            ->get('locations');
        return $query->row();
    }

    function getSubUbicaciones($parent_id)
    {
        $query = $this->db
            ->where('loc_parent_fk', $parent_id)
            ->get('locations');
        return $query->row();
    }

    function update_location($id)
    {

        $sql_update = array(
            $this->flexi_cart_admin->db_column('locations', 'name') => $this->input->post('name'),
            $this->flexi_cart_admin->db_column('locations', 'parent') => $this->input->post('parent_location'),
            $this->flexi_cart_admin->db_column('locations', 'shipping_zone') => $this->input->post('shipping_zone'),
            $this->flexi_cart_admin->db_column('locations', 'tax_zone') => $this->input->post('tax_zone'),
            $this->flexi_cart_admin->db_column('locations', 'status') => $this->input->post('status')
        );

        $this->flexi_cart_admin->update_db_location($sql_update, $id);

    }

    function insert_zone()
    {

        $sql_insert = array(
            $this->flexi_cart_admin->db_column('location_zones', 'name') => $this->input->post('name'),
            $this->flexi_cart_admin->db_column('location_zones', 'description') => $this->input->post('desc'),
            $this->flexi_cart_admin->db_column('location_zones', 'status') => $this->input->post('status')
        );

        return $this->flexi_cart_admin->insert_db_location_zone($sql_insert);

    }

    function getZone($id)
    {
        $query = $this->db
            ->where('lzone_id', $id)
            ->get('location_zones');
        return $query->row();
    }

    function update_zone($id)
    {
        $sql_update = array(
            $this->flexi_cart_admin->db_column('location_zones', 'name') => $this->input->post('name'),
            $this->flexi_cart_admin->db_column('location_zones', 'description') => $this->input->post('desc'),
            $this->flexi_cart_admin->db_column('location_zones', 'status') => $this->input->post('status')
        );

        $this->flexi_cart_admin->update_db_location_zone($sql_update, $id);
    }

    function createShippingOption()
    {

        $sql_insert = array(
            $this->flexi_cart_admin->db_column('shipping_options', 'name') => '',
        );

        return $this->flexi_cart_admin->insert_db_shipping($sql_insert);
    }

    function update_shipping($id)
    {
        $sql_insert = array(
            $this->flexi_cart_admin->db_column('shipping_options', 'name') => $this->input->post('name'),
            $this->flexi_cart_admin->db_column('shipping_options', 'description') => $this->input->post('desc'),
            $this->flexi_cart_admin->db_column('shipping_options', 'location') => $this->input->post('parent_location'),
            $this->flexi_cart_admin->db_column('shipping_options', 'zone') => $this->input->post('zone'),
            $this->flexi_cart_admin->db_column('shipping_options', 'inc_sub_locations') => $this->input->post('inc_sub_locations'),
            $this->flexi_cart_admin->db_column('shipping_options', 'tax_rate') => $this->input->post('tax_rate'),
            $this->flexi_cart_admin->db_column('shipping_options', 'discount_inclusion') => $this->input->post('discount_inclusion'),
            $this->flexi_cart_admin->db_column('shipping_options', 'status') => $this->input->post('status'),
            'ship_temporal' => 0,
        );

        return $this->flexi_cart_admin->update_db_shipping($sql_insert, $id);
    }

    function getShippingOption($id)
    {
        $query = $this->db
            ->where('ship_id', $id)
            ->get('shipping_options');
        return $query->row();
    }

    function insert_shipping_rate($shipping_id)
    {
        $sql_insert = array(
            $this->flexi_cart_admin->db_column('shipping_rates', 'parent') => $shipping_id,
            $this->flexi_cart_admin->db_column('shipping_rates', 'value') => $this->input->post('value'),
            $this->flexi_cart_admin->db_column('shipping_rates', 'tare_weight') => $this->input->post('tare_weight'),
            $this->flexi_cart_admin->db_column('shipping_rates', 'min_weight') => $this->input->post('min_weight'),
            $this->flexi_cart_admin->db_column('shipping_rates', 'max_weight') => $this->input->post('max_weight'),
            $this->flexi_cart_admin->db_column('shipping_rates', 'min_value') => $this->input->post('min_value'),
            $this->flexi_cart_admin->db_column('shipping_rates', 'max_value') => $this->input->post('max_value'),
            $this->flexi_cart_admin->db_column('shipping_rates', 'status') => $this->input->post('status')
        );

        return $this->flexi_cart_admin->insert_db_shipping_rate($sql_insert);
    }

    function getTarifaEnvio($id)
    {
        $query = $this->db
            ->where('ship_rate_id', $id)
            ->get('shipping_rates');
        return $query->row();
    }

    function update_shipping_rate($id)
    {
        $sql_update = array(
            $this->flexi_cart_admin->db_column('shipping_rates', 'value') => $this->input->post('value'),
            $this->flexi_cart_admin->db_column('shipping_rates', 'tare_weight') => $this->input->post('tare_weight'),
            $this->flexi_cart_admin->db_column('shipping_rates', 'min_weight') => $this->input->post('min_weight'),
            $this->flexi_cart_admin->db_column('shipping_rates', 'max_weight') => $this->input->post('max_weight'),
            $this->flexi_cart_admin->db_column('shipping_rates', 'min_value') => $this->input->post('min_value'),
            $this->flexi_cart_admin->db_column('shipping_rates', 'max_value') => $this->input->post('max_value'),
            $this->flexi_cart_admin->db_column('shipping_rates', 'status') => $this->input->post('status')
        );

        $this->flexi_cart_admin->update_db_shipping_rate($sql_update, $id);
    }

    function insert_tax()
    {
        $sql_insert = array(
            $this->flexi_cart_admin->db_column('tax', 'name') => $this->input->post('name'),
            $this->flexi_cart_admin->db_column('tax', 'location') => $this->input->post('parent_location'),
            $this->flexi_cart_admin->db_column('tax', 'zone') => $this->input->post('zone'),
            $this->flexi_cart_admin->db_column('tax', 'rate') => $this->input->post('rate'),
            $this->flexi_cart_admin->db_column('tax', 'status') => $this->input->post('status')
        );

        return $this->flexi_cart_admin->insert_db_tax($sql_insert);
    }

    function getTax($id)
    {
        $query = $this->db
            ->where('tax_id', $id)
            ->get('tax');
        return $query->row();
    }

    function update_tax($id)
    {
        $sql_update = array(
            $this->flexi_cart_admin->db_column('tax', 'name') => $this->input->post('name'),
            $this->flexi_cart_admin->db_column('tax', 'location') => $this->input->post('parent_location'),
            $this->flexi_cart_admin->db_column('tax', 'zone') => $this->input->post('zone'),
            $this->flexi_cart_admin->db_column('tax', 'rate') => $this->input->post('rate'),
            $this->flexi_cart_admin->db_column('tax', 'status') => $this->input->post('status')
        );

        $this->flexi_cart_admin->update_db_tax($sql_update, $id);
    }

    function insert_discount()
    {
        $this->load->library('form_validation');

        // Set validation rules.
        $this->form_validation->set_rules('insert[type]', 'Discount Type', 'greater_than[0]');
        $this->form_validation->set_rules('insert[method]', 'Discount Method', 'required|greater_than[0]');
        $this->form_validation->set_rules('insert[usage_limit]', 'Usage Limit', 'required');
        $this->form_validation->set_rules('insert[valid_date]', 'Valid Date', 'required');
        $this->form_validation->set_rules('insert[expire_date]', 'Expire Date', 'required');

        // The following fields are not validated, however must be included as done below or their data will not be repopulated by CI.
        $this->form_validation->set_rules('insert[tax_method]');
        $this->form_validation->set_rules('insert[location]');
        $this->form_validation->set_rules('insert[zone]');
        $this->form_validation->set_rules('insert[group]');
        $this->form_validation->set_rules('insert[item]');
        $this->form_validation->set_rules('insert[code]');
        $this->form_validation->set_rules('insert[description]');
        $this->form_validation->set_rules('insert[quantity_required]');
        $this->form_validation->set_rules('insert[quantity_discounted]');
        $this->form_validation->set_rules('insert[value_required]');
        $this->form_validation->set_rules('insert[value_discounted]');
        $this->form_validation->set_rules('insert[recursive]');
        $this->form_validation->set_rules('insert[unique]');
        $this->form_validation->set_rules('insert[void_reward]');
        $this->form_validation->set_rules('insert[force_shipping]');
        $this->form_validation->set_rules('insert[custom_status_1]');
        $this->form_validation->set_rules('insert[custom_status_2]');
        $this->form_validation->set_rules('insert[custom_status_3]');
        $this->form_validation->set_rules('insert[order_by]');

        // Validate fields.
        if ($this->form_validation->run())
        {
            $row = $this->input->post('insert');

            $sql_insert = array(
                $this->flexi_cart_admin->db_column('discounts', 'type') => $row['type'],
                $this->flexi_cart_admin->db_column('discounts', 'method') => $row['method'],
                $this->flexi_cart_admin->db_column('discounts', 'tax_method') => $row['tax_method'],
                $this->flexi_cart_admin->db_column('discounts', 'location') => $row['location'],
                $this->flexi_cart_admin->db_column('discounts', 'zone') => $row['zone'],
                $this->flexi_cart_admin->db_column('discounts', 'group') => $row['group'],
                $this->flexi_cart_admin->db_column('discounts', 'item') => $row['item'],
                $this->flexi_cart_admin->db_column('discounts', 'code') => $row['code'],
                $this->flexi_cart_admin->db_column('discounts', 'description') => $row['description'],
                $this->flexi_cart_admin->db_column('discounts', 'quantity_required') => $row['quantity_required'],
                $this->flexi_cart_admin->db_column('discounts', 'quantity_discounted') => $row['quantity_discounted'],
                $this->flexi_cart_admin->db_column('discounts', 'value_required') => $row['value_required'],
                $this->flexi_cart_admin->db_column('discounts', 'value_discounted') => $row['value_discounted'],
                $this->flexi_cart_admin->db_column('discounts', 'recursive') => $row['recursive'],
                $this->flexi_cart_admin->db_column('discounts', 'non_combinable') => $row['non_combinable'],
                $this->flexi_cart_admin->db_column('discounts', 'void_reward_points') => $row['void_reward'],
                $this->flexi_cart_admin->db_column('discounts', 'force_shipping_discount') => $row['force_shipping'],
                $this->flexi_cart_admin->db_column('discounts', 'custom_status_1') => $row['custom_status_1'],
                $this->flexi_cart_admin->db_column('discounts', 'custom_status_2') => $row['custom_status_2'],
                $this->flexi_cart_admin->db_column('discounts', 'custom_status_3') => $row['custom_status_3'],
                $this->flexi_cart_admin->db_column('discounts', 'usage_limit') => $row['usage_limit'],
                $this->flexi_cart_admin->db_column('discounts', 'valid_date') => $row['valid_date'],
                $this->flexi_cart_admin->db_column('discounts', 'expire_date') => $row['expire_date'],
                $this->flexi_cart_admin->db_column('discounts', 'status') => $row['status'],
                $this->flexi_cart_admin->db_column('discounts', 'order_by') => $row['order_by']
            );

            return $this->flexi_cart_admin->insert_db_discount($sql_insert);
        }
        else
        {
            echo validation_errors('<p class="error_msg">', '</p>');
            return FALSE;
        }
    }

    function getDescuento($id)
    {
        $query = $this->db
            ->where('disc_id', $id)
            ->get('discounts');
        return $query->row();
    }

    function update_discount($discount_id)
    {
        $this->load->library('form_validation');

        // Set validation rules.
        $this->form_validation->set_rules('insert[type]', 'Discount Type', 'greater_than[0]');
        $this->form_validation->set_rules('insert[method]', 'Discount Method', 'greater_than[0]');
        $this->form_validation->set_rules('insert[usage_limit]', 'Usage Limit', 'required');
        $this->form_validation->set_rules('insert[valid_date]', 'Valid Date', 'required');
        $this->form_validation->set_rules('insert[expire_date]', 'Expire Date', 'required');

        // The following fields are not validated, however must be included as done below or their data will not be repopulated by CI.
        $this->form_validation->set_rules('insert[tax_method]');
        $this->form_validation->set_rules('insert[location]');
        $this->form_validation->set_rules('insert[zone]');
        $this->form_validation->set_rules('insert[group]');
        $this->form_validation->set_rules('insert[item]');
        $this->form_validation->set_rules('insert[code]');
        $this->form_validation->set_rules('insert[description]');
        $this->form_validation->set_rules('insert[quantity_required]');
        $this->form_validation->set_rules('insert[quantity_discounted]');
        $this->form_validation->set_rules('insert[value_required]');
        $this->form_validation->set_rules('insert[value_discounted]');
        $this->form_validation->set_rules('insert[recursive]');
        $this->form_validation->set_rules('insert[unique]');
        $this->form_validation->set_rules('insert[void_reward]');
        $this->form_validation->set_rules('insert[force_shipping]');
        $this->form_validation->set_rules('insert[custom_status_1]');
        $this->form_validation->set_rules('insert[custom_status_2]');
        $this->form_validation->set_rules('insert[custom_status_3]');
        $this->form_validation->set_rules('insert[order_by]');

        // Validate fields.
        if ($this->form_validation->run())
        {
            $row = $this->input->post('insert');

            $sql_update = array(
                $this->flexi_cart_admin->db_column('discounts', 'type') => $row['type'],
                $this->flexi_cart_admin->db_column('discounts', 'method') => $row['method'],
                $this->flexi_cart_admin->db_column('discounts', 'tax_method') => $row['tax_method'],
                $this->flexi_cart_admin->db_column('discounts', 'location') => $row['location'],
                $this->flexi_cart_admin->db_column('discounts', 'zone') => $row['zone'],
                $this->flexi_cart_admin->db_column('discounts', 'group') => $row['group'],
                $this->flexi_cart_admin->db_column('discounts', 'item') => $row['item'],
                $this->flexi_cart_admin->db_column('discounts', 'code') => $row['code'],
                $this->flexi_cart_admin->db_column('discounts', 'description') => $row['description'],
                $this->flexi_cart_admin->db_column('discounts', 'quantity_required') => $row['quantity_required'],
                $this->flexi_cart_admin->db_column('discounts', 'quantity_discounted') => $row['quantity_discounted'],
                $this->flexi_cart_admin->db_column('discounts', 'value_required') => $row['value_required'],
                $this->flexi_cart_admin->db_column('discounts', 'value_discounted') => $row['value_discounted'],
                $this->flexi_cart_admin->db_column('discounts', 'recursive') => $row['recursive'],
                $this->flexi_cart_admin->db_column('discounts', 'non_combinable') => $row['non_combinable'],
                $this->flexi_cart_admin->db_column('discounts', 'void_reward_points') => $row['void_reward'],
                $this->flexi_cart_admin->db_column('discounts', 'force_shipping_discount') => $row['force_shipping'],
                $this->flexi_cart_admin->db_column('discounts', 'custom_status_1') => $row['custom_status_1'],
                $this->flexi_cart_admin->db_column('discounts', 'custom_status_2') => $row['custom_status_2'],
                $this->flexi_cart_admin->db_column('discounts', 'custom_status_3') => $row['custom_status_3'],
                $this->flexi_cart_admin->db_column('discounts', 'usage_limit') => $row['usage_limit'],
                $this->flexi_cart_admin->db_column('discounts', 'valid_date') => $row['valid_date'],
                $this->flexi_cart_admin->db_column('discounts', 'expire_date') => $row['expire_date'],
                $this->flexi_cart_admin->db_column('discounts', 'status') => isset($row['status']) ? 1 : 0,
                $this->flexi_cart_admin->db_column('discounts', 'order_by') => $row['order_by']
            );

            $this->flexi_cart_admin->update_db_discount($sql_update, $discount_id);

            return $this->flexi_cart_admin->get_messages('admin');

        }
        else
        {
            echo validation_errors('<p class="error_msg">', '</p>');
            return FALSE;
        }
    }

    function insert_discount_group()
    {

        $sql_insert = array(
            $this->flexi_cart_admin->db_column('discount_groups', 'name') => '',
            $this->flexi_cart_admin->db_column('discount_groups', 'status') => 1
        );

        return $this->flexi_cart_admin->insert_db_discount_group($sql_insert);
    }

    function update_discount_groups($id)
    {
        $sql_update = array(
            $this->flexi_cart_admin->db_column('discount_groups', 'name') => $this->input->post('name'),
            $this->flexi_cart_admin->db_column('discount_groups', 'status') => $this->input->post('status'),
            'disc_group_temporary' => 0,
        );

        $this->flexi_cart_admin->update_db_discount_group($sql_update, $id);
    }

    function getGrupoDescuento($id)
    {
        $query = $this->db
            ->where('disc_group_id', $id)
            ->get('discount_groups');
        return $query->row();
    }

    function getGrupoDescuentoItems($id)
    {
        $query = $this->db
            ->where('disc_group_item_group_fk', $id)
            ->join('productos', 'productos.productoId = discount_group_items.disc_group_item_item_fk')
            ->join('es_productos', 'es_productos.productoId = productos.productoId')
            ->join('producto_categorias', 'producto_categorias.id = productos.categoriaId')
            ->join('es_producto_categorias', 'es_producto_categorias.productoCategoriaId = producto_categorias.id')
            ->get('discount_group_items');
        return $query->result();
    }

    function insert_discount_group_items($group_id)
    {

        $ids = json_decode($this->input->post('seccionesAdmin'));

        //Delete any items already there
        $sql_where = array($this->flexi_cart_admin->db_column('discount_group_items', 'group') => $group_id);
        $this->flexi_cart_admin->delete_db_discount_group_item($sql_where);

        //Insert the items
        foreach($ids as $id)
        {
            $sql_insert = array(
                $this->flexi_cart_admin->db_column('discount_group_items', 'group') => $group_id,
                $this->flexi_cart_admin->db_column('discount_group_items', 'item') => $id
            );

            $this->flexi_cart_admin->insert_db_discount_group_item($sql_insert);
        }
    }

    /**
     * Inserts products into a discount group by only passing the category ids
     *
     * @param $group_id
     */
    function insert_discount_group_items_categories($group_id)
    {

        $this->load->model('admin/catalogo_model', 'Catalogo');

        //Delete any items already there
        $sql_where = array($this->flexi_cart_admin->db_column('discount_group_items', 'group') => $group_id);
        $this->flexi_cart_admin->delete_db_discount_group_item($sql_where);

        $categories = $this->input->post('category');

        if(!$categories) {
            return;
        }

        //Get the products of each category
        foreach ($categories as $id) {

            $products = $this->Catalogo->getProductos($id);

            //Assign each product to the discount group
            foreach ($products as $prod) {

                $sql_insert = array(
                    $this->flexi_cart_admin->db_column('discount_group_items', 'group') => $group_id,
                    $this->flexi_cart_admin->db_column('discount_group_items', 'item') => $prod->id
                );

                $this->flexi_cart_admin->insert_db_discount_group_item($sql_insert);

            }

        }

    }

    function insert_currency()
    {
        $sql_insert = array(
            $this->flexi_cart_admin->db_column('currency', 'name') => $this->input->post('name'),
            $this->flexi_cart_admin->db_column('currency', 'exchange_rate') => $this->input->post('exchange_rate'),
            $this->flexi_cart_admin->db_column('currency', 'symbol') => $this->input->post('symbol'),
            $this->flexi_cart_admin->db_column('currency', 'symbol_suffix') => $this->input->post('symbol_suffix'),
            $this->flexi_cart_admin->db_column('currency', 'thousand_separator') => $this->input->post('thousand'),
            $this->flexi_cart_admin->db_column('currency', 'decimal_separator') => $this->input->post('decimal'),
            $this->flexi_cart_admin->db_column('currency', 'status') => $this->input->post('status')
        );

        return $this->flexi_cart_admin->insert_db_currency($sql_insert);
    }

    function getMoneda($id)
    {
        $query = $this->db
            ->where('curr_id', $id)
            ->get('currency');

        return $query->row();
    }

    function update_currency($id)
    {
        $sql_update = array(
            $this->flexi_cart_admin->db_column('currency', 'name') => $this->input->post('name'),
            $this->flexi_cart_admin->db_column('currency', 'exchange_rate') => $this->input->post('exchange_rate'),
            $this->flexi_cart_admin->db_column('currency', 'symbol') => $this->input->post('symbol'),
            $this->flexi_cart_admin->db_column('currency', 'symbol_suffix') => $this->input->post('symbol_suffix'),
            $this->flexi_cart_admin->db_column('currency', 'thousand_separator') => $this->input->post('thousand'),
            $this->flexi_cart_admin->db_column('currency', 'decimal_separator') => $this->input->post('decimal'),
            $this->flexi_cart_admin->db_column('currency', 'status') => $this->input->post('status')
        );

        $this->flexi_cart_admin->update_db_currency($sql_update, $id);
    }

    function insert_order_status()
    {
        $sql_insert = array(
            $this->flexi_cart_admin->db_column('order_status', 'status') => $this->input->post('name') ?: 0,
            $this->flexi_cart_admin->db_column('order_status', 'cancelled') => $this->input->post('cancelled') ?: 0,
            $this->flexi_cart_admin->db_column('order_status', 'save_default') => $this->input->post('save_default') ?: 0,
            $this->flexi_cart_admin->db_column('order_status', 'resave_default') => $this->input->post('resave_default') ?: 0
        );

        return $this->flexi_cart_admin->insert_db_order_status($sql_insert);
    }

    function getEstado($id)
    {
        $query = $this->db
            ->where('ord_status_id', $id)
            ->get('order_status');

        return $query->row();
    }

    function update_order_status($id)
    {
        $sql_update = array(
            $this->flexi_cart_admin->db_column('order_status', 'status') => $this->input->post('name'),
            $this->flexi_cart_admin->db_column('order_status', 'cancelled') => $this->input->post('cancelled'),
            $this->flexi_cart_admin->db_column('order_status', 'save_default') => $this->input->post('save_default'),
            $this->flexi_cart_admin->db_column('order_status', 'resave_default') => $this->input->post('resave_default')
        );

        $this->flexi_cart_admin->update_db_order_status($sql_update, $id);
    }

    function update_defaults()
    {
        $data = $this->input->post('update');

        ###+++++++++++++++++++++++++++++++++###

        // Reset all cart defaults.
        $sql_update = array('curr_default' => 0);
        $this->flexi_cart_admin->update_db_currency($sql_update);

        $sql_update = array('loc_ship_default' => 0, 'loc_tax_default' => 0);
        $this->flexi_cart_admin->update_db_location($sql_update);

        $sql_update = array('ship_default' => 0);
        $this->flexi_cart_admin->update_db_shipping($sql_update);

        $sql_update = array('tax_default' => 0);
        $this->flexi_cart_admin->update_db_tax($sql_update);

        ###+++++++++++++++++++++++++++++++++###

        // Set new cart defaults.
        $sql_update = array('curr_default' => 1);
        $this->flexi_cart_admin->update_db_currency($sql_update, $data['currency']);

        $sql_update = array('loc_ship_default' => 1);
        $this->flexi_cart_admin->update_db_location($sql_update, $data['shipping_location']);

        $sql_update = array('loc_tax_default' => 1);
        $this->flexi_cart_admin->update_db_location($sql_update, $data['tax_location']);

        $sql_update = array('ship_default' => 1);
        $this->flexi_cart_admin->update_db_shipping($sql_update, $data['shipping_option']);

        $sql_update = array('tax_default' => 1);
        $this->flexi_cart_admin->update_db_tax($sql_update, $data['tax_rate']);
    }

    function update_config()
    {
        $data = $this->input->post('update');

        $sql_update = array(
            $this->flexi_cart_admin->db_column('configuration', 'order_number_prefix') => $data['order_number_prefix'],
            $this->flexi_cart_admin->db_column('configuration', 'order_number_suffix') => $data['order_number_suffix'],
            $this->flexi_cart_admin->db_column('configuration', 'increment_order_number') => $data['increment_order_number'],
            $this->flexi_cart_admin->db_column('configuration', 'minimum_order') => $data['minimum_order'],
            $this->flexi_cart_admin->db_column('configuration', 'quantity_decimals') => $data['quantity_decimals'],
            $this->flexi_cart_admin->db_column('configuration', 'increment_duplicate_item_quantity') => $data['increment_duplicate_item_quantity'],
            $this->flexi_cart_admin->db_column('configuration', 'quantity_limited_by_stock') => $data['quantity_limited_by_stock'],
            $this->flexi_cart_admin->db_column('configuration', 'remove_no_stock_items') => $data['remove_no_stock_items'],
            $this->flexi_cart_admin->db_column('configuration', 'auto_allocate_stock') => $data['auto_allocate_stock'],
            $this->flexi_cart_admin->db_column('configuration', 'weight_type') => $data['weight_type'],
            $this->flexi_cart_admin->db_column('configuration', 'weight_decimals') => $data['weight_decimals'],
            $this->flexi_cart_admin->db_column('configuration', 'display_tax_prices') => $data['display_tax_prices'],
            $this->flexi_cart_admin->db_column('configuration', 'price_inc_tax') => $data['price_inc_tax'],
            $this->flexi_cart_admin->db_column('configuration', 'multi_row_duplicate_items') => $data['multi_row_duplicate_items'],
            $this->flexi_cart_admin->db_column('configuration', 'dynamic_reward_points') => $data['dynamic_reward_points'],
            $this->flexi_cart_admin->db_column('configuration', 'reward_point_multiplier') => $data['reward_point_multiplier'],
            $this->flexi_cart_admin->db_column('configuration', 'reward_voucher_multiplier') => $data['reward_voucher_multiplier'],
            $this->flexi_cart_admin->db_column('configuration', 'reward_point_to_voucher_ratio') => $data['reward_point_to_voucher_ratio'],
            $this->flexi_cart_admin->db_column('configuration', 'reward_point_days_pending') => $data['reward_point_days_pending'],
            $this->flexi_cart_admin->db_column('configuration', 'reward_point_days_valid') => $data['reward_point_days_valid'],
            $this->flexi_cart_admin->db_column('configuration', 'reward_voucher_days_valid') => $data['reward_voucher_days_valid'],
            $this->flexi_cart_admin->db_column('configuration', 'save_banned_shipping_items') => $data['save_banned_shipping_items'],
            $this->flexi_cart_admin->db_column('configuration', 'custom_status_1') => $data['custom_status_1'],
            $this->flexi_cart_admin->db_column('configuration', 'custom_status_2') => $data['custom_status_2'],
            $this->flexi_cart_admin->db_column('configuration', 'custom_status_3') => $data['custom_status_3'],
            'config_use_payment_system' => isset($data['use_payment_system']) ? 1 : 0,
            'config_payment_system' => json_encode($data['payment_system']),
        );

        $this->flexi_cart_admin->update_db_config($sql_update);

        // Destroy the current cart and all settings so that new config settings can be set.
        // Note: The 'destroy_cart()' function is apart of the standard library.
        $this->load->library('flexi_cart');
        $this->flexi_cart->destroy_cart();
    }

    function getOrdersFilter()
    {

        $groupPost = $this->input->post('group');
        $orderPost = $this->input->post('order');

        if(!$orderPost) {
            $orderPost = 'desc';
        }

        // Get an array of all saved orders.
        // Using a flexi cart SQL function, set the order the order data so that dates are listed newest to oldest.
        $this->flexi_cart_admin->sql_order_by($this->flexi_cart_admin->db_column('order_summary', 'date'), $orderPost);
        $items = $this->flexi_cart_admin->get_db_order_array();
        $this->flexi_cart_admin->sql_clear();

        $orders = [];

        switch ($groupPost) {
            case 'day':

                $groupBy = $this->db->query("SELECT ord_date FROM order_summary GROUP BY YEAR(ord_date), DAY(ord_date) ORDER BY ord_date $orderPost")
                    ->result_array();

                foreach ($groupBy as $group) {

                    $dt = DateTime::createFromFormat('!Y-m-d H:i:s', $group['ord_date']);
                    $time = strtotime($group['ord_date']);
                    $start_date = \Carbon\Carbon::create(date('Y', $time), date('m', $time), date('d', $time), 0, 0, 0);
                    $end_date = \Carbon\Carbon::create(date('Y', $time), date('m', $time), date('d', $time), 24, 0, 0);

                    foreach ($items as $item) {

                        $current_time = strtotime($item['ord_date']);
                        $current_date = \Carbon\Carbon::createFromDate(date('Y', $current_time), date('m', $current_time),  date('d', $current_time));

                        if($current_date->gte($start_date) && $current_date->lte($end_date)) {
                            $orders[$dt->format('Y - M - d')][] = $item;
                        }

                    }

                }

                break;

            case 'week':

                $groupBy = $this->db->query("SELECT ord_date FROM order_summary GROUP BY YEAR(ord_date), WEEK(ord_date) ORDER BY ord_date $orderPost")
                    ->result_array();

                foreach ($groupBy as $group) {

                    $time = strtotime($group['ord_date']);
                    $dt = DateTime::createFromFormat('!Y-m-d H:i:s', $group['ord_date']);

                    foreach ($items as $item) {

                        $start_date = date('Y', $time).'-'.date('m', $time).'-'.date('d', $time).' 00:00:00';
                        $end_date = date('Y', $time).'-'.date('m', $time).'-'.date('d', $time).' 24:00:00';

                        if($this->checkInRange($start_date, $end_date, $item['ord_date'])) {
                            $orders[$dt->format('Y - \S\e\m\a\n\a \#W')][] = $item;
                        }
                    }

                }

                break;

            case 'month':

                $groupBy = $this->db->query("SELECT ord_date FROM order_summary GROUP BY YEAR(ord_date), MONTH(ord_date) ORDER BY ord_date $orderPost")
                    ->result_array();

                foreach ($groupBy as $group) {

                    $dt = DateTime::createFromFormat('!Y-m-d H:i:s', $group['ord_date']);
                    $time = strtotime($group['ord_date']);
                    $start_date = \Carbon\Carbon::create(date('Y', $time), date('m', $time), 1, 0, 0, 0);
                    $end_date = \Carbon\Carbon::create(date('Y', $time), date('m', $time), cal_days_in_month(CAL_GREGORIAN, date('m', $time), date('Y', $time)) -1, 24, 59, 59);

                    foreach ($items as $item) {

                        $current_time = strtotime($item['ord_date']);
                        $current_date = \Carbon\Carbon::createFromDate(date('Y', $current_time), date('m', $current_time),  date('d', $current_time));
                        
                        if($current_date->gte($start_date) && $current_date->lte($end_date)) {
                            $orders[$dt->format('Y - M')][] = $item;
                        }
                    }

                }

                break;

            //Status
            default:

                $groupBy = $this->flexi_cart_admin->get_db_order_status_array();

                foreach ($groupBy as $group) {

                    foreach ($items as $item) {
                        if($item['ord_status_id'] == $group['ord_status_id']) {
                            $orders[$group['ord_status_description']][] = $item;
                        }
                    }

                }

                break;
        }

        $this->flexi_cart_admin->sql_clear();

        $data['groups'] = $orders;

        $data['url_rel'] = base_url('admin/cart');
        $data['url_sort'] = '';
        $data['idx_item_id'] = 'ord_order_number';
        $data['idx_item_nombre'] = 'ord_order_number';
        $data['url_modificar'] = base_url('admin/cart/detalle');
        $data['url_eliminar'] = base_url('admin/cart/eliminar');
        $data['url_search'] = base_url("admin/search/cart");
        $data['url_filter'] = base_url("admin/filter/cart");

        $data['search'] = true;
        $data['drag'] = false;
        $data['nivel'] = 'nivel2';
        $data['list_id'] = '';

        $data['select_group'] = $groupPost;
        $data['select_order'] = $orderPost;

        $data['txt_titulo'] = 'Carrito de Compras';
        $data['txt_grupoNombre'] = 'Estado';

        /*
         * Menu
         */
        $data['menu'] = array();
        $data['menu'][] = anchor(base_url('admin/cart/reportes'), 'Reportes', array('class' => $data['nivel'] . ' ajax boton n8'));
        $data['menu'][] = anchor(base_url('admin/cart/ubicaciones'), 'locaciones y Zonas', array('class' => $data['nivel'] . ' ajax boton n7'));
        $data['menu'][] = anchor(base_url('admin/cart/envios'), 'Envios e Impuestos', array('class' => $data['nivel'] . ' ajax boton n6'));
        $data['menu'][] = anchor(base_url('admin/cart/descuentos'), 'Descuentos', array('class' => $data['nivel'] . ' ajax boton n5'));
        //TODO entender que hace esto: http://haseydesign.com/flexi-cart/admin_library/user_reward_points
        //$data['menu'][] = anchor(base_url('admin/cart/recompensas'), 'Puntos de Recompensa', array('class' => $data['nivel'] . ' ajax boton n5'));
        //$data['menu'][] = anchor(base_url('admin/cart/vouchers'), 'Vouchers', array('class' => $data['nivel'] . ' ajax boton n4'));
        $data['menu'][] = anchor(base_url('admin/cart/monedas'), 'Monedas', array('class' => $data['nivel'] . ' ajax boton n4'));
        $data['menu'][] = anchor(base_url('admin/cart/estados'), 'Estados', array('class' => $data['nivel'] . ' ajax boton n3'));
        $data['menu'][] = anchor(base_url('admin/cart/valores_defecto'), 'Valores por defecto', array('class' => $data['nivel'] . ' ajax boton n2'));
        $data['menu'][] = anchor(base_url('admin/cart/config'), 'Configuraci&oacute;n', array('class' => $data['nivel'] . ' ajax boton n1'));

        $data['bottomMargin'] = count($data['menu']) * 34;

        $this->load->view('admin/listadoOrdenes_view', $data);
    }

    private function checkInRange($start_date, $end_date, $date_from_user)
    {
        // Convert to timestamp
        $start_ts = strtotime($start_date);
        $end_ts = strtotime($end_date);
        $user_ts = strtotime($date_from_user);

        // Check that user date is between start & end
        return (($user_ts >= $start_ts) && ($user_ts <= $end_ts));
    }
    
    public function reportVentas($fecha_inicial, $fecha_final)
    {
        return $this->db->query("
                    SELECT distinct DATE_FORMAT(ord_date,'%d-%m-%Y') as ord_date, ord_order_number, ord_bill_first_name, ord_bill_last_name, ord_bill_city, ord_item_summary_total, ord_status_description, ord_bill_comments
                    FROM order_summary as summ 
                    inner join order_details on summ.ord_order_number = ord_det_order_number_fk
                    inner join order_status on ord_status = ord_status_id 
                    where ord_date >= '$fecha_inicial' and  ord_date <= '$fecha_final'
                ");
    }

    public function reportInventario()
    {
        return $this->db->query("
                    SELECT productos.productoId as prodId, 
                    (SELECT productoCampoRelContenido FROM productos 
                    inner join producto_campos_rel on productos.productoId = producto_campos_rel.productoId
                    inner join  es_producto_campos_rel on producto_campos_rel.productoCampoRelId = es_producto_campos_rel.productoCampoRelId
                    where productos.productoId = prodId and producto_campos_rel.productoCampoId = 67 ) as codigo , 
                    productoNombre, 
                    (SELECT productoCampoRelContenido FROM productos 
                    inner join producto_campos_rel on productos.productoId = producto_campos_rel.productoId
                    inner join  es_producto_campos_rel on producto_campos_rel.productoCampoRelId = es_producto_campos_rel.productoCampoRelId
                    where productos.productoId = prodId and producto_campos_rel.productoCampoId = 66 )  as precio ,
                    stock_quantity
                    FROM productos 
                    inner join es_productos on es_productos.productoId = productos.productoId 
                    order by productos.productoId
                ");
    }

    public function reportClientes()
    {
        return $this->db->query("
                    SELECT distinct id, first_name, last_name, email, 
                    (SELECT userFieldRelContent FROM user_fields_rel
                    where userFieldId = 2 and userId= id) as ciudad , 
                    (SELECT userFieldRelContent FROM user_fields_rel
                    where userFieldId = 1 and userId= id) as telef ,
                    (SELECT sum(ord_item_summary_total) FROM order_summary where ord_user_fk=id) as total
                    FROM users  
                    inner join order_summary on users.id = order_summary.ord_user_fk 
                ");
    }

}