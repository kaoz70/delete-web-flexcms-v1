<?php
use DrewM\MailChimp\MailChimp;

class Submit_model extends CI_Model
    {

        function getInputName($id)
        {
            $this->db->join('input', 'input.inputId = contacto_campos.inputId', 'LEFT');
            $this->db->join('es_contacto_campos', 'es_contacto_campos.contactoCampoId = contacto_campos.contactoCampoId', 'LEFT');
            $this->db->where('contacto_campos.contactoCampoId', $id);
            $this->db->order_by("contactoCampoPosicion", "asc");
            $query = $this->db->get('contacto_campos');
            return $query->row();
        }

        function getContact($id)
        {
            $this->db->where('contactoId', $id);
            $query = $this->db->get('contactos');
            return $query->row();
        }

        function getContacts()
        {
            $query = $this->db->get('contactos');
            return $query->result();
        }

        function mailchimpNewsletter($list_id)
        {
            $this->load->helper('mailing');
            //Load the Mailchimp Library with the config set in the config file
            $this->config->load('mailing');

            $options = $this->input->post();
            $response = new stdClass();
            $response->success = false;
            $response->message = 'No se pudo suscribir este correo';

            //Some basic verifications to see if we have the correct data
            if (!$list_id) {
                $response->message = 'Debe pasar un LISTID como parametro';
            }

            if (!$options) {
                $response->message = 'No hay ningun campo de formulario';
            }

            if ($list_id && $options) {

                //Create the MERGE TAGS before: in Mailchimp's Dashboard (List's Settings)
                //so that we can pair the data form the form
                $merge_tags = [
                    'FNAME' => isset($options['first_name']) ? $options['first_name'] : '',
                    'LNAME' => isset($options['last_name']) ? $options['last_name'] : '',
                    //'PHONE' => $options['phone'],
                    //'COMPANY' => $options['company'],
                ];

                $MailChimp = new MailChimp($this->config->item('apikey'));
                $MailChimp->verify_ssl = $this->config->item('verify_ssl');

                //Subscribe the user
                $MailChimp->post("lists/$list_id/members", [
                    'email_address' => $options['email'],
                    'status'        => 'subscribed',
                ]);

                //Get the user ID
                $subscriber_hash = $MailChimp->subscriberHash($options['email']);

                //Add some user fields
                $MailChimp->patch("lists/$list_id/members/$subscriber_hash", [
                    'merge_fields' => $merge_tags,
                ]);

                if ($MailChimp->success()) {
                    $response->message = "Se le envi&oacute; un correo a {$options['email']} para que active/confirme su suscripci&oacute;n";
                    $response->success = true;
                } else {
                    $response->error_message = $MailChimp->getLastError();
                }

            }
            
            return $response;
            
        }

    }