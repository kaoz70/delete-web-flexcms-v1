<?php
use Cartalyst\Sentinel\Native\Facades\Sentinel;

/**
 * Created by PhpStorm.
 * User: Miguel
 * Date: 03-Oct-16
 * Time: 04:49 PM
 */
class TicketComment extends \Illuminate\Database\Eloquent\Model
{

    function ticket()
    {
        return $this->belongsTo('Ticket')->first();
    }

    function user()
    {
        return Sentinel::findById($this->user_id);
    }

    /**
     * Get the user's name set in the ticket
     */
    function userName()
    {

        if($this->is_support) {
            $user = $this->user();
            $name = $user->first_name . ' ' . $user->last_name;
        } else {
            $name = $this->ticket()->user_name;
        }

        return $name;
    }

}