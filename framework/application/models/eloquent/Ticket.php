<?php
use Cartalyst\Sentinel\Native\Facades\Sentinel;

/**
 * Created by PhpStorm.
 * User: Miguel
 * Date: 03-Oct-16
 * Time: 04:49 PM
 */
class Ticket extends \Illuminate\Database\Eloquent\Model
{
    function comments()
    {
        return $this->hasMany('TicketComment')->get();
    }

    function supportUser()
    {
        return Sentinel::findById($this->asigned_to);
    }

    function user()
    {
        return Sentinel::findById($this->user_id);
    }

    /**
     * Get the support email, if no user is set, send the email to a default address
     *
     * @param $defaultEmail
     * @return mixed
     */
    function supportEmail($defaultEmail)
    {

        if($user = $this->supportUser()) {
            $email = $user->email;
        } else {
            $email = $defaultEmail;
        }

        return $email;
    }

}