<?php

class Usuarios_model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function get()
    {
        return $this->db
            ->join('users_groups', 'users_groups.user_id = users.id', 'LEFT')
            ->group_by('user_id')
            ->get('users')->result();
    }

    public function getCampos($idioma, $authFields = false)
    {
        $this->db->join($idioma.'_user_fields', $idioma.'_user_fields.userFieldId = user_fields.userFieldId', 'LEFT');
        $this->db->join('input', 'input.inputId = user_fields.inputId', 'LEFT');

        if($authFields) {
            $this->db->where('userFieldRequiredAuth', 1);
        }

        $this->db->order_by('userFieldPosition', 'ASC');
        $query = $this->db->get('user_fields');
        return $query->result();
    }

    public function insertarCampos($user)
    {
        $campos = $this->input->post('campo');

        $dbFields = $this->db->join('input', 'input.inputId = user_fields.inputId', 'LEFT')
            ->order_by('userFieldPosition', 'ASC')
            ->get('user_fields')
            ->result();

        foreach ($dbFields as $field) {

            $data = [
                'userFieldId' => $field->userFieldId,
                'userId' => $user,
            ];

            if($campos && array_key_exists($field->userFieldId, $campos)) {
                $data['userFieldRelContent'] = $campos[$field->userFieldId];
            }

            $this->db->insert('user_fields_rel', $data);

        }

    }

    public function actualizarCampos($user, $inputArrayName = 'campo')
    {
        $campos = $this->input->post($inputArrayName);

        if( ! $campos) {
            $stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
            $order_data = json_decode($stream_clean);
            $campos = (array) $order_data->user_fields;
        }

        if( ! $campos) {
            return;
        }

        $dbFields = $this->db->join('input', 'input.inputId = user_fields.inputId', 'LEFT')
            ->order_by('userFieldPosition', 'ASC')
            ->get('user_fields')
            ->result();

        foreach ($dbFields as $field) {

            if($campos && array_key_exists($field->userFieldId, $campos)) {
                $data['userFieldRelContent'] = $campos[$field->userFieldId];
            } else {
                $data['userFieldRelContent'] = $field->userFieldOrderCol ? $campos[$field->userFieldOrderCol] : '';
            }

            $this->db->where('userId', $user);
            $this->db->where('userFieldId', $field->userFieldId);
            $this->db->update('user_fields_rel', $data);

        }

    }

    public function getCamposUser($userId, $idioma)
    {
        $this->db->join($idioma.'_user_fields', $idioma.'_user_fields.userFieldId = user_fields.userFieldId', 'LEFT');
        $this->db->join('input', 'input.inputId = user_fields.inputId', 'LEFT');
        $this->db->join('user_fields_rel', 'user_fields_rel.userFieldId = user_fields.userFieldId', 'LEFT');
        $this->db->where('userId', $userId);
        $this->db->order_by('userFieldPosition', 'ASC');
        $query = $this->db->get('user_fields');

        return $query->result();
    }

    public function countries()
    {
        $query = $this->db->get('user_countries');
        return $query->result();
    }

}