<?php

/**
 * Created by PhpStorm.
 * User: miguel
 * Date: 12/21/15
 * Time: 2:06 PM
 */
class Upload extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('submit_model', 'Submit');
        $this->load->model('configuracion_model', 'Config');
        $this->load->model('contact_model', 'Contacto');

        $this->m_config = $this->Config->get();
        $this->theme_config = $this->load->set_theme($this->m_config->theme);

    }

    public function file()
    {

        /**
         * If apache_request_headers() function does not exist, create it.
         * Source: http://www.php.net/manual/en/function.apache-request-headers.php#70810
         */
        if(!function_exists('apache_request_headers') ) {
            function apache_request_headers() {
                $arh = array();
                $rx_http = '/\AHTTP_/';
                foreach($_SERVER as $key => $val) {
                    if( preg_match($rx_http, $key) ) {
                        $arh_key = preg_replace($rx_http, '', $key);
                        // do some nasty string manipulations to restore the original letter case
                        // this should work in most cases
                        $rx_matches = explode('_', $arh_key);
                        if( count($rx_matches) > 0 and strlen($arh_key) > 2 ) {
                            foreach($rx_matches as $ak_key => $ak_val) $rx_matches[$ak_key] = ucfirst($ak_val);
                            $arh_key = implode('_', $rx_matches);
                        }
                        $arh[$arh_key] = $val;
                    }
                }
                return( $arh );
            }
        }

        $headers = apache_request_headers();

        $fn = isset($headers['FILENAME']) ? $headers['FILENAME'] : $headers['Filename'];
        $return = new stdClass();

        if ($fn) {

            $tfn = tempnam('/tmp', 'upl');

            if ($tfn)
            {

                // put contents from file data from the AJAX call
                file_put_contents(
                    $tfn,
                    file_get_contents('php://input')
                );

                $return->filename = $fn;
                $return->status = 'success';
                $return->temp_path = $tfn;
                echo json_encode($return);
                exit();

            }

            else {
                $return->status =  "Error: Could not create temp filename";
            }

        }
        else {
            $return->status =  "Error: Could not get filename";
        }

        echo json_encode($return);

    }

    public function invoice($method = 'input')
    {

        $return = new stdClass();
        $return->success =  FALSE;

        if($method === 'input') {
            $headers = apache_request_headers();

            $fn = $headers['FILENAME'] ? $headers['FILENAME'] : $headers['Filename'];

            if ($fn) {

                $tfn = tempnam(sys_get_temp_dir(), 'upl');

                if ($tfn) {

                    // put contents from file data from the AJAX call
                    file_put_contents($tfn, file_get_contents('php://input'));

                    $return->success = TRUE;
                    $return->tempFilename = $tfn;
                    $return->origFilename = $fn;

                }

                else {
                    $return->message =  "Could not create temp filename";
                }

            }
            else {
                $return->message =  "Could not get filename";
            }
        }

        //POST method
        else {

            $config['upload_path'] = sys_get_temp_dir();
            $config['allowed_types'] = 'xml|pdf';
            $config['max_size'] = '1024';
            $this->load->library('upload');

            $files = [];

            foreach ($_FILES as $fieldname => $fileObject)  //fieldname is the form field name
            {
                if (!empty($fileObject['name']))
                {
                    $this->upload->initialize($config);
                    if (!$this->upload->do_upload($fieldname))
                    {
                        $return->message = $this->upload->display_errors();
                    }
                    else
                    {
                        $data = $this->upload->data();
                        $returnData = new stdClass();
                        $returnData->tempFilename = $data['file_name'];
                        $returnData->origFilename = $data['orig_name'];
                        $files[] = $returnData;
                    }
                }
            }

            $this->postInvoice($files);

        }

        echo json_encode($return);

    }

    /**
     * This gets called when all the invoices have been uploaded
     *
     * @param array $files
     */
    public function postInvoice($files = [])
    {

        //From ajax
        if( ! count($files)) {
            $files = $this->input->post('files');
        }

        $return = new stdClass();
        $return->success =  FALSE;
        $return->message = "";

        $success_count = 0;

        foreach($files as $key => $file) {

            //Check and see if the file has already been moved
            if ( ! file_exists($file['tempFilename'])) {
                continue;
            }

            $finfo = finfo_open(FILEINFO_MIME_TYPE); // return mime type ala mimetype extension
            $mime = finfo_file($finfo, $file['tempFilename']);
            finfo_close($finfo);

            //Get the filename
            $filename = pathinfo($file['origFilename'], PATHINFO_FILENAME);

            //Remove current file
            $filesArr = $files;
            unset($filesArr[$key]);

            //Get the pdf file
            $dup = [];
            foreach ($filesArr as $f) {

                $fname = pathinfo($f['origFilename'], PATHINFO_FILENAME);

                //Do we have a match?
                if($filename === $fname) {
                    $dup = $f;
                    break;
                }

            }

            //If there is no PDF or XML match then continue to the next loop
            if(!count($dup)) {
                $extension = (strpos($mime,'xml') !== false) ? "PDF" : "XML";
                $return->message .= "<p>El archivo {$file['origFilename']} no tiene la versi&oacute;n en {$extension}</p>";
                continue;
            }


            //Only process XML files
            if (strpos($mime,'xml') !== false) {

                try {
                    $return->message = $this->processXML($file, $dup);
                    $return->success =  TRUE;
                    $success_count++;
                } catch (Exception $e) {
                    $return->message = $e->getMessage();
                }

            }

        }

        echo json_encode($return);

    }

    private function processXML($xml, $pdf)
    {

        $message = "";

        $reader = new Sabre\Xml\Reader();
        $reader->open($xml['tempFilename']);

        $reader->elementMap = [
            '{}autorizacion' => 'Sabre\Xml\Element\KeyValue',
        ];

        $factura = $reader->parse();

        $reader->xml($factura['value']['{}comprobante']);

        $reader->elementMap = [
            '{}factura' => 'Sabre\Xml\Element\KeyValue',
            '{}infoTributaria' => 'Sabre\Xml\Element\KeyValue',
            '{}infoFactura' => 'Sabre\Xml\Element\KeyValue',
            '{}totalConImpuestos' => 'Sabre\Xml\Element\KeyValue',
            '{}detalles' => 'Sabre\Xml\Element\KeyValue',
            '{}infoAdicional' => 'Sabre\Xml\Element\KeyValue',
        ];

        $comprobante = $reader->parse();

        $establishment = $comprobante['value']['{}infoTributaria']['{}estab'];
        $pointOfEmision = $comprobante['value']['{}infoTributaria']['{}ptoEmi'];
        $secuential = $comprobante['value']['{}infoTributaria']['{}secuencial'];
        $userId = $comprobante['value']['{}infoFactura']['{}identificacionComprador'];

        $filename = "{$establishment}-{$pointOfEmision}-{$secuential}";

        if( ! $factura['value']['{}estado'] === 'AUTORIZADO') {
            throw new Exception("La factura {$secuential} no ha sido autorizada por el SRI!");
        }

        //Create the user if he doesn't exist
        if( ! $user = \Cartalyst\Sentinel\Sentinel::getUser()) {

            $names = explode(' ', $comprobante['value']['{}infoFactura']['{}razonSocialComprador']);

            $username = '';
            $password = uniqid();
            $email = $comprobante['value']['{}infoAdicional']['{}campoAdicional'];

            //TODO what happens when there are 3 or more names?
            $additional_data = [
                'first_name' => $names[0],
                'last_name' => isset($names[1]) ? $names[1] : $names[0],
            ];
            $group = [ '2' ];




            if( ! $this->ion_auth->register($username, $password, $email, $additional_data, $group)) {
                throw new Exception($this->ion_auth->errors());
            }

            if($em_message = $this->sendEmail($email, $userId, $password, $comprobante['value']['{}infoFactura']['{}razonSocialComprador']) !== TRUE) {
                $message = $em_message;
            }

        }

        $this->moveFile($xml['tempFilename'], "assets/public/files/invoices/{$filename}.xml");
        $this->moveFile($pdf['tempFilename'], "assets/public/files/invoices/{$filename}.pdf");

        $invoice = new Invoice();
        $invoice->type = 'factura';
        $invoice->user_id = $userId;
        $invoice->establishment = $establishment;
        $invoice->point_of_emision = $pointOfEmision;
        $invoice->number = $secuential;
        $invoice->filename = $filename;
        $invoice->amount = $comprobante['value']['{}infoFactura']['{}importeTotal'];
        $invoice->date = $comprobante['value']['{}infoFactura']['{}fechaEmision'];
        $invoice->save();

        return $message;

    }

    private function moveFile($oldFilePath, $newFilepath) {

        //Check if these is already a file with the same name
        if(file_exists($newFilepath)) {
            throw new Exception("Ya existe un archivo con el mismo nombre en: {$newFilepath}");
        }

        //Does the file we want to move exist?
        if( ! file_exists($oldFilePath)) {
            throw new Exception('No existe el archivo: ' . $oldFilePath);
        }

        //Move the file
        if(rename($oldFilePath, $newFilepath)) {
            // Read and write for owner, read for everybody else
            // We set this just in case (in some cases it was creating the file as 600)
            chmod($newFilepath, 0644);
        }
        else {
            throw new Exception('No se pudo mover el archivo!');
        }

    }

    private function sendEmail($email, $username, $password, $names)
    {

        $data = [
            'names' => $names,
            'username' => $username,
            'password' => $password,
        ];

        $output = $this->load->view('email/invoice_password_view', $data, TRUE);

        $contact = $this->Submit->getContacts()[0];
        $contact->contactoNombre = $this->Contacto->getContactoTranslation('es', $contact->contactoId)->contactoNombre;

        $config['mailtype'] = 'html';
        $this->email->initialize($config);

        //It has to be an email that's created on the server
        $this->email->from($contact->contactoEmail, $contact->contactoNombre);
        $this->email->reply_to($contact->contactoEmail, $contact->contactoNombre);
        $this->email->to($email);

        $this->email->subject('Registro desde ' . $_SERVER['SERVER_NAME']);
        $this->email->message($output);

        if($this->email->send()) {
            return TRUE;
        } else {
            return "No se ha podido enviar la contrase&ntilde;a {$password} al correo {$email}. Por favor env&iacute;elo manualmente";
        }

    }

}