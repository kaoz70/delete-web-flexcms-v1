<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class TinyMceUploader extends CI_Controller
{

    /* Constructor */

    public function __construct()
    {
        parent::__construct();

        $this->load->library('Seguridad');
        $this->seguridad->init();

        $this->load->library('session');

        /*$this->load->helper(array('jbimages','language'));

        // is_allowed is a helper function which is supposed to return False if upload operation is forbidden
        // [See jbimages/is_alllowed.php]

        if (is_allowed() === FALSE)
        {
            exit;
        }*/

        // User configured settings
        $this->config->load('uploader_settings', true);
    }

    /* Language set */

    private function _lang_set($lang)
    {
        // We accept any language set as lang_id in **_dlg.js
        // Therefore an error will occur if language file doesn't exist

        $this->config->set_item('language', $lang);
        $this->lang->load('jbstrings', $lang);
    }

    /* Default upload routine */

    public function upload($lang = 'english')
    {
        // Set language
        $this->_lang_set($lang);

        // Get configuartion data (we fill up 2 arrays - $config and $conf)

        $conf['img_path'] = $this->config->item('img_path', 'uploader_settings');
        $conf['show_path'] = $this->config->item('show_path', 'uploader_settings');
        $conf['allow_resize'] = $this->config->item('allow_resize', 'uploader_settings');

        $config['allowed_types'] = $this->config->item('allowed_types', 'uploader_settings');
        $config['max_size'] = $this->config->item('max_size', 'uploader_settings');
        $config['encrypt_name'] = $this->config->item('encrypt_name', 'uploader_settings');
        $config['overwrite'] = $this->config->item('overwrite', 'uploader_settings');
        $config['upload_path'] = $this->config->item('upload_path', 'uploader_settings');

        if (!$conf['allow_resize']) {
            $config['max_width'] = $this->config->item('max_width', 'uploader_settings');
            $config['max_height'] = $this->config->item('max_height', 'uploader_settings');
        } else {
            $conf['max_width'] = $this->config->item('max_width', 'uploader_settings');
            $conf['max_height'] = $this->config->item('max_height', 'uploader_settings');

            if ($conf['max_width'] == 0 and $conf['max_height'] == 0) {
                $conf['allow_resize'] = false;
            }
        }

        // Load uploader
        $this->load->library('upload', $config);

        if ($this->upload->do_upload()) // Success
        {
            // General result data
            $result = $this->upload->data();

            // Shall we resize an image?
            if ($conf['allow_resize'] and $conf['max_width'] > 0 and $conf['max_height'] > 0 and (($result['image_width'] > $conf['max_width']) or ($result['image_height'] > $conf['max_height']))) {
                // Resizing parameters
                $resizeParams = array
                (
                    'source_image' => $result['full_path'],
                    'new_image' => $result['full_path'],
                    'width' => $conf['max_width'],
                    'height' => $conf['max_height']
                );

                // Load resize library
                $this->load->library('image_lib', $resizeParams);

                // Do resize
                $this->image_lib->resize();
            }

            $this->session->set_flashdata('message_text', $result['file_name'] . " subido correctamente.");
            $this->session->set_flashdata('message_class', 'success');

        } else // Failure
        {

            $this->session->set_flashdata('message_text', $this->upload->display_errors(' ', ' '));
            $this->session->set_flashdata('message_class', 'alert');

        }

        redirect('admin/tiny_mce_uploader/browser');

    }

    /* Blank Page (default source for iframe) */

    public function blank($lang = 'english')
    {
        $this->_lang_set($lang);
        $this->load->view('blank');
    }

    public function index($lang = 'english')
    {
        $this->blank($lang);
    }

    public function browser()
    {
        $carpeta_ficheros = $this->config->item('img_path', 'uploader_settings');
        $path = $this->config->item('show_path', 'uploader_settings');
        $files = [];

        // Opens Folder
        $directorio = opendir($carpeta_ficheros);

        // reads every file
        while ($fichero = readdir($directorio)) {

            // Omits the folders and non image files
            if (!is_dir($fichero) && @is_array(getimagesize($carpeta_ficheros . DIRECTORY_SEPARATOR . $fichero))){
                $file = [
                    'path' => $path . DIRECTORY_SEPARATOR . $fichero,
                    'name' => $fichero,
                ];
                $files[] = $file;
            }
        }

        $data['files'] = $files;
        $data['message'] = [
            'text' => $this->session->flashdata('message_text'),
            'class' => $this->session->flashdata('message_class'),
        ];

        $this->load->view('admin/file_browser', $data);

    }

}

/* End of file uploader.php */
/* Location: ./application/controllers/uploader.php */
