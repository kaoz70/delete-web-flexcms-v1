<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Invoices extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('Seguridad');
		$this->seguridad->init();
	}

	public function index()
	{

		$data['items'] = Invoice::whereNull("payment_type")->get();

		$data['url_rel'] = "";
		$data['url_sort'] = "";
		$data['url_modificar'] = base_url('admin/invoices/edit/');
		$data['url_eliminar'] = "";
		$data['url_search'] = '';

		$data['search'] = false;
		$data['drag'] = false;
		$data['nivel'] = 'nivel2';
		$data['list_id'] = 'invoices';

		$data['idx_id'] = 'id';
		$data['idx_nombre'] = 'number';

		$data['txt_titulo'] = 'Facturas';

		$data['url_path'] =  "";
		$data['method'] =  'invoice/';

		$data['width'] = "0";
		$data['height'] = "0";

		$data['idx_extension'] = 'filename';

		/*
		 * Menu
		 */
		$data['menu'] = array();
		$data['bottomMargin'] = count($data['menu']) * 34;

		$this->load->view('admin/listadoInvoices_view', $data);
	}

	public function edit()
	{

	}

}