<?php
use Cartalyst\Sentinel\Native\Facades\Sentinel;

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tickets extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->lang->load('ticket', 'spanish');
        $this->load->library('Seguridad');
        $this->load->library('CMS_General');
        $this->load->model('configuracion_model', 'Config');
        $this->seguridad->init();

    }

    public function edit($id)
    {

        $ticket = Ticket::find($id);
        $data['ticket'] = $ticket;

        $data['titulo'] = 'Modificar Ticket';

        $data['nuevo'] = '';
        $data['removeUrl'] = '';
        $data['user'] = Sentinel::getUser();
        $data['role'] =  Sentinel::findRoleBySlug($this->Config->item('tickets_role')->tickets_role);
        $data['users'] = Sentinel::getUserRepository()->all();

        $data['txt_boton'] = 'Modificar Ticket';
        $data['link'] = base_url('admin/tickets/update/' .$ticket->id);

        $this->load->view('admin/tickets/ticket_view', $data);

    }

    public function update($id)
    {

        $response = new stdClass();
        $response->error_code = 0;

        try{

            $user = Sentinel::getUser();
            $ticket = Ticket::find($id);

            //Asign new user
            if($ticket->asigned_to !== (int)$this->input->post('asigned_to')) {
                $ticket->asigned_to = $this->input->post('asigned_to');

                //Send email
                $this->load->set_theme($this->Config->item('theme')->theme);
                $output = $this->load->view('email/ticket_assignment_view', [], true);
                $this->load->set_admin_theme();
                $this->sendEmail($ticket, $output, $ticket->supportEmail($this->Config->item('tickets_default_email')->tickets_default_email));

            }

            //Ticket is open, support will comment
            if( ! $ticket->closed && !(bool)$this->input->post('closed')) {
                $ticket->status = lang('ticket_status_pending_user');
                $this->createComment($ticket, $user);
            }

            //Ticket is open, support will close it without a comment
            elseif( ! $ticket->closed && (bool)$this->input->post('closed') && !$this->input->post('detail')) {
                $ticket->status = lang('ticket_status_closed_support');
                $ticket->closed = 1;
            }

            //Ticket is open, support will close it with a comment
            elseif( ! $ticket->closed && $this->input->post('detail')) {
                $ticket->status = lang('ticket_status_closed_support');
                $ticket->closed = 1;
                $this->createComment($ticket, $user, true);
            }

            //Ticket is closed, support will reopen it
            elseif($ticket->closed && !(bool)$this->input->post('closed') && !$this->input->post('detail')) {
                $ticket->closed = 0;
                $this->createComment($ticket, $user);
            }

            //Ticket is closed, support will reopen it with comment
            elseif($ticket->closed && $this->input->post('detail')) {
                $ticket->status = lang('ticket_status_pending_user');
                $ticket->closed = 0;
                $this->createComment($ticket, $user, false, true);
            }



            $ticket->save();

        } catch (Exception $e) {
            $response = $this->cms_general->error('Ocurri&oacute; un problema al enviar el comentario!', $e);
        }

        $this->load->view('admin/request/json', array('return' => $response));

    }

    /**
     * @param $ticket
     * @param $user
     * @param bool $willClose
     * @param bool $willOpen
     */
    private function createComment($ticket, $user, $willClose = false, $willOpen = false)
    {

        //Create the ticket comment
        $ticketDetail = new TicketComment();
        $ticketDetail->ticket_id = $ticket->id;
        $ticketDetail->detail = $this->input->post('detail');
        $ticketDetail->user_id = $user->id;
        $ticketDetail->is_support = 1;

        //It's a opening comment
        if($willOpen) {
            $ticketDetail->status = 'open';
        }

        //It's a closing comment
        if($willClose) {
            $ticketDetail->status = 'close';
        }

        $ticketDetail->save();

        //Upload any files
        $this->uploadFiles($ticket, $ticketDetail);

        $this->load->set_theme($this->Config->item('theme')->theme);
        $output = $this->load->view('email/ticket_comment_view', $ticketDetail, true);
        $this->load->set_admin_theme();
        $this->sendEmail($ticket, $output, $ticket->user()->email);

    }

    public function delete()
    {
        $id = $this->uri->segment(4);

        $response = new stdClass();
        $response->error_code = 0;

        try{

            /*$ticket = Ticket::find($id);
            $replicated = $ticket->replicate();
            $ticket->delete();

            $output = '[Ticket eliminado]';
            $this->sendEmail($replicated, $output);*/

        } catch (Exception $e) {
            $response = $this->cms_general->error('Ocurri&oacute; un problema al eliminar el art&iacute;culo!', $e);
        }

        $this->load->view('admin/request/json', array('return' => $response));
    }

    public function closed()
    {
        $this->lang->load('ticket', 'spanish');

        $data['items'] = Ticket::where('closed', 1)->get();
        $data['grupos'] = [
            [
                'id' => 1,
                'name' => lang('ticket_priority_1')
            ],
            [
                'id' => 2,
                'name' => lang('ticket_priority_2')
            ],
            [
                'id' => 3,
                'name' => lang('ticket_priority_3')
            ],
        ];

        $data['url_rel'] = base_url('admin/tickets');
        $data['url_sort'] = '';
        $data['url_modificar'] = base_url('admin/tickets/edit');
        $data['url_eliminar'] = base_url('admin/tickets/delete');
        $data['url_search'] = base_url("admin/search/tickets");

        $data['search'] = true;
        $data['drag'] = false;
        $data['nivel'] = 'nivel3';
        $data['list_id'] = 'tickets';

        $data['idx_nombre'] = 'name';
        $data['idx_grupo_id'] = 'id';
        $data['idx_grupo_item_id'] = 'priority';
        $data['idx_item_id'] = 'id';
        $data['idx_item_nombre'] = 'uuid';

        $data['txt_titulo'] = 'Tickets';
        $data['txt_grupoNombre'] = 'Prioridad';

        /*
         * Menu
         */
        $data['menu'] = array();
        $data['bottomMargin'] = count($data['menu']) * 34;

        $this->load->view('admin/listadoAgrupado_view', $data);
    }

    private function uploadFiles($ticket, $ticketDetail)
    {
        if( ! empty($_FILES['userfile']['name'][0])) {

            //Create the main ticket folder
            $path = "assets/public/files/tickets/" . $ticket->uuid;

            if (!file_exists($path)) {
                mkdir($path, 0755, TRUE);
            }

            //Create the subfolder
            $path .= '/' . $ticketDetail->id;

            if (!file_exists($path)) {
                mkdir($path, 0755, TRUE);
            }

            //Upload files
            $config['upload_path'] = $path;
            $config['allowed_types'] = '*';

            $this->load->library('upload', $config);

            if ($fileData = $this->upload->do_multi_upload('userfile')) {
                $ticketDetail->files = json_encode($fileData);
                $ticketDetail->save();
            } else {
                throw new Exception($this->upload->display_errors());
            }
        }
    }

    public function config()
    {
        $data['config'] = $this->Config->get('tickets');

        $data['titulo'] = 'Configuraci&oacute;n';
        $data['txt_guardar'] = 'Guardar Configuraci&oacute;n';
        $data['roles'] =  Sentinel::getRoleRepository()->all();

        $this->load->view('admin/tickets/config_view', $data);
    }

    /**
     * @param $ticket
     * @param $emailBody
     * @return bool
     * @throws Exception
     */
    private function sendEmail($ticket, $emailBody, $to)
    {

        $config['mailtype'] = 'html';
        $this->email->initialize($config);

        //It has to be an email thats created on the server
        $this->email->from("no-reply@{$_SERVER['SERVER_NAME']}", 'Soporte');
        $this->email->to($to);

        $this->email->subject("[TICKET #{$ticket->uuid}] - {$ticket->title}");
        $this->email->message($emailBody);

        if (@$this->email->send()) {
            return true;
        } else {
            log_message('error', $this->email->print_debugger());
            throw new Exception("No se pudo enviar el correo de notificaci&oacute;n", 1);
        }

    }

}