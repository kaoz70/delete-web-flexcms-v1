<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Filter extends CI_Controller {

    function __construct(){

        parent::__construct();

        //$this->load->model('filter_model', 'Filter');

        // IMPORTANT! This global must be defined BEFORE the flexi cart library is loaded!
        // It is used as a global that is accessible via both models and both libraries, without it, flexi cart will not work.
        $this->flexi = new stdClass;

        $this->load->model('admin/cart_model', 'Cart');

        // Load 'admin' flexi cart library by default.
        $this->load->library('flexi_cart_admin');

        $this->load->library('Seguridad');
        $this->seguridad->init();

    }

    public function cart()
    {
        $this->Cart->getOrdersFilter();
    }

}