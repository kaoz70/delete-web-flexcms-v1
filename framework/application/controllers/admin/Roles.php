<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Cartalyst\Sentinel\Native\Facades\Sentinel as Sentinel;

class Roles extends CI_Controller {

    function __construct(){
        parent::__construct();

        $this->load->model('idiomas_model', 'Idioma');
        $this->load->model('admin/general_model', 'General');
        $this->load->model('configuracion_model', 'Config');

        $this->load->library('Seguridad');
        $this->load->library('CMS_General');

        $this->seguridad->init();

    }

    public function index()
    {

        $data['items'] = Sentinel::getRoleRepository()->all();

        $data['txt_titulo'] = 'Roles';
        $data['url_rel'] = base_url('admin/roles');
        $data['url_sort'] = '';
        $data['url_modificar'] = base_url('admin/roles/modificar');
        $data['url_eliminar'] = base_url('admin/roles/eliminar');

        $data['search'] = false;
        $data['drag'] = false;
        $data['nivel'] = 'nivel3';
        $data['list_id'] = 'roles';

        $data['idx_nombre'] = 'name';
        $data['idx_id'] = 'id';

        /*
         * Menu
         */
        $data['menu'] = array();

        $atts = array(
            'id' => 'crear',
            'class' => $data['nivel'] . ' ajax boton importante n1'
        );

        $data['menu'][] = anchor(base_url('admin/roles/crear'), 'crear nuevo rol', $atts);
        $data['bottomMargin'] = count($data['menu']) * 34;

        $this->load->view('admin/listado_view', $data);
    }

    public function crear()
    {

        $data['role'] = new \Cartalyst\Sentinel\Roles\EloquentRole();
        $data['nuevo'] = 'nuevo';
        $data['user'] = Sentinel::getUser();
        $data['sections'] = $this->Config->getSecciones();

        $root = CatalogTree::allRoot()->first();
        $root->findChildren(999);
        $data['catalog_root_node'] = $root;

        $data['catalog_names'] = array(
            'category' => 'productoCategoriaNombre',
        );

        $root = GalleryTree::allRoot()->first();
        $root->findChildren(999);
        $data['gallery_root_node'] = $root;

        $data['gallery_names'] = array(
            'category' => 'descargaCategoriaNombre',
        );

        $data['catalog_permissions'] = [];
        $data['gallery_permissions'] = [];

        foreach (CatalogTree::all() as $category) {
            $data['catalog_permissions'][] = $category->id;
        }

        foreach (GalleryTree::all() as $category) {
            $data['gallery_permissions'][] = $category->id;
        }

        $data['hasPermissions'] = false;
        if($data['catalog_root_node']->getChildren() || $data['gallery_root_node']->getChildren()) {
            $data['hasPermissions'] = true;
        }

        $data['titulo'] = 'Crear Rol de Usuario';
        $data['link'] = base_url("admin/roles/insertar");
        $data['txt_boton'] = "crear nuevo rol";

        $this->load->view('admin/authentication/role_view', $data);
    }

    public function modificar($id)
    {

        $data['role'] = $role = Sentinel::findRoleById($id);
        $data['user'] = Sentinel::getUser();
        $data['sections'] = $this->Config->getSecciones();
        $data['nuevo'] = '';

        $root = CatalogTree::allRoot()->first();
        $root->findChildren(999);
        $data['catalog_root_node'] = $root;

        $data['catalog_names'] = array(
            'category' => 'productoCategoriaNombre',
        );

        $root = GalleryTree::allRoot()->first();
        $root->findChildren(999);
        $data['gallery_root_node'] = $root;

        $data['gallery_names'] = array(
            'category' => 'descargaCategoriaNombre',
        );

        $data['active'] = '';
        if(Sentinel::getActivationRepository()->completed($data['user'])) {
            $data['active'] = 'checked="checked"';
        }

        $data['catalog_permissions'] = [];
        $data['gallery_permissions'] = [];

        foreach ($role->permissions as $key => $permission) {
            if(str_contains($key, 'catalog.')) {
                $catId = explode('.', $key);
                $data['catalog_permissions'][] = $catId[1];
            }
            if(str_contains($key, 'gallery.')) {
                $galId = explode('.', $key);
                $data['gallery_permissions'][] = $galId[1];
            }
        }

        $data['hasPermissions'] = false;
        if(!$role->hasAccess(['admin']) && ($data['catalog_root_node']->getChildren() || $data['gallery_root_node']->getChildren())) {
            $data['hasPermissions'] = true;
        }

        $data['titulo'] = 'Modificar Rol de Usuario';
        $data['link'] = base_url("admin/roles/actualizar/" . $id);
        $data['txt_boton'] = "modificar rol";

        $this->load->view('admin/authentication/role_view', $data);
    }

    public function insertar()
    {

        $response = new stdClass();
        $response->error_code = 0;

        try {

            $role = new \Cartalyst\Sentinel\Roles\EloquentRole();
            $this->_process($role);
            $response->new_id = $role->id;

        } catch (Exception $e) {
            $response->error_code = 1;
            $response->message = $e->getMessage();
        }

        $this->load->view('admin/request/json', array('return' => $response));

    }

    public function actualizar($id)
    {

        $response = new stdClass();
        $response->error_code = 0;

        try{

            $role = Sentinel::findRoleById($id);
            $this->_process($role);

        } catch (Exception $e) {
            $response = $this->cms_general->error('Ocurri&oacute; un problema al actualizar el usuario!', $e);
        }

        $this->load->view('admin/request/json', array('return' => $response));

    }

    private function _process(\Cartalyst\Sentinel\Roles\EloquentRole $role)
    {
        $role->name = $this->input->post('name');
        $role->slug = $this->input->post('slug');

        $permissions = [];
        if($this->input->post('permissions')) {
            foreach ($this->input->post('permissions') as $name) {
                $permissions[$name] = true;
            }
        }

        //Set catalog permissions
        if($this->input->post('catalog')) {
            foreach ($this->input->post('catalog') as $catId) {
                $permissions['catalog.' . $catId] = true;
            }
        }

        //Set gallery permissions
        if($this->input->post('gallery')) {
            foreach ($this->input->post('gallery') as $catId) {
                $permissions['gallery.' . $catId] = true;
            }
        }

        $role->permissions = $permissions;

        $role->save();
    }

    public function eliminar($id)
    {

        $response = new stdClass();
        $response->error_code = 0;

        try{
            $role = Sentinel::findRoleById($id);
            $role->delete();
        } catch (Exception $e) {
            $response = $this->cms_general->error('Ocurri&oacute; un problema al eliminar el rol!', $e);
        }

        $this->load->view('admin/request/json', array('return' => $response));

    }

}