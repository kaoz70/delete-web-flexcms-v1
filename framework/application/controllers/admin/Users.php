<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Cartalyst\Sentinel\Native\Facades\Sentinel as Sentinel;

class Users extends CI_Controller {

    function __construct(){
        parent::__construct();

        $this->load->model('admin/usuarios_model', 'Usuarios');
        $this->load->model('idiomas_model', 'Idioma');
        $this->load->model('admin/general_model', 'General');
        $this->load->model('configuracion_model', 'Config');

        $this->load->library('Seguridad');
        $this->load->library('CMS_General');

        $this->seguridad->init();

    }

    public function index()
    {

        $data['users'] = Sentinel::getUserRepository()->all();
        $data['roles'] =  Sentinel::getRoleRepository()->all();
        $data['titulo'] = 'Usuarios';
        $data['txt_usuarios'] = "crear nuevo usuario";

        $this->load->view('admin/authentication/usuarios_view', $data);
    }

    public function config()
    {
        $data['config'] = $this->Config->get('users');

        $data['titulo'] = 'Configuraci&oacute;n';
        $data['txt_guardar'] = 'Guardar Configuraci&oacute;n';
        $data['roles'] =  Sentinel::getRoleRepository()->all();

        $this->load->view('admin/authentication/config_view', $data);
    }

    public function crear()
    {

        $data['user'] = new \Cartalyst\Sentinel\Users\EloquentUser();
        $data['user']->id = $this->cms_general->generarId('users');

        $data['active'] = 'checked="checked"';

        $data['roles'] =  Sentinel::getRoleRepository()->all();
        $data['nuevo'] = 'nuevo';

        $data['countries'] = $this->Usuarios->countries();

        $campos = $this->Usuarios->getTemplate();

        foreach($campos as $key => $campo) {
            $campos[$key]->userFieldRelContent = '';
        }

        $data['campos'] = $campos;

        $root = CatalogTree::allRoot()->first();
        $root->findChildren(999);
        $data['catalog_root_node'] = $root;

        $data['catalog_names'] = array(
            'category' => 'productoCategoriaNombre',
        );

        $root = GalleryTree::allRoot()->first();
        $root->findChildren(999);
        $data['gallery_root_node'] = $root;

        $data['gallery_names'] = array(
            'category' => 'descargaCategoriaNombre',
        );

        $data['catalog_permissions'] = [];
        $data['gallery_permissions'] = [];

        foreach (CatalogTree::all() as $category) {
            $data['catalog_permissions'][] = $category->id;
        }

        foreach (GalleryTree::all() as $category) {
            $data['gallery_permissions'][] = $category->id;
        }

        $data['hasPermissions'] = false;
        if($data['catalog_root_node']->getChildren() || $data['gallery_root_node']->getChildren()) {
            $data['hasPermissions'] = true;
        }

        $data['titulo'] = 'Crear Usuario';
        $data['link'] = base_url("admin/users/insertar");
        $data['txt_boton'] = "crear nuevo usuario";

        $data['imagen'] = '';
        $data['imagenOrig'] = '';
        $data['imagenExtension'] = '';
        $data['txt_botImagen'] = 'Subir Imagen';
        $data['cropDimensions'] = $this->General->getCropImage(13);
        $data['image_coord'] = '';

        $this->load->view('admin/authentication/usuarioCrear_view', $data);
    }

    public function modificar($id)
    {

        $data['user'] = $user = Sentinel::findById($id);
        $data['roles'] = Sentinel::getRoleRepository()->all();
        $data['nuevo'] = '';

        $root = CatalogTree::allRoot()->first();
        $root->findChildren(999);
        $data['catalog_root_node'] = $root;

        $data['catalog_names'] = array(
            'category' => 'productoCategoriaNombre',
        );

        $root = GalleryTree::allRoot()->first();
        $root->findChildren(999);
        $data['gallery_root_node'] = $root;

        $data['gallery_names'] = array(
            'category' => 'descargaCategoriaNombre',
        );

        $data['active'] = '';
        if(Sentinel::getActivationRepository()->completed($data['user'])) {
            $data['active'] = 'checked="checked"';
        }

        $data['catalog_permissions'] = [];
        $data['gallery_permissions'] = [];

        foreach ($user->permissions as $key => $permission) {
            if(str_contains($key, 'catalog.')) {
                $catId = explode('.', $key);
                $data['catalog_permissions'][$catId[1]] = $permission;
            }
            if(str_contains($key, 'gallery.')) {
                $galId = explode('.', $key);
                $data['gallery_permissions'][$galId[1]] = $permission;
            }
        }

        $data['hasPermissions'] = false;
        if($data['catalog_root_node']->getChildren() || $data['gallery_root_node']->getChildren()) {
            $data['hasPermissions'] = true;
        }

        $data['titulo'] = 'Modificar Usuario';
        $data['link'] = base_url("admin/users/actualizar");
        $data['txt_boton'] = "modificar usuario";

        $data['campos'] = $this->Usuarios->getUserFieldsTemplate($id);
        $data['countries'] = $this->Usuarios->countries();

        $data['txt_botImagen'] = 'Subir Imagen';
        $data['cropDimensions'] = $this->General->getCropImage(13);
        $data['image_coord'] = urlencode($data['user']->image_coord);
        $data['imagen'] = '';
        $data['imagenOrig'] = '';
        $data['imagenExtension'] = '';

        if($data['user']->image_extension != '')
        {
            //Eliminamos el cache del navegador
            $extension = $data['user']->image_extension;
            $extension = preg_replace('/\?+\d{0,}/', '', $extension);
            $data['imagen'] = '<img src="' . base_url() . 'assets/public/images/usuarios/usuario_' . $data['user']->id . '_admin.' . $extension . '" />';
            $data['imagenOrig'] = base_url() . 'assets/public/images/usuarios/usuario_' . $data['user']->id . '_orig.' . $extension;
            $data['imagenExtension'] = $data['user']->image_extension;
        }

        $this->load->view('admin/authentication/usuarioCrear_view', $data);
    }

    public function insertar()
    {

        $response = new stdClass();
        $response->error_code = 0;

        try {

            $activate = (bool) $this->Config->item('automatic_activation');

            if($this->input->post('userPass1') != $this->input->post('userPass2')) {
                throw new BadFunctionCallException("Las contrase&ntilde;as no coinciden");
            }

            $data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'image_extension' => $this->input->post('image_extension'),
                'image_coord' => urldecode($this->input->post('image_coord')),
                'password' => $this->input->post('userPass1'),
                'email' => $this->input->post('email'),
            );

            $user = Sentinel::register($data, $activate);

            $this->setPermissions($user);

            $this->Usuarios->insertFields($user->id);

            if($this->input->post('roles')) {
                $role = Sentinel::findRoleById($this->input->post('roles'));
                $role->users()->attach($user);
            }

            $response->new_id = $user->id;

        } catch (Exception $e) {
            $response->error_code = 1;
            $response->message = $e->getMessage();
        }

        $this->load->view('admin/request/json', array('return' => $response));

    }

    public function actualizar()
    {

        $response = new stdClass();
        $response->error_code = 0;

        $id = $this->input->post('userId');
        $user = Sentinel::findById($id);

        $active = (bool)$this->input->post('active');
        $activationRepo = Sentinel::getActivationRepository();

        if($active != (bool) $activationRepo->completed($user)) {

            if($active) {

                //If no actication exists create a new one
                if(!$activation = $activationRepo->exists($user)) {
                    $activation = $activationRepo->create($user);
                }

                //Activate the user
                if($activationRepo->complete($user, $activation->code)) {
                    //mandar mail activacion
                    $output = 'Su cuenta en ' . $_SERVER['SERVER_NAME'] . ' ha sido activada.';

                    $config['mailtype'] = 'html';
                    $this->email->initialize($config);
                    $this->email->from('contacto@' . str_replace('www.', '', $_SERVER['SERVER_NAME']), 'Website Mailer');
                    $this->email->to($this->input->post('email'));
                    $this->email->subject('Contact form ' . $_SERVER['SERVER_NAME']);
                    $this->email->message($output);
                }

            }
            else {

                $activationRepo->remove($user);

                //mandar mail desactivacion
                $output = 'Ud ha sido desactivado de ' . $_SERVER['SERVER_NAME'];

                $config['mailtype'] = 'html';
                $this->email->initialize($config);
                $this->email->from('contacto@' . str_replace('www.', '', $_SERVER['SERVER_NAME']), 'Website Mailer');
                $this->email->to($this->input->post('email'));
                $this->email->subject('Contact form ' . $_SERVER['SERVER_NAME']);
                $this->email->message($output);

            }

            $this->email->send();

        }


        try{

            $data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'username' => $this->input->post('username'),
                'email' => $this->input->post('email'),
                'active' => $active,
                'image_extension' => $this->input->post('image_extension'),
                'image_coord' => urldecode($this->input->post('image_coord'))
            );

            if($this->input->post('userPass1')) {
                $data['password'] = $this->input->post('userPass1');
            }

            Sentinel::update($user, $data);

            //Update the role
            if($this->input->post('roles')) {

                //Remove user from any role
                $roles = Sentinel::getRoleRepository()->all();
                foreach ($roles as $role) {
                    $role->users()->detach($user);
                }

                //Add the user to the role
                $role = Sentinel::findRoleById($this->input->post('roles'));
                if(!$user->inRole($role)) {
                    $role->users()->attach($user);
                }

            }

            $this->setPermissions($user);

            $this->Usuarios->updateFields($id);

        } catch (Exception $e) {
            $response = $this->cms_general->error('Ocurri&oacute; un problema al actualizar el usuario!', $e);
        }

        $this->load->view('admin/request/json', array('return' => $response));

    }

    /**
     * Sets the user's permissions to access catalog and gallery categories,
     * this overrides the permissions set in the user's role
     *
     * @param $user
     */
    private function setPermissions($user)
    {

        $permissions = [];
        $active = $this->input->post('active');

        if($active && isset($active['catalog'])) {
            foreach ($active['catalog'] as $id) {
                $permissions['catalog.' . $id] = isset($this->input->post('catalog')[$id]);
            }
        }

        if($active && isset($active['gallery'])) {
            foreach ($active['gallery'] as $id) {
                $permissions['gallery.' . $id] = isset($this->input->post('gallery')[$id]);
            }
        }

        $user->permissions = $permissions;
        $user->save();

    }

    public function eliminar($id)
    {

        $response = new stdClass();
        $response->error_code = 0;

        try{
            $user = Sentinel::findById($id);
            $user->delete();
        } catch (Exception $e) {
            $response = $this->cms_general->error('Ocurri&oacute; un problema al eliminar el usuario!', $e);
        }

        $this->load->view('admin/request/json', array('return' => $response));

    }

    public function template()
    {
        $data['campos'] = $this->Usuarios->getTemplate();
        $data['titulo'] = 'Campos';
        $this->load->view('admin/authentication/usuariosCampos_view', $data);
    }

    public function crearCampo()
    {

        $data['userFieldId'] = $this->cms_general->generarId('user_fields');
        $data['titulo'] = "Crear Campo";
        $data['link'] = base_url("admin/users/guardarCampo");
        $data['txt_boton'] = "crear";
        $data['nuevo'] = 'nuevo';

        /*
         * TRADUCCIONES
         */
        $traducciones = array();
        $data['idiomas'] = $this->Idioma->getLanguages();

        foreach ($data['idiomas'] as $idioma) {
            $traducciones[$idioma['idiomaDiminutivo']] = new stdClass();
            $traducciones[$idioma['idiomaDiminutivo']]->userFieldLabel = '';
            $traducciones[$idioma['idiomaDiminutivo']]->userFieldPlaceholder = '';
        }

        $data['traducciones'] = $traducciones;

        //obtengo entradas
        $data['inputId'] = '';
        $data['habilitado'] = 'checked="checked"';
        $data['userFieldRequired'] = '';
        $data['userFieldRequiredAuth'] = 'checked="checked"';
        $data['userFieldValidation'] = '';
        $data['inputs'] = $this->Usuarios->getInputs();
        $data['userFieldClass'] = '';
        $data['userFieldType'] = '';
        $data['userFieldOrderCol'] = '';
        $data['twoCheckoutName'] = '';
        $this->load->view('admin/authentication/usuariosCampoCrear_view', $data);
    }

    public function guardarCampo()
    {

        $response = new stdClass();
        $response->error_code = 0;

        try{
            $response->new_id = $this->Usuarios->guardarCampo();
        } catch (Exception $e) {
            $response = $this->cms_general->error('Ocurri&oacute; un problema al eliminar el usuario!', $e);
        }

        $this->load->view('admin/request/json', array('return' => $response));

    }

    public function modificarCampo($campoId)
    {

        $campo = $this->Usuarios->getCampo($campoId);

        $data['titulo'] = 'Editar Campo';

        $data['habilitado'] = '';
        if($campo->userFieldActive) {
            $data['habilitado'] = 'checked="checked"';
        }

        $data['userFieldRequired'] = '';
        if($campo->userFieldRequired) {
            $data['userFieldRequired'] = 'checked="checked"';
        }

        $data['userFieldRequiredAuth'] = '';
        if($campo->userFieldRequiredAuth) {
            $data['userFieldRequiredAuth'] = 'checked="checked"';
        }

        $data['userFieldId'] = $campo->userFieldId;
        $data['inputId'] = $campo->inputId;
        $data['inputs'] = $this->Usuarios->getInputs();
        $data['userFieldClass'] = $campo->userFieldClass;
        $data['userFieldValidation'] = $campo->userFieldValidation;
        $data['userFieldType'] = $campo->userFieldType;
        $data['userFieldOrderCol'] = $campo->userFieldOrderCol;
        $data['twoCheckoutName'] = $campo->twoCheckoutName;
        $data['txt_boton'] = 'Modificar Campo';
        $data['link']  = base_url('admin/users/actualizarCampo');
        $data['nuevo'] = '';

        /*
         * TRADUCCIONES
         */
        $traducciones = array();
        $data['idiomas'] = $this->Idioma->getLanguages();

        foreach ($data['idiomas'] as $idioma) {
            $campoTraduccion = $this->Usuarios->getCampoTranslation($idioma['idiomaDiminutivo'], $campoId);
            $traducciones[$idioma['idiomaDiminutivo']] = new stdClass();

            if((bool)$campoTraduccion) {
                $traducciones[$idioma['idiomaDiminutivo']]->userFieldLabel = $campoTraduccion->userFieldLabel;
                $traducciones[$idioma['idiomaDiminutivo']]->userFieldPlaceholder = $campoTraduccion->userFieldPlaceholder;
            } else {
                $traducciones[$idioma['idiomaDiminutivo']]->userFieldLabel = '';
                $traducciones[$idioma['idiomaDiminutivo']]->userFieldPlaceholder = '';
            }

        }

        $data['traducciones'] = $traducciones;

        $this->load->view('admin/authentication/usuariosCampoCrear_view', $data);
    }

    public function actualizarCampo()
    {

        $response = new stdClass();
        $response->error_code = 0;

        try{
            $this->Usuarios->actualizarCampo();
        } catch (Exception $e) {
            $response = $this->cms_general->error('Ocurri&oacute; un problema al actualizar el campo!', $e);
        }

        $this->load->view('admin/request/json', array('return' => $response));

    }

    public function eliminarCampo($id)
    {

        $response = new stdClass();
        $response->error_code = 0;

        try{
            $this->Usuarios->eliminarCampo($id);
        } catch (Exception $e) {
            $response = $this->cms_general->error('Ocurri&oacute; un problema al eliminar el usuario!', $e);
        }

        $this->load->view('admin/request/json', array('return' => $response));

    }

    public function reorganizarCampos()
    {
        $this->Usuarios->reorganizarCampos();
        $this->template();
    }

}