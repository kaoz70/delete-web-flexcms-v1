<?php
use Cartalyst\Sentinel\Native\Facades\Sentinel;

/**
 * Created by PhpStorm.
 * User: Miguel
 * Date: 08/01/14
 * Time: 02:32 PM
 */

class Login extends CI_Controller {

    private $assets_css = array(
        'assets/admin/css/admin.scss',
        'assets/admin/css/tableEditor.scss',
        'assets/admin/css/datepicker_dashboard.css',
        'assets/admin/css/MooEditable.css',
        'assets/admin/css/MooEditable.Extras.css',
        'assets/admin/css/MooEditable.UploadImage.css',
        'assets/admin/css/MooEditable.Forecolor.css',
        'assets/admin/css/MooEditable.SilkTheme.css',
        'assets/admin/css/Tree.css',
        'assets/admin/css/ImageManipulation.css',
        'assets/admin/css/Scrollable.css',
        'assets/admin/css/Uploader.css',
        'assets/admin/css/reportes.scss',
    );

    function __construct(){
        parent::__construct();
        $this->load->model('configuracion_model', 'Config');
        $this->load->model('admin/usuarios_model', 'Usuarios');
        $this->load->model('admin/module_model', 'Modulo');

        $this->load->library('session');
        $this->load->library('form_validation');

        $this->load->helper('date');

        $this->load->set_admin_theme();

    }

    /**
     * Initial check
     */
    public function index()
    {

        $user = Sentinel::getUser();

        $config = $this->Config->get();
        $data['title'] = $config->site_name;
        $data['error'] = $this->session->flashdata('error');
        $data['assets_css'] = $this->assets_css;

        //User is loged in and is administrator
        if ($user && Sentinel::hasAccess(['admin'])) {
            $this->renderAdmin();
        }

        //User is logged in but not admin
        else if ($user && !Sentinel::hasAccess(['admin'])) {
            $data['error'] = '<div class="error">Usted no tiene los permisos necesarios para poder entrar a esta secci&oacute;n. <a href="'. base_url() .'">regresar el inicio</a></div>';
            $this->load->view('admin/login_view', $data);
        }

        //Not loged in
        else {
            $this->load->view('admin/login_view', $data);
        }

    }

    private function renderAdmin()
    {

        date_default_timezone_set("America/Guayaquil");
        setlocale(LC_ALL, 'es_ES');

        $user = Sentinel::getUser();

        $config = $this->Config->get();
        $data['titulo'] = $config->site_name . " Control Panel";
        $data['user'] = $user;

        $root = PageTree::allRoot()->first();
        $root->findChildren(9999);

        $data['root_node'] = $root;
        $data['visible'] = $this->Modulo->getContentPages();

        $data['sess_expiration'] = $this->config->item('sess_expiration');

        $role = Sentinel::findRoleById(1);
        $data['permissions'] = $role->permissions;
        $data['sections'] = $this->Config->getSecciones();

        $data['assets_css'] = $this->assets_css;

        $data['assets_js'] = array();

        $this->load->view('admin/index_view', $data);
    }

    public function terminate()
    {
        Sentinel::logout(null, true);
        redirect('login');
    }

    public function logged_in()
    {
        $data['return'] = Sentinel::check();
        $this->load->view('admin/request/json', $data);
    }

    public function form(){
        $data['error'] = '';
        $data['form_action'] = base_url('login/validate');
        $this->load->view('admin/login_form_view', $data);
    }

    /**
     * Checks the user login
     */
    public function validate(){

        $this->form_validation->set_rules('username', 'Usuario', 'required');
        $this->form_validation->set_rules('password', 'Contraseña', 'required');

        /*
         * IF FORM IS VALID
         */
        if ($this->form_validation->run() == true) {

            try {

                $credentials = [
                    'email'    => $this->input->post('username'),
                    'password' => $this->input->post('password'),
                ];

                $user = Sentinel::authenticateAndRemember($credentials);

                if(!$user) {
                    throw new Exception('No se pudo validar sus credenciales');
                }

                if($user && !Sentinel::hasAccess(['admin'])) {
                    throw new Exception("No tiene acceso a esta secci&oacute;n");
                }

                redirect('login');

            } catch (Exception $e) {

                if($this->input->is_ajax_request()) {
                    $data['return'] = $e->getMessage();
                    $this->load->view('admin/request/html', $data);
                }
                else {
                    $this->session->set_flashdata('error', "<div class='error_red'>{$e->getMessage()}</div>");
                    redirect('login');
                }

            }

        }

        /*
         * IF INVALID FORM
         */
        else {
            $this->session->set_flashdata('error', '<div class="error">No se pudo verificar sus datos, por favor intentelo de nuevo</div>');
            redirect('login');
        }

    }

} 