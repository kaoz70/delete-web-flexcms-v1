<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Submit extends CI_Controller
{

    function __construct()
    {

        parent::__construct();

        $this->load->library('form_validation');
        $this->load->library('email');

        $this->load->model('submit_model', 'Submit');
        $this->load->model('mapas_model', 'Mapa');
        $this->load->model('idiomas_model', 'Idiomas');
        $this->load->model('configuracion_model', 'Config');

        $this->m_config = $this->Config->get();
        $this->theme_config = $this->load->set_theme($this->m_config->theme);

    }

    public function index()
    {

    }

    /**
     * Sends an email from the website's form
     *
     * @param $lang
     */
    public function contacto($lang)
    {

        if ($this->input->post('contacto')) {
            $contactoId = $this->input->post('contacto');
        } else {
            $contacts = $this->Submit->getContacts();
            $contactoId = $contacts[0]->contactoId;
        }

        $return = new stdClass();

        //Load the langue file for the Interface Translations
        $idioma = $this->Idiomas->get($lang);
        if (!$idioma) {
            show_my_404(current_url(), $this->m_config->theme);
        }
        $this->lang->load('ui', $idioma->idiomaNombre);

        $campos = $this->input->post('campos');

        $contacto = $this->Submit->getContact($contactoId);

        $name = 'Website Mailer';

        if (!$campos) {
            show_my_404(current_url(), $this->m_config->theme);
        }

        $campoIds = array_keys($campos);

        $contactoDeMapa = '';

        $campoArr = array();

        for ($i = 0; $i < count($campos); $i++) {

            $row = $this->Submit->getInputName($campoIds[$i]);

            $cont = $campos[$campoIds[$i]];

            //Es contacto de mapa
            if ($row->inputTipoId == 8) {
                $contactoDeMapa = $this->Mapa->getUbicacion($cont);
                $cont = $contactoDeMapa->mapaUbicacionNombre;
            }

            if (stristr($row->contactoCampoClase, 'name')) {
                $name = $campos[$campoIds[$i]];
            }

            if ($row->contactoCampoRequerido) {
                $this->form_validation->set_rules('campos['.$campoIds[$i].']', $row->contactoCampoValor, 'required');
            }

            if ($row->contactoCampoValidacion === 'email') {
                $this->form_validation->set_rules('campos['.$campoIds[$i].']', $row->contactoCampoValor, 'valid_email');
                $email = $campos[$campoIds[$i]];
            }

            $campoArr[$i] = new stdClass();
            $campoArr[$i]->data = $row;
            $campoArr[$i]->post_data = $cont;

        }

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        if ($this->form_validation->run() == false) {
            $return->sent = false;
            $return->message = $this->lang->line('ui_contact_form_error');
            $return->message .= validation_errors();
            $return->error = validation_errors();
        } else {

            $data['campos'] = $campoArr;

            $output = $this->load->view('email/contact_view', $data, true);

            $config['mailtype'] = 'html';
            $this->email->initialize($config);

            //It has to be an email thats created on the server
            $this->email->from($email, $name);
            $this->email->reply_to($email, $name);

            if ($contactoDeMapa == '') {
                $this->email->to($contacto->contactoEmail);
            } else {
                if ($contactoDeMapa->mapaUbicacionMail != '') {
                    $this->email->to($contactoDeMapa->mapaUbicacionMail);
                }
            }

            $this->email->subject('Contacto desde '.$_SERVER['SERVER_NAME']);
            $this->email->message($output);

            $mailto = mailto($contacto->contactoEmail, $contacto->contactoEmail);

            if ($return->sent = @$this->email->send()) {
                $return->message = $this->lang->line('ui_contact_sent');
            } else {
                $return->message = $this->lang->line('ui_contact_error');
                $return->message .= "<p>Si el problema persiste env&iacute;e un correo directamente a: $mailto</p>";
                $return->error = $this->email->print_debugger();
                log_message('error', $this->email->print_debugger());
            }

        }

        $this->load->set_admin_theme();
        $this->load->view('admin/request/json', ['return' => $return]);

    }

    /**
     * Subscribes an email to a list
     *
     * Form example:
     * <form action="<?=base_url('submit/boletin/024a8be22b')?>" method="post">
     * <input type="text" name="first_name">
     * <input type="text" name="phone">
     * <input type="text" name="email">
     * <input type="text" name="company">
     * <input type="submit" value="enviar">
     * </form>
     *
     * @param null $list_id
     */
    function boletin($list_id = null)
    {
        $response = $this->Submit->mailchimpNewsletter($list_id);

        $this->load->set_admin_theme();
        $this->load->view('admin/request/json', array('return' => $response));
    }

}