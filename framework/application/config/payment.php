<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: miguel
 * Date: 11/5/15
 * Time: 11:59 AM
 */

$development = 'development';
$production = 'production';
$sandbox = 'sandbox';
$live = 'live';
$environment =  'environment';

//Dummy array index for the parent class
$config['Payment'] = [];

$config['Interdin'] = [];

$config['TwoCheckoutGateway'] = [

    //Current payment development environment
    $environment => $sandbox,

    //Account Number
    'sellerId' => '901319517',

    $sandbox => [
        'url' => 'https://sandbox.2checkout.com/checkout/purchase',
        'secret_word' => 'MGUwNDYzZjAtNzU4My00N2UwLTk4MDgtY2Q2NzliMDliMmVl',
    ],

    $production => [
        'url' => 'https://www.2checkout.com/checkout/purchase',
        'secret_word' => 'MGUwNDYzZjAtNzU4My00N2UwLTk4MDgtY2Q2NzliMDliMmVl',
    ],
];

$config['PayPalGateway'] = [

    //$sandbox or $live
    $environment => $sandbox,

    /*
     * 1) Create a PayPal app here:
     * https://developer.paypal.com/developer/applications/create
     * 2) Get the Client ID and secret
     */
    $sandbox => [
        'client_id' => 'ASwr0yTtfjshdThsOc6pXS6ynHz1QpHA4iH32bYjV-JOQhoFoEY1myOQ2qTUcguPX7IFg8C81v_1WIHb',
        'client_secret' => 'EOa9VAv7QAJ8f4IQhOKQep0SUu-Nm6fH54bocNVDg6JeWsHnt04cg7wY9KpL4u7umBWXGXRJHeq0iZtb',
    ],

    $live => [
        'client_id' => '',
        'client_secret' => '',
    ],

];

$config['PayMe'] = [

    /**
     * Moneda según Estándar numérico ISO Tipo Alfanumérico
     * Dólares Americanos = 840
     */
    'currency_code' => 840,

    //Current payment development environment
    $environment => $development,

    $development => [
        'wallet_idEntCommerce' => '232',
        'vpos2_idEntCommerce' => '123',
        'commerce_id' => '7597',
        'wallet_url' => 'https://test2.alignetsac.com/WALLETWS/services/WalletCommerce?wsdl',
        'vpos2_url' => 'https://test2.alignetsac.com/VPOS2/faces/pages/startPayme.xhtml',
        'wallet_key' => 'dLbedZBAwkzgWSFwmKD_869387443777',
        'vpos2_key' => 'CXXfMRBAnxknnVLDv$4289672488',
    ],

    $production => [
        'wallet_idEntCommerce' => '52',
        'vpos2_idEntCommerce' => '23', //IDACQUIRER
        'commerce_id' => '6346', //IDECOMERCE
        'wallet_url' => 'https://www.pay-me.pe/WALLETWS/services/WalletCommerce?wsdl',
        'vpos2_url' => 'https://vpayment.verifika.com/VPOS2/faces/pages/startPayme.xhtml',
        'wallet_key' => 'JdWVaaqaLVxADDjemJ-79356546876',
        'vpos2_key' => 'NhHgXLAJHGUJcwzLYx*54933873533',
    ],

];

$config['DineroElectronico'] = [

    //ES, EN, QU (Español, Inglés y Quechua)
    'language' => 'ES',

    //Current payment development environment
    $environment => $development,

    'errors' => [
        'connection' => '<div class="error">El servicio de Dinero Electr&oacute;nico del BCE est&aacute; experimentando problemas, por favor vuelva a intentarlo en unos minutos.</div>',
    ],

    $development => [

        // The administrator user, has read and write access to the WS
        'user' => 'AdmProfaint01',
        'pass' => '19967e8a25',

        // The user that has read only access rights to the WS
        'readonly_user' => 'SupProfaint01',
        'readonly_pass' => 'c679a881cd',

        'url' => 'https://181.211.102.40:8443/mts_bce/services/MTSService?wsdl',

        // BCE send a .p12 file, in the console (linux) we use:
        // openssl pkcs12 -in s_priv_pub.p12 -out s_priv_pub.pem -nodes
        // to convert it to a .pem that PHP can read it
        'certificate_path' => APPPATH . 's_priv_pub.pem',
        'certificate_pass' => 'bce',

    ],

    $production => [

    ],

];