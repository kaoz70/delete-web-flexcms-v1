<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config = array(
	'apikey' => '', // Insert your api key from http://admin.mailchimp.com/account/api
	'verify_ssl' => FALSE   // Optional (defaults to FALSE)
);