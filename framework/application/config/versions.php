<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config = [
    'flexcms' => '1.7.9',
    'min_php' => '5.4',
    'min_sql' => '5.1',
    'php_modules' => [
        'pdo'
    ]
];