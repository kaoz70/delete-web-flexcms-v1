<?php

/**
 * Created by PhpStorm.
 * User: Miguel
 * Date: 6/3/2016
 * Time: 2:34 PM
 */
interface PaymentInterface
{

    public static function button($title, $orderId, $lang);

    public static function getResult();

}