<?php

/**
 * Created by PhpStorm.
 * User: Miguel
 * Date: 6/10/2016
 * Time: 2:06 PM
 */
interface SocialAuthInterface
{

    /**
     * Gets the user redirect url, the one we use to ask for app permissions
     *
     * @param $base_url
     * @return mixed
     */
    public function getRedirectUrl($base_url);

    /**
     * Handles the callback data returned by the social platform
     *
     * @return mixed
     */
    public function callback();

    /**
     * Returns the user's token provided by the callback method
     *
     * @return mixed
     */
    public  function getUserToken();

}