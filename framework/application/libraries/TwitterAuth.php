<?php
use Abraham\TwitterOAuth\TwitterOAuth;

/**
 * Created by PhpStorm.
 * User: Miguel
 * Date: 6/10/2016
 * Time: 2:05 PM
 */
class TwitterAuth extends SocialAuth implements SocialAuthInterface
{

    private static $connection;
    private static $config;
    private $CI;
    private $userToken;

    const OAUTH_TOKEN = 'oauth_token';
    const OAUTH_TOKEN_SECRET = 'oauth_token_secret';

    public function __construct($params)
    {
        $this->CI =& get_instance();
        static::$config = $params[0];
        static::$connection = new TwitterOAuth(
            static::$config->twitter_consumer_key,
            static::$config->twitter_consumer_secret
        );
    }

    public function getRedirectUrl($base_url)
    {
        $request_token = static::$connection->oauth(
            'oauth/request_token',
            [
                'oauth_callback' => $base_url
            ]
        );

        $_SESSION[static::OAUTH_TOKEN] = $request_token[static::OAUTH_TOKEN];
        $_SESSION[static::OAUTH_TOKEN_SECRET] = $request_token[static::OAUTH_TOKEN_SECRET];

        return static::$connection->url('oauth/authorize', [static::OAUTH_TOKEN => $request_token[static::OAUTH_TOKEN]]);
    }

    /**
     * @return mixed
     */
    public function getUserToken()
    {
        return $this->userToken;
    }

    public function callback()
    {

        $request_token = [];
        $request_token[static::OAUTH_TOKEN] = $_SESSION[static::OAUTH_TOKEN];
        $request_token[static::OAUTH_TOKEN_SECRET] = $_SESSION[static::OAUTH_TOKEN_SECRET];

        // Abort! Something is wrong.
        if (isset($_REQUEST[static::OAUTH_TOKEN]) && $request_token[static::OAUTH_TOKEN] !== $_REQUEST[static::OAUTH_TOKEN]) {
            throw new Exception('No coinciden las credenciales, por seguridad no se va a continuar');
        }

        $connection = new TwitterOAuth(
            static::$config->twitter_consumer_key,
            static::$config->twitter_consumer_secret,
            $request_token[static::OAUTH_TOKEN],
            $request_token[static::OAUTH_TOKEN_SECRET]
        );

        $access_token = $connection->oauth("oauth/access_token", ["oauth_verifier" => $this->CI->input->get('oauth_verifier')]);

        $connection = new TwitterOAuth(
            static::$config->twitter_consumer_key,
            static::$config->twitter_consumer_secret,
            $access_token[static::OAUTH_TOKEN],
            $access_token[static::OAUTH_TOKEN_SECRET]
        );

        $credentials = $connection->get("account/verify_credentials");

        if(isset($credentials->errors)) {
            throw new Exception($credentials->errors[0]->message);
        }

        $this->userToken = $access_token;

        return $credentials;
        
    }
}