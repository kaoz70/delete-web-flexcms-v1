<?php
use Cartalyst\Sentinel\Native\Facades\Sentinel;

/**
 * Created by PhpStorm.
 * User: miguel
 * Date: 10/28/15
 * Time: 4:21 PM
 */

class TwoCheckoutGateway extends Payment implements PaymentInterface {

    static protected $name = '2Checkout';

    public static function button($title, $orderId, $lang)
    {

        static::$config = static::readConfig();
        $env_config = static::$config[static::$config['environment']];

        $CI =& get_instance();
        $user = Sentinel::getUser();

        if(!$user) {
            return;
        }

        $data['user'] = $user;
        $data['orderId'] = $orderId;
        $data['lang'] = $lang;
        $data['user_fields'] = static::getUserFields($user, $lang);

        $data['config'] = static::$config;
        $data['env_config'] = $env_config;

        //Are they tangible products? Are they going to be shipped?
        $data['tangible'] = (boolean) $CI->flexi_cart->shipping_total(TRUE, FALSE, TRUE);
        $CI->load->view('payment/two_checkout', $data);

    }

    public static function getResult()
    {
        $CI =& get_instance();

        $post = $CI->input->post();

        $env_config = static::$config[static::$config['environment']];

        if($post) {
            $orderNumber = $CI->input->post("order_number");
            $key = $CI->input->post("key");
            $sid = $CI->input->post("sid");
        } else {
            $orderNumber = $CI->input->get("order_number");
            $key = $CI->input->get("key");
            $sid = $CI->input->get("sid");
        }

        //2Checkout Secret Word
        $hashSecretWord = $env_config['secret_word'];

        //2Checkout account number
        $hashSid = static::$config["sellerId"];

        //Sale total to validate against
        $hashTotal = $CI->flexi_cart->total(TRUE, FALSE, TRUE);

        //2Checkout Order Number
        $hashOrder = $orderNumber;
        $StringToHash = strtoupper(md5($hashSecretWord . $hashSid . $hashOrder . $hashTotal));

        if ($StringToHash != $key) {
            $error = 'Fail - Hash Mismatch for order: ' . $orderNumber;
            log_message("error", $error);
            throw new Exception($error);
        }

        return $sid;

    }

}