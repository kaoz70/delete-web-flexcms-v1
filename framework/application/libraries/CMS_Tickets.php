<?php use Cartalyst\Sentinel\Native\Facades\Sentinel;

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CMS_Tickets {

    private $CI;
    private $user;
    private $lang;

    public function __construct()
    {
        $this->CI =& get_instance();
        $this->user = Sentinel::getUser();
        $this->lang = $this->CI->m_idioma;
        $this->CI->load->helper('ticket');
        $this->CI->load->model('configuracion_model', 'Config');
        $this->CI->lang->load('ticket', $this->CI->m_idioma_object->idiomaNombre);

        if(!$this->user) {
            return "Debe estar loqueado para acceder a esta secci&oacute;n";
        }

    }

    public function create($data)
    {

        $method = $this->CI->uri->segment(3);
        $html = $this->CI->load->view('paginas/content_open_view', $data, true);

        $data['message'] = $this->CI->session->flashdata('message');
        $data['error'] = $this->CI->session->flashdata('error');

        switch ($method)
        {
            case 'new':
                $html .= $this->newTicket($data);
                break;
            case 'insert':
                $html .= $this->insertTicket($data);
                break;
            case 'ticket':
                $html .= $this->viewTicket($data, $this->CI->uri->segment(4));
                break;
            case 'comment':
                $html .= $this->insertComment($data, $this->CI->uri->segment(4));
                break;
            case 'close':
                $html .= $this->closeTicket($data, $this->CI->uri->segment(4));
                break;
            case 'closed':
                $html .= $this->closed($data);
                break;
            default:
                $html .= $this->index($data);
        }

        $html .= $this->CI->load->view('paginas/content_close_view', $data, true);

        return $html;

    }

    private function index($data)
    {
        $data['tickets'] = Ticket::where('user_id', $this->user->id)->where('closed', 0)->get();
        return $this->CI->load->view('paginas/tickets/list_opened_view', $data, true);
    }

    private function closed($data)
    {
        $data['tickets'] = Ticket::where('user_id', $this->user->id)->where('closed', 1)->get();
        return $this->CI->load->view('paginas/tickets/list_closed_view', $data, true);
    }

    private function newTicket($data)
    {
        $data['user'] = $this->user;
        return $this->CI->load->view('paginas/tickets/new_view', $data, true);
    }

    private function insertTicket($data)
    {

        try {

            //Create the ticket
            $ticket = new Ticket();
            $ticket->uuid = $this->gen_uuid();
            $ticket->institution = $this->CI->input->post('institution');
            $ticket->user_id = $this->user->id;
            $ticket->user_name = $this->CI->input->post('user_name');
            $ticket->area = $this->CI->input->post('area');
            $ticket->title = $this->CI->input->post('title');
            $ticket->status = lang('ticket_status_pending_support');
            $ticket->type = $this->CI->input->post('type');
            $ticket->contract_number = $this->CI->input->post('contract_number');
            $ticket->contract_end_date = $this->CI->input->post('contract_end_date');
            $ticket->priority = $this->CI->input->post('priority');
            $ticket->level = $this->CI->input->post('level');
            $ticket->save();

            //Add the ticket detail, this is used like a conversation
            $ticketDetail = new TicketComment();
            $ticketDetail->ticket_id = $ticket->id;
            $ticketDetail->detail = $this->CI->input->post('detail');
            $ticketDetail->user_id = $this->user->id;
            $ticketDetail->status = 'open';
            $ticketDetail->save();

            //Upload any files
            $this->uploadFiles($ticket, $ticketDetail);

            $output = $this->CI->load->view('email/ticket_comment_view', $ticketDetail, true);
            $this->sendEmail($ticket, $output);

        } catch (Exception $e) {
            $this->CI->session->set_flashdata('error', $e->getMessage());
            redirect($this->lang . '/' . $data['pagina_url']);
        }

        $this->CI->session->set_flashdata('message', "Ticket creado exitosamente");
        redirect($this->lang . '/' . $data['pagina_url']);

    }

    private function viewTicket($data, $uuid)
    {
        $data['user'] = $this->user;
        $data['ticket'] = Ticket::where('user_id', $this->user->id)->where('uuid', $uuid)->first();
        return $this->CI->load->view('paginas/tickets/detail_view', $data, true);
    }

    private function insertComment($data, $uuid) {

        $messageAppend = '';

        try {

            $ticket =  Ticket::where('user_id', $this->user->id)->where('uuid', $uuid)->first();
            $ticket->status = lang('ticket_status_pending_support');

            $ticketDetail = new TicketComment();
            $ticketDetail->ticket_id = $ticket->id;
            $ticketDetail->detail = $this->CI->input->post('detail');
            $ticketDetail->user_id = $this->user->id;

            if($ticket->closed) {

                $ticket->reopened_at = date('Y-m-d H:i:s', time());
                $ticket->closed = 0;
                $messageAppend = '. Se ha vuelto a abrir el ticket.';

                $ticketDetail->status = 'open';
            }

            $ticket->save();
            $ticketDetail->save();

            $this->uploadFiles($ticket, $ticketDetail);

            $output = $this->CI->load->view('email/ticket_comment_view', $ticketDetail, true);
            $this->sendEmail($ticket, $output);

        } catch (Exception $e) {
            $this->CI->session->set_flashdata('error', $e->getMessage());
            redirect("{$this->lang}/{$data['pagina_url']}/ticket/$uuid");
        }

        $this->CI->session->set_flashdata('message', "Comentario enviado exitosamente$messageAppend");
        redirect("{$this->lang}/{$data['pagina_url']}/ticket/$uuid");

    }

    private function uploadFiles($ticket, $ticketDetail)
    {
        if( ! empty($_FILES['userfile']['name'][0])) {

            //Create the main ticket folder
            $path = "assets/public/files/tickets/" . $ticket->uuid;
            mkdir($path, 0755, TRUE);

            //Create the subfolder
            $path .= '/' . $ticketDetail->id;
            mkdir($path, 0755, TRUE);

            //Upload files
            $config['upload_path'] = $path;
            $config['allowed_types'] = '*';

            $this->CI->load->library('upload', $config);

            if ($fileData = $this->CI->upload->do_multi_upload('userfile')) {
                $ticketDetail->files = json_encode($fileData);
                $ticketDetail->save();
            } else {
                throw new Exception($this->CI->upload->display_errors());
            }
        }
    }

    private function closeTicket($data, $uuid)
    {

        try {

            $ticket =  Ticket::where('user_id', $this->user->id)->where('uuid', $uuid)->first();
            $ticket->closed = 1;
            $ticket->status = lang('ticket_status_closed_user');
            $ticket->closed_at = date('Y-m-d H:i:s', time());
            $ticket->save();

            if($this->CI->input->post('close_comment')) {

                $ticketDetail = new TicketComment();
                $ticketDetail->ticket_id = $ticket->id;
                $ticketDetail->detail = $this->CI->input->post('close_comment');
                $ticketDetail->user_id = $this->user->id;
                $ticketDetail->status = 'close';
                $ticketDetail->save();

                $output = $this->CI->load->view('email/ticket_comment_view', $ticketDetail, true);

            } else {
                $output = '[Cerrado sin comentario]';
            }

            $this->sendEmail($ticket, $output);

        }  catch (Exception $e) {
            $this->CI->session->set_flashdata('error', $e->getMessage());
            redirect("{$this->lang}/{$data['pagina_url']}/ticket/$uuid");
        }

        $this->CI->session->set_flashdata('message', "Ticket cerrado exitosamente");
        redirect("{$this->lang}/{$data['pagina_url']}");

    }

    /**
     * Generates a random unique id
     *
     * @param int $len
     * @return string
     */
    private function gen_uuid($len = 8)
    {

        $uuid = substr(str_shuffle("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $len);

        //Check if there is a ticket with this id already
        if(Ticket::where('uuid', $uuid)->first()) {
            //Generate a new id until we find one that's not used
            $uuid = $this->gen_uuid($len);
        }

        return $uuid;
    }

    /**
     * @param $ticket
     * @param $emailBody
     * @return bool
     * @throws Exception
     */
    private function sendEmail($ticket, $emailBody)
    {

        $config['mailtype'] = 'html';
        $this->CI->email->initialize($config);

        //It has to be an email thats created on the server
        $this->CI->email->from("no-reply@{$_SERVER['SERVER_NAME']}", 'Soporte');
        $this->CI->email->to($ticket->user()->email);
        $this->CI->email->to($ticket->supportEmail($this->CI->Config->item('tickets_default_email')));

        $this->CI->email->subject("[TICKET #{$ticket->uuid}] - {$ticket->title}");
        $this->CI->email->message($emailBody);

        if (@$this->CI->email->send()) {
            return true;
        } else {
            log_message('error', $this->CI->email->print_debugger());
            throw new Exception("No se pudo enviar el correo de notificaci&oacute;n");
        }

    }

}

/* End of file CMS_Tickets.php */