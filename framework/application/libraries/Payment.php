<?php

/**
 * Created by PhpStorm.
 * User: Miguel
 * Date: 6/3/2016
 * Time: 2:34 PM
 */
class Payment
{

    static protected $config;
    static protected $config_file = 'payment';
    static protected $name;

    function __construct()
    {
        try {
            static::$config = $this->readConfig();
        } catch (Exception $e) {
            echo $e->getMessage();
        }

    }

    protected function getConfig()
    {
        return static::$config;
    }

    /**
     * Read the configuration stored in the config file
     *
     * @return mixed
     * @throws Exception
     */
    protected static function readConfig()
    {

        $CI =& get_instance();
        $CI->config->load(static::$config_file, TRUE);

        if( ! $config = $CI->config->item(static::$config_file)) {
            throw new Exception("No existe el archivo de configuracion: " . static::$config_file . ".php");
        }

        if( ! array_key_exists(get_called_class(), $config)) {
            throw new Exception("No existe configuracion para: " . get_called_class());
        }

        return $CI->config->item(static::$config_file)[get_called_class()];

    }

    protected static function getUserFields($user, $lang)
    {
        $CI =& get_instance();
        $fields = [];
        $user_fields = $CI->Usuarios->getCamposUser($user->id, $lang);

        foreach ($user_fields as $field) {
            $fields[$field->userFieldType][] = $field;
        }

        return $fields;

    }

    /**
     * @return mixed
     */
    public static function getName()
    {
        return static::$name;
    }

}