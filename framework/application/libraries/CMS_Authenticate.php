<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

use Cartalyst\Sentinel\Native\Facades\Sentinel;

class CMS_Authenticate
{

    private $config;
    private $CI;
    private $lang;
    private $currentPage;
    private $userConfig;

    public function __construct($config)
    {

        $this->config = $config['config'];

        $CI =& get_instance();
        $this->CI = $CI;
        $CI->load->library('session');
        $CI->load->library('form_validation');

        $CI->load->helper('url');

        $CI->load->model('usuarios_model', 'Usuarios');
        $CI->load->model('pedido_model', 'Pedido');

        $CI->load->model('configuracion_model', 'Config');
        $CI->load->model('submit_model', 'Submit');
        
        $CI->load->library('SocialAuth');

        $this->userConfig = $CI->Config->get('users');

        if ((bool)$this->config->facebook_login) {
            $CI->load->library('FacebookAuth', [$this->config]);
        }

        if ((bool)$this->config->twitter_login) {
            $CI->load->library('TwitterAuth', [$this->config]);
        }

    }

    public function create($data, $idioma, $currentPage, $metodo)
    {
        $CI =& get_instance();

        $this->lang = $idioma;
        $this->currentPage = $currentPage;

        $CI->form_validation->set_error_delimiters('<small class="error">', '</small>');

        $html = '';

        switch ($metodo) {

            case '':

                /*
                 * Pagina perfil del usuario
                 */
                if (Sentinel::check()) {
                    $message = $CI->session->userdata('message');
                    $CI->session->unset_userdata('mensaje');

                    $html .= $this->renderIndex($data, Sentinel::getUser(), $message);
                }

                /*
                 * Pagina de login (Formulario)
                 */
                else {
                    $html .= $this->renderLoginView($data, $idioma, $currentPage, '');
                }

                break;

            /*
            * LOGIN CONTROLLER
            */
            case 'login':

                //validate form input
                $CI->form_validation->set_rules(
                    $this->userConfig->login_identity,
                    $CI->lang->line('ui_auth_email'),
                    'required'
                );
                $CI->form_validation->set_rules('password', $CI->lang->line('ui_auth_password'), 'required');

                if ($CI->form_validation->run() == true) {
                    $this->login(
                        $CI->input->post($this->userConfig->login_identity),
                        $CI->input->post('password'),
                        $data
                    );
                } else {

                    //Get the user data
                    if ($session = $CI->facebook->get_session()) {
                        $user = $CI->facebook->get_user();
                        $this->login($user['email'], $user['id'], $data);
                    } else {
                        $CI->session->set_flashdata('error', validation_errors());
                        redirect($data['diminutivo'].'/'.$data['pagAutenticacion']->paginaNombreURL);
                    }

                }

                break;

            case 'logout':
                Sentinel::logout();
                $CI->session->set_flashdata('error', lang('logout_successful'));
                $CI->session->unset_userdata('redirect_url');
                redirect($data['diminutivo'].'/'.$data['pagAutenticacion']->paginaNombreURL);
                break;

            case 'password':

                $passwordMethod = $CI->uri->segment(4, '');
                $data['linkForgotPasswordSuccess'] = base_url($idioma.'/'.$currentPage.'/password/confirm');

                switch ($passwordMethod) {

                    /*
                     * Pagina recuperar contraseña
                     */
                    case '':

                        $data['message'] = '';
                        $data['email'] = $CI->input->post('email');

                        $html .= $CI->load->view('paginas/autenticacion/forgot_password_view', $data, true);

                        break;

                    case 'edit':

                        $CI =& get_instance();

                        if ($user = Sentinel::getUser()) {
                            $data['message'] = $CI->session->flashdata('error');
                            $data['update_link'] = base_url($this->lang . '/' . $currentPage . '/password/update');
                            return $CI->load->view('paginas/autenticacion/usuario_password_edit_view', $data, true);
                        } else {
                            Sentinel::logout();
                            $CI->session->set_flashdata('error', 'El usuario registrado ya no existe');
                            redirect($data['diminutivo'].'/'.$data['pagAutenticacion']->paginaNombreURL);
                        }

                        break;

                    case 'update':

                        $CI =& get_instance();

                        if ($user = Sentinel::getUser()) {

                            $credentials = [
                                $this->userConfig->login_identity => $user->{$this->userConfig->login_identity},
                                'password' => $CI->input->post('old_password'),
                            ];

                            if(Sentinel::stateless($credentials)) {

                                $CI->form_validation->set_rules(
                                    'password',
                                    'Contraseña',
                                    'required|min_length['.$this->userConfig->password_min_length.']|max_length['.$this->userConfig->password_max_length.']|matches[password_confirm]'
                                );
                                $CI->form_validation->set_rules(
                                    'password_confirm',
                                    'Verificación de Contraseña',
                                    'required'
                                );

                                if ($CI->form_validation->run() === true) {
                                    Sentinel::update($user, [
                                        'password' => $CI->input->post('password')
                                    ]);

                                    $CI->session->set_flashdata('message', lang('password_change_successful'));
                                    redirect($data['diminutivo'].'/'.$data['pagAutenticacion']->paginaNombreURL);

                                } else {
                                    $CI->session->set_flashdata('error', validation_errors());
                                    redirect($data['diminutivo'].'/'.$data['pagAutenticacion']->paginaNombreURL . '/password/edit');
                                }

                            } else {
                                $CI->session->set_flashdata(
                                    'error',
                                    'La contrase&ntilde;a ingresada no coincide con la actual'
                                );
                                redirect($data['diminutivo'].'/'.$data['pagAutenticacion']->paginaNombreURL . '/password/edit');
                            }

                            return $CI->load->view('paginas/autenticacion/usuario_password_edit_view', $data, true);
                        } else {
                            Sentinel::logout();
                            $CI->session->set_flashdata('error', 'El usuario registrado ya no existe');
                            redirect($data['diminutivo'].'/'.$data['pagAutenticacion']->paginaNombreURL);
                        }

                        break;

                    /*
                    * Pagina de confirmacion de envio
                    */
                    case 'confirm':

                        try {

                            $CI->form_validation->set_rules('email', $CI->lang->line('ui_auth_email'), 'required');

                            if ($CI->form_validation->run() === true) {

                                $user = Sentinel::findByCredentials([
                                    'email' => $CI->input->post('email')
                                ]);

                                if(!$user) {
                                    throw new Exception("El usuario no existe");
                                }

                                $reminder = Sentinel::getReminderRepository()->create($user);

                                if ($reminder) {

                                    $contacts = $CI->Submit->getContacts();
                                    $output = $CI->load->view('paginas/autenticacion/email/forgot_password_view', [
                                        'identity' => $user->email,
                                        'code' => $reminder->code,
                                    ], true);

                                    if($this->sendEmail($contacts[0]->contactoEmail, $user->email, lang('forgot_password_successful'), $output)) {
                                        $data['message'] = '<div class="success">Email enviado satisfactoriamente</div>';
                                        $CI->session->unset_userdata('message');
                                        $html .= $CI->load->view(
                                            'paginas/autenticacion/forgot_password_confirmation_view',
                                            $data,
                                            true
                                        );
                                    } else {
                                        throw new Exception("No se pudo enviar el correo");
                                    }

                                } else {
                                    throw new Exception("no se pudo obtener c&oacute;digo");
                                }

                            } else {
                                throw new Exception(validation_errors());
                            }


                        } catch (Exception $e) {

                            //set any errors and display the form
                            $data['message'] = $e->getMessage();
                            $data['email'] = str_replace(
                                '"',
                                '&quot;',
                                $CI->input->post('email', true)
                            ); //Replace " in case of XSS

                            $html .= $CI->load->view('paginas/autenticacion/forgot_password_view', $data, true);

                        }

                        break;

                    //Reseteo de contraseña
                    case 'reset':

                        $code = $CI->uri->segment(5);

                        try {

                            $reminder = Sentinel::findReminderByCode($code);

                            if (!$reminder) {
                                show_404();
                            }

                            if ($user = Sentinel::findById($reminder->user_id)) {
                                //if the code is valid then display the password reset form

                                $CI->form_validation->set_rules(
                                    'new',
                                    $data['ui_auth_password'],
                                    'required|min_length['.$this->userConfig->password_min_length.']|max_length['.$this->userConfig->password_max_length.']|matches[new_confirm]'
                                );
                                $CI->form_validation->set_rules('new_confirm', $data['ui_auth_password_again'], 'required');

                                if ($CI->form_validation->run() == false) {
                                    //display the form
                                    $data['min_password_length'] = $this->userConfig->password_min_length;

                                    $data['user_id'] = array(
                                        'name' => 'user_id',
                                        'id' => 'user_id',
                                        'type' => 'hidden',
                                        'value' => $user->id,
                                    );
                                    $data['csrf'] = $this->_get_csrf_nonce();
                                    $data['code'] = $code;
                                    $data['link'] = $idioma.'/'.$currentPage.'/password/reset/'.$code;

                                    //render
                                    $html .= $CI->load->view('paginas/autenticacion/reset_password', $data, true);
                                } else {

                                    // do we have a valid request?
                                    if ($this->_valid_csrf_nonce() === true && $user->id == $CI->input->post('user_id')) {

                                        if ($reminder = Sentinel::reminderComplete($user, $code, $CI->input->post('new'))) {
                                            //if the password was successfully changed
                                            $CI->session->set_flashdata('message', lang('password_change_successful'));
                                            redirect($idioma.'/'.$currentPage.'/login/', 'refresh');
                                        } else {
                                            throw new RuntimeException(lang('password_change_unsuccessful'));
                                        }

                                    }

                                    //something fishy might be up
                                    else {
                                        throw new \Illuminate\Session\TokenMismatchException("This form post did not pass our security checks");
                                    }

                                }
                            } else {
                                throw new InvalidArgumentException("Codigo Invalido");
                            }

                        } catch (Exception $e) {

                            //if the code is invalid then send them back to the forgot password page
                            $CI->session->set_flashdata('error', $e->getMessage());
                            redirect($idioma.'/'.$currentPage.'/password', 'refresh');

                        }

                        break;

                }


                break;

            case 'fb-callback':
                $html .= $this->facebookCallback($html, $idioma, $data, $currentPage);
                break;

            case 'tw-callback':
                $html .= $this->twitterCallback($html, $idioma, $data, $currentPage);
                break;

            /*
             * Pagina de registro
             */
            case 'register':

                $registerMethod = $CI->uri->segment(4, '');

                switch ($registerMethod) {

                    /*
                     * Pagina formulario registro
                     */
                    case '':
                        if (Sentinel::check()) {
                            $html .= $this->renderProfileView(
                                $data,
                                Sentinel::getUser(),
                                '',
                                $idioma,
                                $currentPage
                            );
                        } else {
                            $campos = $CI->Usuarios->getCampos($idioma, true);
                            foreach ($campos as &$campo) {
                                $campo->userFieldRelContent = '';
                            }

                            $data['userCampos'] = $campos;

                            $html .= $this->renderRegisterView('', $campos, $data, $idioma, $currentPage);
                        }

                        break;

                    /*
                    * Pagina validacion registro
                    */
                    case 'add':

                        //TODO: xss_clean not cleaning netsparker's attack correctly
                        $CI->form_validation->set_rules('first_name', 'Nombre', 'required');
                        $CI->form_validation->set_rules('last_name', 'Apelido', 'required');
                        $CI->form_validation->set_rules(
                            $this->userConfig->login_identity,
                            $this->userConfig->login_identity,
                            ($this->userConfig->login_identity == 'email' ? 'valid_email|' : '').'required|is_unique[users.'.$this->userConfig->login_identity.']'
                        );
                        $CI->form_validation->set_rules(
                            'password',
                            'Contraseña',
                            'required|min_length['.$this->userConfig->password_min_length.']|max_length['.$this->userConfig->password_max_length.']|matches[password_confirm]'
                        );
                        $CI->form_validation->set_rules('password_confirm', 'Verificación de Contraseña', 'required');

                        $campos = $CI->Usuarios->getCampos($idioma, true);
                        foreach ($campos as $key => $campo) {
                            if ($campo->userFieldClass != '') {
                                $CI->form_validation->set_rules(
                                    'campo['.$campo->userFieldId.']',
                                    $campo->userFieldLabel,
                                    $campo->userFieldClass
                                );
                            } else {
                                $CI->form_validation->set_rules(
                                    'campo['.$campo->userFieldId.']',
                                    $campo->userFieldLabel
                                );
                            }

                        }

                        if (isset($_POST) && !empty($_POST)) {

                            try {

                                if ($CI->form_validation->run() === true) {

                                    $password = $CI->input->post('password');

                                    $userData = array(
                                        'first_name' => $CI->input->post('first_name'),
                                        'last_name' => $CI->input->post('last_name'),
                                        'password' => $password,
                                        'email' => $CI->input->post('email'),
                                    );

                                    if($this->userConfig->login_identity != 'email') {
                                        $userData[$this->userConfig->login_identity] = $CI->input->post($this->userConfig->login_identity);
                                    }

                                    if ($user = Sentinel::register($userData, (bool) $this->userConfig->automatic_activation)) {

                                        //If the user wants to be subscribed to the newsletter
                                        if($newsletterListId = $CI->uri->segment(5) && $CI->input->post('newsletter')) {

                                            $response = $CI->Submit->mailchimpNewsletter($newsletterListId);
                                            if($response->success === false) {

                                                $CI->session->set_flashdata('message', $response->message);

                                                log_message("error", $response->message);
                                            }
                                        }

                                        $this->post_register($user, $password);

                                    } else {

                                        throw new RuntimeException(lang('account_creation_unsuccessful'));
                                    }
                                } else {
                                    throw new Exception(validation_errors());
                                }

                            } catch (Exception $e) {

                                $CI->session->set_flashdata('error', $e->getMessage());

                                if($redirect = $CI->session->userdata('current_url')){
                                    redirect($redirect);
                                } else {
                                    redirect($idioma . '/' . $currentPage . '/register');
                                }

                            }

                        }

                        break;

                }

                break;

            case 'edit':

                if ($user = Sentinel::check()) {
                    $message = $CI->session->userdata('message');
                    $CI->session->unset_userdata('mensaje');

                    $html .= $this->renderProfileView(
                        $data,
                        $user,
                        $message,
                        $idioma,
                        $currentPage
                    );
                }

                /*
                 * Pagina de login (Formulario)
                 */
                else {
                    $html .= $this->renderLoginView($data, $idioma, $currentPage, '');
                }

                break;

            case 'update':

                if ($user = Sentinel::getUser()) {

                    //validate form input
                    $CI->form_validation->set_rules('first_name', 'Nombre', 'required');
                    $CI->form_validation->set_rules('last_name', 'Apelido', 'required');

                    $camposInput = $CI->input->post('campo');
                    $campos = $CI->Usuarios->getCampos($idioma);

                    foreach ($campos as $key => $campo) {

                        if ($campo->userFieldClass != '') {
                            $CI->form_validation->set_rules(
                                'campo['.$campo->userFieldId.']',
                                $campo->userFieldLabel,
                                $campo->userFieldClass
                            );
                        } else {
                            $CI->form_validation->set_rules(
                                'campo['.$campo->userFieldId.']',
                                $campo->userFieldLabel
                            );
                        }

                        if ($camposInput && array_key_exists($campo->userFieldId, $camposInput)) {
                            $campos[$key]->userFieldRelContent = $camposInput[$campo->userFieldId];
                        } else {
                            log_message(
                                'error',
                                'Undefined offset '.$campo->userFieldId.' for var $camposInput on file: '.__FILE__.'. Line 557'
                            );
                        }

                    }

                    if (isset($_POST) && !empty($_POST)) {

                        try {

                            if ($CI->form_validation->run() === true) {

                                $user_data = array(
                                    'first_name' => $CI->input->post('first_name'),
                                    'last_name' => $CI->input->post('last_name'),
                                    'email' => $CI->input->post('email'),
                                );

                                $user = Sentinel::update($user, $user_data);
                                $CI->Usuarios->actualizarCampos($user->id);
                                $CI->session->set_flashdata('error', lang('update_successful'));
                                redirect($data['diminutivo'].'/'.$data['pagAutenticacion']->paginaNombreURL);

                            } else {
                                throw new Exception(validation_errors());
                            }

                        } catch (Exception $e) {
                            $CI->session->set_flashdata('error', $e->getMessage());
                            redirect($data['diminutivo'].'/'.$data['pagAutenticacion']->paginaNombreURL . '/edit');
                        }
                    }


                } else {
                    $html .= $this->renderLoginView($data, $idioma, $currentPage, '');
                }

                break;

            //404 Error
            default:
                show_my_404(base_url($idioma.'/'.$currentPage), $CI->m_config->theme);
                break;
        }

        return $html;

    }

    private function renderIndex($data, $user, $message)
    {
        $CI =& get_instance();

        if ($user) {

            $data['user'] = $user;
            $data['userCampos'] = $CI->Usuarios->getCamposUser($user->id, $this->lang);
            $data['message'] = $message;
            $data['countries'] = $CI->Usuarios->countries();

            return $CI->load->view('paginas/autenticacion/usuario_perfil_view', $data, true);
        } else {
            Sentinel::logout();
            $CI->session->set_flashdata('error', 'El usuario registrado ya no existe');
            redirect($data['diminutivo'].'/'.$data['pagAutenticacion']->paginaNombreURL);
        }
    }

    private function facebookCallback($html, $idioma, $data, $currentPage)
    {

        $CI =& get_instance();

        try {

            $user = $this->CI->facebookauth->callback();
            $accessToken = $this->CI->facebookauth->getUserToken();
            $data['returnUrl'] = base_url($idioma.'/'.$currentPage);

            //Check if the user exists
            $localUser = $this->CI->db->where('email', $user->getEmail())->get('users')->row();

            if ($localUser) {
                $this->login($user->getEmail(), $user->getId(), $data);
            } else {

                $userData = array(
                    'first_name' => $user->getFirstName(),
                    'last_name' => $user->getLastName(),
                    'facebook_id' => $user->getId(),
                    'access_token' => (string )$accessToken,
                    'token_expires' => date('Y-m-d H:i:s', $accessToken->getExpiresAt()->getTimestamp()),
                    'facebook_login' => 1,
                );

                if ($user = Sentinel::register($userData)) {
                    $this->post_register($user, $user->getId());
                } else {

                    if($redirect = $CI->session->userdata('current_url')){
                        redirect($redirect);
                    } else {
                        redirect($idioma . '/' . $currentPage . '/register');
                    }

                }
            }

            return $html;

        } catch (BadMethodCallException $e) {

            //Did not receive the users email, ask again form email permisions
            $data['facebookUrl'] = $this->CI->facebookauth->getReRequestUrl(base_url($idioma.'/'.$currentPage.'/fb-callback'));
            return $this->CI->load->view('paginas/autenticacion/facebook_permisions_again_view', $data, true);

        } catch (Exception $e) {
            $this->CI->session->set_flashdata('error', $e->getMessage());

            if($redirect = $CI->session->userdata('current_url')){
                redirect($redirect);
            } else {
                redirect($data['diminutivo'].'/'.$data['pagAutenticacion']->paginaNombreURL);
            }
            
        }

    }

    private function twitterCallback($html, $idioma, $data, $currentPage)
    {

        $CI =& get_instance();

        try {

            $user = $this->CI->twitterauth->callback();
            $access_token = $this->CI->twitterauth->getUserToken();

            //Check if the user exists
            $localUser = $this->CI->db->where('twitter_id', $user->id)->get('users')->row();
            if ($localUser) {
                $this->login($user->id, $user->id, $data, "twitter_id");
            }

            //If the user does not exist register him
            else {

                $nameArr = explode(' ', $user->name);
                if(count($nameArr) === 1) {
                    $first_name = $user->name;
                    $last_name = '';
                } else {
                    $first_name = $nameArr[0];
                    $last_name = $nameArr[1];
                }

                $userData = array(
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'twitter_id' => $user->id,
                    'access_token' => $access_token[TwitterAuth::OAUTH_TOKEN_SECRET],
                    'token_expires' => date('Y-m-d H:i:s', $access_token['x_auth_expires']),
                    'twitter_login' => 1,
                );

                if ($user = Sentinel::register($userData)) {
                    $this->post_register($user, $user->id);
                } else {

                    if($redirect = $CI->session->userdata('current_url')){
                        redirect($redirect);
                    } else {
                        redirect($data['diminutivo'] . '/' . $data['pagAutenticacion']->paginaNombreURL . '/register');
                    }

                }

            }

            return $html;

        } catch (Exception $e) {
            $this->CI->session->set_flashdata('error', $e->getMessage());

            if($redirect = $CI->session->userdata('current_url')){
                redirect($redirect);
            } else {
                redirect($data['diminutivo'].'/'.$data['pagAutenticacion']->paginaNombreURL);
            }

        }

    }

    private function login($identity, $password, $data, $identityColumn = 'email')
    {

        $CI =& get_instance();

        try {

            //Store the url to go back to when the user has finished login in or registering
            if($CI->input->post('redirect_url')) {
                $newdata = [
                    'redirect_url'  => base_url($CI->input->post('redirect_url')),
                ];
                $CI->session->set_userdata($newdata);
            }

            $credentials = [
                $identityColumn => $identity,
                'password' => $password,
            ];

            $user = Sentinel::authenticateAndRemember($credentials);

            if(!$user) {
                throw new Exception("El usuario con $identityColumn '$identity' no existe");
            }
            
            $CI->session->set_flashdata('message', lang('login_successful'));

            $pedidos = $CI->session->userdata('pedidos');

            //Save any session orders into the database
            if (($pedidos) > 0 && isset($pedidos)) {
                $CI->Pedido->addFromSession($pedidos, $user);
                $CI->session->unset_userdata('pedidos');
                $CI->session->unset_userdata('totalPedidos');
            }

            if($redirect = $CI->session->userdata('redirect_url')){
                redirect($redirect);
            } else {
                redirect(base_url(), 'refresh');
            }


        } catch (Exception $e) {

            $CI->session->set_flashdata('error',$e->getMessage());

            if($redirect = $CI->session->userdata('current_url')){
                $CI->session->unset_userdata('current_url');
                redirect($redirect);
            } else {
                redirect($data['diminutivo'].'/'.$data['pagAutenticacion']->paginaNombreURL);
            }

        }

    }


    /**
     * This gets executed after a successful user registration
     *
     * @param $user
     * @param $password
     */
    private function post_register($user, $password)
    {

        $CI =& get_instance();

        $contacts = $CI->Submit->getContacts();
        $contactoId = $contacts[0]->contactoId;
        $contacto = $CI->Submit->getContact($contactoId);

        $CI->Usuarios->insertarCampos($user->id);

        //Set the user role
        $role = Sentinel::findRoleBySlug($this->userConfig->registered_role);
        $role->users()->attach($user);

        //If its automatic activation log the person in and send an email
        if ($this->userConfig->automatic_activation) {

            //User email
            $message = 'Esta es una notificación de que Ud. se ha registrado en '.$_SERVER['SERVER_NAME'];
            $subject = 'Confirmación de registro desde '.$_SERVER['SERVER_NAME'];

            $credentials = [
                'email'    => $user->email,
                'password' => $password,
            ];

            $user = Sentinel::authenticateAndRemember($credentials);

            //We try to log in
            if ($user) {
                $pedidos = $CI->session->userdata('pedidos');

                if (($pedidos) > 0 && isset($pedidos)) {
                    $CI->Pedido->addFromSession($pedidos, $user);
                    $CI->session->unset_userdata('pedidos');
                    $CI->session->unset_userdata('totalPedidos');
                }

                //Twitter does not give user emails as default permissions
                if(!$user->twitter_login) {
                    $this->sendEmail($contacto->contactoEmail, $user->email, $subject, $message);
                    $CI->session->set_flashdata('message', lang('login_successful'). ", Un email notificando su registro ha sido enviado a su correo");
                } else {
                    $CI->session->set_flashdata('message', lang('login_successful'));
                }

            }

            //If there was a problem logging in
            else {
                //Twitter does not give user emails as default permissions
                if(!$user->twitter_login) {
                    $this->sendEmail($contacto->contactoEmail, $user->email, $subject, $message);
                    $CI->session->set_flashdata('error', lang('login_unsuccessful').', adicionalmente se ha enviado un email notificando su registro a su correo'
                    );
                } else {
                    $CI->session->set_flashdata('error', lang('login_unsuccessful'));
                }

            }

        }

        //If its manual activation send an email to user to wait for the activation
        //and send admin the email
        else {

            //User email
            $message = 'Tiene que esperar a que un administrador active su cuenta';
            $subject = 'Espere activacion desde '.$_SERVER['SERVER_NAME'];
            $this->sendEmail($contacto->contactoEmail, $user->email, $subject, $message);

            //Admin email
            $message = 'El usuario '.$CI->input->post('first_name').' '.$CI->input->post(
                    'last_name'
                ).' está en espera de que active su cuenta';
            $subject = $user->first_name.' '.$user->last_name.' espera activacion en '.$_SERVER['SERVER_NAME'];
            $this->sendEmail($user->email, $contacto->contactoEmail, $subject, $message);

            $CI->session->set_flashdata('message', 'Un email de confirmaci&oacute;n ha sido enviado a su correo, por favor espere a que un administrador active su cuenta');

        }

        if($redirect = $CI->session->userdata('redirect_url')){
            redirect($redirect);
        } else {
            redirect(base_url(), 'refresh');
        }


    }

    function renderLoginView($data, $idioma, $currentPage, $message)
    {
        $CI =& get_instance();

        $campos = $CI->Usuarios->getCampos($idioma);
        foreach ($campos as &$campo) {
            $campo->userFieldRelContent = '';
        }

        $data['userCampos'] = $campos;

        //Facebook login button
        if ((bool)$this->config->facebook_login) {
            $data['facebookUrl'] =  $this->CI->facebookauth->getRedirectUrl(base_url($idioma.'/'.$currentPage.'/fb-callback'));
        }

        //Twitter login button
        if((bool)$this->config->twitter_login) {
            $data['twitterUrl'] = $this->CI->twitterauth->getRedirectUrl(base_url($idioma.'/'.$currentPage.'/tw-callback'));
        }

        $data['linkForgotPassword'] = base_url($idioma.'/'.$currentPage.'/password');
        $data['mensaje'] = $message;

        $data['link'] = $idioma.'/'.$currentPage.'/login';

        return $CI->load->view('paginas/autenticacion/login_view', $data, true);
    }

    function renderProfileView($data, $user, $message, $idioma, $currentPage)
    {
        $CI =& get_instance();

        if ($user) {
            $data['user'] = $user;
            $data['userCampos'] = $CI->Usuarios->getCamposUser($user->id, $idioma);
            $data['message'] = $message;
            $data['diminutivo'] = $idioma;
            $data['update_link'] = base_url($idioma.'/'.$currentPage.'/update');

            $data['countries'] = $CI->Usuarios->countries();

            return $CI->load->view('paginas/autenticacion/usuario_perfil_edit_view', $data, true);
        } else {
            Sentinel::logout();
            $CI->session->set_flashdata('error', 'El usuario registrado ya no existe');
            redirect($data['diminutivo'].'/'.$data['pagAutenticacion']->paginaNombreURL);
        }

    }

    function renderRegisterView($message, $campos, $data, $idioma, $currentPage)
    {
        $CI =& get_instance();

        //Facebook login button
        if ((bool)$this->config->facebook_login) {
            $data['facebookUrl'] =  $this->CI->facebookauth->getRedirectUrl(base_url($idioma.'/'.$currentPage.'/fb-callback'));
        }

        //Twitter login button
        if((bool)$this->config->twitter_login) {
            $data['twitterUrl'] = $this->CI->twitterauth->getRedirectUrl(base_url($idioma.'/'.$currentPage.'/tw-callback'));
        }


        $data['message'] = $message;
        $data['user'] = new stdClass();
        $data['user']->first_name = $CI->input->post('first_name');
        $data['user']->last_name = $CI->input->post('last_name');
        $data['user']->email = $CI->input->post('email');
        $data['user']->cedula = $CI->input->post('cedula');
        $data['userCampos'] = $campos;
        $data['countries'] = $CI->Usuarios->countries();

        return $CI->load->view('paginas/autenticacion/registro_view', $data, true);
    }

    function _get_csrf_nonce()
    {
        $CI =& get_instance();

        $CI->load->helper('string');
        $key = random_string('alnum', 8);
        $value = random_string('alnum', 20);
        $CI->session->set_flashdata('csrfkey', $key);
        $CI->session->set_flashdata('csrfvalue', $value);

        return array($key => $value);
    }

    function _valid_csrf_nonce()
    {

        $CI =& get_instance();

        if ($CI->input->post($CI->session->flashdata('csrfkey')) !== false &&
            $CI->input->post($CI->session->flashdata('csrfkey')) == $CI->session->flashdata('csrfvalue')
        ) {
            return true;
        } else {
            return false;
        }
    }

    function sendEmail($from, $to, $subject, $message)
    {

        $CI =& get_instance();
        $conf = $CI->Config->get();

        $CI->email->from($from, $conf->site_name);
        $CI->email->to($to);
        $CI->email->subject($subject);
        $CI->email->message($message);

        if (@!$CI->email->send()) {
            return false;
        } else {
            return true;
        }

    }

}

/* End of file CMS_Authenticate.php */