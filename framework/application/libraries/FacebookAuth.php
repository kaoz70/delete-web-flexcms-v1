<?php

/**
 * Created by PhpStorm.
 * User: Miguel
 * Date: 6/10/2016
 * Time: 2:05 PM
 */
class FacebookAuth extends SocialAuth implements SocialAuthInterface
{

    private static $facebook;
    private static $config;
    private $userToken;

    public function __construct($params)
    {
        static::$config = $params[0];
        static::$facebook = new Facebook\Facebook(
            [
                'app_id' => static::$config->facebook_app_id,
                'app_secret' => static::$config->facebook_app_secret,
                'default_graph_version' => 'v2.2',
            ]
        );
    }
    
    public function getRedirectUrl($base_url)
    {
        $helper = static::$facebook->getRedirectLoginHelper();

        $permissions = [
            'email',
            'public_profile'
        ];
        $loginUrl = $helper->getLoginUrl($base_url, $permissions);

        return htmlspecialchars($loginUrl);
    }

    public function getReRequestUrl($base_url)
    {
        $helper = static::$facebook->getRedirectLoginHelper();

        $permissions = [
            'email',
            'public_profile'
        ];
        $loginUrl = $helper->getReRequestUrl($base_url, $permissions);

        return htmlspecialchars($loginUrl);
    }

    /**
     * @return mixed
     */
    public function getUserToken()
    {
        return $this->userToken;
    }

    public function callback()
    {

        $error = isset($_GET['error']) ? $_GET['error'] : null;

        if ($error) {
            throw new Exception($error);
        } else {

            $helper = static::$facebook->getRedirectLoginHelper();
            $accessToken = $helper->getAccessToken();

            if (!isset($accessToken)) {
                if ($helper->getError()) {
                    throw new Exception($helper->getError());
                } else {
                    throw new Exception('Bad request');
                }
            }

            // Logged in

            // The OAuth 2.0 client handler helps us manage access tokens
            $oAuth2Client = static::$facebook->getOAuth2Client();

            // Get the access token metadata from /debug_token
            $tokenMetadata = $oAuth2Client->debugToken($accessToken);

            // Validation (these will throw FacebookSDKException's when they fail)
            $tokenMetadata->validateAppId(static::$config->facebook_app_id);
            $tokenMetadata->validateExpiration();

            if (!$accessToken->isLongLived()) {
                // Exchanges a short-lived access token for a long-lived one
                $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
            }

            // Returns a `Facebook\FacebookResponse` object
            $response = static::$facebook->get('/me?fields=id,first_name,last_name,email', $accessToken);

            $user = $response->getGraphUser();
            $this->userToken = $accessToken;

            if (!$user->getEmail()) {
                throw new BadMethodCallException('Dabe proporcionar un correo electronico');
            }

            return $user;

        }
    }
}