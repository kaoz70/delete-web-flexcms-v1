<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

use Assetic\FilterManager;
use Assetic\Filter\ScssphpFilter;
use Assetic\Filter\CssMinFilter;
use Assetic\Filter\LessphpFilter;
use Assetic\Filter\JSMinFilter;

use Assetic\AssetWriter;
use Assetic\Asset\AssetCache;
use Assetic\Cache\FilesystemCache;

use Assetic\Factory\AssetFactory;

use Assetic\Asset\FileAsset;
use Assetic\AssetManager;

class Assets {

    //Path to where generated assets go
    static $asset_path = 'assets/cache';

    static $cache;

    static $writer;

    /**
     * Create the stylesheet tag
     * @param string $prefix
     * @param array $files
     * @param null $theme
     * @param string $media
     */
    static function css_group($prefix = '', $files = array(), $theme = NULL, $media = 'screen')
    {

        $CI =& get_instance();
        $CI->benchmark->mark($prefix . '_css_start');

        static::init($theme);

        //Generate CSS and minify each file
        if(ENVIRONMENT === 'production' || ENVIRONMENT === 'testing') {

            try {

                $am = new AssetManager();

                $fm = new FilterManager();
                $fm->set('css_min', new CssMinFilter());
                $fm->set('scss', new ScssphpFilter());
                $fm->set('less', new LessphpFilter());

                $factory = static::getAssetFactory($am, $fm);

                $asset = $factory->createAsset($files, array(
                        '?css_min',
                        'scss',
                        'less'
                ), array('output' => $prefix . '.css'));

                static::$cache = static::getAssetCache($asset);
                $compiled_path = static::$asset_path . '/' . static::$cache->getTargetPath();
                $asset_path = base_url() . static::$asset_path . '/' . static::$cache->getTargetPath();

                static::writeFile($files, $compiled_path);

                echo '<link href="' . $asset_path . '" rel="stylesheet" media="' . $media . '">';

            } catch(RuntimeException $e) {
                echo $e->getMessage();
            } catch(Exception $e) {
                echo $e->getMessage();
            }

        }

        //Return CSS and individual files if in DEVELOPMENT
        else {

            $paths = array();

            foreach ($files as $key => $file) {

                $fileinfo = pathinfo($file);

                //Load only the required filters
                $filters = [];
                if($fileinfo['extension'] === 'less') {
                    $filters[] = new LessphpFilter();
                } else if($fileinfo['extension'] === 'scss' || $fileinfo['extension'] === 'sass') {
                    $filters[] = new ScssphpFilter();
                }

                $asset = new FileAsset($file, $filters);

                $scripts = new AssetManager();
                $asset->setTargetPath('dev.' . $fileinfo['filename'] . '.css');
                $scripts->set($key, $asset);

                static::$writer->writeAsset($asset);

                $paths[] = $asset->getTargetPath();

            }

            //Remove duplicates and print links
            foreach (array_unique($paths) as $path) {
                echo '<link href="' . base_url() . static::$asset_path . '/' . $path . '?' . time() . '" rel="stylesheet" media="' . $media . '">' . PHP_EOL;
            }

        }

        $CI->benchmark->mark($prefix . '_css_end');

    }

    /**
     * Create the javascript tag
     * @param string $prefix
     * @param array $files
     * @param null $theme
     */
    static function js_group($prefix = '', $files = array(), $theme = NULL)
    {

        $CI =& get_instance();
        $CI->benchmark->mark($prefix . '_js_start');

        static::init($theme);

        //Concatenate JS and minify each file
        if(ENVIRONMENT === 'production' || ENVIRONMENT === 'testing') {

            try {

                $am = new AssetManager();

                $fm = new FilterManager();
                $fm->set('js_min', new JSMinFilter());

                $factory = static::getAssetFactory($am, $fm);
                $asset = $factory->createAsset($files, array(
                        '?js_min',
                ), array('output' => $prefix . '.js'));

                static::$cache = static::getAssetCache($asset);

                $compiled_path = static::$asset_path . '/' . static::$cache->getTargetPath();
                $asset_path = base_url() .$compiled_path;

                static::writeFile($files, $compiled_path);

                echo '<script src="' . $asset_path . '" ></script>';

            }  catch(RuntimeException $e) {
                echo $e->getMessage();
            } catch(Exception $e) {
                echo $e->getMessage();
            }

        }

        else {

            foreach ($files as $file) {
                echo '<script src="' . base_url() . $file . '?' . time() . '" ></script>' . PHP_EOL;
            }

        }

        $CI->benchmark->mark($prefix . '_js_end');

    }

    private static function getAssetFactory($am, $fm)
    {
        $factory = new AssetFactory('./');
        $factory->setAssetManager($am);
        $factory->setFilterManager($fm);

        if(ENVIRONMENT !== 'production' && ENVIRONMENT !== 'testing') {
            set_time_limit(0);
            $factory->setDebug(TRUE);
        }

        return $factory;
    }

    private static function getAssetCache($asset)
    {
        return new AssetCache(
            $asset,
            new FilesystemCache(APPPATH .'/cache/assetic/')
        );
    }

    private static function init($theme)
    {

        //Make the script access more memory to parse big files
        ini_set('memory_limit', '128M');
        @set_time_limit(0);

        //Set the theme's asset path
        if ($theme) {
            static::$asset_path = 'themes/'.$theme.'/cache';
        }

        //Initialize the asset writer
        static::$writer = new AssetWriter(static::$asset_path);

    }

    /**
     * Checks the modified time of all files and writes the final asset if one of them is modified
     *
     * @param $files
     * @param $compiled_path
     */
    private static function writeFile($files, $compiled_path)
    {
        //Check if files have benn modified
        $files = array_combine($files, array_map("filemtime", $files));
        arsort($files);
        $latest_file = key($files);

        //If files have been modified, write the new asset
        if($files[$latest_file] > filemtime($compiled_path)) {
            static::$writer->writeAsset(static::$cache);
        }
    }

}


/* End of file assets.php */