<?php
use Cartalyst\Sentinel\Native\Facades\Sentinel;


/**
 * Created by PhpStorm.
 * User: miguel
 * Date: 10/28/15
 * Time: 4:21 PM
 */
class PayMe extends Payment implements PaymentInterface {

    static protected $name = 'Payme (Alignet)';

    private static $messages = [
        'autorizada' => 'Su tarjeta de credito fue autorizada',
        'denegada' => 'Su tarjeta de credito no fue autorizada',
        'rechazada' => 'Su tarjeta de credito fue rechazada',
    ];

    /**
     * Renders the button with the form
     *
     * @param $title
     * @param $orderId
     * @param $lang
     */
    static function button($title, $orderId, $lang)
    {

        $CI =& get_instance();
        $user = Sentinel::getUser();
        $price = $CI->flexi_cart->total(TRUE, FALSE, TRUE);

        try {

            if(!$user) {
                throw new Exception('PayMe necesita que el usuario este en sesion');
            }

            static::$config = static::readConfig();
            $env_config = static::$config[static::$config['environment']];

            //Does the user have a Wallet account created?
            // If not, register the user
            if (!$user->payme_wallet_code) {
                static::registerWallet($user);
                //Get the user again because we changed his data
                $user = Sentinel::getUser();
            }

            /**
             * Identificador (numerico) único por cada transacción, dado por el comercio.
             */
            $data['purchaseOperationNumber'] = static::randomNumber(9);

            /**
             * Valor total de la compra, dado por el Comercio. el monto debe ir sin separador decimal
             * (Si el monto es 100.30 dólares entonces la cantidad a enviar es 10030)
             */
            $data['price'] = str_replace('.', '', number_format($price, 2, '.', ''));

            $data['config'] = static::$config;
            $data['user'] = $user;
            $data['product'] = $title;
            $data['purchaseVerification'] = openssl_digest(
                $env_config['vpos2_idEntCommerce'].$env_config['commerce_id'].$data['purchaseOperationNumber'].$data['price'].static::$config['currency_code'].$env_config['vpos2_key'],
                'sha512'
            );

            $data['orderId'] = $orderId;
            $data['form_url'] = $env_config['vpos2_url'];

            $CI->load->view('payment/payme', $data);

        } catch (Exception $e) {
            echo $e->getMessage();
        }

    }

    public static function getResult()
    {

        $CI =& get_instance();

        $post = $CI->input->post();

        /**
         * 00 - Operación Autorizada
         * 01 - Operación Denegada
         * 05 - Operación Rechazada
         */
        if ($post['authorizationResult'] === 1) {
            throw new Exception(static::messages['denegada'], $post['authorizationResult']);
        } elseif ($post['authorizationResult'] === 5) {
            throw new Exception(static::messages['rechazada'], $post['authorizationResult']);
        }

        //TODO return purchace ID

    }

    /**
     * Creates a PayMe Wallet account for faster or "one click" payments
     *
     * @param $user
     *
     * @return mixed
     */
    static function registerWallet($user)
    {

        $CI =& get_instance();

        $config = static::$config;
        $config = $config[$config['environment']];

        $mail = $user->email;
        $codCardHolderCommerce = $user->id;
        $registerVerification = openssl_digest(
            $config['wallet_idEntCommerce'].$codCardHolderCommerce.$mail.$config['wallet_key'],
            'sha512'
        );

        try {

            $client = new SoapClient($config['wallet_url']);

            $data = [
                'idEntCommerce' => $config['wallet_idEntCommerce'],
                'codCardHolderCommerce' => $codCardHolderCommerce,
                'names' => $user->first_name,
                'lastNames' => $user->last_name,
                'mail' => $mail,
                'registerVerification' => $registerVerification
            ];

            $result = $client->RegisterCardHolder($data);

            if ($result->ansCode == '000') {

                //Update user with wallet code
                if ($result->codAsoCardHolderWallet != '') {

                    $user = Sentinel::getUser();
                    $user->payme_wallet_code = $result->codAsoCardHolderWallet;
                    $user->save();

                }

            } else {
                log_message("error", $result->ansDescription);
            }

        } catch (Exception $e) {
            var_dump($e->getMessage());
            exit;
        }

        return $result;

    }

    private static function randomNumber($length) {
        $result = '';

        for($i = 0; $i < $length; $i++) {
            $result .= mt_rand(0, 9);
        }

        return $result;
    }

}