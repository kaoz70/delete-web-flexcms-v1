<?php

/**
 * Created by PhpStorm.
 * User: miguel
 * Date: 11/4/15
 * Time: 3:17 PM
 */
use Cartalyst\Sentinel\Native\Facades\Sentinel;

/**
 * Class DineroElectronico
 *
 * 1) Ask the WS people unblock the server's IP, to check whats the originating requests's IP use:
 * var_dump(file_get_contents('http://bot.whatismyipaddress.com'));
 *
 */


class DineroElectronico extends Payment implements PaymentInterface{

	static protected $name = 'Dinero Electronico';

	/**
	 * Renders the button with the form
	 *
	 * @param $price
	 * @param string $method
	 * @param string $param1
	 */
	static function button($price, $method = '', $param1 = '')
	{

		$CI =& get_instance();

		self::$config = self::readConfig();
		$user = Sentinel::getUser();

		$data['error'] = "";

		//We check if the service is available just in case
		try {
			self::serviceAvailable();
		} catch (SoapFault $e) {
			$data['error'] = $e->getMessage();
		}

		$data['user'] = $user;
		$data['price'] = $price;
		$data['form_url'] = base_url('ajax/payment/' . $method . '/' . $param1);

		$CI->load->view('payment/dinero_electronico', $data);

	}

	/**
	 * Sends the payment info to the WS, this has to be called by ajax because the end user has to
	 * input his code by USSD code sent to his phone, and the browser will hang or timeout otherwise.
	 *
	 * @param $price
	 * @param $input
	 *
	 * @return mixed
	 * @throws Exception
	 */
	function send($price, $input)
	{

		$CI =& get_instance();
		$env_config = self::$config[self::$config['environment']];

		//Remove the timeout limit because the user has to input the code, 30sec until WS timeouts
		set_time_limit(0);

		//Check if its a valid mobile phone number
		if(!preg_match('/0\d{9}/', $CI->input->post('phone'))) {
			throw new Exception('Por favor ingrese un n&uacute;mero m&oacute;vil valido');
		}

		//TODO try to use CERT file instead of disabling this:
		$context = stream_context_create( [
			'ssl' => [
				'verify_peer' => false,
				'verify_peer_name' => false,
				'allow_self_signed' => true
			]
		] );

		$soapParams =  [
			'location' => $env_config['url'],
			'login' => $env_config['user'],
			'password' => $env_config['pass'],
			'stream_context' => $context,
			'trace' => TRUE,
			'soap_version' => SOAP_1_2
		];

		$soapClient = new SoapClient($env_config['url'], $soapParams);
		$cash = new stdClass();
		$cash->dtoRequestCobroConfirm = new stdClass();
		$cash->dtoRequestCobroConfirm->user = $env_config['user'];
		$cash->dtoRequestCobroConfirm->pin = $env_config['pass'];
		$cash->dtoRequestCobroConfirm->password = $env_config['pass'];
		$cash->dtoRequestCobroConfirm->amount = $price;
		$cash->dtoRequestCobroConfirm->brandId = 1;
		$cash->dtoRequestCobroConfirm->currency = 1;
		$cash->dtoRequestCobroConfirm->language = self::$config['language'];
		$cash->dtoRequestCobroConfirm->document = $input['document']; //TODO dynamic
		$cash->dtoRequestCobroConfirm->msisdnSource = $input['phone']; //TODO dynamic
		$cash->dtoRequestCobroConfirm->msisdnTarget = $env_config['user'];

		// Unique transaction identifier
		// TODO: Is timestamp good enough? WS people recommend it (64 chars max)
		$cash->dtoRequestCobroConfirm->utfi = time();

		$res = $soapClient->cobroConfirm($cash);

		// 1 is correct
		// 2 is error
		if($res->return->resultCode === 2) {
			throw new Exception($res->return->resultText);
		}

		return $res;

	}

	public static function getResult()
	{
		//TODO: implement method
	}

	private static function serviceAvailable()
	{

		$env_config = self::$config[self::$config['environment']];

		if(!@file_get_contents($env_config['url'])) {
			throw new SoapFault('Server', self::$config['errors']['connection']);
		}

		$soapParams =  [
			'login' => $env_config['user'],
			'password' => $env_config['pass'],
			'trace' => TRUE,
			'soap_version' => SOAP_1_2,
			'exceptions' => true,
		];

		$soapClient = new SoapClient($env_config['url'], $soapParams);
		$version = $soapClient->getVersionId();

		return $version->return;

	}

}