<?php use Cartalyst\Sentinel\Native\Facades\Sentinel;

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CMS_Catalog {

    private $data;
    private $lang;

    public function __construct()
    {
        $CI =& get_instance();
        $CI->load->model('catalogo_model', 'Catalog');
        $CI->load->model('module_model', 'Modulos');
        $CI->load->model('search_model', 'Search');
        $CI->load->model('imagenes_model', 'Images');
        $CI->load->library('CMS_Authenticate', ['config' => $CI->m_config]);
    }

    public function create($page, $data, $idioma, $currentPage, $module)
    {

        $data['paginaCatalogoUrl'] = $page->paginaNombreURL;

        $CI =& get_instance();
        $catalogConfig = $CI->Catalog->getConfiguration();

        $catalog_id = (int)$CI->uri->segment(3);
        $producto_id = $CI->uri->segment(5);

        $pagina = is_numeric($producto_id);

        $html = '';
        $ret = new stdClass();
        $ret->html = '';
        $ret->data = $data;

        $this->data = $data;
        $this->lang = $idioma;

        try {

            /****************************************
             * PAGINA PRINCIPAL DEL CATALOGO
             ***************************************/
            //if(gettype($catId) == 'integer' && $catId == 0)
            if(!$catalog_id)
            {

                $tree = CatalogTree::allRoot()->first();
                $tree->lang = $idioma;
                $tree->findChildren(2);

                $categorias = $tree->getChildren();

                //Add any products to the category
                foreach ($categorias as &$cat) {
                    $cat->productos = $CI->Catalog->getProductsByCategory($cat->id, $idioma);
                }

                /*
                 * SI HAY UNA SOLA CATEGORIA MOSTRAMOS EL LISTADO DE PRODUCTOS
                 */
                if(count($categorias) == 1){

                    $categoria = $categorias[0];

                    $this->checkUserAccess($categoria);

                    if(!$catalogConfig->productoMostarProductoInicio) {
                        $html .= $this->renderProductList($categoria, $data, $idioma, $currentPage, $module);
                    }

                    /*
                     * VISTA DEL DETALLE DE UN PRODUCTO (primer producto de primera categoria)
                    */
                    else {
                        $producto =  $CI->Catalog->getFirsProductOfFirstCategory($idioma);
                        $categoria = $CI->Catalog->getCategory((int)$producto->categoriaId, $idioma);
                        $catData = $this->renderProductDetail($producto, $categoria,$idioma, $currentPage, $data);
                        $html .= $catData->html;
                        $ret->data = $catData->data;
                    }

                }
                else
                {

                    /*
                     * LISTADO DE CATEGORIAS
                    */
                    if(!$catalogConfig->productoMostarProductoInicio) {
                        $html .= $this->renderCategoryList($module, $categorias, $idioma, $page, $data, $tree);
                    }

                    /*
                     * VISTA DEL DETALLE DE UN PRODUCTO (primer producto de primera categoria)
                    */
                    else {
                        $producto =  $CI->Catalog->getFirsProductOfFirstCategory($idioma);
                        $categoria = $CI->Catalog->getCategory((int)$producto->categoriaId, $idioma);

                        $this->checkUserAccess($categoria);

                        $catData = $this->renderProductDetail($producto, $categoria,$idioma, $currentPage, $data);
                        $html .= $catData->html;
                        $ret->data = $catData->data;
                    }

                }

            }

            //Vista de los productos y subcategorias de una categoria
            else if($catalog_id || !$pagina) {

                /****************************************
                 * LISTADOS DE PRODUCTOS EN LA CATEGORIA
                 ***************************************/

                $categoria = $CI->Catalog->getCategory($catalog_id, $idioma);

                if(!$categoria) {
                    show_my_404(base_url($idioma . '/' . $currentPage), $CI->m_config->theme);
                }

                $this->checkUserAccess($categoria);

                if(!$producto_id || $pagina) {
                    $html .= $this->renderProductList($categoria, $data, $idioma, $currentPage, $module);
                }

                /*****************************************
                 * DETALLE DEL PRODUCTO
                 ****************************************/
                else {
                    $producto = $CI->Catalog->getProduct($producto_id, $idioma);

                    if (!$producto) {
                        show_my_404(base_url($idioma . '/' . $currentPage), $CI->m_config->theme);
                    }

                    $catData = $this->renderProductDetail($producto, $categoria,$idioma, $currentPage, $data);
                    $html .= $catData->html;
                    $ret->data = $catData->data;
                }

            }

            /****************************************
             * PAGINA RESULTADO BUSQUEDA POR FILTROS
             ***************************************/
            else if (gettype($catalog_id) == 'object') {

                $filters = $catalog_id;
                $headerData['clase'] = 'class="catalogo busqueda content"';
                $headerData['titulo'] = 'Busqueda';

                $paginaCatalogo = $CI->Modulos->getPageByType(4, $idioma);
                $productosIds = $CI->Search->catalogFilters($filters, $idioma);

                //Get all the common values in a multidimentional array
                $arrays = count($productosIds);
                $match = array();
                $duplicates = array();
                foreach($productosIds as $one){
                    foreach($one as $single){
                        $var = (array)$single;
                        if(!isset($match[$var['productoId']])) { $match[$var['productoId']] = 0; }
                        $match[$var['productoId']]++;
                        if($match[$var['productoId']] == $arrays){
                            $duplicates[] = (int)$var['productoId'];
                        }
                    }
                }

                $data['products'] = $CI->Catalog->getProductsByIds($duplicates, $idioma);
                $data['link_base'] = base_url() . $idioma . '/' . $paginaCatalogo->paginaNombreURL . '/';

                $html = $CI->load->view('paginas/catalogo/header_view', $headerData, true);
                $html .= $CI->load->view('paginas/catalogo/producto_busqueda_view', $data, true);
                $html .= $CI->load->view('paginas/catalogo/footer_view', '', true);
            }

        } catch (Exception $e) {
            $html = $e->getMessage();
        }

        $ret->html = $html;

        return $ret;

    }

    public function renderCategoryList($module, $categorias, $idioma, $page, $data, $tree, $only_content = FALSE)
    {

        $CI =& get_instance();

        if($module->moduloVerPaginacion) {

            $noticiasCant = count($categorias);

            $pag_config = array();

            //We are going to use "moduloMostrarTitulo for ajax pagination because I'm out of params
            //Is it Ajax pagination?
            if($module->moduloMostrarTitulo) {
                $pag_config['base_url'] = base_url('ajax/pagination/category/' . $tree->id . '/' . $idioma);
            } else {
                $pag_config['base_url'] = base_url($idioma . '/' . $CI->uri->segment(2) . '/' . $CI->uri->segment(3) . '/');
            }

            $pag_config['total_rows'] =$noticiasCant;
            $pag_config['per_page'] = $module->moduloParam4;

            $CI->pagination->initialize($pag_config);
            $pagination = $CI->pagination->create_links();

            $data['pagination'] = $pagination;

            $data['categorias'] = array_slice($categorias, $CI->input->get('page', 0), $module->moduloParam4);

        } else {
            $data['categorias'] = $categorias;
        }

        $html = '';

        if(!$only_content) {
            $headerData['clase'] = 'class="catalogo content"';
            $headerData['titulo'] = $page->paginaNombre;
            $headerData['categoria'] = NULL;
            $html .= $CI->load->view('paginas/catalogo/header_view', $headerData, true);
        }

        $html .= $CI->load->view('paginas/catalogo/categorias_view', $data, true);

        if(!$only_content) {
            $html .= $CI->load->view('paginas/catalogo/footer_view', '', true);
        }

        return $html;

    }

    public function renderProductList($categoria, $data, $idioma, $currentPage, $module, $products = NULL, $segment = 3)
    {
        $CI =& get_instance();

        if($module->moduloVerPaginacion) {

            $productsInCat = count($CI->Catalog->getProductsByCategory($categoria->id, $idioma));

            $pag_config = array();

            //We are going to use "moduloMostrarTitulo for ajax pagination because I'm out of params
            //Is it Ajax pagination
            if($module->moduloMostrarTitulo) {
                $pag_config['base_url'] = base_url('ajax/pagination/product/' . $CI->uri->segment($segment) . '/' . $idioma);
            } else {
                $pag_config['base_url'] = base_url($idioma . '/' . $CI->uri->segment(2) . '/' . $CI->uri->segment(3) . '/' . $CI->uri->segment(4));
            }

            $pag_config['total_rows'] = $productsInCat;
            $pag_config['per_page'] = $module->moduloParam4;

            $CI->pagination->initialize($pag_config);
            $pagination = $CI->pagination->create_links();

            $data['pagination'] = $pagination;

            if( ! $products) {
                $products = $CI->Modulos->getItemsForCatalog($categoria->id, $module->moduloParam4, $CI->input->get('page', 0), $idioma);
            }

            $ids = array();

            foreach ($products as $product) {
                $ids[] = $product->productoId;
            }

            $data['products'] = $CI->Catalog->getProductsByIds($ids, $idioma, true);
        } else {
            $data['pagination'] = '';
            $data['products'] = $CI->Catalog->getProductsByCategory($categoria->id, $idioma, true);
        }

        $data['link_base'] = base_url() . $idioma . '/' . $currentPage . '/'. $categoria->id . '/'. $categoria->productoCategoriaUrl . '/';;

        $headerData['clase'] = 'class="catalogo listado content cat_' . $categoria->id . '"';
        $headerData['titulo'] = $categoria->productoCategoriaNombre;

        //Get the subcategories
        $tree = CatalogTree::find($categoria->id);
        $tree->lang = $idioma;
        $tree->findChildren(9999);
        $dataCat['categorias'] = $tree->getChildren();

        //Get the sibling categories
        $parent = $tree->getParent();
        $parent->lang = $idioma;
        $parent->findChildren(9999);
        $dataCat['categoriasPadre'] = $parent->getChildren();

        $dataCat['categoria'] = $data['categoria'] = $headerData['categoria'] = $categoria;

        $html = $CI->load->view('paginas/catalogo/header_view', $headerData, true);
        $html .= $CI->load->view('paginas/catalogo/producto_listado_view', $data, true);
        $html .= $this->renderCategoryList($module, $tree->getChildren(), $idioma, '', $data, $tree, TRUE);
        $html .= $CI->load->view('paginas/catalogo/footer_view', $data, true);

        return $html;
    }

    private function renderProductDetail($producto, $categoria, $idioma, $currentPage, $data)
    {
        $CI =& get_instance();

        $ret = new stdClass();

        $headerData['clase'] = 'class="catalogo detalle cat_' . $categoria->id . '"';
        $headerData['titulo'] = $categoria->productoCategoriaNombre;
        $headerData['categoria'] = $data['categoria'] = $categoria;

        $data['link_base'] = base_url() . $idioma . '/' . $currentPage . '/'. $categoria->id . '/'. $categoria->productoCategoriaUrl . '/';
        $data['og_description'] = $data['meta_description'] = $producto->productoDescripcion;

        $data['meta_title'] = $producto->productoMetaTitulo;
        $data['meta_keywords'] = $producto->productoKeywords;

        $campos = $CI->Catalog->getProductFields($producto->productoId, $idioma);

        //Related product by keywords
        $palabras = explode(',', $producto->productoKeywords);
        $productosRelacionados = $CI->Catalog->getProductosRelacionados($palabras, $idioma);

        //If above result is empty get the related products form the same category
        if (empty($productosRelacionados)) {
            $productosRelacionados = $CI->Catalog->getProductsByCategory($producto->categoriaId, $idioma, true);
        }

        //Remove this product from related list
        foreach ($productosRelacionados as $key => $rel) {
            if ($rel->productoId === $producto->productoId) {
                unset($productosRelacionados[$key]);
            }
        }

        //Get the sibling categories
        $tree = CatalogTree::find($categoria->id);
        $parent = $tree->getParent();
        $parent->lang = $idioma;
        $parent->findChildren(9999);
        $data['categoriasPadre'] = $parent->getChildren();

        $producto = $CI->Catalog->consolidateProduct($producto, $campos, $idioma, $categoria);
        $producto->productosRelacionados = $productosRelacionados;

        $data['producto'] = $producto;
        $data['regresarCatalogo'] = $data['link_base'];

        $data['productoAnterior'] = $CI->Catalog->getPrev($producto, $idioma);
        $data['productoSiguiente'] = $CI->Catalog->getNext($producto, $idioma);

        $html = $CI->load->view('paginas/catalogo/header_view', $headerData, true);
        $html .= $CI->load->view('paginas/catalogo/producto_detalle_view', $data, true);
        $html .= $CI->load->view('paginas/catalogo/footer_view', '', true);

        //Facebook ObjectGraph
        if($producto->productoImagenExtension) {
            $image_conf = $CI->Images->getImages(5);
            $data['og_image'] = base_url('assets/public/images/catalog/prod_' . $producto->productoId . $image_conf[0]->imagenSufijo . '.' . $producto->productoImagenExtension);
        } else if (
            $producto->imagenes &&
            array_key_exists(0, $producto->imagenes) &&
            $producto->imagenes[0]->contenido &&
            array_key_exists(0, $producto->imagenes[0]->contenido)
        ) {
            $image_conf = $CI->Images->getImages(6);
            $image = $producto->imagenes[0]->contenido[0];
            $data['og_image'] = base_url('assets/public/images/catalog/gal_' . $producto->productoId . '_' . $image->productoImagenId . $image_conf[0]->imagenSufijo . '.' . $image->productoImagen);
        }
        $data['og_title'] = $producto->productoNombre;

        $ret->html = $html;
        $ret->data = $data;

        return $ret;
    }

    /**
     * Check if the user has access to this category
     *
     * @param $category
     * @return bool
     * @throws Exception
     */
    private function checkUserAccess($category)
    {
        $user = Sentinel::getUser();
        $CI =& get_instance();

        //Is the category public?
        if($category->visible_to === 'public' || (Sentinel::check() && Sentinel::hasAccess(['admin']))) {
            return true;
        }

        //Category is not public, is the user logged in?
        if(Sentinel::guest()) {

            $html = $CI->cms_authenticate->renderLoginView(
                $this->data,
                $this->lang,
                $this->data['pagAutenticacion']->paginaNombreURL,
                lang('no_access')
            );
            throw new Exception($html);
        }

        //User is logged in, does he have access to it?
        else {

            if(!$user->hasAnyAccess(['catalog.' . $category->id, 'admins'])) {
                throw new Exception($CI->load->view('paginas/autenticacion/no_access_view', '', true));
            }

        }

        return true;

    }

}

/* End of file CMS_Catalog.php */