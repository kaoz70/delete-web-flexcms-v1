<?php
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;

/**
 * Created by PhpStorm.
 * User: miguel
 * Date: 10/28/15
 * Time: 4:21 PM
 */
class PayPalGateway extends Payment implements PaymentInterface {

    private static $apiToken;
    static protected $name = 'Paypal';

    public static function button($title, $orderId, $lang)
    {
        static::$config = static::readConfig();
        $CI =& get_instance();
        $CI->load->view('payment/paypal');
    }

    public function send()
    {

        static::$config = static::readConfig();
        $env_config = static::$config[static::$config['environment']];
        $CI =& get_instance();
        $total = $CI->flexi_cart->total(TRUE, FALSE, TRUE);

        //Get an access token using client id and client secret.
        $sdkConfig = array(
            "mode" => static::$config['environment']
        );
        $cred = new OAuthTokenCredential($env_config['client_id'], $env_config['client_secret'], $sdkConfig);

        $apiContext = new ApiContext($cred, 'Request' . time());
        $apiContext->setConfig($sdkConfig);

        $payer = new Payer();
        $payer->setPaymentMethod("paypal");

        $itemList = new ItemList();
        foreach ($CI->flexi_cart->cart_items() as $item) {
            $itemList->addItem($this->addItem($item['name'], $item['quantity'], $item['internal_price']));
        }

        $details = new Details();
        $details->setShipping($CI->flexi_cart->shipping_total(TRUE, FALSE, TRUE));
        $details->setSubtotal($CI->flexi_cart->item_summary_total(TRUE, FALSE, TRUE));
        $details->setTax($CI->flexi_cart->tax_total(TRUE, FALSE, TRUE));

        $amount = new Amount();
        $amount->setCurrency("USD");
        $amount->setDetails($details);
        $amount->setTotal($total);

        $transaction = new Transaction();
        $transaction->setDescription("creating a payment");
        $transaction->setItemList($itemList);
        $transaction->setAmount($amount);

        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl(base_url($CI->uri->segment(1) . '/' . $CI->uri->segment(2) . '/payment-processed/PayPalGateway'));
        //$redirectUrls->setReturnUrl('http://localhost/web-flexcms-1.6.0/' . $CI->uri->segment(1) . '/' . $CI->uri->segment(2) . '/payment-processed/PayPalGateway');
        $redirectUrls->setCancelUrl(base_url($CI->uri->segment(1) . '/' . $CI->uri->segment(2) . '/payment-canceled'));
        //$redirectUrls->setCancelUrl('http://localhost/web-flexcms-1.6.0/' . $CI->uri->segment(1) . '/' . $CI->uri->segment(2) . '/payment-canceled');

        $payment = new \PayPal\Api\Payment();
        $payment->setIntent("sale");
        $payment->setPayer($payer);
        $payment->setRedirectUrls($redirectUrls);
        $payment->setTransactions(array($transaction));

        $payment->create($apiContext);
        redirect($payment->getApprovalLink());
    }

    public static function getResult()
    {

        $CI =& get_instance();
        $env_config = static::$config[static::$config['environment']];

        //Execute a payment by constructing a payment object with payment id and use the payer id returned from PayPal and make the API call using the access token
        //Get an access token using client id and client secret.
        $sdkConfig = array(
            "mode" => static::$config['environment']
        );
        $cred = new OAuthTokenCredential($env_config['client_id'], $env_config['client_secret'], $sdkConfig);
        static::$apiToken = $cred->getAccessToken($sdkConfig);

        $apiContext = new ApiContext(new OAuthTokenCredential(
            $env_config['client_id'],
            $env_config['client_secret']
        ));

        $payment = \PayPal\Api\Payment::get($CI->input->get('paymentId'), $apiContext);
        $execution = new PaymentExecution();
        $execution->setPayerId($CI->input->get('PayerID'));
        $payment->execute($execution, $apiContext);

        return $CI->input->get('paymentId');

    }

    /**
     * @param $name
     * @param $quantity
     * @param $price
     * @return $this
     */
    private function addItem($name, $quantity, $price)
    {
        return (new Item)
            ->setName($name)
            ->setCurrency("USD")
            ->setQuantity($quantity)
            ->setPrice($price);
    }

}