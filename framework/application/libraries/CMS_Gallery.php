<?php use Cartalyst\Sentinel\Native\Facades\Sentinel;

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CMS_Gallery {

    private $data;
    private $lang;

    public function __construct()
    {
        $CI =& get_instance();
        $CI->load->model('descargas_model', 'Downloads');
    }

    public function create($data, $idioma)
    {
        $CI =& get_instance();
        $categoriaId = $CI->uri->segment(3);
        $html = '';

        $this->data = $data;
        $this->lang = $idioma;

        try {

            /*************************
             * LISTADO CATEGORIAS
             *************************/
            if(!$categoriaId)
            {

                $tree = GalleryTree::allRoot()->first();
                $tree->lang = $idioma;
                $tree->findChildren(9999);

                $data['categorias'] = $tree->getChildren();
                foreach($data['categorias'] as $key => &$category) {

                    if(!$category->temporal) {
                        $category->images = $CI->Downloads->getDownloads((int)$category->id, $idioma);
                    }

                    //Remove any temporary categories
                    else {
                        unset($data['categorias'][$key]);
                    }

                }

                if(count($data['categorias']) === 1) {

                    //Get the first category
                    $category = reset($data['categorias']);

                    $html .= $this->renderList($category, $data, $idioma);

                } else {
                    $html .= $CI->load->view('paginas/descargas/categories_view', $data, true);
                }

            }

            /*********************************
             * LISTADO DENTRO DE LA CATEGORIA
             ********************************/
            else if ($categoriaId) {

                $categoria = $CI->Downloads->getCategory((int)$categoriaId, $idioma);
                $this->checkUserAccess($categoria);

                if($categoria){
                    $html .= $this->renderList($categoria, $data, $idioma);
                } else {
                    show_my_404(base_url($idioma . '/' . $CI->m_currentPage), $CI->m_config->theme);
                }
            }

        } catch (Exception $e) {
            $html = $e->getMessage();
        }


        return $html;

    }

    private function renderList($categoria, $data, $idioma)
    {

        $CI =& get_instance();
        $html = '';

        $tree = GalleryTree::find($categoria['id']);
        $tree->lang = $idioma;
        $tree->findChildren(9999);

        $data['categorias'] = $tree->getChildren();
        foreach($data['categorias'] as &$category) {
            $category->images = $CI->Downloads->getDownloads((int)$category->id, $idioma);
        }

        $descargas = $CI->Downloads->getDownloads((int)$categoria['id'], $idioma);

        $data['title'] = $categoria['descargaCategoriaNombre'];
        $data['cat_link'] = $categoria['descargaCategoriaEnlace'];
        $data['back'] = base_url($idioma.'/'.$CI->uri->segment(2));
        $data['cat_clase'] = $categoria['descargaCategoriaClase'];

        $html .= $CI->load->view('paginas/content_open_view', $data, true);
        $html .= $CI->load->view('paginas/descargas/categories_view', $data, true);

        if(count($descargas) > 0){
            $data['descargas'] = $descargas;
            $html .= $CI->load->view('paginas/descargas/items_view', $data, true);
        }

        $html .= $CI->load->view('paginas/content_close_view', $data, true);

        return $html;

    }

    /**
     * Check if the user has access to this category
     *
     * @param $category
     * @return bool
     * @throws Exception
     */
    private function checkUserAccess($category)
    {
        $user = Sentinel::getUser();
        $CI =& get_instance();

        //Check if this category is public or the user is an admin
        if($category['visible_to'] === 'public' || (Sentinel::check() && Sentinel::hasAccess(['admin']))) {
            return true;
        }

        //Category is not public, is the user logged in?
        if(Sentinel::guest()) {
            $html = $CI->cms_authenticate->renderLoginView(
                $this->data,
                $this->lang,
                $this->data['pagAutenticacion']->paginaNombreURL,
                lang('no_access')
            );
            throw new Exception($html);
        }

        //User is logged in, does he have access to it?
        else {

            if(!$user->hasAnyAccess(['gallery.' . $category['id'], 'admins'])) {
                throw new Exception($CI->load->view('paginas/autenticacion/no_access_view', '', true));
            }

        }

        return true;

    }

}

/* End of file CMS_Gallery.php */