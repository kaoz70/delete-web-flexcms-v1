# FlexCMS

Content management system made simple

## Current version

1.7.6

## Instalation
Copy the source to the localhost / server

Create database with the file:

/sql/flexcms.sql

Remove this folder in production for security

### Database Configuration
framework/application/config/database.php

Example:
```
#!php
'username' => 'root',
'password' => 'mysql',
'database' => 'flexcms',

```

## Admin access

http://domain.com/login/

* User: miguel@dejabu.ec
* Pass: admin