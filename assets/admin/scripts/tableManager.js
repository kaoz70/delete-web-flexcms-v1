/* global Sortables, $, $$, Element, StickyWin */

var tableManager = {

	sortable: null,
	alerta: null,
	cellControls: "<div class=\"bot_separateCell\"></div><div class=\"bot_separateCellVert\"></div><div class=\"bot_mergeCell\"></div><div class=\"bot_mergeCellVert\"></div><div class=\"bot_editCell\"></div>",

	init: function() {
		"use strict";

		var maxWidth = 0;

		$$(".table_editor").each(function(item){

			var elem = item.getElement(".tableGrid"),
					width = item.getDimensions().x;

			item.addEvent("click:relay(.add_column)", function(ev, el) {
				tableManager.addColumn(el);
			});

			item.addEvent("click:relay(.add_row)", function(ev, el) {
				tableManager.addRow(el);
			});

			elem.addEvent("click:relay(.bot_deleteColumn)", tableManager.removeColumn);
			elem.addEvent("click:relay(.bot_deleteRow)", tableManager.removeRow);

			elem.addEvent("click:relay(.table_editor .bot_separateCell)", tableManager.separateCells);
			elem.addEvent("click:relay(.table_editor .bot_mergeCell)", tableManager.mergeCells);

			elem.addEvent("click:relay(.table_editor .bot_separateCellVert)", tableManager.separateCellsVert);
			elem.addEvent("click:relay(.table_editor .bot_mergeCellVert)", tableManager.mergeCellsVert);

			elem.addEvent("click:relay(.table_editor .bot_editCell)", tableManager.editCell);

			if (width > maxWidth) {
				maxWidth = width + 150;
				item.getParent(".contenido_col").setStyle("width", maxWidth);
			}

			tableManager.addDeleteControls(elem);
			tableManager.addCellControls(elem);

			tableManager.resizeRowButton(elem);
			tableManager.resizeColumnButton(elem);

			tableManager.sortable = new Sortables(elem.getElement("tbody"), {
				constrain: true,
				revert: true,
				clone: true,
				opacity: 0
			});

		});

	},

	mergeCells: function(e) {
		"use strict";

		var currentCell = e.target.getParent(),
			nextCell = currentCell.getNext(),
			currentText = currentCell.get("text"),
			nextText = "",
			currentSpan = currentCell.getProperty("colspan").toInt(),
			nextSpan = nextCell.getProperty("colspan").toInt();

		if(nextSpan) {
			currentCell.setProperty("colspan", currentSpan + nextSpan);
		} else {
			currentCell.setProperty("colspan", currentSpan + 1);
		}

		if(nextCell) {
			nextText = nextCell.get("text");
			nextCell.remove();
		}

		currentCell.set("text", currentText + " " + nextText);
		tableManager.addCellControls(currentCell.getParent("table"));

	},


	mergeCellsVert: function(e) {
		"use strict";

		var currentCell = e.target.getParent(),
			currentRow = currentCell.getParent(),
			cellIndex = currentRow.getChildren().indexOf(currentCell),
			nextCell = currentRow.getNext().getChildren()[cellIndex],
			currentSpan = currentCell.getProperty("rowspan").toInt(),
			nextSpan,
			currentText = currentCell.get("text"),
			i = 0,
			nextText = "";

		/*while (nextCell = currentRow.getNext().getChildren()[cellIndex - i] && (cellIndex - i) > 0) {
			i++;
		}*/

		if(nextSpan = nextCell.getProperty("rowspan").toInt()) {
			currentCell.setProperty("rowspan", currentSpan + nextSpan);
		} else {
			currentCell.setProperty("rowspan", currentSpan + 1);
		}

		if(nextCell) {
			nextText = nextCell.get("text");
			nextCell.remove();
		}

		currentCell.set("text", currentText + " " + nextText);
		tableManager.addCellControls(currentCell.getParent("table"));

	},

	separateCells: function(e, elem) {
		"use strict";

		var cell = elem.getParent(),
				row = cell.getParent(),
				spans = cell.getProperty("colspan").toInt();

		cell.removeAttribute("colspan");

		for(var i = 0; i < (spans - 1); i++) {
			var newCell = new Element(cell.get("tag"), {
				html: tableManager.cellControls
			});
			newCell.inject(row);
		}

	},

	resizeRowButton: function(elem) {
		"use strict";

		var tableSize = elem.getSize();

		elem.getParent(".table_editor").getElements(".add_row")[0].set("styles", {
			width: tableSize.x
		});

	},

	resizeColumnButton: function(elem) {
		"use strict";

		var tableSize = elem.getSize();

		elem.getParent(".table_editor").getElements(".add_column")[0].set("styles", {
			height: tableSize.y
		});

	},

	addCellControls: function(elem) {
		"use strict";

		Array.each(elem.getElements("td, th"), function(cell) {
			if(!cell.getElement(".bot_separateCell")) {
				cell.set("html", cell.get("html") + tableManager.cellControls);
			}
		});

	},

	addDeleteControls: function(elem) {
		"use strict";

		//Column delete Controls
		var rowHtml = "",
				row,
				rows,
				maxColumns = 0;

		Array.each(elem.getElements("tr"), function(rowEl) {
			if(rowEl.children.length > maxColumns) {
				maxColumns = rowEl.children.length;
			}
		});

		for(var i = 0; i < maxColumns; i++) {
			if(i !== 0) {
				rowHtml += "<td class='delete_column'><div class='bot_deleteColumn'></div></td>";
			} else {
				rowHtml += "<td class='delete_column'></td>";
			}
		}

		if(maxColumns === 1) {
			rowHtml = "<td class='delete_column'></td>";
		}

		row = new Element("tr", {
			html: rowHtml,
			class: "delete_column"
		});

		row.inject(elem.getElement("tbody"), "top");

		//Row delete controls
		rows = elem.getElements("tr");

		Array.each(rows, function(rowItem, index) {

			var emptyCell;

			if(index === 0) {

				emptyCell = new Element("td", {
					class: "delete_column"
				});

				emptyCell.inject(rowItem, "top");

			} else if(index === 1) {

				emptyCell = new Element("th", {
					class: "delete_row"
				});

				emptyCell.inject(rowItem, "top");

			} else if(index === 2) {

				emptyCell = new Element("td", {
					class: "delete_row"
				});

				emptyCell.inject(rowItem, "top");

			} else {
				var botDelete = new Element("td", {
					class: "delete_row",
					html: "<div class='bot_deleteRow'></div>"
				});

				botDelete.inject(rowItem, "top");
			}

		});

	},

	addColumn: function(elem) {
		"use strict";

		var table = elem.getParent().getPrevious().getElement(".tableGrid"),
				rows = table.getElements("tr");

		Array.each(rows, function(row, index) {

			if(index === 0) {
				var botDelete = new Element("td", {
					class: "delete_column",
					html: "<div class='bot_deleteColumn'></div>"
				});

				botDelete.inject(row);

			} else if(index === 1) {
				var header = new Element("th", {
					html: "nombre cabecera"
				});

				header.inject(row);
			} else {
				var column = new Element("td");
				column.inject(row);
			}

		});

		tableManager.resizeRowButton(table);
		tableManager.resizeContainer(table);
		tableManager.addCellControls(table);

	},

	addRow: function(elem) {
		"use strict";

		var table = elem.getPrevious().getElement(".tableGrid"),
				columnHtml = "",
				maxColumns = 0;

		Array.each(table.getElements("tr"), function(row) {
			if(row.children.length > maxColumns) {
				maxColumns = row.children.length;
			}
		});

		for(var i = 0; i < maxColumns; i++) {
			if(i === 0) {
				columnHtml += "<td class='delete_row'><div class='bot_deleteRow'></div></td>";
			} else {
				columnHtml += "<td></td>";
			}
		}

		var row = new Element("tr", {
			html: columnHtml
		});

		tableManager.sortable.addItems(row);

		row.inject(table.getChildren()[0]);

		tableManager.resizeColumnButton(table);
		tableManager.addCellControls(table);

	},

	removeColumn: function(event, elem) {
		"use strict";

		var parent = elem.getParent("tr");
		var items = parent.getChildren();
		var indexToDelete;
		var parentTable = elem.getParent("table");

		Array.each(items, function(item, index) {
			if(item === elem.getParent("td")) {
				indexToDelete = index;
			}
		});

		tableManager.alerta = new StickyWin({
			content: StickyWin.ui("Alerta", "Esta seguro que desea eliminar esta columna?", {
				width: "400px",
				buttons: [{
					text: "eliminar",
					onClick: function() {

						var rows = elem.getParent("tbody").getChildren("tr");

						Array.each(rows, function(item) {
							if(item.children[indexToDelete]) {
								item.children[indexToDelete].destroy();
							}
						});

						tableManager.resizeContainer(parentTable);
						tableManager.resizeRowButton(parentTable);

					}
				}, {
					text: "cancelar"
				}]
			})
		});

	},

	removeRow: function(event, elem) {
		"use strict";

		var parentTable = elem.getParent("table");

		tableManager.alerta = new StickyWin({
			content: StickyWin.ui("Alerta", "Esta seguro que desea eliminar esta fila?", {
				width: "400px",
				buttons: [{
					text: "eliminar",
					onClick: function() {
						elem.getParent("tr").destroy();
						tableManager.resizeColumnButton(parentTable);
					}
				}, {
					text: "cancelar"
				}]
			})
		});

	},

	editCell: function(event) {
		"use strict";

		var elem = event.target.getParent();

		if(!elem.hasClass("delete_column") && !elem.hasClass("delete_row") && !elem.hasClass("no_edit")) {

			Array.each($$(".cellinput"), function(item) {
				item.destroy();
			});

			var text = elem.get("html").replace(/\<br \/\>|\<br\>/g, "\n").replace(tableManager.cellControls, "");

			var cellInput = new Element("textarea", {
				value: text,
				class: "cellinput",
				events: {
					blur: function() {
						tableManager.setCellInputValue(this, elem);
					},
					keypress: function(e) {

						/*if (e.key == 9) {  //tab pressed
							e.preventDefault(); // stops its action
						}*/

						console.log(e);
						/*if (e.keyCode == "13"){
						 tableManager.setCellInputValue(this, elem);
						 }*/
					}
				}
			});

			var parentDiv = elem.getParent(".table_editor");
			var position = elem.getPosition(parentDiv);
			var size = elem.getSize();
			cellInput.setPosition(position);
			cellInput.inject(parentDiv);
			cellInput.set("styles", {
				width: size.x - 3,
				height: size.y - 5
			});
			cellInput.focus();

		}

	},

	setCellInputValue: function(elem, target) {
		"use strict";

		var text = elem.get("value").replace(/\n/g, "<br />");
		text += tableManager.cellControls;

		target.set("html", text);
		elem.destroy();
	},

	resizeContainer: function(elem) {
		"use strict";

		var size = elem.getParent(".contenido_col").scrollWidth,
				margen = 0,
				columna = elem.getParent(".columnas");

		columna.setStyles({
			width: size + margen,
			"min-width": size + margen
		});

		elem.getParent(".contenido_col").setStyles({
			width: size - 10,
			"min-width": size - 10
		});

		columna.retrieve("scrollable").reposition();

	}
};
