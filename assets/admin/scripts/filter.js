/**
 * Created by Miguel on 17/05/2016.
 */

var filter = {

    group: null,
    order: null,
    groupEl: null,
    orderEl: null,
    url: null,
    lang: null,
    container_string: null,

    init : function (container, url, lang) {
        "use strict";

        filter.container_string = container;
        filter.content_container = $(container).getParent('.contenido');
        filter.url = url;
        filter.lang = lang;

        filter.groupEl = $$('.filter').getElement('[name="group"]');
        filter.orderEl = $$('.filter').getElement('[name="order"]');

        filter.groupEl.addEvent('change', filter.changeListener);
        filter.orderEl.addEvent('change', filter.changeListener);

        filter.getValues();

    },

    changeListener: function () {
        filter.getValues();

        filter.content_container.setStyle('opacity', 0);

        var jsonRequest = new Request.HTML({
            url: filter.url,
            update : filter.content_container,
            data: {
                'group': filter.group,
                'order': filter.order,
                'csrf_test': csrf_cookie
            },
            onSuccess: function () {
                filter.content_container.setStyle('opacity', 1);
            }
        });

        jsonRequest.send();

    },

    getValues: function () {
        filter.group = filter.groupEl.getElement(':selected').get('value')[0];
        filter.order = filter.orderEl.getElement(':selected').get('value')[0];
    }
    
};