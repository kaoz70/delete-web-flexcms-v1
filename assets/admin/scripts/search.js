/**
 * Created by Miguel on 17/05/2016.
 */

/**
 * General search functionality for the backend
 * @type {{timer: null, submitButton: null, resultContainer: null, input: null, keyTimeout: number, initialSearchString: string, language: string, url: string, originalList: null, init: Function, clickListener: Function, blurListener: Function, keyupListener: Function, start: Function, generateResultBox: Function, hideResultBox: Function, generateResult: Function, generateNoResult: Function}}
 */
var search = {

    timer : null,
    submitButton : null,
    resultContainer : null,
    input : null,
    keyTimeout : 500,
    initialSearchString: '',
    language : '',
    url : '',
    originalList: null,

    /**
     * Initializes the search functionality
     * @param url (string) url of the query/result file
     * @param lang (string) language for the search content
     */
    init : function (url, lang) {
        "use strict";
        search.input = $$('.buscar input[name="searchString"]')[0];
        search.submitButton = $$('.buscar .searchButton')[0];

        search.input.addEvent('click', search.clickListener);
        search.input.addEvent('blur', search.blurListener);
        search.input.addEvent('keyup', search.keyupListener);
        search.submitButton.addEvent('click', search.hideResultBox);

        search.resultContainer = $$('.searchResults')[0];
        search.initialSearchString = $$('.buscar input[name="searchString"]')[0].get('value');
        search.originalList = search.resultContainer.getChildren();

        search.language = lang;
        search.url = url;

        search.input.focus();
        search.input.set('value', '');

    },

    /**
     * Handles the click event on the input
     * @param event
     */
    clickListener : function (event) {
        "use strict";
        var value = event.target.get('value');

        if (value === search.initialSearchString) {
            event.target.set('value', '');
        }

    },

    /**
     * Handles the blur event of the input
     * @param event
     */
    blurListener : function (event) {
        "use strict";
        var value = event.target.get('value');

        if (value === '') {
            event.target.set('value', search.initialSearchString);
            search.hideResultBox();
        }

    },

    /**
     * Handles the keyup event of the input
     * @param event
     */
    keyupListener : function (event) {
        "use strict";
        clearTimeout(search.timer);
        search.timer = setTimeout(function () {
            search.start(event.target.get('value'), event.target.get('data-page-id'));
        }, search.keyTimeout);
    },

    /**
     * Starts the search, sends the query to the server
     * @param value (string) query string
     */
    start : function (value, pageId) {
        "use strict";

        var param = "";

        if(pageId != undefined) {
            param = '/' + pageId;
        }

        var jsonRequest = new Request.JSON({
            url : search.url + param,
            onRequest : function () {
                search.submitButton.addClass('loading');
                search.submitButton.removeClass('cancel');
            },
            onSuccess : function (result) {
                search.generateResultBox(result);
                search.submitButton.removeClass('loading');
                search.submitButton.addClass('cancel');
            },
            onFailure : function (xhr) {
                search.submitButton.removeClass('loading');
                search.submitButton.removeClass('cancel');
                failureRequestHandler(xhr, search.url);
            }
        }).get({
            'query' : value,
            'language' : search.language
        });

    },

    /**
     * Shows the result box with the contents
     * @param result
     */
    generateResultBox : function (result) {
        "use strict";

        if (result.length > 0) {
            search.generateResult(result);
        } else {
            search.generateNoResult();
        }

        search.resultContainer.fade(1);

    },

    /**
     * Hides the resutl box
     */
    hideResultBox : function () {
        "use strict";

        search.resultContainer.getElements('li li').each(function (item) {
            item.reveal();
        });

        if(search.resultContainer.getElements('li li').length === 0) {
            search.resultContainer.getElements('li').each(function (item) {
                item.reveal();
            });
        }

        search.submitButton.removeClass('loading');
        search.submitButton.removeClass('cancel');

        search.input.set('value', search.initialSearchString);
    },

    /**
     * Generates the result with each item, then it puts it into the result box
     * @param elements
     */
    generateResult : function (elements) {
        "use strict";

        var list = search.resultContainer.getElements('li.pagina li'),
            show = [],
            hide;

        if (list.length === 0 && search.resultContainer.getElements('li.pagina').length === 0) {
            list = search.resultContainer.getElements('li');
        }

        hide = list;

        elements.each(function (elem) {
            list.each(function (li, inx) {
                if (li.id === elem.id) {
                    show.push(li);
                    hide.splice(inx, 1);
                }
            });
        });

        show.each(function (item) {
            item.reveal();
        });

        list.each(function (item) {
            item.dissolve();
        });


    },

    /**
     * Handles the empty result (when the search didn't find anything)
     */
    generateNoResult : function () {
        "use strict";
        search.originalList.each(function (list) {
            list.getElements('a.nombre').each(function (a) {
                a.getParent('.listado').dissolve();
            });
        });

    }
};