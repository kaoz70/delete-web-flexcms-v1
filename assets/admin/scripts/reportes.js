/*global window,$,$$,system */
/**
 * Created by Miguel Suarez
 * Dejabu Agencia Multimedia 2016
 */


/**
 * Starts all the events and functions
 */
window.addEvent('domready', function () {
    "use strict";

    $(document.body).addEvents({

        'click:relay(.tabs li, a.buscar)': function (event, elem) {

            var request,
                type = elem.getProperty("data-type"),
                formData = elem.getParent('form');

            Array.each($$(".tabs").getChildren(), function(elem, index){
                elem.removeClass("active");
            });
            elem.addClass("active");

            request = new Request.HTML({
                url : system.base_url + "admin/cart/reportes/" + type,
                update : $("report_content"),
                onSuccess : function (responseTree, responseElements, responseHTML) {

                },
                onFailure : function (xhr) {
                    failureRequestHandler(xhr, elem.getParent('form').get('action'));
                }
            });

            request.post(formData);
        }
    });

});